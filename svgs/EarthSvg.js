import React from "react";
import { SvgXml } from "react-native-svg";
export default function EarthSvg(){
    const earth = `<svg xmlns="http://www.w3.org/2000/svg" width="31" height="31" viewBox="0 0 31 31">
      <g id="Groupe_118" data-name="Groupe 118" transform="translate(-121 -345)">
        <rect id="Rectangle_1348" data-name="Rectangle 1348" width="31" height="31" rx="5" transform="translate(121 345)" fill="#007aff"/>
        <path id="Icon_metro-earth" data-name="Icon metro-earth" d="M14.071,1.928a11.5,11.5,0,1,0,11.5,11.5,11.5,11.5,0,0,0-11.5-11.5Zm0,21.563a10.029,10.029,0,0,1-3.981-.819l5.237-5.891a.719.719,0,0,0,.182-.478V14.147a.719.719,0,0,0-.719-.719c-2.538,0-5.216-2.638-5.242-2.664a.719.719,0,0,0-.508-.211H6.164a.719.719,0,0,0-.719.719v4.313a.719.719,0,0,0,.4.643l2.478,1.239v4.22A10.069,10.069,0,0,1,4.977,9.116H7.6a.719.719,0,0,0,.508-.21L10.985,6.03a.719.719,0,0,0,.211-.508V3.783a10.091,10.091,0,0,1,7.284.6c-.093.079-.184.161-.271.248a4.313,4.313,0,0,0,3.046,7.362q.107,0,.214-.005a16.878,16.878,0,0,1-.189,8.363.717.717,0,0,0-.019.117,10.031,10.031,0,0,1-7.191,3.025Z" transform="translate(122.429 347.072)" fill="#fff"/>
      </g>
    </svg>`;

    const EarthSvg = () => <SvgXml xml={earth} width="50%" height="50%" />;
    return <EarthSvg />;
}
