//import {navigatorRef} from '../App';
//import {NavigationActions} from 'react-navigation';
//import * as React from "react";
//import { Alert, NativeModules } from 'react-native';

//const BASE_URL = "https://webdev.youseeme.pro/";
const BASE_URL = "https://web.youseeme.pro/";

export function setAccountFromApiWithInformations(user) {
  const date = user.birthYear + '-' + user.birthMonth + '-' + user.birthDay;
  return fetch(BASE_URL + 'api/v1/register', {
    method: 'POST',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        first_name: user.firstName,
        last_name: user.lastName,
        mobile_number: user.phone,
        password: user.password,
        email: user.email,
        pincode: user.pincode,
        birthdate: user.birthDate,
        password_confirmation: user.passwordVerification,
        gender: user.gender,
        title: user.civility,
        //sponsor
    })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}


export function setPasswordFromApiWithEmail(email) {
    return fetch(BASE_URL + 'api/v1/register/send-reset-password-email', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email,

        })
    })
        .then((response) => response.text())
        .catch((error) => console.error(error))
}


export function setBronzeProfileFromApiWithInfos(token, bronzeProfile) {
    return fetch(BASE_URL + 'api/v1/user/profile/update-bronze', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
            first_name: bronzeProfile.firstName,
            last_name: bronzeProfile.lastName,
            title: bronzeProfile.civility,
            birthdate: bronzeProfile.birthDate,
            mobile_number: bronzeProfile.mobileNumber
        })
    })
        .then((response) => response.text())
        .catch((error) => console.log(error))
}

export function setSilverProfileFromApiWithInfos(token, silverProfile) {
    return fetch(BASE_URL + 'api/v1/user/profile/update-silver', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
            street: silverProfile.street,
            postal_code: silverProfile.zipCode,
            city: silverProfile.city,
            country_id: silverProfile.countryId,
        })
    })
        .then((response) => response.text())
        .catch((error) => console.error(error))
}

export function setGoldProfileFromApiWithInfos(token, goldProfile) {
    return fetch(BASE_URL + 'api/v1/user/profile/update-gold', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
            marital_status: goldProfile.maritalStatus,
            profession: goldProfile.profession,
            interests: goldProfile.interests,
        })
    })
        .then((response) => response.text())
        .catch((error) => console.error(error))
}

export function setPlatinumProfileFromApiWithInfos(token, platinumProfile) {
    return fetch(BASE_URL + 'api/v1/user/profile/update-platinum', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        },
        body: JSON.stringify({
            revenue: platinumProfile.revenue
        })
    })
        .then((response) => response.text())
        .catch((error) => console.error(error))
}


export function getLoginFromApiWithAccountCredentials(email, password) {
  return fetch(BASE_URL + 'api/v1/login', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          email: email,
          password: password,
      })

  })
  .then((response) => response.json())
  .catch((error) => console.error(error));

}

export function setShopRatingFromApiWithShopId(token, shopId, rate) {
  return fetch(BASE_URL + 'api/v1/shops/' + shopId + '/rate', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          rating: rate
      })
  })
      .then((response) => response.text())
      .catch((error) => console.error(error))
}



//mCommerce

/*export function getBillFromApiWithOrderId(token, id) {
  //return call(BASE_URL + 'api/v1/mcommerce/bill/' + id, token);
  console.log(id)
  return fetch(BASE_URL + 'api/v1/mcommerce/bill/' + id, {
    method: 'GET',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}*/

export function newCardRegisterOrderFromApiWithInfo(token, order) {
  return fetch(BASE_URL + 'api/v1/mcommerce/order/register', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
        shop_id: order.shop_id,
        card_id: order.card_id,
        amount: order.amount,
        payment_method: order.payment_method,
        delivery_type: order.delivery_type,
        delivery_address: order.delivery_address,
        items: JSON.stringify(order.items)
      })
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}

export function basicRegisterOrderFromApiWithInfo(token, order) {
  return fetch(BASE_URL + 'api/v1/mcommerce/order', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
        shop_id: order.shop_id,
        card_id: order.card_id,
        amount: order.amount,
        payment_method: order.payment_method,
        delivery_type: order.delivery_type,
        delivery_address: order.delivery_address,
        items: JSON.stringify(order.items)
      })
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}

export function askRefundFromApiWithOrderId(token, id) {
  return fetch(BASE_URL + 'api/v1/mcommerce/refund/' + id, {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({

      })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}


export function sendPurchaseFromApiWithAmount(token, amount) {
  return call(BASE_URL + 'api/v1/mcommerce/order/token?amount=' + amount, token);
}

export function getUserAddressesFromApiWithToken(token) {
  return call(BASE_URL + 'api/v1/mcommerce/addresses', token);
}

export function getShopAddressesFromApiWithId(token, id) {
  return call(BASE_URL + 'api/v1/mcommerce/addresses/shop/' + id, token);
}

export function addUserAddressFromApiWithInfos(token, name, address, zipCode, city) {
  return fetch(BASE_URL + 'api/v1/mcommerce/addresses/add', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          name: name,
          address: address,
          zipcode: zipCode,
          city: city
      })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function getCardsFromApiWithAccountToken(token) {
  return call(BASE_URL + 'api/v1/mcommerce/cards', token)
}

export function removeCardFromApiWithId(token, cardId) {
  return fetch(BASE_URL + 'api/v1/mcommerce/cards/' + cardId + '/delete', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function removeUserAddressFromApiWithId(token, addressId) {
  return fetch(BASE_URL + 'api/v1/mcommerce/addresses/' + addressId + '/delete', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function getDeliveryFromApiWithShopId(token, id) {
  return call(BASE_URL + 'api/v1/mcommerce/delivery/' + id, token)
}


//Settings

export function getWalletInfoFromApiWithToken(token) {
  return call(BASE_URL + 'api/v1/iban', token);
}

export function sendReloadFromApiWithInfos(token, amount, memorizeCard) {
  return call(BASE_URL + 'api/v1/payment-token?amount=' + amount + '&checked=' + memorizeCard, token);
}

export function logReloadFromApiWithAmount(token, amount) {
  return call(BASE_URL + 'api/v1/payment/log?amount=' + amount, token);
}


export function getSettingStateFromApiWithToken(token) {
  return call(BASE_URL + 'api/v1/settings/all', token);
}

export function setNotificationsFromApiWithValue(value, token) {
  return fetch(BASE_URL + 'api/v1/settings/update/push', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          value: value,
      })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function setGeofencingFromApiWithValue(value, token) {
  return fetch(BASE_URL + 'api/v1/settings/update/geofencing', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          value: value,
      })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function contactYouseemeFromApiWithContent(token, contactMessage) {
  return fetch(BASE_URL + 'api/v1/contact', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          email: contactMessage.email,
          name: contactMessage.subject,
          body: contactMessage.body
      })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function sendMoneyToPhoneNumberFromApiWithInfos(token, phone, amount, devise) {
  return fetch(BASE_URL + 'api/v1/money/transfer', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          mobile: phone,
          amount: amount,
          devise: devise
      })
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}

export function sendMoneyToBankAccountFromApiWithIbanId(token, ibanId, amount) {
  return fetch(BASE_URL + 'api/v1/money/out', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
        ibanId: ibanId,
        amount: amount
      })
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}

export function getUserFromApiWithPhone(token, phone) {
  return call(BASE_URL + 'api/v1/user/phone?mobile=' + phone, token);
}

export function getCountriesFromApi() {
  return call(BASE_URL + 'api/v1/data/countries', '');
}

export function getInterestsFromApi() {
  return call(BASE_URL + 'api/v1/data/interests', '');
}

export function getCategoriesFromApi() {
  return call(BASE_URL + 'api/v1/data/categories', '');
}

export function getSubCategoriesFromApiWithCategoryId(id) {
  return call(BASE_URL + 'api/v1/data/categories/' + id + '/children', '');
}

export function getNewsFromApi() {
  return call(BASE_URL + 'api/v1/news', '');
}

export function getNewsFromApiWithId(id) {
  return call(BASE_URL + 'api/v1/news/' + id + '/show', '');
}

export function setFavoriteFromApiWithShopId(token, uid) {
  return fetch(BASE_URL + 'api/v1/shops/' + uid + '/favorite', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      }
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function setPinCodeFromApiWithCode(token, code) {
  return fetch(BASE_URL + 'api/v1/user/pincode/update', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          pincode: code
      })
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
}

export function sendTicketFromApiWithPicture(token, picture) {
  console.log(transaction.amount)
  return fetch(BASE_URL + 'api/v1/user/send-ticket', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          image: picture
      })
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}

export function sendPaymentFromApiWithInfos(token, transaction) {
  console.log(transaction.amount)
  console.log(transaction.cartContent.toString())
  return fetch(BASE_URL + 'api/v1/send-payment', {
      method: 'POST',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify({
          addressId: transaction.addressId,
          merchantWalletId: transaction.merchantWalletId,
          merchantId: transaction.merchantId,
          amount: transaction.amount,
          discount: transaction.discount,
          cartContent: JSON.stringify(transaction.cartContent)
      })
  })
  .then((response) => response.text())
  .catch((error) => console.error(error));
}

export function getShopListFromApiWithToken(token) {
  return call(BASE_URL + 'api/v1/shops/list', token)
}

export function getShopFromApiWithId(token, id) {
  return call(BASE_URL + 'api/v1/shops/' + id + '/show', token)
}

export function getTransactionsFromApiWithAccountToken(token, type, method) {
  if(type === 'credit')
    return call(BASE_URL + 'api/v1/user/payments/credits', token);
  else if(type === 'debit')
    return call(BASE_URL + 'api/v1/user/payments/debits', token);
}

export function getUserProfileFromApiWithAccountToken(token) {
  return call(BASE_URL + 'api/v1/user/profile', token);
}

export function getUserBalanceFromApiWithAccountToken(token) {
  return call(BASE_URL + 'api/v1/user/balance', token);
}

export function getOrdersFromApiWithAccountToken(token, month, year) {
  return call(BASE_URL + 'api/v1/mcommerce/orders?month=' + month + '&year=' + year, token);
}

export function getOrderDetailsFromApiWithId(token, id) {
  return call(BASE_URL + 'api/v1/mcommerce/orders/' + id, token);
}

export function logoutWithToken(token) {
  return call(BASE_URL + 'api/v1/logout', token)
}


function call(url, token) {
  return fetch(url, {
    method: 'GET',
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
    }
  })
  .then((response) => response.json())
  .catch((error) => console.error(error));
  //.catch((error) => {
    //if(lostConnection != null)
      //lostConnection();
    //navigatorRef.dispatch(NavigationActions.reset({
      //index: 0,
      //actions: [NavigationActions.navigate({routeName: 'LostConnection'})],
    //}));
    /*Alert.alert(
      "Connexion perdue",
      "Oups, aucune connexion internet",
      [
        {
          text: "Réessayer",
          onPress: () => {NativeModules.DevSettings.reload()}
        }
      ],
      {
        cancelable: false
      }
    );*/
  //});
}
