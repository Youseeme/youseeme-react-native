import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer, createSwitchavigator} from "react-navigation";
import Intro from '../components/Intro';
import First from '../components/Login';
import Second from '../components/second_page';
import Tree from '../components/tree_page';
import Four from '../components/four_page';
import KycRegistration from '../components/KycRegistration';
import KycRegistrationAccepted from '../components/KycRegistrationAccepted';
import Rank from '../components/RankPage';
import Home from '../components/Map';
import Settings from '../components/Settings';
import Transfer from '../components/Transfer';
import Profile from '../components/Profile';
import Wallet from '../components/Wallet';
import Reload from '../components/Reload';
import AccountStatement from '../components/AccountStatement';
import Kyc from '../components/Kyc';
import ResetPassword from '../components/ResetPassword';
import Scan from '../components/Scan';
import TakePicture from '../components/TakePicture';
import Dashboard from '../components/Dashboard';
import ShopList from '../components/ShopList';
import Browser from '../components/Browser';
import ChangePinCode from '../components/ChangePinCode';
import Sponsorship from '../components/Sponsorship';
import Addresses from '../components/Addresses';
import NewAddress from '../components/NewAddress';
import Status from '../components/Status';
import News from '../components/News';
import NewsDetails from '../components/NewsDetails';
import Orders from '../components/Orders';
import OrderDetails from '../components/OrderDetails';
import BankCards from '../components/BankCards';
import BankCardsChoice from '../components/BankCardsChoice';
import PDFViewer from '../components/PDFViewer';
import ShopDetails from '../components/ShopDetails';
import ProductsDetails from '../components/ProductsDetails';
import ProductDetailWithOptions from '../components/ProductDetailWithOptions';
import ShoppingBasket from '../components/ShoppingBasket';
import PlaceOrder from '../components/PlaceOrder';
import DeliveryWay from '../components/DeliveryWay';
import DeliveryAddress from '../components/DeliveryAddress';
import PaymentWay from '../components/PaymentWay';
import SuccessPurchase from '../components/SuccessPurchase';
import Contact from '../components/Contact';
import LostConnection from '../components/LostConnection';


const Screens = {
    // Page par default affiché par le routeur
    Intro: {
      screen: Intro,
      navigationOptions: {
        headerShown: false
      },
    },
    First: {
      screen: First,
      navigationOptions: {
        headerShown: false
      },
    },
    ResetPassword: {
      screen: ResetPassword,
      navigationOptions: {
          headerShown: false
      },
    },
    Kyc: {
        screen: Kyc,
        navigationOptions: {
          title: 'KYC',
          headerTitleAlign: 'center',
          headerTintColor: 'white',
          headerShown: true,
          headerTransparent: true,
          headerBackTitleVisible: false,
          headerTitleStyle: {
            fontSize:25,
            color: "#f8cd3e",
            fontFamily: "SciFly",
            alignSelf: 'center',
          },
        },
    },
    AccountStatement: {
        screen: AccountStatement,
        navigationOptions: {
            /*title: 'Mon relevé de compte',
            headerStyle: {
              height: 148,
            },
            headerTitleStyle: {
              fontSize:30,
              color: "#f8cd3e",
              fontFamily: "SciFly",
              alignSelf: 'center',
            },
            headerBackTitleStyle: {
              color: 'white',
            },
            headerBackTitleVisible: false,
            headerBackImage: () => {
              return(<Image
              source={require('../assets/back.png')}
              style={{width:25, height: 25, marginLeft: 8}}
              />)
            },
            headerTitleAlign: 'center',*/

            title: 'Mon relevé de compte',
            headerTintColor: 'white',
            headerShown: true,
            headerTransparent: true,
            headerBackTitleVisible: false,
            headerTitleAlign: 'center',
            headerTitleStyle: {
              fontSize:25,
              color: "#f8cd3e",
              fontFamily: "SciFly",
              alignSelf: 'center',
            },
        },
    },
    Second: {
      screen: Second,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
      }
    },
    Tree: {
      screen: Tree,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
      }
    },
    Four: {
      screen: Four,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
      }
    },
    KycRegistration: {
      screen: KycRegistration,
      navigationOptions: {
        title: '',
        headerTintColor: 'transparent',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
      }
    },
    KycRegistrationAccepted: {
      screen: KycRegistrationAccepted,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
      }
    },
    Rank: {
        screen: Rank,
        navigationOptions: {
            headerShown: false
        },

    },
    Home: {
        screen: Home,
        navigationOptions: {
            headerShown: false
        },

    },
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            headerShown: false
        },
    },
    Settings: {
      screen: Settings,
      navigationOptions: {
        title: 'Paramètres',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Transfer: {
      screen: Transfer,
      navigationOptions: {
        title: 'Transfert',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: 'Mon profil',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Wallet: {
      screen: Wallet,
      navigationOptions: {
        title: 'Porte-monnaie',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Reload: {
      screen: Reload,
      navigationOptions: {
        title: 'Recharger',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Scan: {
      screen: Scan,
      navigationOptions: {
        title: 'Scanne le QR-code ',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    TakePicture: {
      screen: TakePicture,
      navigationOptions: {
        title: 'Photographiez votre ticket',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    ShopList: {
      screen: ShopList,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Browser: {
      screen: Browser,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    ChangePinCode: {
      screen: ChangePinCode,
      navigationOptions: {
        title: 'Code pin',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Sponsorship: {
      screen: Sponsorship,
      navigationOptions: {
        title: 'Parrainage',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Addresses: {
      screen: Addresses,
      navigationOptions: {
        title: 'Adresses',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    NewAddress: {
      screen: NewAddress,
      navigationOptions: {
        title: 'Nouvelle adresse',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Status: {
      screen: Status,
      navigationOptions: {
        title: 'Mon statut',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    News: {
      screen: News,
      navigationOptions: {
        title: 'Actualités',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    NewsDetails: {
      screen: NewsDetails,
      navigationOptions: {
        title: 'Actualités',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    Orders: {
      screen: Orders,
      navigationOptions: {
        title: 'Mes commandes',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    OrderDetails: {
      screen: OrderDetails,
      navigationOptions: {
        title: 'Détails de la commande',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    BankCards: {
      screen: BankCards,
      navigationOptions: {
        title: 'Cartes bancaires',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    BankCardsChoice: {
      screen: BankCardsChoice,
      navigationOptions: {
        title: 'Cartes bancaires',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    PDFViewer: {
      screen: PDFViewer,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    ShopDetails: {
      screen: ShopDetails,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    ProductsDetails: {
      screen: ProductsDetails,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    ProductDetailWithOptions: {
      screen: ProductDetailWithOptions,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    ShoppingBasket: {
      screen: ShoppingBasket,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    PlaceOrder: {
      screen: PlaceOrder,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    DeliveryWay: {
      screen: DeliveryWay,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    DeliveryAddress: {
      screen: DeliveryAddress,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    PaymentWay: {
      screen: PaymentWay,
      navigationOptions: {
        title: '',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:21,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
          zIndex: 2
        },
        headerStyle: {
          backgroundColor: '#674171',
          borderBottomWidth: 0,
          shadowColor: 'transparent',
        },
      }
    },
    SuccessPurchase: {
      screen: SuccessPurchase,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: false,
      }
    },
    Contact: {
      screen: Contact,
      navigationOptions: {
        title: 'Contact & Suggestions',
        headerTitleAlign: 'center',
        headerTintColor: 'white',
        headerShown: true,
        headerTransparent: true,
        headerBackTitleVisible: false,
        headerTitleStyle: {
          fontSize:25,
          color: "#f8cd3e",
          fontFamily: "SciFly",
          alignSelf: 'center',
        }
      }
    },
    LostConnection: {
      screen: LostConnection,
      navigationOptions: {
        title: '',
        headerTintColor: 'white',
        headerShown: false,
      }
    }
}




const HomeStack = createStackNavigator(Screens);

export default createAppContainer(HomeStack);
