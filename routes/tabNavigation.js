import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from "react-navigation-stack";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';


import Four from '../components/four_page';
import Rank from '../components/RankPage';
import Home from '../components/Map';



const TopTabNavigator = createMaterialTopTabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{color: tintColor}]} size={25} name={'ios-home'} />
                    </View>
                ),
            }
        },
        Profile: {
            screen: Rank,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{color: tintColor}]} size={25} name={'ios-person'} />
                    </View>
                ),
                activeColor: '#ffffff',
                inactiveColor: '#a3c2fa',
                barStyle: { backgroundColor: '#2163f6' },
            }
        },
        History: {
            screen: Four,
            navigationOptions: {
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{color: tintColor}]} size={25} name={'ios-images'} />
                    </View>
                ),
                activeColor: '#ffffff',
                inactiveColor: '#92c5c2',
                barStyle: { backgroundColor: '#2c6d6a' },
            }
        },

    },
    {
        initialRouteName: 'Home',
        activeColor: '#ffffff',
        inactiveColor: '#bda1f7',
        barStyle: { backgroundColor: '#6948f4' },
    }
);
//const TabNavigator = createStackNavigator(Screens);

//export default createAppContainer(TabNavigator);

export default TopTabNavigator;
