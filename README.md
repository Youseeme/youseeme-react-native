# README #

React-Native Youseeme application

### What is this repository for? ###

* Youseeme Application
* Version 3.0

## 📚 Requirements :

```sh
$ [sudo] apt-get update
```

Install Node.Js :
```sh
$ [sudo] apt-get install nodejs npm
```
Install Expo Framework :
```sh
$ [sudo] npm install -g expo-cli
```

## 📥 Installation

Copy Repository :
```sh
$ git clone [YOUR_URL]
```

## 🚀 Launch

In the project directory:
```sh
$ npm start
```

### Contribution guidelines ###

* Test on "Testflight"


### Credit ###

* Thibault ANIN
* Youseeme 2020