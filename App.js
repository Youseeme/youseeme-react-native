import React, { Component } from 'react';
import { StyleSheet, Text, View, AppRegistry, Alert } from 'react-native';
import Navigator from './routes/HomeStack';
import Intro from './components/Intro'
import NetInfo from '@react-native-community/netinfo';
import {NavigationActions} from 'react-navigation';

export let navigatorRef;

/*const unsubscribe = NetInfo.addEventListener(state => {
    if(!state.isConnected) {
      Alert.alert(
        "Connexion perdue",
        "Oups, aucune connexion internet",
        [
          {
            text: "Réessayer",
            onPress: () => {}
          }
        ],
        {
          cancelable: false
        }
      );
    }
});*/

export default class App extends Component {

  componentDidMount() {
    navigatorRef = this.navigator;
  }

  render() {
    return (
      <Navigator ref={nav => { this.navigator = nav; }} />
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
