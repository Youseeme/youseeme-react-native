import React from 'react';
import {Alert, Animated, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Dimensions, TouchableHighlight, TouchableOpacity, ScrollView} from 'react-native';
import * as Font from 'expo-font';
import {getCardsFromApiWithAccountToken, removeCardFromApiWithId} from '../API/YSMApi';
import CreditCardItem from "./CreditCardItem";
import { SwipeListView } from 'react-native-swipe-list-view';
import { Ionicons, SimpleLineIcons, Feather } from '@expo/vector-icons';
import * as Haptics from 'expo-haptics';
import Root from './RootPopup';
import Toast from './Toast';

const rowSwipeAnimatedValues = {};
/*Array(2)
    .fill('')
    .forEach((_, i) => {
        rowSwipeAnimatedValues[`${i}`] = new Animated.Value(0);
    });*/

class BankCardsChoice extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });

    this._refreshSelectedSavedCard = this.props.navigation.getParam('refreshSelectedSavedCard');

    this.state = {
      cards: this.props.navigation.getParam('cards'),
      isLoading: true,
      selectedIndex: 0,
    };

  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
        title: title,
        text: message,
        color: '#2ecc71',
        icon:(<Ionicons
            name="ios-checkmark"
            style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
          />)
      });
    }
    else {
      Toast.show({
        title: title,
        text: message,
        color: '#FF453A',
        icon:(<Ionicons
            name="ios-close"
            style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
          />)
      });
    }
  }

  _removeCard = (cardId) => {
    removeCardFromApiWithId(this.token, cardId).then(data => {
      this._loadCards();
      if(data != null && data.message === "success")
        this._showToast('Suppression terminée', 'La suppression a bien été effectuée.', true);
      else
        this._showToast('Suppression annulée', 'Un problème est survenu lors de la suppression, veuillez réessayer ultérieurement.', false);
    });
  }

  _loadCards() {
    var promises = this.state.cards.map(card => {
    //var promises = data.map(card => {
      rowSwipeAnimatedValues[`${card.id}`] = new Animated.Value(0);
    });
    Promise.all(promises).then(() => {
      console.log()
      this.setState({
        isLoading: false,
        //cards: this.state.cards
      });
    });
  }

  componentDidMount() {
    this._loadCards();
  }

  render() {
    const closeRow = (rowMap, rowKey) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = (rowMap, rowKey) => {
        //closeRow(rowMap, rowKey);
        Alert.alert(
          "Attention",
          "Êtes vous sur de vouloir supprimer cette carte bancaire ?",
          [
            {
              text: "Non",
              onPress: () => {
                //this._loadCards();
                this._showToast('Suppression annulée', 'La suppression a bien été annulée', false);
                closeRow(rowMap, rowKey);
              }
            },
            {
              text: "Oui",
              onPress: () => {
                this._removeCard(rowKey);
              }
            }
          ],
          {
            cancelable: false
          }
        );
    };

    const onRowDidOpen = rowKey => {
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
    };

    const onSwipeValueChange = swipeData => {
        const { key, value } = swipeData;
        rowSwipeAnimatedValues[key].setValue(Math.abs(value));
    };

    const renderItem = data => (
        <TouchableHighlight
            onPress={() => {
              this._refreshSelectedSavedCard(data.item);
            }}
            style={styles.rowFront}
            underlayColor={'rgba(103, 65, 113, 0.8)'}
            activeOpacity={0.8}
        >
          <CreditCardItem card={data.item} />
        </TouchableHighlight>
    );

    const renderHiddenItem = (data, rowMap) => (
        <View style={styles.rowBack}>
            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnRight]}
                onPress={() => {
                  deleteRow(rowMap, data.item.id)
                }}
            >
                <Animated.View
                    style={[
                        styles.trash,
                        {
                            transform: [
                                {
                                    scale: rowSwipeAnimatedValues[
                                        data.item.id
                                    ].interpolate({
                                        inputRange: [20, 94],
                                        outputRange: [0, 1],
                                        extrapolate: 'clamp',
                                    }),
                                },
                            ],
                        },
                    ]}
                >
                    <Feather
                      name="trash-2"
                      style={styles.trash}
                    />
                </Animated.View>
            </TouchableOpacity>
        </View>
    );

    return (
      <Root>
        <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
          <View style={styles.topHeadband}>
              <Text style={styles.descriptiveText}>
                  Choisissez une carte bancaire enregistrée.
              </Text>
          </View>
          <View style={styles.mainContainer}>
            {this.state.isLoading ?
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' color='#674171' />
            </View> :
            this.state.cards.length > 0 ?
            <SwipeListView
              data={this.state.cards}
              style={{borderRadius: 15, marginTop: 1, overflow: 'hidden'}}
              contentContainerStyle={{paddingTop: 10, paddingBottom: 30, borderRadius: 15}}
              keyExtractor={(item) => item.id.toString()}
              renderItem={renderItem}
              renderHiddenItem={renderHiddenItem}
              leftOpenValue={0}
              rightOpenValue={-95}
              disableRightSwipe={true}
              disableLeftSwipe={true}
              stopRightSwipe={-120}
              onRowDidOpen={onRowDidOpen}
              onSwipeValueChange={onSwipeValueChange}
            /> :
            <ScrollView
              style={styles.noSavedCardContainer}
              contentContainerStyle={{
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 110,
                paddingBottom: 60
              }}
            >
              <Text style={styles.noSavedCardMessage}>{"Aucune cartes enregistrées"}</Text>
              <View style={styles.noSavedCardImageContainer}>
                <Image
                  style={styles.noSavedCardImage}
                  source={require('../assets/savedCard.png')}
                />
              </View>
            </ScrollView>}
          </View>
        </ImageBackground>
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: '#D9D8D9',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    //paddingHorizontal: 5
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:35,
    color: "#f8cd3e",
    marginTop:10,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },

  rowFront: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D9D8D9',
    height: 240,
    marginBottom: 0,
    borderRadius: 15,
    overflow: 'hidden',
    marginHorizontal: Platform.OS === 'ios' ? 15 : 8,
  },
  rowBack: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: '#D9D8D9',
    flexDirection: 'row',
    borderRadius: 15,
    paddingLeft: 5,
  },
  backRightBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 75,
    height: 75,
    //borderRadius: 15
  },
  backRightBtnLeft: {
    backgroundColor: '#674171',
    right: 75,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15
  },
  backRightBtnRight: {
    backgroundColor: '#FF3B30',
    right: 15,
    borderRadius: 40,
    alignSelf: 'center'
  },
  trash: {
    height: 45,
    width: 45,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: 'white',
    fontSize: 30,
    paddingTop: 4,
    borderRadius: 30
  },
  noSavedCardContainer: {
    flex: 1,
  },
  noSavedCardMessage: {
    fontSize: 20,
    color: '#674171'
  },
  noSavedCardImageContainer: {
    width: 150,
    height: 150,
    borderRadius: 100,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#674171'
  },
  noSavedCardImage: {
    width: '90%',
    height: '90%',

    alignSelf: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    borderRadius: 100,
  }
});

export default BankCardsChoice;
