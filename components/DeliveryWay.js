import React from 'react';
import {AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Platform, TouchableOpacity, ScrollView} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import SegmentedControlTab from "react-native-segmented-control-tab";
import * as Font from 'expo-font';
import { BlurView } from 'expo-blur';
import {getDeliveryFromApiWithShopId} from '../API/YSMApi';

class DeliveryWay extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.token = this.props.navigation.getParam('token');
    this.shopId = this.props.navigation.getParam('shopId');
    this.refreshDeliveryWay = this.props.navigation.getParam('refreshDeliveryWay');

    this.state = {
      delivery: null,
      deliveryWay: this.props.navigation.getParam('deliveryWay'),
      deliveryFee: 0.00,
      deliveryDelay: null,

      //totalAmount: 0.00,

      isLoading: true,
    };
  }

  _displayArticleCount() {
    var articleCount = this.state.shoppingBasket.articleCount;

    return (
      <View style={styles.articleCountContainer}>
        <Text style={styles.articleCountValue}>{articleCount}</Text>
        <Text style={styles.articleCountDescription}>{articleCount > 1 ? "articles" : "article"}</Text>
      </View>
    );
  }


  /*_setDeliveryWay(deliveryWay, state) {
      if(this.state.deliveryWay === "HOME")
        this.setState({
          deliveryWay: null,
          deliveryFee: this.state.delivery.delivery_price,
          deliveryDelay: null
        });
        //return;
      else {
        this.setState({
          deliveryWay: "HOME",
          deliveryFee: 0.00,
          deliveryDelay: null
        }, () => {
          this.refreshDeliveryWay("HOME", this.state.deliveryFee, this.state.deliveryDelay);
          this.props.navigation.goBack();
        });
      }
  }*/



  _loadDeliveryInfos() {
    console.log(this.token);
    getDeliveryFromApiWithShopId(this.token, this.shopId).then(data => {
      console.log(data);
      this.setState({
        delivery: data,
        isLoading: false
      });
    });
  }


  componentDidMount() {
    this._loadDeliveryInfos();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#674171'}}>
        <View style={styles.topHeadband}>
            <Text  style={styles.topTitle}>
                {"Mode de livraison"}
            </Text>
        </View>
        <View style={styles.mainContainer}>
          {this.state.isLoading ?
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' color='#674171' />
            </View> :
            <ScrollView
              style={styles.listContainer}
              scrollIndicatorInsets={{ right: 1 }}
              contentContainerStyle={{ paddingBottom: 90}}
            >
            {this.state.delivery.click_collect ?
              <View style={[styles.chooseContainer, {marginTop: 5}]}>
                {/*<Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>*/}
                <TouchableOpacity
                  style={
                    [styles.btnChoose,
                    {borderColor: this.state.deliveryWay === "CLICK & COLLECT" ? '#34C759' : 'transparent'}]
                  }
                  onPress={() => {
                    if(this.state.deliveryWay === "CLICK & COLLECT") {
                      this.setState({
                        deliveryWay: null,
                        deliveryFee: 0.00,
                        deliveryDelay: null
                      });
                      this.refreshDeliveryWay(null, 0.00, null);
                    }
                    else {
                      this.setState({
                        deliveryWay: "CLICK & COLLECT",
                        deliveryFee: 0.00,
                        deliveryDelay: null
                      }, () => {
                        this.refreshDeliveryWay("CLICK & COLLECT", this.state.deliveryFee, this.state.deliveryDelay);
                        this.props.navigation.goBack();
                      });
                    }
                  }}
                >
                  <View style={styles.deliveryFeeContainer}>
                    <Text style={styles.deliveryFee}>{"GRATUIT"}</Text>
                  </View>
                  <View style={styles.rightContainer}>
                    <View style={styles.infoChooseContainer}>
                      <Text style={styles.btnChooseTitle}>{"Click & Collect"}</Text>
                      <Text style={styles.btnChooseDescription}>{"Je récupère ma commande en boutique"}</Text>
                    </View>
                    <View style={styles.chooseImageContainer}>
                      <Image
                        style={styles.chooseImage}
                        source={require('../assets/clickAndCollect.png')}
                      />
                    </View>
                  </View>
                  <View style={styles.chooseIconContainer}>
                    <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.deliveryWay === "CLICK & COLLECT" ? '#34C759' : '#D9D8D9'}]}>
                      <Ionicons
                        name="ios-checkmark"
                        style={[styles.chooseIcon, {color: this.state.deliveryWay === "CLICK & COLLECT" ? 'white' : '#D9D8D9'}]}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View> : null}
              {this.state.delivery.house_delivery ?
                <View style={styles.chooseContainer}>
                  {/*<Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>*/}
                  <TouchableOpacity
                    style={
                      [styles.btnChoose,
                      {borderColor: this.state.deliveryWay === "HOME" ? '#34C759' : 'transparent'}]
                    }
                    onPress={() => {
                      if(this.state.deliveryWay === "HOME") {
                        this.setState({
                          deliveryWay: null,
                          deliveryFee: 0.00,
                          deliveryDelay: null
                        });
                        this.refreshDeliveryWay(null, 0.00, null);
                      }
                      else {
                        this.setState({
                          deliveryWay: "HOME",
                          deliveryFee: this.state.delivery.delivery_price,
                          deliveryDelay: null
                        }, () => {
                          this.refreshDeliveryWay("HOME", this.state.deliveryFee, this.state.deliveryDelay);
                          this.props.navigation.goBack();
                        });
                      }
                    }}
                  >
                    <View style={styles.deliveryFeeContainer}>
                      <Text style={styles.deliveryFee}>{this.state.delivery.delivery_price == 0 ? "GRATUIT" : this.state.delivery.delivery_price.toFixed(2) + " €"}</Text>
                    </View>
                    <View style={styles.rightContainer}>
                      <View style={styles.infoChooseContainer}>
                        <Text style={styles.btnChooseTitle}>{"Livraison à domicile"}</Text>
                        <Text style={styles.btnChooseDescription}>{"Je reçois ma commande à domicile"}</Text>
                        <Text style={styles.btnChooseDescription}>{"Environ " + this.state.delivery.delivery_delay}</Text>
                      </View>
                      <View style={styles.chooseImageContainer}>
                        <Image
                          style={styles.chooseImage}
                          source={require('../assets/homeDelivery.png')}
                        />
                      </View>
                    </View>
                    <View style={styles.chooseIconContainer}>
                      <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.deliveryWay === "HOME" ? '#34C759' : '#D9D8D9'}]}>
                        <Ionicons
                          name="ios-checkmark"
                          style={[styles.chooseIcon, {color: this.state.deliveryWay === "HOME" ? 'white' : '#D9D8D9'}]}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View> : null}
            </ScrollView>}

          {/*<BlurView intensity={Platform.OS === 'ios' ? 96 : 220} style={styles.bottomContainer}>
            <View style={styles.bottomSubContainer}>
            {this._displayArticleCount()}
            <View style={styles.shoppingBasketInfo}>
              <View style={styles.amountContainer}>
                <Text style={styles.amountTitle}>{"Montant du panier (TTC) :"}</Text>
                <Text style={styles.amountValue}>{this.state.shoppingBasket.totalAmount.toFixed(2) + " €"}</Text>
              </View>
              <View style={styles.amountContainer}>
                <Text style={styles.amountTitle}>{"Frais de livraison (TTC) :"}</Text>
                <Text style={styles.amountValue}>{this.state.deliveryFee.toFixed(2) + " €"}</Text>
              </View>
              <View style={styles.amountContainer}>
                <Text style={styles.amountTitle}>{"TOTAL (TTC) :"}</Text>
                <Text style={styles.amountValue}>{(this.state.shoppingBasket.totalAmount + this.state.deliveryFee).toFixed(2) + " €"}</Text>
              </View>
            </View>
            <TouchableOpacity
              style={styles.btnNext}
              onPress={() => {}}
            >
              <Text style={styles.textBtnNext}>{"Acheter"}</Text>
            </TouchableOpacity>
            </View>
          </BlurView>*/}
          </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171'
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    overflow: 'hidden'
  },
  listContainer: {
    width: '100%',
    //flex: 1,
    //paddingTop: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    paddingHorizontal: 10,
    paddingTop: 15,
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:22,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  bottomContainer: {
    width: '100%',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    borderRadius: 15,
    //flexDirection: 'row',
    //justifyContent: 'space-around',
    //alignItems: 'center',
    //paddingHorizontal: 10,
    //paddingTop: 5,
    //paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    zIndex: 1
  },
  bottomSubContainer: {
    flex: 1,
    backgroundColor: 'rgba(109, 72, 119, 0.45)',
    justifyContent: 'space-around',
    //alignItems: 'center',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    borderRadius: 15,
    zIndex: 1
  },
  btnNext: {
    backgroundColor: '#F8CE3E',
    width: 200,
    height: 45,
    alignSelf: 'center',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    marginBottom: 5,
    paddingHorizontal: 1
  },
  textBtnNext: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  shoppingBasketInfo: {
    flex: 0.85,
    justifyContent: 'center'
  },
  amountContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  amountTitle: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingRight: 8
  },
  amountValue: {
    color: '#F8CE3E',
    fontSize: 19,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  amountVAT: {
    color: '#F8CE3E',
    fontSize: 12,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  articleCountContainer: {
    width: 110,
    height: 28,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -2,
    backgroundColor: '#674171',
    borderRadius: 30,
    paddingHorizontal: 4,
    marginTop: 4,
    marginBottom: 6
  },
  articleCountValue: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
    paddingRight: 6
  },
  articleCountDescription: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
  },
  chooseContainer: {
    width: '99%',
    alignSelf: 'center',
    marginTop: 15
  },
  chooseDescriptionText: {
    color: '#674171',
    fontSize: 15,
    fontWeight: '600',
    marginBottom: 8,
    paddingLeft: 20
  },
  btnChoose: {
    width: '100%',
    height: 260,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1.25,
    borderColor: 'transparent',

    //backgroundColor: '#674171',
    backgroundColor: 'white',

    borderRadius: 15,
    paddingTop: 15,
    paddingBottom: 5,
    paddingLeft: 3,
    paddingRight: 12,

    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.5
  },
  chooseIconContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  chooseIconSubContainer: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    backgroundColor: '#D9D8D9',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30
  },
  chooseIcon: {
    color: 'white',
    fontSize: 30
  },
  deliveryFeeContainer: {
    height: 25,
    position: 'absolute',
    top: 14,
    right: 14,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    backgroundColor: '#674171',
    paddingHorizontal: 12
  },
  deliveryFee: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#F8CE3E',
    fontFamily: 'SciFly',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  rightContainer: {
    flex: 0.9,
    //alignItems: 'flex-start',
    justifyContent: 'space-around',
    marginLeft: 10,
    marginRight: 15
  },
  infoChooseContainer: {
    flex: 0.25,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  chooseImageContainer: {
    flex: 0.75,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    paddingRight: 20
  },
  chooseImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    paddingTop: 15,
    //backgroundColor: 'red'
  },
  btnChooseTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    //color: 'white',
    color: '#674171',
    marginBottom: 15
  },
  btnChooseDescription: {
    fontSize: 15,
    fontWeight: '400',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.7)',
  },
  btnChooseNoSelect: {
    fontSize: 15,
    fontWeight: '400',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.7)',
  }
});

export default DeliveryWay;
