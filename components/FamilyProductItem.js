import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, Modal} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import Moment from 'react-moment';
import 'moment-timezone';
import ProductItem from './ProductItem';

class FamilyProductItem extends React.Component {
  state = {
    modalVisible: false
  };

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  }

  render() {
    const {item, addToShoppingBasket, displayProductDetailWithOptions} = this.props;
    return (
      <TouchableOpacity
        style={styles.mainContainer}
        onPress={() => {
          this.setModalVisible(true);
        }}
      >
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <TouchableOpacity
                style={styles.btnCornerModal}
                activeOpacity={0.99}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
                >
                  {/*this._favored(this.state.favored)*/}
                  <Ionicons
                    name={"ios-close"}
                    style={{/*color: '#674171'*/color: '#F8CE3E', fontSize: 35}}
                  />
              </TouchableOpacity>
              <ProductItem
                product={item}
                addToShoppingBasket={addToShoppingBasket}
                displayProductDetailWithOptions={(product) => {
                  this.setModalVisible(!this.state.modalVisible);
                  displayProductDetailWithOptions(product);
                }}
                descriptionStyle={styles.descriptionStyle}
              />
            </View>
          </View>
        </Modal>

        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{uri: item.image}}
          />
        </View>
        <View style={styles.infosContainer}>
          <Text numberOfLines={2} style={styles.itemName}>{item.name}</Text>
          <Text numberOfLines={2} style={styles.itemDescription}>{item.description}</Text>
          <Text numberOfLines={1} style={styles.itemPrice}>{item.price != null ? parseFloat(item.price_ttc.replace(/,/g,".")).toFixed(2) + " €" : "0.00 €"}</Text>
        </View>
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: 240,
    width: 170,
    marginTop: 5,
    marginHorizontal: 10,
    paddingVertical: 8,
    paddingHorizontal: 8,
    borderRadius: 15,
    backgroundColor: '#674171'
  },
  imageContainer: {
    flex: 0.65,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden'
  },
  infosContainer: {
    flex: 0.35,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    marginTop: 2,
    paddingHorizontal: 3
  },
  itemName: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 14,
    fontWeight: 'bold',
    marginRight: 5,
  },
  itemDescription: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 11,
    fontWeight: '500',
    textAlign: 'justify',
    fontFamily: "SciFly"
  },
  itemPrice: {
    color: '#F8CE3E',
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 3
  },
  image: {
    width: '100%',
    height: '100%'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0, 0, 0, 0.7)'
  },
  modalView: {
    width: '96%',
    height: 310,
    paddingTop: 20,
    alignSelf: 'center',
    backgroundColor: '#674171',
    borderRadius: 15,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  btnCornerModal: {
    position: 'absolute',
    top: 0,
    right: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  descriptionStyle: {
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'flex-start',
    marginTop: 12,
    paddingHorizontal: 3
  }
});

export default FamilyProductItem;
