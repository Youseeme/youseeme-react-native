import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, ScrollView, TouchableOpacity, Switch} from 'react-native';
import * as Font from 'expo-font';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';

import EarthSvg from '../svgs/EarthSvg';

class SettingItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isEnabled: false
    }
  }

  render() {
    const {name, image, isLast, textColor, type, align, action, switchState} = this.props;
    if(type === "arrow")
      return (
          <TouchableOpacity
            style={isLast ? styles.setting : [styles.setting, {borderBottomWidth: StyleSheet.hairlineWidth, borderColor: 'grey'}]}
            onPress={() => {action()}}
          >
            <View style={styles.settingImageContainer}>
              {image()}
              {/*<Image
                style={styles.settingImage}
                source={image}
              />*/}
              {/*<SvgEarth />*/}
            </View>

            <View style={styles.settingNameContainer}>
              <Text style={styles.settingName}>{name}</Text>
            </View>
            <View style={styles.settingArrowContainer}>
              <Ionicons
                name="ios-arrow-forward"
                style={styles.settingArrow}
              />
            </View>
          </TouchableOpacity>
      );
    else if(type === "switch")
      return (
          <View
            style={isLast ? styles.setting : [styles.setting, {borderBottomWidth: StyleSheet.hairlineWidth, borderColor: 'grey'}]}
            onPress={() => action()}
          >
            <View style={styles.settingImageContainer}>
              {/*<Image
                style={styles.settingImage}
                source={image}
              />*/}
              {image()}
              {/*<SvgEarth />*/}
            </View>
            <View style={styles.settingNameContainer}>
              <Text style={styles.settingName}>{name}</Text>
            </View>
            <View style={styles.settingArrowContainer}>
              <Switch
                  style={{marginLeft: -40}}
                  trackColor={{ false: "white", true: "#674171" }}
                  thumbColor={switchState ? "white" : "white"}
                  ios_backgroundColor="#f4f3f4"
                  onValueChange={() => {action()}}
                  value={switchState}
                />
            </View>
          </View>
      );
    else
      return (
        <TouchableOpacity
          style={
            isLast ? [styles.setting, {justifyContent: align, alignItems: 'center'}] :
            [styles.setting, {borderBottomWidth: StyleSheet.hairlineWidth, borderColor: 'grey', justifyContent: align, alignItems: 'center', paddingLeft: 17}]}
          onPress={() => action()}
        >
          <Text style={[styles.settingName2, {color: textColor}]}>{name}</Text>
        </TouchableOpacity>
      );
  };
}

const styles = StyleSheet.create({
  setting: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 50,
    paddingTop: 3,
    paddingBottom: 5,
    marginLeft: 3,
    marginRight: 3,
    borderRadius: 5,
  },
  settingImageContainer: {
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  settingImage: {
    width: 30,
    height: 30,
  },
  settingNameContainer: {
    flex: 0.92,
    justifyContent: 'space-around',
  },
  settingName: {
    color: '#674171',
    fontSize: 17,
  },
  settingName2: {
    color: '#674171',
    fontSize: 17,
    textAlign: 'center'
  },
  settingArrowContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  settingArrow: {
    color: "rgba(103, 65, 113, 0.6)",
    fontSize: 20
  }
});

export default SettingItem;
