import React from 'react';
import {Alert, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import AddressItem from './AddressItem';
import * as Font from 'expo-font';
import {getUserAddressesFromApiWithToken, removeUserAddressFromApiWithId} from '../API/YSMApi';


class Addresses extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.currentPage = 0;


    this.state = {
      addresses: [],
      isLoading: false,
      selectedIndex: 0,
    };

  }

  _loadAddresses() {
    this.setState({isLoading: true});
    console.log(" reload adresse");
    getUserAddressesFromApiWithToken(this.token).then(data => {
      this.setState({
        addresses: data,
        isLoading: false
      });
    });
  }

  _removeAddress = (id) => {
    Alert.alert(
      "Attention",
      "Êtes vous sur de vouloir supprimer cette adresse ?",
      [
        {
          text: "Non",
        },
        {
          text: "Oui",
          onPress: () => {
            this.setState({isLoading: true});
            removeUserAddressFromApiWithId(this.token, id).then(data => {
              if(data.message === "success") {
                this._loadAddresses();
              }
            });
          }
        }
      ],
      {
        cancelable: false
      }
    );

    /*Alert.alert(
      "Adresse",
      "Êtes vous sur de vouloir supprimer cette adresse ?"
      [
        {
          text: "Non",
        },
        {
          text: "Oui",
          //onPress: () => console.log("OK Pressed")
        }
      ],
      {
        cancelable: false
      }
    );*/
  }

  _navigateToNewAddressPage = (btnSubmitText, initialAddress) => {
    this.props.navigation.navigate('NewAddress', {token: this.token, btnSubmitText: btnSubmitText, address: initialAddress, onGoBack: () => this._loadAddresses()})
  }


  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
  }

  componentDidMount() {
    this._loadAddresses();
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            {/*<Image
                style={styles.logo}
                source={require('../assets/logo.png')}
              />

            <Text  style={styles.topTitle}>
                Mon relevé de compte
            </Text>*/}
            <Text style={styles.descriptiveText}>
                {"Gère tes adresses de livraison"}
            </Text>
        </View>
        <View style={styles.mainContainer}>
        <FlatList
          style={styles.listContainer}
          contentContainerStyle={{paddingBottom: 100}}
          ListFooterComponent={
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <TouchableOpacity
                style={styles.btnNewAddress}
                onPress= {() => this._navigateToNewAddressPage("Ajouter")}
              >
                <Text style={styles.textNewAddress}>{"Ajouter une adresses"}</Text>
              </TouchableOpacity>
            </View>
          }
          data={this.state.addresses}
          extraData={this.state.addresses}
          keyExtractor={(item) => (item.id).toString()}
          renderItem={({item}) => {
            return (<AddressItem address={item} addAddress={this._addAddress} removeAddress={this._removeAddress} navigateToNewAddressPage={this._navigateToNewAddressPage} />);
          }}
          onEndReachedThreshold={0.5}
          onRefresh={() => this._loadAddresses()}
          refreshing={this.state.isLoading}
          onEndReached={() => {
            //this._loadAddresses();
          }}
        />
        {/*this._displayLoading()*/}
      </View>
      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listContainer: {
    width: '98%',
    marginTop: 1,
    paddingTop: 10,
    paddingHorizontal: 10,
    marginHorizontal: 5,
    //backgroundColor: 'white',
    //borderTopLeftRadius: 10,
    //borderTopRightRadius: 10
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:35,
    color: "#f8cd3e",
    marginTop:10,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
  },
  btnNewAddress: {
    width: 200,
    backgroundColor: '#674171',
    borderRadius: 15,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30
  },
  textNewAddress: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '600'
  }
});

export default Addresses;
