import React from 'react';
import {AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, ScrollView, TouchableOpacity, Linking, Platform} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import * as Font from 'expo-font';
import {logoutWithToken, setNotificationsFromApiWithValue, setGeofencingFromApiWithValue, getSettingStateFromApiWithToken} from '../API/YSMApi';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome } from '@expo/vector-icons';
import SettingItem from './SettingItem';
import { NavigationActions, StackActions } from 'react-navigation';

class Settings extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.token = this.props.navigation.getParam('token')

    this.state = {
      notifications: false,
      geofencing: false
    };

  }

  async _logout() {
    logoutWithToken(this.token);
    try {
      //await AsyncStorage.removeItem('credentials');
      AsyncStorage.clear();
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }

    this.props.navigation.dispatch(StackActions.reset({
      index:0,
      actions:[
        NavigationActions.navigate({
          routeName:'First'
        })
      ]
    }));
  }

  _loadWebPage(url, name) {
    this.props.navigation.navigate('Browser', {url: url, name: name});
  }

  _loadSettings() {
    getSettingStateFromApiWithToken(this.token).then(data => {
      this.setState({
        notifications: data.push_notifications,
        geofencing: data.geofencing
      });
    });
  }

  _setNotifications() {
    setNotificationsFromApiWithValue(!this.state.notifications, this.token).then(data => {
      this._loadSettings();
    });

  }

  _setGeofencing() {
    setGeofencingFromApiWithValue(!this.state.geofencing, this.token).then(data => {
      this._loadSettings();
    });
  }

  componentDidMount() {
    this._loadSettings();
  }


  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
          <View style={styles.topHeadband}>

          </View>
          <ScrollView
            style={styles.mainContainer}
            contentContainerStyle={{paddingBottom:60}}
          >
            <View style={styles.settingsBlockContainer}>
              <View style={styles.settingsBlockTitleContainer}>
                <Text style={styles.settingsBlockTitle}>RENSEIGNEMENTS</Text>
              </View>
              <View style={styles.settingsContainer}>
                <SettingItem name={"Actualités"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#007AFF', borderRadius: 5}}>
                    <FontAwesome5
                      name="newspaper"
                      style={{ color: "white", fontSize: 18, paddingTop: Platform.OS === 'ios' ? 2 : 0}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"arrow"} action={() => this.props.navigation.navigate('News', {token: this.token})} />
                <SettingItem name={"FAQ"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#007AFF', borderRadius: 5}}>
                    <FontAwesome5
                      name="question"
                      style={{ color: "white", fontSize: 20, paddingTop: Platform.OS === 'ios' ? 2 : 0, paddingLeft: 0.5}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"arrow"} action={() => this._loadWebPage("https://www.youseeme.pro/04-helpuser/", "FAQ")} />
                <SettingItem name={"Site web"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#007AFF', borderRadius: 5}}>
                    <MaterialCommunityIcons
                      name="earth"
                      style={{ color: "white", fontSize: 26, paddingTop: Platform.OS === 'ios' ? 2 : 0, paddingLeft: 0.5}}
                    />
                  </View>);
                }}
                isLast={true} textColor={'#674171'} type={"arrow"} action={() => Linking.openURL('https://www.youseeme.pro/')} />
              </View>
            </View>
            <View style={styles.settingsBlockContainer}>
              <View style={styles.settingsBlockTitleContainer}>
                <Text style={styles.settingsBlockTitle}>ALERTES</Text>
              </View>
              <View style={styles.settingsContainer}>
                <SettingItem name={"Notification"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FF453A', borderRadius: 5}}>
                    <Ionicons
                      name="ios-notifications"
                      style={{ color: "white", fontSize: 26, paddingTop: Platform.OS === 'ios' ? 2 : 0}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"switch"} activeSwitch={true} switchState={this.state.notifications} action={() => this._setNotifications()} />
                <SettingItem name={"Localisation"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FF453A', borderRadius: 5}}>
                    <Ionicons
                      name="md-locate"
                      style={{ color: "white", fontSize: 23, paddingTop: Platform.OS === 'ios' ? 2 : 0, paddingLeft: 0.5}}
                    />
                  </View>);
                }} isLast={true} textColor={'#674171'} type={"switch"} activeSwitch={true} switchState={this.state.geofencing} action={() => this._setGeofencing()} />
              </View>
            </View>
            <View style={styles.settingsBlockContainer}>
              <View style={styles.settingsBlockTitleContainer}>
                <Text style={styles.settingsBlockTitle}>SÉCURITÉ</Text>
              </View>
              <View style={styles.settingsContainer}>
                <SettingItem name={"Code pin"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FF9F0A', borderRadius: 5}}>
                    <MaterialCommunityIcons
                      name="textbox-password"
                      style={{ color: "white", fontSize: 24, paddingTop: Platform.OS === 'ios' ? 2 : 0}}
                    />
                  </View>);
                }} isLast={true} textColor={'#674171'} type={"arrow"} action={() => this.props.navigation.navigate('ChangePinCode', {token: this.token})}/>
              </View>
            </View>
            <View style={styles.settingsBlockContainer}>
              <View style={styles.settingsBlockTitleContainer}>
                <Text style={styles.settingsBlockTitle}>PLUS</Text>
              </View>
              <View style={styles.settingsContainer}>
                <SettingItem name={"Comment ça marche ?"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#34C759', borderRadius: 5}}>
                    <MaterialCommunityIcons
                      name="presentation-play"
                      style={{ color: "white", fontSize: 23, paddingTop: Platform.OS === 'ios' ? 3 : 0}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"arrow"} action={() => this._loadWebPage("https://www.youseeme.pro/tuto/", "Tutorial")}/>
                {/*<SettingItem name={"Comment ça marche ?"} isLast={false} textColor={'#5AC8FA'} align={'flex-start'} action={() => this._loadWebPage("https://www.youseeme.pro/tuto/", "Tutorial")}/>*/}
                <SettingItem name={"Contact & Suggestions"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FF453A', borderRadius: 5}}>
                    <Ionicons
                      name="ios-mail"
                      style={{ color: "white", fontSize: 25, paddingTop: Platform.OS === 'ios' ? 2 : 0}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"arrow"} action={() => this.props.navigation.navigate('Contact', {token: this.token})} />
                {/*<SettingItem name={"Apparence"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#AEAEB2', borderRadius: 5}}>
                    <Ionicons
                      name="ios-settings"
                      style={{ color: "white", fontSize: 26, paddingTop: 2}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"arrow"} action={() => {}} />*/}
                <SettingItem name={"À propos"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#AEAEB2', borderRadius: 5}}>
                    <MaterialCommunityIcons
                      name="dots-horizontal"
                      style={{ color: "white", fontSize: 26, paddingTop: Platform.OS === 'ios' ? 2 : 0}}
                    />
                  </View>);
                }} isLast={false} textColor={'#674171'} type={"arrow"} action={() => this._loadWebPage("https://www.youseeme.pro/apropos/", "À propos")} />
                <SettingItem name={"CGU & RGPD"} image={() => {
                  return(
                  <View style={{width: 30, height: 30, alignItems: 'center', justifyContent: 'center', backgroundColor: '#007AFF', borderRadius: 5}}>
                    <Ionicons
                      name="ios-paper"
                      style={{ color: "white", fontSize: 23, paddingTop: Platform.OS === 'ios' ? 2 : 0}}
                    />
                  </View>);
                }} isLast={true} textColor={'#674171'} type={"arrow"} action={() => this._loadWebPage("https://www.youseeme.pro/cgu/", "CGU & RGPD")} />
              </View>
            </View>
            <View style={styles.settingsBlockContainer}>
              <View style={styles.settingsContainer}>
                <SettingItem name={"Déconnexion"} isLast={true} textColor={'#FF453A'} align={'center'} action={() => this._logout()} />
              </View>
            </View>
        </ScrollView>
      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 0
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingBottom: 40
  },
  settingsBlockContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: 30
  },
  settingsBlockTitleContainer: {
    width: '86%',
    alignItems: 'flex-start'
  },
  settingsBlockTitle: {
    color: 'hsla(240, 1%, 48%, 0.98)'
  },
  settingsContainer: {
    width: '95%',
    borderRadius: 10,
    backgroundColor: 'white',
    marginTop: 5
  }
});

export default Settings;
