import React from 'react';
import {AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Platform, TouchableOpacity} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import SegmentedControlTab from "react-native-segmented-control-tab";
import ShoppingBasketItem from './ShoppingBasketItem';
import * as Font from 'expo-font';
import { BlurView } from 'expo-blur';


class ShoppingBasket extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.clearShoppingBasket = this.props.navigation.getParam('clearShoppingBasket');
    this.shoppingBasketCode = this.props.navigation.getParam('shoppingBasketCode');
    this.refreshShoppingBasket = this.props.navigation.getParam('refreshShoppingBasket');

    this.state = {
      shoppingBasket: this.props.navigation.getParam('shoppingBasket'),
      //totalAmount: 0.00,
      isLoading: false,
    };
  }

  _displayArticleCount() {
    var articleCount = this.state.shoppingBasket.articleCount;

    return (
      <View style={styles.articleCountContainer}>
        <Text style={styles.articleCountValue}>{articleCount}</Text>
        <Text style={styles.articleCountDescription}>{articleCount > 1 ? "articles" : "article"}</Text>
      </View>
    );
  }

  _displayValidateButton() {
    var valid = this.state.shoppingBasket.articleCount > 0;
    return (
      <TouchableOpacity
        style={[styles.btnNext, {backgroundColor: valid ? '#F8CE3E' : '#D9D8D9'}]}
        disabled={!valid}
        onPress={() => this.props.navigation.navigate('PlaceOrder', {
          token: this.token,
          shoppingBasket: this.state.shoppingBasket,
          shoppingBasketCode: this.shoppingBasketCode
        })}
      >
        <Text style={styles.textBtnNext}>{"Valider"}</Text>
      </TouchableOpacity>
    );
  }

  _refreshQuantity = async (article, sign) => {
    const index = this.state.shoppingBasket.articles.map(
      e => e.article_code + "-" + e.bar_code + "-" + e.name
    ).indexOf(article.article_code + "-" + article.bar_code + "-" + article.name);
    try {
      if(index !== -1) {
        var articles = this.state.shoppingBasket.articles;
        if(sign === "+") {
          articles[index].quantity++;
          await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
            articles: articles,
            articleCount: this.state.shoppingBasket.articleCount + 1,
            totalAmount: this.state.shoppingBasket.totalAmount + parseFloat(article.price_ttc.replace(/,/g,"."))
          }));
        }
        else {
          articles[index].quantity--;
          await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
            articles: articles,
            articleCount: this.state.shoppingBasket.articleCount - 1,
            totalAmount: this.state.shoppingBasket.totalAmount - parseFloat(article.price_ttc.replace(/,/g,"."))
          }));
        }
      }
      this._loadShoppingBasket();
      this.refreshShoppingBasket();
    } catch (error) {

    }
  }

  _removeProduct = async (article) => {
    const index = this.state.shoppingBasket.articles.map(
      e => e.article_code + "-" + e.bar_code + "-" + e.name
    ).indexOf(article.article_code + "-" + article.bar_code + "-" + article.name);
    try {
      if(index !== -1) {
        var articles = this.state.shoppingBasket.articles;
        articles.splice(index, 1);
        await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
          articles: articles,
          articleCount: this.state.shoppingBasket.articleCount - article.quantity,
          totalAmount: this.state.shoppingBasket.totalAmount - (parseFloat(article.price_ttc.replace(/,/g,".") * article.quantity))
        }));
      }
      this._loadShoppingBasket();
      this.refreshShoppingBasket();
    } catch (error) {

    }
  }

  async _clearShoppingBasket() {
    this.clearShoppingBasket();
    this.setState({
      shoppingBasket: {
        articles: [],
        articleCount: 0,
        totalAmount: 0.00
      }
    })
  }

  _loadShoppingBasket() {
    AsyncStorage.getItem(this.shoppingBasketCode).then(data => {
      if(data != null) {
        var shoppingBasket = JSON.parse(data);
        this.setState({shoppingBasket: shoppingBasket});
      }
    });
  }


  componentDidMount() {
    //this._calculTotalAmount();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#674171'}}>
      <View style={styles.topHeadband}>
          <Text  style={styles.topTitle}>
              {"Mon panier"}
          </Text>
          <TouchableOpacity
            style={{position: 'absolute', bottom: 14, right: 8}}
            onPress={() => this._clearShoppingBasket()}
          >
            {/*<FontAwesome
              name="trash-o"
              style={{fontSize: 28, paddingRight: 0, color: 'white'}}
            />*/}
            <Feather
              name="trash-2"
              style={{fontSize: 28, paddingRight: 0, color: 'white'}}
            />

          </TouchableOpacity>
      </View>
        <View style={styles.mainContainer}>
          <FlatList
            style={styles.listContainer}
            contentContainerStyle={{marginTop: 8, paddingBottom: 110}}
            data={this.state.shoppingBasket.articles}
            keyExtractor={(item) => (++this.cptId).toString()}
            renderItem={({item}) => <ShoppingBasketItem
              product={item}
              refreshQuantity={this._refreshQuantity}
              removeProduct={this._removeProduct}/>
            }
            refreshing={this.state.isLoading}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              //this._loadTransactions();
            }}
          />
          <BlurView intensity={Platform.OS === 'ios' ? 96 : 220} style={styles.bottomContainer}>
            <View style={styles.bottomSubContainer}>
            <View style={styles.shoppingBasketInfo}>
              <View style={styles.totalAmountContainer}>
                <Text style={styles.totalAmountTitle}>{"TOTAL :"}</Text>
                <Text style={styles.totalAmountValue}>{this.state.shoppingBasket.totalAmount.toFixed(2) + " €"}</Text>
              </View>
              {this._displayArticleCount()}
            </View>
              {this._displayValidateButton()}
            </View>
          </BlurView>
      </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171',
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    overflow: 'hidden'
  },
  listContainer: {
    width: '100%',
    //flex: 1,
    paddingTop: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    //overflow: 'hidden',
    //alignSelf: 'center',
    //backgroundColor: 'red',
    paddingHorizontal: 5
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:22,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
  },
  tabView: {
    marginTop: 15,
    marginRight: 40,
    marginLeft: 40
  },
  tabStyle: {
    borderColor: '#6D4877',
    height: 35,
    backgroundColor: 'transparent'
  },
  tabTextStyle: {
    color:'#6D4877',
    fontFamily: "SciFly"
  },
  activeTabStyle: {
    backgroundColor: '#6D4877',
  },
  activeTabTextStyle: {
    color:'white'
  },
  bottomContainer: {
    width: '100%',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    //flexDirection: 'row',
    //justifyContent: 'space-around',
    //alignItems: 'center',
    //paddingHorizontal: 10,
    //paddingTop: 5,
    //paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    zIndex: 1
  },
  bottomSubContainer: {
    flex: 1,
    backgroundColor: 'rgba(109, 72, 119, 0.45)',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    zIndex: 1
  },
  btnNext: {
    backgroundColor: '#F8CE3E',
    width: 125,
    height: 45,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    paddingHorizontal: 1
  },
  textBtnNext: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  shoppingBasketInfo: {
    flex: 0.85,
    justifyContent: 'center'
  },
  totalAmountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  totalAmountTitle: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingRight: 8
  },
  totalAmountValue: {
    color: '#F8CE3E',
    fontSize: 22,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  articleCountContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: -2
  },
  articleCountValue: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
    paddingRight: 6
  },
  articleCountDescription: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
  }
});

export default ShoppingBasket;
