import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, ScrollView, TouchableOpacity, Clipboard, Share, Dimensions} from 'react-native';
import * as Font from 'expo-font';
import { Ionicons, SimpleLineIcons, Feather, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons } from '@expo/vector-icons';
//import Clipboard from "@react-native-community/clipboard";
import Root from './RootPopup';
import Toast from './Toast';

const { width, height } = Dimensions.get('screen')

class Sponsorship extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });

    this.sponsorshipCode = this.props.navigation.getParam('sponsorshipCode');
    this.message = "Rejoins Youseeme (-› https://www.youseeme.pro) et fais des économies à chaque achat 💸🍾.\nTu me feras gagner au passage 2 Bartcoins en utilisant mon code 😃 : " + this.sponsorshipCode;

    this.state = {

    };
  }

  _onShare = async () => {
    try {
      const result = await Share.share({
        message: this.message
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  _copyToClipboard() {
    Clipboard.setString(this.message);
    Toast.show({
        title: 'Presse-papier',
        text: 'Votre code de parrainage a bien été copié.',
        color: '#2ecc71',
        icon:(<Ionicons
            name="ios-checkmark"
            style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
          />)
    });
  }


  componentDidMount() {

  }

  render() {
    return (
      <Root>
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
      {/*
          <View>
              <TouchableOpacity
                  style= {{marginTop: 100}}
                  onPress={() =>
                      Toast.show({
                          title: 'User created',
                          text: 'Your user was successfully created, use the app now.',
                          color: '#2ecc71'
                      })
                  }
              >
                  <Text>Call Toast</Text>
              </TouchableOpacity>
          </View>
      */}
        <View style={styles.topHeadband}>
            <Text style={styles.descriptiveText}>
                {"Partage ton code parrain sur les réseaux ou à tes amis !"}
            </Text>
        </View>
        <ScrollView style={styles.mainContainer}>

            <View style={styles.subContainer}>
              {/*<View style={styles.sponsorshipTitleContainer}>
                <Text style={styles.sponsorshipTitle}>
                    {"PARRAINEZ UN PROCHE"}
                </Text>
              </View>*/}
              <View style={styles.sponsorshipCodeContainer}>
                <Text style={styles.sponsorshipSubTitle}>
                  {"Voici ton code de parrainage :"}
                </Text>
                <Text style={styles.sponsorshipCode}>
                  {this.sponsorshipCode}
                </Text>
              </View>
              <View style={styles.shareItemsContainer}>
                <TouchableOpacity
                  style={styles.shareItem}
                  onPress={this._onShare}
                >
                  <Feather
                    name="share"
                    style={{color: '#674171', fontSize: 40}}
                  />
                  <Text style={styles.shareItemText}>
                    {"Partager"}
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.shareItem}
                  onPress={() => this._copyToClipboard()}
                >
                  <Ionicons
                    name="ios-copy"
                    style={{color: '#674171', fontSize: 40}}
                  />
                  <Text style={styles.shareItemText}>
                    {"Copier"}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.sponsorshipExplanationContainer}>
                <Text style={styles.sponsorshipExplanation}>
                    {"À chaque fois qu’une nouvelle personne rejoint la Youseeme Family avec ton code parrain, tu gagnes 2 Bartcoins dès sa première commande. On compte sur toi pour partager un max !"}
                </Text>
              </View>
            </View>
        </ScrollView>
      </ImageBackground>
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    //height: height,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    //borderTopLeftRadius: 10,
    //borderTopRightRadius: 10
    borderRadius: 15,
  },
  subContainer: {
    height: 500,
    marginTop: 30,
    marginBottom: 30,
    marginRight: 15,
    marginLeft: 15
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  sponsorshipTitleContainer: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F8CE3E',
    borderRadius: 10
  },
  sponsorshipTitle: {
    textAlign: 'center',
  },
  sponsorshipCodeContainer: {
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#674171',
    borderRadius: 15
  },
  sponsorshipSubTitle: {
    textAlign: 'center',
    color: 'white'
  },
  sponsorshipCode: {
    textAlign: 'center',
    fontSize: 50,
    fontWeight: '400',
    color: '#F8CE3E',
    fontFamily: "SciFly",
    padding: 15,
  },
  shareItemsContainer: {
    flex: 0.4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: 'transparent'
  },
  shareItem: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  shareItemText: {
    textAlign: 'center',
    marginTop: 3,
    color: 'grey'
  },
  sponsorshipExplanationContainer: {
    flex: 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: 'white',
    borderRadius: 15,
  },
  sponsorshipExplanation: {
    textAlign: 'justify',
    color: '#674171',
    fontSize: 18,
    fontFamily: "SciFly",
    paddingVertical: 10
  },
});

export default Sponsorship;
