import React from 'react';
import {Animated, AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, TextInput, View, Platform, TouchableOpacity, ScrollView, Easing} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import SegmentedControlTab from "react-native-segmented-control-tab";
import * as Font from 'expo-font';
import { BlurView } from 'expo-blur';
import {sendPurchaseFromApiWithAmount, newCardRegisterOrderFromApiWithInfo, basicRegisterOrderFromApiWithInfo, getUserProfileFromApiWithAccountToken} from '../API/YSMApi';
import Root from './RootPopup';
import Toast from './Toast';

class PlaceOrder extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.token = this.props.navigation.getParam('token');
    this.shoppingBasketCode = this.props.navigation.getParam('shoppingBasketCode');
    this.pinCodeTopValue = new Animated.Value(-500);
    this.pinCode = "";
    this.bought = false;

    this.state = {
      userProfile: null,
      shoppingBasket: this.props.navigation.getParam('shoppingBasket'),
      deliveryWay: null,
      deliveryAddress: null,
      paymentWay: null,

      deliveryFee: 0.00,

      walletAmount: null,
      savedCard: null,

      isLoading: false,

      showPinCodeEntry: false,
      errorMessage: "",

      paymentLoading: false,
    };
  }

  _displayArticleCount() {
    var articleCount = this.state.shoppingBasket.articleCount;

    return (
      <View style={styles.articleCountContainer}>
        <Text style={styles.articleCountValue}>{articleCount}</Text>
        <Text style={styles.articleCountDescription}>{articleCount > 1 ? "articles" : "article"}</Text>
      </View>
    );
  }


  async _clearShoppingBasket() {
    this.clearShoppingBasket();
    this.setState({
      shoppingBasket: {
        articles: [],
        articleCount: 0,
        totalAmount: 0.00
      }
    })
  }

  _loadProfile() {
    getUserProfileFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        userProfile: data
      });
    });
  }

  async _clearBasket() {
    try {
      await AsyncStorage.removeItem(this.shoppingBasketCode);
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  _verificationBeforeOrderRegister() {
    if(this.pinCode === this.state.userProfile.pincode) {
      this._hidePinCodeEntry(false);
      this._registerOrder(true);
    }
    else {
      this.setState({
        errorMessage: "Le code pin n'est pas valide."
      });
    }
  }

  _registerOrder(basic) {
    const regex1 = new RegExp('error', 'i');
    const regex2 = new RegExp('the receiver is blocked', 'i');
    const regex3 = new RegExp('the sender is blocked', 'i');

    var deliveryWayCode = () => {
      switch (this.state.deliveryWay) {
        case "HOME":
          return "home_delivery";
          break;
        case "CLICK & COLLECT":
          return "click_collect";
      }
    }

    var paymentWayCode = () => {
      switch (this.state.paymentWay) {
        case "E-WALLET":
          return "wallet";
        case "B-WALLET":
          return "bartcoin";
        case "NEW-CARD":
          return "new_card";
        case "SAVED-CARD":
          return "preset_card";
      }
    }

    var deliveryAddress = () => {
      const address = this.state.deliveryAddress.address + ", " +
      this.state.deliveryAddress.zipcode + ", " +
      this.state.deliveryAddress.city;

      return address;
    }

    var articlesFormat = () => {
      var items = {};
      var promises = this.state.shoppingBasket.articles.map(item => {
        items[item.article_code] = item.quantity;
      });

      return items;
    }

    var order = {
      shop_id: this.shoppingBasketCode.split("-")[1],
      card_id: this.state.savedCard != null ? this.state.savedCard.id : null,
      amount: (parseFloat(this.state.shoppingBasket.totalAmount + this.state.deliveryFee)).toFixed(2),
      payment_method: paymentWayCode(),
      delivery_type: deliveryWayCode(),
      delivery_address: deliveryAddress(),
      items: articlesFormat()
    };

    console.log("+**************+");
    console.log(order);
    console.log("**************");
    //console.log(data);
    console.log("/**************/");

    this.setState({paymentLoading: true}, () => {
      if(basic) {
        basicRegisterOrderFromApiWithInfo(this.token, order).then(data => {
          if(!regex1.test(data)) {
            this._clearBasket();
            this.setState({paymentLoading: false});
            this._showToast('Paiement terminé', 'L\'achat a bien été effectué.', true);
            this.props.navigation.navigate('SuccessPurchase', {token: this.token});
          }
          else {
            if(regex2.test(data))
              this._showToast('Paiement annulé', 'Il semblerait que ce commerçant n\'accepte que des bartcoins.', false);
            if(regex3.test(data))
              this._showToast('Paiement annulé', 'Tu dois d\'abord réaliser ton KYC pour payer en euros.', false);
            else
              this._showToast('Paiement annulé', 'Un problème est survenu lors de l\'achat, veuillez réessayer ultérieurement.', false);
          }
        });
      }
      else {
        newCardRegisterOrderFromApiWithInfo(this.token, order).then(data => {
          if(!regex1.test(data)) {
            this._clearBasket();
            this.setState({paymentLoading: false});
            this._showToast('Paiement terminé', 'L\'achat a bien été effectué.', true);
            this.props.navigation.navigate('SuccessPurchase');
          }
          else {
            if(regex2.test(data))
              this._showToast('Paiement annulé', 'Il semblerait que ce commerçant n\'accepte que des bartcoins.', false);
            if(regex3.test(data))
              this._showToast('Paiement annulé', 'Tu dois d\'abord réaliser ton KYC pour payer en euros.', false);
            else
              this._showToast('Paiement annulé', 'Un problème est survenu lors de l\'achat, veuillez réessayer ultérieurement.', false);
          }
        });
        console.log("----------------- RECUP ? -------------------");
      }
    });
  }

  _finishPurchase = (response) => {
    console.log("****************************************** CATCH ! ******************************************");
    if(response === "success" && !this.bought) {
      this.bought = true;
      this._registerOrder(false);
    }
    else if(response === "cancel") {
      this._showToast('Paiement annulé', 'L\'achat a bien été annulé.', false);
    }
    else
      this._showToast('Un problème est survenu lors de l\'achat, veuillez réessayer ultérieurement.', false);
  }

  _buy() {
    if(this.state.paymentWay === "NEW-CARD") {
      sendPurchaseFromApiWithAmount(this.token, (this.state.shoppingBasket.totalAmount + this.state.deliveryFee).toFixed(2)).then(data => {
        console.log(data);
        if(data == null || Object.entries(data).length === 0)
          this._showToast('Paiement annulé', 'Un problème est survenu lors de l\'achat, veuillez réessayer ultérieurement.', false);
        else {
          this.props.navigation.navigate('Browser', {
            url: "https://webkit.lemonway.fr/mb/youseeme/prod/?moneyintoken=" + data.TOKEN,
            name: "Paiement",
            type: "payment-webkit",
            feedback: this._finishPurchase
          });
        }
      });
    }
    else
      this._showPinCodeEntry();
  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
          title: title,
          text: message,
          color: '#2ecc71',
          icon:(<Ionicons
              name="ios-checkmark"
              style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
            />)
      });
    }
    else {
      Toast.show({
          title: title,
          text: message,
          color: '#FF453A',
          icon:(<Ionicons
              name="ios-close"
              style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
            />)
      });
    }
  }

  _showPinCodeEntry() {
    Animated.parallel (
      [
        Animated.timing(
          this.pinCodeTopValue,
          {
            toValue: 100,
            duration: 500,
            easing: Easing.out(Easing.exp)
          }
        ),
      ],
      {
        useNativeDriver: true
      }
    ).start(() => {
      this.setState({showPinCodeEntry: true});
    });
  }

  _hidePinCodeEntry(forced) {
    Animated.parallel (
      [
        Animated.timing(
          this.pinCodeTopValue,
          {
            toValue: -500,
            duration: 500,
            easing: Easing.out(Easing.exp)
          }
        ),
      ],
      {
        useNativeDriver: true
      }
    ).start(() => {
      this.setState({
        showPinCodeEntry: false,
        errorMessage: "",
        scanned: false
      }, () => {
        if(forced)
          this._showToast('Transaction', 'La transaction a bien été annulée.', false);
      });

    });
  }

  _refreshDeliveryWay = (deliveryWay, deliveryFee) => {
    this.setState({
      deliveryWay: deliveryWay,
      deliveryFee: deliveryFee
    });
  }

  _refreshDeliveryAddress = (deliveryAddress) => {
    console.log(deliveryAddress);
    this.setState({
      deliveryAddress: deliveryAddress,
    });
  }

  _refreshPaymentWay = (paymentWay, extra) => {
    console.log(extra);
    if(paymentWay === "SAVED-CARD") {
      this.setState({
        paymentWay: paymentWay,
        savedCard: extra
      });
    }
    else {
      this.setState({
        paymentWay: paymentWay,
        walletAmount: extra,
      });
    }
  }

  _displayDeliveryWay() {
    const shopId = this.shoppingBasketCode.split("-")[1];

    this.props.navigation.navigate('DeliveryWay', {
      token: this.token,
      shopId: shopId,
      deliveryWay: this.state.deliveryWay,
      refreshDeliveryWay: this._refreshDeliveryWay
    });
  }

  _displayDeliveryAddress() {
    const shopId = this.shoppingBasketCode.split("-")[1];

    this.props.navigation.navigate('DeliveryAddress', {
      token: this.token,
      shopId: shopId,
      deliveryWay: this.state.deliveryWay,
      deliveryAddress: this.state.deliveryAddress,
      refreshDeliveryAddress: this._refreshDeliveryAddress
    });
  }

  _displayPaymentWay() {
    this.props.navigation.navigate('PaymentWay', {
      token: this.token,
      deliveryWay: this.state.deliveryWay,
      totalAmount: this.state.shoppingBasket.totalAmount + this.state.deliveryFee,
      paymentWay: this.state.paymentWay,
      selectedSavedCard: this.state.savedCard,
      refreshPaymentWay: this._refreshPaymentWay
    });
  }

  _displayDeliveryWayInfo() {
    var deliveryWayTitle, deliveryWayDescription;;
    switch (this.state.deliveryWay) {
      case "CLICK & COLLECT":
        deliveryWayTitle = "Click & Collect"
        deliveryWayDescription = "Je récupère ma commande en boutique"
        break;
      case "HOME":
        deliveryWayTitle = "Livraison à domicile"
        deliveryWayDescription = "Je reçois ma commande à domicile"
        break;
      default:
        return (
          <View style={styles.infoChooseContainer}>
            <Text style={styles.btnChooseNoSelect}>{"Veuillez sélectionner votre mode de livraison"}</Text>
          </View>
        );
    }
    return (
      <View style={styles.infoChooseContainer}>
        <Text style={styles.btnChooseTitle}>{deliveryWayTitle}</Text>
        <Text style={styles.btnChooseDescription}>{deliveryWayDescription}</Text>
      </View>
    );
  }

  _displayDeliveryAddressInfo() {
    var {deliveryWay} = this.state;
    var everythingIsSelected = deliveryWay;
    var deliveryAddressDescription, deliveryAddressNoSelect;
    var disabledColor = '#D9D8D9';

    switch (this.state.deliveryWay) {
      case "CLICK & COLLECT":
        deliveryAddressDescription = "ADRESSE DE RÉCUPÉRATION";
        deliveryAddressNoSelect = "Veuillez sélectionner l'adresse du commerce";
        break;
      case "HOME":
        deliveryAddressDescription = "ADRESSE DE LIVRAISON";
        deliveryAddressNoSelect = "Veuillez sélectionner l'adresse de livraison";
        break;
      default:
        deliveryAddressDescription = "ADRESSE DE LIVRAISON";
        deliveryAddressNoSelect = "Veuillez sélectionner l'adresse de livraison";
    }

    return (
      <View style={styles.chooseContainer}>
        <Text style={styles.chooseDescriptionText}>{deliveryAddressDescription}</Text>
        <TouchableOpacity
          style={[styles.btnChoose]}
          disabled={!everythingIsSelected}
          onPress={() => this._displayDeliveryAddress()}
        >
          <View style={[styles.chooseIconContainer, {backgroundColor: everythingIsSelected ? '#674171' : disabledColor}]}>
            <MaterialIcons
              name={"location-on"}
              style={[styles.chooseIcon, {paddingLeft: 0, color: everythingIsSelected ? '#F8CE3E' : 'grey'}]}
            />
          </View>
          {this.state.deliveryAddress ?
          <View style={styles.infoChooseContainer}>
            <Text numberOfLines={2} style={[styles.btnChooseTitle, {color: everythingIsSelected ? '#674171' : disabledColor}]}>{this.state.deliveryAddress.address}</Text>
            <Text numberOfLines={1} style={[styles.btnChooseDescription, {color: everythingIsSelected ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{this.state.deliveryAddress.zipcode + ", " + this.state.deliveryAddress.city}</Text>
          </View> :
          <View style={styles.infoChooseContainer}>
            <Text style={[styles.btnChooseNoSelect, {color: everythingIsSelected ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{deliveryAddressNoSelect}</Text>
          </View>
          }
          <View style={styles.chooseArrowContainer}>
            <Ionicons
              name="ios-arrow-forward"
              style={[styles.chooseArrow, {color: everythingIsSelected ? 'rgba(103, 65, 113, 0.8)' : disabledColor}]}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  _displayPaymentWayInfo() {
    var {deliveryWay, deliveryAddress} = this.state;
    var everythingIsSelected = deliveryWay && deliveryAddress;
    var paymentWayTitle, paymentWayDescription;
    var disabledColor = '#D9D8D9';

    switch (this.state.paymentWay) {
      case "E-WALLET":
        paymentWayTitle = "Porte-monnaie euros";
        paymentWayDescription = "Votre solde est de : " + this.state.walletAmount.toFixed(2) + " €";
        break;
      case "B-WALLET":
        paymentWayTitle = "Porte-monnaie bartcoins";
        paymentWayDescription = "Votre solde est de : " + this.state.walletAmount.toFixed(2) + " bartcoin(s)";
        break;
      case "NEW-CARD":
        paymentWayTitle = "Nouvelle carte";
        paymentWayDescription = "Utilisez une carte que vous n'avez pas enregistrée pour cet achat";
        break;
      case "SAVED-CARD":
        paymentWayTitle = "Carte enregistrée";
        paymentWayDescription = this.state.savedCard.code.replace(/X/g, "•") + "\n" + this.state.savedCard.expiration;
        break;
      default:
    }

    return (
      <View style={styles.chooseContainer}>
        <Text style={styles.chooseDescriptionText}>{"MOYEN DE PAIEMENT"}</Text>
        <TouchableOpacity
          style={[styles.btnChoose]}
          disabled={!everythingIsSelected}
          onPress={() => this._displayPaymentWay()}
        >
          <View style={[styles.chooseIconContainer, {backgroundColor: everythingIsSelected ? '#674171' : disabledColor}]}>
            <FontAwesome5
              name={"money-bill"}
              style={[styles.chooseIcon, {paddingTop: 2, paddingLeft: 1, fontSize: 23, color: everythingIsSelected ? '#F8CE3E' : 'grey'}]}
            />
          </View>
          {this.state.paymentWay ?
          <View style={styles.infoChooseContainer}>
            <Text style={[styles.btnChooseTitle, {color: everythingIsSelected ? '#674171' : disabledColor}]}>{paymentWayTitle}</Text>
            <Text style={[styles.btnChooseDescription, {color: everythingIsSelected ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{paymentWayDescription}</Text>
          </View> :
          <View style={styles.infoChooseContainer}>
            <Text style={[styles.btnChooseNoSelect, {color: everythingIsSelected ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{"Veuillez sélectionner votre mode de paiement"}</Text>
          </View>
          }
          <View style={styles.chooseArrowContainer}>
            <Ionicons
              name="ios-arrow-forward"
              style={[styles.chooseArrow, {color: everythingIsSelected ? 'rgba(103, 65, 113, 0.8)' : disabledColor}]}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  _displayBuyButton() {
    var {deliveryWay, deliveryAddress, paymentWay, paymentLoading} = this.state;
    var condition = deliveryWay && deliveryAddress && paymentWay && (this.state.userProfile != null) && !paymentLoading;
    return (
      <TouchableOpacity
        style={[styles.btnNext, {backgroundColor: condition ? '#F8CE3E' : '#D9D8D9'}]}
        disabled={!condition}
        onPress={() => this._buy()}
      >
        <Text style={styles.textBtnNext}>{"Acheter"}</Text>
      </TouchableOpacity>
    );
  }

  componentDidMount() {
    this._loadProfile();
  }

  render() {
    return (
      <Root>
        <View style={{flex: 1, backgroundColor: '#674171'}}>
          <View style={styles.topHeadband}>
            <Text  style={styles.topTitle}>
                {"Ma commande"}
            </Text>
            <TouchableOpacity
              style={{position: 'absolute', bottom: 14, right: 8}}
              onPress={() => this._clearShoppingBasket()}
            >
              {/*<FontAwesome
                name="trash-o"
                style={{fontSize: 28, paddingRight: 0, color: 'white'}}
              />*/}
              {/*<Feather
                name="trash-2"
                style={{fontSize: 28, paddingRight: 0, color: 'white'}}
              />*/}

            </TouchableOpacity>
          </View>
            <View style={styles.mainContainer}>
              <ScrollView
                style={styles.listContainer}
                contentContainerStyle={{paddingBottom: '65%'}}
              >
                <View style={[styles.chooseContainer, {marginTop: 4}]}>
                  <Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>
                  <TouchableOpacity
                    style={styles.btnChoose}
                    onPress={() => this._displayDeliveryWay()}
                  >
                    <View style={styles.chooseIconContainer}>
                      <MaterialCommunityIcons
                        name={"truck-delivery"}
                        style={[styles.chooseIcon, {paddingLeft: 1}]}
                      />
                    </View>
                      {this._displayDeliveryWayInfo()}
                    <View style={styles.chooseArrowContainer}>
                      <Ionicons
                        name="ios-arrow-forward"
                        style={styles.chooseArrow}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
                {this._displayDeliveryAddressInfo()}
                {this._displayPaymentWayInfo()}
              </ScrollView>

              <BlurView intensity={Platform.OS === 'ios' ? 96 : 220} style={styles.bottomContainer}>
                <View style={styles.bottomSubContainer}>
                {this._displayArticleCount()}
                <View style={styles.shoppingBasketInfo}>
                  <View style={styles.amountContainer}>
                    <Text style={styles.amountTitle}>{"Montant du panier (TTC) :"}</Text>
                    <Text style={styles.amountValue}>{this.state.shoppingBasket.totalAmount.toFixed(2) + " €"}</Text>
                  </View>
                  <View style={styles.amountContainer}>
                    <Text style={styles.amountTitle}>{"Frais de livraison (TTC) :"}</Text>
                    <Text style={styles.amountValue}>{this.state.deliveryFee.toFixed(2) + " €"}</Text>
                  </View>
                  <View style={styles.amountContainer}>
                    <Text style={styles.amountTitle}>{"TOTAL (TTC) :"}</Text>
                    <Text style={styles.amountValue}>{(this.state.shoppingBasket.totalAmount + this.state.deliveryFee).toFixed(2) + " €"}</Text>
                  </View>
                </View>
                {this._displayBuyButton()}
              </View>
            </BlurView>
          </View>
        </View>
        {this.state.showPinCodeEntry ?
          <Animated.View elevation={5} style={[styles.paymentContainer, {top: this.pinCodeTopValue}]}>
            <TouchableOpacity
              style={styles.paymentClose}
              activeOpacity={0.9}
              onPress={() => this._hidePinCodeEntry(true)}
            >
              <Ionicons
                name={"ios-close"}
                style={{color: '#F8CE3E', fontSize: 35}}
              />
            </TouchableOpacity>
            <Text style={styles.paymentTitle}>{"Paiement"}</Text>
            <View>
              <Text style={styles.paymentBottomText}>
              {"Entrez votre code pin pour effectuer votre achat :"}
              </Text>
              <View style={styles.codePinInputBackground}>
                <TextInput
                    ref={ref => this.codePinInput = ref}
                    style={styles.codePinInput}
                    autoFocus={true}
                    secureTextEntry={true}
                    maxLength={4}
                    placeholder="Code pin"
                    fontSize={18}
                    textAlign='center'
                    placeholderTextColor={'rgb(100, 95, 107)'}
                    selectionColor={'#674171'}
                    keyboardType={'numeric'}
                    returnKeyType={'done'}
                    onChangeText={(pinCode) => {
                      this.pinCode = pinCode;
                    }}
                />
              </View>
            </View>
            {this.state.errorMessage !== "" ? <View style={styles.errorContainer}>
            <View style={styles.errorTopPart}>
              <Ionicons
                name="ios-warning"
                style={styles.errorIcon}
              />
              <Text style={styles.mainTxtError}>{this.state.errorMessage.split("$")[0]}</Text>
            </View>
              {/*<Text style={styles.subTxtError}>{this.state.errorMessage.split("$")[1]}</Text>*/}
            </View> : null}
            <View style={styles.actionsContainer}>
              <TouchableOpacity
                style={[styles.btn, {backgroundColor: '#674171'}]}
                onPress={() => this._verificationBeforeOrderRegister()}
              >
                <Text style={styles.btnText}>{"Payer"}</Text>
              </TouchableOpacity>
            </View>
          </Animated.View>
        : null}
        {this.state.paymentLoading ?
        <View style={styles.loadingContainer}>
          <ActivityIndicator size='large' color='#674171' />
        </View> : null}
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171'
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    overflow: 'hidden'
  },
  listContainer: {
    width: '100%',
    //flex: 1,
    //paddingTop: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    paddingHorizontal: 10,
    paddingVertical: 15
  },
  loadingContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:22,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  bottomContainer: {
    width: '100%',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    //flexDirection: 'row',
    //justifyContent: 'space-around',
    //alignItems: 'center',
    //paddingHorizontal: 10,
    //paddingTop: 5,
    //paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    zIndex: 1
  },
  bottomSubContainer: {
    flex: 1,
    backgroundColor: 'rgba(109, 72, 119, 0.45)',
    justifyContent: 'space-around',
    //alignItems: 'center',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    zIndex: 2
  },
  btnNext: {
    backgroundColor: '#F8CE3E',
    width: 200,
    height: 45,
    alignSelf: 'center',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    marginBottom: 5,
    paddingHorizontal: 1
  },
  textBtnNext: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  shoppingBasketInfo: {
    flex: 0.85,
    justifyContent: 'center'
  },
  amountContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  amountTitle: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: 'bold',
    paddingRight: 8
  },
  amountValue: {
    color: '#F8CE3E',
    fontSize: 19,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  amountVAT: {
    color: '#F8CE3E',
    fontSize: 12,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  articleCountContainer: {
    width: 110,
    height: 28,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -2,
    backgroundColor: '#674171',
    borderRadius: 30,
    paddingHorizontal: 4,
    marginTop: 4,
    marginBottom: 6
  },
  articleCountValue: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
    paddingRight: 6
  },
  articleCountDescription: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
    fontWeight: '500',
  },
  chooseContainer: {
    width: '99%',
    alignSelf: 'center',
    marginTop: 30
  },
  chooseDescriptionText: {
    color: '#674171',
    fontSize: 15,
    fontWeight: '600',
    marginBottom: 8,
    paddingLeft: 20
  },
  btnChoose: {
    width: '100%',
    height: 90,
    flexDirection: 'row',
    alignSelf: 'center',

    //backgroundColor: '#674171',
    backgroundColor: 'white',

    borderRadius: 20,
    paddingVertical: 12,
    paddingHorizontal: 14,

    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.4
  },
  chooseIconContainer: {
    flex: 0.18,
    alignSelf: 'center',
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: 'rgba(249, 207, 62, 1)',
    backgroundColor: '#674171',
    //marginVertical: -12,
    marginLeft: -14.2,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
  },
  chooseIcon: {
    color: 'rgba(249, 207, 62, 1)',
    //color: 'rgba(255, 255, 255, 0.65)',
    //color: '#674171',
    //color: 'white',

    fontSize: 30
  },
  chooseArrowContainer: {
    flex: 0.1,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  chooseArrow: {
    //color: 'rgba(249, 207, 62, 0.85)',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.8)',
    fontSize: 30
  },
  infoChooseContainer: {
    flex: 0.72,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    marginLeft: 10
  },
  btnChooseTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    //color: 'white',
    color: '#674171',
  },
  btnChooseDescription: {
    fontSize: 15,
    fontWeight: '400',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.7)',
  },
  btnChooseNoSelect: {
    fontSize: 15,
    fontWeight: '400',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.7)',
  },
  paymentContainer: {
    position: 'absolute',
    width: '85%',
    //height: '65%',
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 15,
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.7
  },
  paymentTitle: {
    textAlign: 'center',
    fontSize: 35,
    fontWeight: 'bold',
    fontFamily: "SciFly",
    color: '#674171',
    marginBottom: 8
  },
  paymentBottomText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '500',
    fontFamily: "SciFly",
    color: '#674171',
    marginTop: 60,
  },
  actionsContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20
  },
  btn: {
    width: 125,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  btnText: {
    color: 'white',
    fontSize: 19,
    fontFamily: 'SciFly',
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  paymentClose: {
    position: 'absolute',
    top: 3,
    right: 3,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  codePinInputBackground: {
    width:'60%',
    //backgroundColor:'rgba(255, 255, 255, 0.4)',
    backgroundColor: '#D9D8D9',
    borderRadius: 8,
    height: 50,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  codePinInput: {
    flex: 1,
    color:'black',
    paddingHorizontal: 10,
    width: 100,
    textAlign: 'center',
    alignSelf: 'center',
  },
  errorContainer: {
    flex: 1,
    //flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  errorTopPart: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  mainTxtError: {
    fontSize: 14,
    fontWeight: '500',
    color: '#FF453A',
    marginLeft: 10,
    textAlign: 'center'
  },
  subTxtError: {
    fontSize: 13,
    fontWeight: '500',
    color: '#FF453A',
    textAlign: 'center'
  }
});

export default PlaceOrder;
