import React, { useState, useEffect } from 'react';
import { Animated, Alert, Text, TextInput, View, StyleSheet, Button, TouchableOpacity, Easing } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import {sendPaymentFromApiWithInfos} from '../API/YSMApi';

import Root from './RootPopup';
import Toast from './Toast';
import { BlurView } from 'expo-blur';

class Scan extends React.Component {
  constructor(props) {
    super(props);
    this.token = this.props.navigation.getParam('token');
    this.profileData = this.props.navigation.getParam('profileData');
    this.pinCodeTopValue = new Animated.Value(-500);
    this.animatedQRValue = new Animated.Value(250);
    this.pinCode = "";

    this.state = {
      scanned: false,
      data: "",
      hasPermission: null,
      type: BarCodeScanner.Constants.Type.back,
      transaction: null,
      showPinCodeEntry: false,
      errorMessage: ""
    }
  }

  async componentDidMount() {
    //this._runQRAnimation();
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasPermission: status === 'granted' });
  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
          title: title,
          text: message,
          color: '#2ecc71',
          icon:(<Ionicons
              name="ios-checkmark"
              style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
            />)
      });
    }
    else {
      Toast.show({
          title: title,
          text: message,
          color: '#FF453A',
          icon:(<Ionicons
              name="ios-close"
              style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
            />)
      });
    }
  }

  _showPinCodeEntry() {
    Animated.parallel (
      [
        Animated.timing(
          this.pinCodeTopValue,
          {
            toValue: 100,
            duration: 500,
            easing: Easing.out(Easing.exp)
          }
        ),
      ],
      {
        useNativeDriver: true
      }
    ).start(() => {
      this.setState({showPinCodeEntry: true});
    });
  }

  _hidePinCodeEntry(forced) {
    Animated.parallel (
      [
        Animated.timing(
          this.pinCodeTopValue,
          {
            toValue: -500,
            duration: 500,
            easing: Easing.out(Easing.exp)
          }
        ),
      ],
      {
        useNativeDriver: true
      }
    ).start(() => {
      this.setState({
        showPinCodeEntry: false,
        errorMessage: "",
        scanned: false
      }, () => {
        if(forced)
          this._showToast('Transaction', 'La transaction a bien été annulée.', false);
      });

    });
  }

  _runQRAnimation(reverse) {
    //this.animatedQRValue.setValue(300);
    if(reverse) {
      Animated.parallel (
        [
          Animated.timing(this.animatedQRValue, {
            toValue: 250,
            duration: 2000,
            delay: 50,
            //easing: Easing.linear,
          })
        ],
        {
          useNativeDriver: true
        }
      ).start(() => this._runQRAnimation(false));
    }
    else {
      Animated.parallel (
        [
          Animated.timing(this.animatedQRValue, {
            toValue: 200,
            duration: 2000,
            delay: 50,
            //easing: Easing.linear,
          })
        ],
        {
          useNativeDriver: true
        }
      ).start(() => this._runQRAnimation(true));
    }
  }


  _sendPayment(transactionInfos) {
    if(this.profileData != null && this.pinCode === this.profileData.pincode) {
      this._hidePinCodeEntry(false);
      const regex = new RegExp('error', 'i');
      sendPaymentFromApiWithInfos(this.token, transactionInfos).then(data => {
        if(data != null && !regex.test(data)) {
          this.setState({scanned: false});
          this._showToast('Transaction', 'La transaction a bien été effectuée.', true);
          //this.props.navigation.goBack();
        }
        else {
          this.setState({scanned: false});
          this._showToast('Erreur', 'Un problème est survenu lors de la transaction, veuillez réessayer ultérieurement.', false);

          this.props.navigation.navigate('SuccessPurchase', {token: this.token});
        }
      });
    }
    else {
      this.setState({
        errorMessage: "Le code pin n'est pas valide." /*$Rappel : 4 chiffres*/
      });
    }
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({
      scanned:true,
      transaction: data
    }, () => {
      var jsonTransaction;

      try {
        jsonTransaction = JSON.parse(this.state.transaction);
      } catch (error) {
        this.setState({scanned: false});
        this._showToast('Erreur', 'Le QR-code scanné n\'est pas valide.', false);
        return;
      }

      if(jsonTransaction != null && jsonTransaction.merchantWalletId != null) {
        /*Alert.alert(
          "Paiement",
          "Êtes vous sur de vouloir effectuer cette transaction ?",
          [
            {
              text: "Non",
              onPress: () => {
                Toast.show({
                    title: 'Transaction',
                    text: 'La transaction a bien été annulée.',
                    color: '#FF453A',
                    icon:(<Ionicons
                        name="ios-close"
                        style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
                      />)
                });
              }
            },
            {
              text: "Oui",
              onPress: () => this._sendPayment(jsonTransaction)
            }
          ],
          {
            cancelable: false
          }
        );*/

        this._showPinCodeEntry();
      }
      else {
        this.setState({scanned: false}, () => {
          this._showToast('Erreur', 'Le QR-code scanné n\'est pas valide.', false);
        });
      }
    });
    //alert(`Bar code with type ${type} and data ${data} has been scanned!`);


  };


  render(){
    const { hasPermission } = this.state
    if (hasPermission === null) {
      return <View />;
    } else if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <Root>
          <View
            style={{
              flex: 1,
              backgroundColor: '#674171'
            }}>
            <View style={styles.topHeadband}>
              <BlurView style={{width: '100%', height: '100%'}} tint='dark' intensity={Platform.OS === 'ios' ? 96 : 220}>

              </BlurView>
            </View>
            <BarCodeScanner
              onBarCodeScanned={this.state.scanned ? undefined : this.handleBarCodeScanned}
              style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center'
              }}

            >

            {/*this.state.scanned ?
              (
                <Ionicons
                name="ios-qr-scanner"
                style={{ color: "rgba(103, 65, 113, 0.8)", fontSize: 300}}
                />
              ) :
              (
                <Ionicons
                name="ios-qr-scanner"
                style={{ color: "rgba(255, 255, 255, 0.6)", fontSize: 300}}
                />
              )
            */}
            <Animated.View style={{height: this.animatedQRValue, width: this.animatedQRValue, alignSelf: 'center', /*backgroundColor: 'red',*/ justifyContent: 'center', alignItems: 'center'}}>
            {this.state.scanned ?
              (
                <Ionicons
                name="ios-qr-scanner"
                style={{ color: "rgba(103, 65, 113, 0.8)", fontSize: 300, alignSelf: 'center', marginTop: -20}}
                />
              ) :
              (
                <Ionicons
                name="ios-qr-scanner"
                style={{ color: "rgba(255, 255, 255, 0.6)", fontSize: 300, marginTop: -20}}
                />
              )
            }
            </Animated.View>

            {/*<View style={this.state.scanned ? [styles.scanArea, {borderColor: "rgba(103, 65, 113, 0.8)"}] : styles.scanArea}>
            </View>*/}

            {/*this.state.scanned && <Button title={'Appuyez pour numériser à nouveau'} onPress={() => {this.setState({scanned:false, data:""})}} />*/}

            {this.state.showPinCodeEntry ?
              <Animated.View elevation={5} style={[styles.paymentContainer, {top: this.pinCodeTopValue}]}>
                <TouchableOpacity
                  style={styles.paymentClose}
                  activeOpacity={0.9}
                  onPress={() => this._hidePinCodeEntry(true)}
                >
                  <Ionicons
                    name={"ios-close"}
                    style={{color: '#F8CE3E', fontSize: 35}}
                  />
                </TouchableOpacity>
                <Text style={styles.paymentTitle}>{"Paiement"}</Text>
                <View>
                  <Text style={styles.paymentBottomText}>
                  {"Entrez votre code pin pour effectuer cette transaction :"}
                  </Text>
                  <View style={styles.codePinInputBackground}>
                    <TextInput
                        ref={ref => this.codePinInput = ref}
                        style={styles.codePinInput}
                        autoFocus={true}
                        secureTextEntry={true}
                        maxLength={4}
                        placeholder="Code pin"
                        fontSize={18}
                        textAlign='center'
                        placeholderTextColor={'rgb(100, 95, 107)'}
                        selectionColor={'#674171'}
                        keyboardType={'numeric'}
                        returnKeyType={'done'}
                        onChangeText={(pinCode) => {
                          this.pinCode = pinCode;
                        }}
                    />
                  </View>
                </View>
                {this.state.errorMessage !== "" ? <View style={styles.errorContainer}>
                <View style={styles.errorTopPart}>
                  <Ionicons
                    name="ios-warning"
                    style={styles.errorIcon}
                  />
                  <Text style={styles.mainTxtError}>{this.state.errorMessage.split("$")[0]}</Text>
                </View>
                  {/*<Text style={styles.subTxtError}>{this.state.errorMessage.split("$")[1]}</Text>*/}
                </View> : null}
                <View style={styles.actionsContainer}>
                  <TouchableOpacity
                    style={[styles.btn, {backgroundColor: '#674171'}]}
                    onPress={() => this._sendPayment(JSON.parse(this.state.transaction))}
                  >
                    <Text style={styles.btnText}>{"Payer"}</Text>
                  </TouchableOpacity>
                </View>
              </Animated.View>
            : null}

            {/*<Text style={styles.result}>{this.state.debug}</Text>*/}
            </BarCodeScanner>
          </View>
        </Root>
      );
    }
  }
}

const styles = StyleSheet.create({
  topHeadband: {
    position: 'absolute',
    width: '100%',
    height: 90,
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    right: 0,
    zIndex: 2,
    backgroundColor: 'rgba(103, 65, 113, 0.1)'
    //borderRadius: 10,
  },
  result: {
    color: 'white',
    fontSize: 20,
    marginTop: 20
  },
  scanArea: {
    borderRadius: 15,
    borderWidth: 4,
    borderColor: "rgba(255, 255, 255, 0.7)",
    borderStyle: 'dotted',
    width: 300,
    height: 300
  },
  paymentContainer: {
    position: 'absolute',
    width: '85%',
    //height: '65%',
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 20,
    paddingVertical: 20,
    paddingHorizontal: 15,
    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.7
  },
  paymentTitle: {
    textAlign: 'center',
    fontSize: 35,
    fontWeight: 'bold',
    fontFamily: "SciFly",
    color: '#674171',
    marginBottom: 8
  },
  paymentBottomText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: '500',
    fontFamily: "SciFly",
    color: '#674171',
    marginTop: 60,
  },
  actionsContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20
  },
  btn: {
    width: 125,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  btnText: {
    color: 'white',
    fontSize: 19,
    fontFamily: 'SciFly',
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  paymentClose: {
    position: 'absolute',
    top: 3,
    right: 3,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  codePinInputBackground: {
    width:'60%',
    //backgroundColor:'rgba(255, 255, 255, 0.4)',
    backgroundColor: '#D9D8D9',
    borderRadius: 8,
    height: 50,
    marginTop: 30,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  codePinInput: {
    flex: 1,
    color:'black',
    paddingHorizontal: 10,
    width: 100,
    textAlign: 'center',
    alignSelf: 'center',
  },
  errorContainer: {
    flex: 1,
    //flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  errorTopPart: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  mainTxtError: {
    fontSize: 14,
    fontWeight: '500',
    color: '#FF453A',
    marginLeft: 10,
    textAlign: 'center'
  },
  subTxtError: {
    fontSize: 13,
    fontWeight: '500',
    color: '#FF453A',
    textAlign: 'center'
  },
});

export default Scan;
