import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

class CategorieItem extends React.Component {

  render() {
    const {category, displayShops} = this.props;
    return (
      <TouchableOpacity
      style={styles.main_container}
      onPress={() => displayShops(category)}
      >
        <View style={{flex: 0.2, alignItems: 'center', justifyContent: 'center'}}>
        <Image
          style={styles.image}
          source={require('../assets/AndroidIcon.png')}
        />
        </View>
        <View style={styles.infosContainer}>
          <Text style={styles.title}>{category.name}</Text>
          {/*<Text style={styles.numberOfShop}>{category.nbShop + " commerces"}</Text>*/}
        </View>
        <View style={{flex: 0.1, alignItems: 'center', justifyContent: 'center'}}>
          <Ionicons
            name="ios-arrow-forward"
            style={{ color: "rgba(103, 65, 113, 0.8)", fontSize: 30}}
          />
        </View>
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 70,
    marginTop: 5,
    paddingTop: 3,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 15,
    backgroundColor: 'white'
  },
  infosContainer: {
    flex: 0.92,
    justifyContent: 'space-around',
  },
  title: {
    color: '#674171',
    fontSize: 20,
    fontWeight: 'bold'
  },
  numberOfShop: {
    color: 'grey',
    fontSize: 16,
    opacity: 0.6
  },
  image: {
    width: 30,
    height: 30,
  }
});

export default CategorieItem;
