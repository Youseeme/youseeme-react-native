import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Moment from 'react-moment';
import 'moment-timezone';

class OrderItem extends React.Component {

  _displayStatus(status) {
    var color = "#674171", code = status != null ? status.code : "";
    switch (code) {
      case "NEW":
        color = "#007AFF";
        break;
      case "PENDING":
        color = "#FF9500";
        break;
      case "DELIVERING":
        color = "#34C759";
        break;
      case "FINISHED":
        color = "#34C759";
        break;
      case "CANCELED":
        color = "#FF3B30";
        break;
      default:
        return (
          <Text style={[styles.status, {color: "#34C759"}]}>{"TERMINÉE"}</Text>
        );
    }
    return (
      <Text style={[styles.status, {color: color}]}>{status.name.toUpperCase()}</Text>
    );
  }

  render() {
    const {order, displayOrderDetails} = this.props;
    const date = new Date(order.created_at);

    return (
      <TouchableOpacity
        style={styles.main_container}
        onPress={() => displayOrderDetails(order.id)}
      >
        <View style={styles.orderInfos}>
          <Text style={styles.orderName}>{"Commande n°" + order.id}</Text>
          <Moment style={styles.date} element={Text} interval={0} tz="Europe/Paris" format="\L\e DD/MM/YYYY à HH:mm:ss">
            {order.updated_at.substring(0, 10) + " " + order.updated_at.substring(11, 16) + ":" + order.updated_at.substring(17, 19)}
          </Moment>
        </View>
        <View style={styles.orderRightContainer}>
          <Text style={styles.amount}>{order.amount != null ? order.amount.toFixed(2) + " €" : "0.00 €"}</Text>
          {this._displayStatus(order.status)}
        </View>
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'row',
    height: 60,
    marginTop: 5,
    paddingTop: 3,
    paddingBottom: 5,
    paddingHorizontal: 2,
    marginHorizontal: 10,
    borderBottomColor: 'rgba(161, 161, 161, 0.8)',
    borderBottomWidth: 1,
  },
  orderInfos: {
    flex: 2.5,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  orderRightContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  orderName: {
    fontWeight: '600',
    fontSize: 16,
  },
  date: {
    color: '#674171'
  },
  amount: {
    fontSize: 18,
    fontWeight: '500',
    color: '#674171'
  },
  status: {
    color: '#34C759',
    fontSize: 10,
    fontWeight: '500',
    //fontFamily: 'SciFly'
  }
});

export default OrderItem;
