import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Moment from 'react-moment';
import 'moment-timezone';

class AddressItem extends React.Component {

  render() {
    const {address, addAddress, removeAddress, navigateToNewAddressPage} = this.props;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.addressNameContainer}>
          <Text numberOfLines={1} style={styles.addressName}>{address.name}</Text>
        </View>
        <View style={styles.addressContainer}>
          <View style={styles.addressInfos}>
            <View style={styles.textAddressContainer}>
              <Text numberOfLines={2} style={styles.addressPart1}>{address.address + ","}</Text>
              <Text numberOfLines={2} style={styles.addressPart2}>{address.zipcode + ", " + address.city}</Text>
            </View>
            {/*<Text style={styles.date}>{"Modifé le : " + address.updated_at}</Text>*/}
            <Moment style={styles.date} element={Text} interval={0} tz="Europe/Paris" format="\Mo\difié \l\e DD/MM/YYYY à HH:mm:ss">
              {address.updated_at.substring(0, 10) + " " + address.updated_at.substring(11, 16) + ":" + address.updated_at.substring(17, 19)}
            </Moment>
          </View>
          <View style={styles.addressAction}>
            <TouchableOpacity
              style={styles.btnEdit}
              onPress={() => navigateToNewAddressPage("Modifier", address)}
            >
              <Text style={styles.textEdit}>{"Modifier"}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnDelete}
              onPress= {() => removeAddress(address.id)}
            >
              <Text style={styles.textDelete}>{"Supprimer"}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: 150,
    marginTop: 5,
    paddingTop: 3,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 10,
    backgroundColor: 'white'
  },
  addressNameContainer: {
    flex: 0.2,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'grey'
  },
  addressContainer: {
    flex: 0.8,
    paddingTop: 3,
    flexDirection: 'row',
  },
  addressInfos: {
    flex: 0.7,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  addressName: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center'
  },
  textAddressContainer: {
    flex: 0.9,
    alignItems: 'flex-start',
    justifyContent: 'center',
    //backgroundColor: 'red'
  },
  addressPart1: {
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#674171',
    fontSize: 16,
    paddingVertical: 3
  },
  addressPart2: {
    textAlign: 'left',
    color: 'grey',
    paddingVertical: 3
  },
  date: {
    color:'#674171',
    fontSize: 12,
  },
  addressAction: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnEdit: {
    //borderWidth: 1,
    //borderColor: '#007AFF',
    backgroundColor: 'rgba(103, 65, 113, 0.8)',
    width: 90,
    height: 32,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  },
  btnDelete: {
    //borderWidth: 1,
    //borderColor: '#FF453A',
    backgroundColor: '#FF453A',
    width: 90,
    height: 32,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5
  },
  textEdit: {
    color: 'white',
    fontSize: 13,
    textAlign: 'center',
    fontWeight: '600',
  },
  textDelete: {
    color: 'white',
    fontSize: 13,
    textAlign: 'center',
    fontWeight: '600',
  }
});

export default AddressItem;
