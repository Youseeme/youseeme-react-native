import React from 'react';
import { Alert, StyleSheet, View, ImageBackground, TouchableOpacity,Button, Image, Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView, ActivityIndicator, Easing, Platform, KeyboardAvoidingView, Clipboard, Linking} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import {getUserProfileFromApiWithAccountToken, getUserBalanceFromApiWithAccountToken, getWalletInfoFromApiWithToken, sendReloadFromApiWithInfos, logReloadFromApiWithAmount} from '../API/YSMApi';
import { LinearGradient } from 'expo-linear-gradient';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import Root from './RootPopup';
import Toast from './Toast';
import AnimatedLoader from "react-native-animated-loader";

const IBAN_YOUSEEME = "FR76 1123 8000 0100 3137 9370 378";
const BIC_YOUSEEME = "SWILFRPPXXX";

const { width, height } = Dimensions.get('screen')

export default class Reload extends React.Component{
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf')
    });
    this.token = this.props.navigation.getParam('token');
    this.profileData = this.props.navigation.getParam('profileData');
    console.log("----");
    console.log(this.profileData);

    this.reloadAmount = "0.00";
    this.currencyTypes = [{key: 'EUR', name: 'EUR'}];

    //console.log(this.profileData);

    this.state = {
        isLoading: true,
        balance:'',
        currencyType: 'EUR',
        bankInfo: {},
        errorMessage: '',
        memorizeCard: false
    };
  }


  _loadProfile = () => {
    getUserBalanceFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        balance: data,
        errorMessage: '',
        isLoading: false
      });
    });
  }

  _getBankInfo() {
    getWalletInfoFromApiWithToken(this.token).then(data => {
      console.log(data)
      this.setState({
        bankInfo: data
      })
    });
  }

  _displayNoResultsComponent(message) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red'}}>
        <Text style={{color: '#674171'}}>{message}</Text>
      </View>
    );
  }

  _confirmationRequest(title, message, titleCancel, messageCancel, action) {
    Alert.alert(
      title,
      message,
      [
        {
          text: "Non",
          onPress: () => {
            this._loadProfile();
            this._showToast(titleCancel, messageCancel, false);
          }
        },
        {
          text: "Oui",
          onPress: () => action()
        }
      ],
      {
        cancelable: false
      }
    );
  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
          title: title,
          text: message,
          color: '#2ecc71',
          icon:(<Ionicons
              name="ios-checkmark"
              style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
            />)
      });
    }
    else {
      Toast.show({
          title: title,
          text: message,
          color: '#FF453A',
          icon:(<Ionicons
              name="ios-close"
              style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
            />)
      });
    }
  }

  _copyToClipboard(text, feedbackMessage) {
    Clipboard.setString(text);
    Toast.show({
        title: 'Presse-papier',
        text: feedbackMessage,
        color: '#2ecc71',
        icon:(<Ionicons
            name="ios-checkmark"
            style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
          />)
    });
  }

  _finishReload = (response) => {
    this._loadProfile();
    if(response === "success") {
      var amount = parseFloat(this.reloadAmount.replace(/,/g,".")).toFixed(2);
      const regex = new RegExp('Debit completed', 'i');
      logReloadFromApiWithAmount(this.token, amount).then(data => {
        if(regex.test(data)) {
          this._showToast('Paiement terminé', 'Le rechargement à bien été effectué', true);
          this.reloadAmountEntry.clear();
        }
        else
          this._showToast('Un problème est survenu lors du rechargement, veuillez réessayer ultérieurement.', false);
      });
    }
    else if(response === "cancel")
      this._showToast('Paiement annulé', 'Le rechargement a bien été annulé.', false);
    else
      this._showToast('Un problème est survenu lors du rechargement, veuillez réessayer ultérieurement.', false);
  }

  _currentDate() {
    const today = new Date();
    let mm = today.getMinutes();
    let hh = today.getHours();
    let dd = today.getDate();
    let mmmm = today.getMonth()+1;
    const yyyy = today.getFullYear();

    if(mm<10)
    {
        mm=`0${mm}`;
    }

    if(hh<10)
    {
        hh=`0${hh}`;
    }

    if(dd<10)
    {
        dd=`0${dd}`;
    }

    if(mmmm<10)
    {
        mmmm=`0${mmmm}`;
    }

    return (dd + "/" + mmmm + '/' + yyyy + " à " + hh + ":" + mm);
  }

  _sendReload() {
    if(Object.entries(this.state.bankInfo).length === 0 || this.state.bankInfo.status !== "5") {
      Alert.alert(
        "KYC",
        //"Afin de finaliser votre inscription, n'oubliez pas de compléter le formulaire d'identification KYC." +
        //"Sans ces documents, les recharges de portemonnaie ou les paiements ne seront pas possibles.",
        "Vous devez d'abord réaliser votre KYC pour effectuer un rechargement vers votre compte Youseeme.",
        [
          {
            text: "Voir plus",
            //onPress: () => Linking.openURL('https://www.youseeme.pro/kyc-comment-ca-marche/'),
            onPress: () => this.props.navigation.navigate("Kyc")
          },
          {
            text: "OK",
          }
        ],
        {
          cancelable: false
        }
      );
    }
    else {
      //const regex = new RegExp('Reload completed', 'i');
      var amount = parseFloat(this.reloadAmount.replace(/,/g,".")).toFixed(2);
      if(amount > 0) {
        //this._confirmationRequest("Rechargement", "Êtes vous sur de vouloir effectuer ce rechargement de " + amount + " € vers votre porte-monnaie Youseeme ?", 'Rechargement', 'Le rechargement a bien été annulé.', () => {
          sendReloadFromApiWithInfos(this.token, amount, this.state.memorizeCard ? "1" : "0").then(data => {
            if(data == null || Object.entries(data).length === 0)
              this._showToast('Paiement annulé', 'Un problème est survenu lors du rechargement, veuillez réessayer ultérieurement.', false);
            else {
              this.props.navigation.navigate('Browser', {
                url: "https://webkit.lemonway.fr/mb/youseeme/prod/?moneyintoken=" + data.TOKEN,
                name: "Recharger",
                type: "payment-webkit",
                feedback: this._finishReload
              });
            }
          });
        //});
      }
      else {
        this.setState({
          errorMessage: "Le montant n'est pas valide.$Rappel : Le montant du rechargement ne doit pas être nul"
        });
      }
    }
  }

  // Lemonway status
  _displayWalletStatus(status) {
    var statusName, color;
    switch (status) {
      case "4":
        statusName = "En attente";
        color = "#FF9500";
        break;
      case "5":
        statusName = "Validé";
        color = "#34C759";
        break;
      case "8":
        statusName = "Désactivé";
        color = "#FF3B30";
        break;
      case "9":
        statusName = "Rejeté";
        color = "#FF3B30";
        break;
      default:
        statusName = " - ";
        color = "grey";
    }
    return (
      <Text style={[styles.walletStatusName, {color: color}]}>{statusName}</Text>
    );
  }


  componentDidMount() {
    this._loadProfile();
    this._getBankInfo();
  }

  render() {
    return (
      <Root>
        <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
          <View style={styles.topHeadband}>
              <Text style={styles.descriptiveText}>
                  {"Recharge ton compte Youseeme par carte ou par virement"}
              </Text>
          </View>

          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={styles.container}
          >
            <ScrollView
              style={{
                  width: '100%',
                  //alignItems: 'center',
                  marginBottom: 0,
                  backgroundColor:'#D9D8D9',
                  flex:2,
                  opacity: 0.8,
                  borderTopLeftRadius:15,
                  borderTopRightRadius:15,
              }}
              contentContainerStyle={{
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingBottom: 50
              }}
            >
            {this.state.isLoading ?
              <View style={styles.loading_container}>
                <ActivityIndicator size='large' color='#674171' />
              </View>
              :
              <View style={{
                width: '100%',
                //height: height/1.5,
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: 30
              }}>
                <View style={styles.bodyContainer}>
                  {/*<Text>{"Compte :"}</Text>*/}
                  <View style={styles.balanceContainer}>
                    <View style={styles.balanceTextContainer}>
                      <Text style={styles.balanceTitle}>{"Mon solde : "}</Text>
                      <Text style={styles.balanceValue}>{this.state.balance.balance + "€"}</Text>
                    </View>
                    <View style={styles.balanceTextContainer}>
                      <Text style={styles.balanceTitle}>{"Bartcoins : "}</Text>
                      <Text style={styles.balanceValue}>{parseFloat(this.state.balance.points).toFixed(2)}</Text>
                    </View>
                    <Text style={styles.currentDate}>{"Dernière mise à jour le " + this._currentDate()}</Text>
                  </View>
                </View>
                <View style={styles.cardReloadContainer}>
                  <View style={styles.entryContainer}>
                    <Text style={styles.entryDescriptionText}>{"Montant à recharger :"}</Text>
                    <View style={styles.entryBackground}>
                      <TextInput
                        ref={ref => this.reloadAmountEntry = ref}
                        autoCompleteType={'off'}
                        keyboardType={'decimal-pad'}
                        returnKeyType={'done'}
                        style={styles.textEntry}
                        maxLength={50}
                        placeholder="Exemple : 10"
                        fontSize={18}
                        textAlign='left'
                        placeholderTextColor={'rgb(99, 89, 107)'}
                        selectionColor={'#674171'}
                        onChangeText={(amount) => {
                          this.reloadAmount = amount;
                        }}
                      />
                      <TouchableOpacity
                        style={styles.btnEntry}
                        activeOpacity={0.8}
                      >
                        <SectionedMultiSelect
                          items={this.currencyTypes}
                          colors={{
                            primary:'#F8CE3E',
                            success:'#674171',
                            chipColor: 'white'
                          }}
                          styles={currencyTypeSelectStyle}
                          uniqueKey="key"
                          subKey="children"
                          selectText="Choisir..."
                          confirmText="Valider"
                          selectedText="Sélectionnés"
                          removeAllText="Tout supprimer"
                          searchPlaceholderText="Rechercher une devise"
                          showDropDowns={true}
                          showCancelButton={false}
                          showRemoveAll={false}
                          readOnlyHeadings={false}
                          modalWithSafeAreaView={true}
                          modalWithTouchable={true}
                          hideSearch={true}
                          single={true}
                          showChips={false}
                          onConfirm={() => {
                          }}
                          onToggleSelector={() => {

                          }}
                          selectToggleIconComponent={
                            <Ionicons
                              name="ios-arrow-down"
                              style={styles.pickerArrow}
                            />
                          }
                          noItemsComponent={this._displayNoResultsComponent("Désolé, aucune devise n'a été trouvé.")}
                          noResultsComponent={this._displayNoResultsComponent("Désolé, aucune devise n'a été trouvé.")}
                          onSelectedItemsChange={(selectedCurrency) => {
                            const index = this.currencyTypes.map(e => e.key).indexOf(selectedCurrency[0]);
                            if(index !== -1) {
                              this.setState({currencyType: selectedCurrency[0]});
                            }
                          }}
                          selectedItems={[this.state.currencyType]}
                        />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.cardCheckbox}>
                    <BouncyCheckbox
                      borderColor='transparent'
                      fillColor='#674171'
                      unfillColor='#D9D8D9'
                      iconColor='#D9D8D9'
                      textStyle={{
                        paddingTop: Platform.OS === 'ios' ? 3 : 0,
                        marginLeft: -7,
                        fontSize: 16,
                        fontFamily: 'SciFly',
                        color: '#674171'
                      }}
                      disableTextDecoration={true}
                      borderRadius={6}
                      text="Mémoriser ma carte"
                      onPress={() => this.setState({memorizeCard: !this.state.memorizeCard})}
                    />
                  </View>
                  {this.state.errorMessage !== "" ? <View style={styles.errorContainer}>
                  <View style={styles.errorTopPart}>
                    <Ionicons
                      name="ios-warning"
                      style={styles.errorIcon}
                    />
                    <Text style={styles.mainTxtError}>{this.state.errorMessage.split("$")[0]}</Text>
                  </View>
                    <Text style={styles.subTxtError}>{this.state.errorMessage.split("$")[1]}</Text>
                  </View> : null}
                  <TouchableOpacity
                    style={[styles.btnSend, {marginTop: 25}]}
                    activeOpacity={0.6}
                    onPress={() => {
                      this._sendReload();
                    }}
                  >
                    <Text style={styles.btnSendText}>{"Recharger"}</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.ibanReloadContainer}>
                  <Text style={styles.ibanReloadText}>{"En attendant l'approbation de votre formulaire KYC, vous pouvez faire un virement sur les coordonnées suivantes :"}</Text>
                  <View style={[styles.entryContainer, {marginTop: 15}]}>
                    <Text style={styles.entryDescriptionText}>{"IBAN :"}</Text>
                    <TouchableOpacity
                      style={styles.entryBackground}
                      onPress={() => this._copyToClipboard(IBAN_YOUSEEME, 'Le numéro IBAN a bien été copié.')}
                    >
                      <Text style={styles.copyableText}>{IBAN_YOUSEEME}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={[styles.entryContainer, {marginTop: 15, marginBottom: 6}]}>
                    <Text style={styles.entryDescriptionText}>{"BIC :"}</Text>
                    <TouchableOpacity
                      style={styles.entryBackground}
                      onPress={() => this._copyToClipboard(BIC_YOUSEEME, 'Le numéro BIC a bien été copié.')}
                    >
                      <Text style={styles.copyableText}>{BIC_YOUSEEME}</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.ibanWarningContainer}>
                    <Ionicons
                      name="ios-warning"
                      style={styles.warningIcon}
                    />
                    <View style={{flex: 1,justifyContent: 'center', alignItems: 'center'}}>
                      <Text style={styles.ibanWarningText}>{"N'oubliez pas d'indiquer votre nom, prénom et numéro de mobile."}</Text>
                    </View>
                  </View>
                </View>
              </View>}
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Root>
    );
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50,
    position: 'absolute',
    top: 50
  },
  container: {
    marginTop: 170,
    height: '100%',
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyContainer: {
    width: '90%',
    height: 140,
    backgroundColor: 'rgba(103, 65, 113, 0.95)',
    borderRadius: 15,
    paddingVertical: 2,
    paddingHorizontal: 10,
    marginTop: 20
  },
  balanceContainer: {
    flex: 1,
    paddingHorizontal: 5,
    justifyContent: 'space-around'
  },
  balanceTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  cardReloadContainer: {
    width:'90%',
    justifyContent: 'space-between',
    backgroundColor:'white',
    borderRadius:13,
    paddingBottom: 18,
    marginTop: '10%'
  },
  ibanReloadContainer: {
    width:'90%',
    backgroundColor:'white',
    borderRadius:13,
    paddingVertical: 18,
    //paddingHorizontal: 10,
    marginTop: '10%'
  },
  ibanReloadText: {
    fontSize: 16,
    fontWeight: '400',
    color: '#674171',
    fontFamily: "SciFly",
    textAlign: 'justify',
    paddingHorizontal: 15
  },
  ibanWarningContainer: {
    width: '80%',
    alignSelf: 'center',
    flexDirection: 'row',
    paddingRight: 5
  },
  ibanWarningText: {
    fontSize: 11,
    fontWeight: '500',
    color: '#FF9500',
    marginLeft: 10,
    flexWrap: 'wrap',
    textAlign: 'justify',
    //fontFamily: "SciFly",
  },
  copyableText: {
    flex: 1,
    textAlign: 'center',
    fontSize: 16,
    color:'black',
    fontWeight: '500'
  },
  bankInfoContainer: {
    width: '90%',
    alignSelf: 'center'
  },
  balanceTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: "white"
  },
  balanceValue: {
    fontSize: 32,
    fontWeight: 'bold',
    color: "#F8CE3E"
  },
  currentDate: {
    fontSize: 12,
    color: "white",
    textAlign: 'center'
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  loading_container: {
    height: height/1.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  entryContainer: {
    width:'90%',
    alignSelf: 'center',
    //backgroundColor: 'red',
    marginTop: 25
  },
  entryBackground: {
    width:'100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor:'#FFFFFF',
    //backgroundColor: 'rgba(103, 65, 113, 0.8)',
    backgroundColor: '#D9D8D9',
    borderRadius: 8,
    height: 50,
    //borderWidth: 1,
    //borderColor: '#674171'
  },
  textEntry: {
    flex: 1,
    color:'black',
    paddingLeft: 10
  },
  entryDescriptionText: {
    color: '#674171',
    fontSize: 17,
    fontWeight: '600',
    marginBottom: 8
  },
  btnEntry: {
    width: 65,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F8CE3E',
    backgroundColor: '#674171',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,

    marginRight: -2,
  },
  cardCheckbox: {
    marginTop: 5,
    marginLeft: 10
  },
  btnSend: {
    width: 140,
    height: 45,
    alignSelf: 'center',
    backgroundColor: '#674171',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnSendText: {
    fontSize: 20,
    fontWeight: '400',
    color: 'white',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  pickerArrow: {
    color: '#F8CE3E',
    fontSize: 17,
    marginRight: 8,
    marginTop: 0
  },
  receiverInfos: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    paddingLeft: 10,
    paddingTop: 2
  },
  receiverName: {
    fontSize: 16,
    fontWeight: '500',
    color: 'white',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingVertical: 2,
    paddingHorizontal: 10
  },
  receiverStatus: {
    borderRadius: 10,
    //width: 100,
    height: 28,
    borderWidth: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -4
  },
  receiverStatusTextContainer: {
    //flex: 1,
    height: 28,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  textKyc: {
    fontSize: 17,
    fontWeight: '400',
    color: '#674171',
    fontFamily: "SciFly",
    textAlign: 'justify',
    paddingVertical: 2,
    paddingHorizontal: 10,
    marginBottom: 20
  },
  btnKyc: {
    width: 140,
    height: 45,
    alignSelf: 'center',
    backgroundColor: '#674171',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBtnKyc: {
    fontSize: 20,
    fontWeight: '400',
    color: 'white',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  bankInfoItem: {
    flexDirection: 'row',
    marginVertical: 5,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    borderRightColor: '#674171',
    backgroundColor: 'rgba(103, 65, 113, 0.10)'
  },
  bankInfoItemIconContainer: {
    width: 35,
    height: 35,
    borderRadius: 8,
    backgroundColor: '#674171',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5
  },
  bankInfoItemTextContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingVertical: 2
  },
  bankInfoItemTitle: {
    color: 'grey',
    fontSize: 12,
    fontWeight: 'bold'
  },
  bankInfoItemValue: {
    color: 'grey',
    fontWeight: '500',
    fontSize: 12,
  },
  bankInfoIcon: {
    color: '#F8CE3E'
    //color: 'white'
  },
  walletStatusContainer: {
    flex: 0.3,
    alignSelf: 'center',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: 1,
    marginRight: 10
  },
  walletStatusName: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: "SciFly",
    alignSelf: 'flex-end',
    textAlign: 'right'
  },
  errorContainer: {
    flex: 1,
    //flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  errorTopPart: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  warningIcon: {
    fontSize: 30,
    paddingTop: 4,
    color: '#FF9500',
  },
  mainTxtError: {
    fontSize: 14,
    fontWeight: '500',
    color: '#FF453A',
    marginLeft: 10,
    textAlign: 'center'
  },
  subTxtError: {
    fontSize: 13,
    fontWeight: '500',
    color: '#FF453A',
    textAlign: 'center'
  },
});

const currencyTypeSelectStyle = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios' ? 200 : 150,
    marginBottom: Platform.OS === 'ios' ? 400 : 300,
    borderRadius: 10
  },
  confirmText:{
    color: 'white'
  },
  selectToggle:{
    flex: 1,
    width: '100%',
    //alignSelf: 'center',
    //backgroundColor:'transparent',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 15,
    paddingLeft: 11
  },
  selectToggleText: {
    width: '100%',
    //height: '100%',
    color: '#F8CE3E',
    fontSize: 16,
    fontWeight: 'bold',
    paddingRight: 4,
    marginLeft: -10,
    paddingRight: 1,
    textAlign: 'center',
    alignSelf: 'center'
  },
  selectedItemText: {
    color: '#674171',
  },
  itemText: {
    textAlign: 'center',
    fontSize: 18
  }
});
