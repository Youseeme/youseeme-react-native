import React from 'react';
import {  StyleSheet, View,ImageBackground,Picker ,TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager } from 'react-native';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import { Card } from 'galio-framework';

export default class First_page extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            Error: '',
            password: '',
            code : 449,
        };
    }
    componentDidMount() {
        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });

    }

    render() {
        return (
            <ImageBackground  source={require('../assets/fond.png')} style={{width: '100%', height: '100%'
            }}>
                <View style={styles.container}>



                        <View
                            style={{
                                width:350,
                                alignItems: 'center',
                                marginBottom: 15,
                                flex:0.4,
                                marginTop:27

                            }}>
                            <Image
                                style={styles.logo}
                                source={require('../assets/logo.png')}/>
                            <Input
                                style={styles.input}
                                placeholder="Rechercher"
                                fontSize={18}
                                family="antdesign"
                                onChangeText={(email) => this.setState({email})}
                                value={this.state.email}
                                placeholderTextColor={'white'}
                                center={true}
                                color={'white'}
                            />
                        </View>
                    <View
                        style={{
                            width: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: 0,
                            backgroundColor:'white',
                            flex:2,
                            opacity: 0.4,


                        }}>
                        <Card
                            flex
                            borderless
                            style={styles.card}
                            title="Christopher Moon"
                            caption="139 minutes ago"
                            location="Los Angeles, CA"
                            avatar="http://i.pravatar.cc/100?id=skater"
                            imageStyle={styles.cardImageRadius}
                            //imageBlockStyle={{ padding: theme.SIZES.BASE / 2 }}
                            image="https://images.unsplash.com/photo-1497802176320-541c8e8de98d?&w=1600&h=900&fit=crop&crop=entropy&q=300"
                        />
                    </View>



                </View>
            </ImageBackground>

        );

    }



}




const styles = StyleSheet.create({

    container: {
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    input: {
        fontSize:   60,

        color:'black',
        backgroundColor:'black',
        borderRadius: 90,
        opacity: 0.7,
        height: 75,
        marginTop: 0,
        width:280,

    },

    buttonStyle: {
        backgroundColor: "#40BCF8",
        marginRight: 5,
        borderRadius: 50,
        width: 110,
        borderWidth: 0.1,

    },
    buttonStyle1: {
        backgroundColor: "#f8cd3e",
        marginLeft: 15,
        borderRadius: 50,
        width: 110,
        borderWidth: 0.1,

    },
    logo: {
        width: 200,
        height: 60,
        resizeMode: 'contain'
    },
    textStyleforget: {
        fontSize: 20,
        color: "#f8cd3e",
        top:10,
        fontFamily: "SciFly",

    },

    textStyle: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 7,
        paddingBottom: 7,
        fontFamily: "SciFly",

    },
    backblue: {
        backgroundColor: "#FB1352",
        height:80,
        alignSelf : 'stretch',
        textAlign: 'center',
        //marginTop: 5,
        color: 'white',
        fontSize:25,
        paddingTop: 25,
    },
});
