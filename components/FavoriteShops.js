import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import { SearchBar } from 'react-native-elements';
import ShopItem from './ShopItem';
import * as Font from 'expo-font';
import { setFavoriteFromApiWithShopId, getUserProfileFromApiWithAccountToken, getShopFromApiWithId } from '../API/YSMApi';

import { getDistance, getPreciseDistance } from 'geolib';

class FavoriteShops extends React.Component {
  constructor(props) {
    super(props);
    this.token = this.props.token;
    this.profileData = this.props.profileData
    this.refreshShop = this.props.refreshShop;
    this.userLocation = this.props.userLocation;

    this._displayShopDetails=this.props.displayShopDetails;
    //console.log("----==== profileData =====-----");
    //console.log(this.profileData);
    this.state = {
      shops: [],
      isLoading: false,
      search: ''
    };
  }

  _updateSearch(search) {
    console.log(search)
    this.setState({
      search: search,
      shops: []
    }, () => this._loadShops());
  }

  _loadShops() {
    this.setState({isLoading: true});
    var regex = new RegExp("^" + this.state.search, 'i');

    getUserProfileFromApiWithAccountToken(this.token).then(data1 => {
      var enrichedShopsInfos = [];
      const promises = data1.favorites.map((shop) => {
        if(regex.test(shop.name)) {
          return getShopFromApiWithId(this.token, shop.uid).then(data2 => {
            if(data2.distance == null)
              data2.distance = this._getPreciseDistance(data2.addresses);
            enrichedShopsInfos.push(data2);
            enrichedShopsInfos.sort((a, b) => (a.distance > b.distance) ? 1 : -1);
          })
        }
      });


      Promise.all(promises).then(() => {
        this.setState({
          shops: enrichedShopsInfos,
          isLoading: false
        });
      });
    });
  }



  _getPreciseDistance = (shopAddresses) => {
    var pdis, pdisMin = -1;

    if(shopAddresses != null && this.userLocation != null) {
      shopAddresses.forEach((address) => {
        if(address.lat != null && address.lon != null) {
          pdis = getPreciseDistance(
            { latitude: this.userLocation.latitude, longitude: this.userLocation.longitude },
            { latitude: address.lat, longitude: address.lon }
          );
        }

        if(pdis < pdisMin || pdisMin === -1)
          pdisMin = pdis;
      });
    }

    if(pdisMin === -1)
      return -1;
    else
      return (pdisMin/1000);
  }

  _setFavoriteShop = (shop) => {
    setFavoriteFromApiWithShopId(this.token, shop.uid);
    this.refreshShop(shop);
    this._loadShops();
  }

  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='#674171' />
        </View>
      );
    }
  }

  componentDidMount() {
    this._loadShops();
    //this._getPreciseDistance();
  }


  render() {
    return(
      <View style={styles.mainContainer}>
        <SearchBar
          placeholder="Rechercher..."
          containerStyle={{
            backgroundColor: 'transparent',
            borderBottomColor: 'transparent',
            borderTopColor: 'transparent',
            height: 48,
            justifyContent: 'center',
            marginRight: 9
          }}
          inputContainerStyle={{
            backgroundColor: 'rgba(67, 39, 74, 0.8)',
            height: 35
          }}
          inputStyle={{
            color: 'white'
          }}
          onChangeText={(search) => this._updateSearch(search)}
          value={this.state.search}
          round={true}
          showCancel={true}
        />
        <FlatList
          style={styles.listContainer}
          data={this.state.shops}
          keyExtractor={(item) => item.uid.toString()}
          renderItem={({item}) => <ShopItem shop={item} setFavoriteShop={this._setFavoriteShop} getPreciseDistance={this._getPreciseDistance} displayShopDetails={this._displayShopDetails} />}
          onEndReachedThreshold={0.5}
          contentContainerStyle={{paddingBottom:110}}
          onRefresh={() => this._loadShops()}
          refreshing={this.state.isLoading}
          onEndReached={() => {
            //this._loadShops();
          }}
        />
        {/*this._displayLoading()*/}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(103, 65, 113, 0.6)',
    paddingLeft: 10,
    paddingRight: 0
  },
  listContainer: {
    flex: 1,
    paddingRight: 10,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default FavoriteShops;
