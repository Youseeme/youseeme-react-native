import React from 'react';
import {AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Dimensions, TouchableOpacity, Linking, Modal, Platform} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign } from '@expo/vector-icons';
import { Rating, AirbnbRating } from 'react-native-ratings';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import FamilyItem from './FamilyItem';
import * as Haptics from 'expo-haptics';
import * as Font from 'expo-font';
import Root from './RootPopup';
import Toast from './Toast';
import {getShopFromApiWithId, getDeliveryFromApiWithShopId, setShopRatingFromApiWithShopId} from '../API/YSMApi';

const { width, height } = Dimensions.get('screen');

const images = {
  background: require('../assets/fond.png'), // Put your own image here
};

class ShopDetails extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.token = this.props.navigation.getParam('token');
    this.shopId = this.props.navigation.getParam('shopId');
    this.shopDistance = this.props.navigation.getParam('shopDistance');
    this.setFavoriteShop = this.props.navigation.getParam('setFavoriteShop');

    this.shoppingBasketCode = 'shoppingBasket-' + this.shopId;

    this.state = {
      shop: null,
      delivery: null,
      shopLoading: true,
      deliveryLoading: true,
      ratingModalVisible: false,
      currentRating: 0,
      shoppingBasket: {
        articles: [],
        articleCount: 0,
        totalAmount: 0.00
      }
    };
  }

  _renderNavBar = () => (
    <Image
      style={styles.coverImage}
      source={{uri: this.state.shop.cover_image}}
    />
  )

  _renderContent = () => (
    <View style={{width: '100%', backgroundColor: 'green'}}>

    </View>
  )

  _loadShop() {
    getShopFromApiWithId(this.token, this.shopId).then(data => {
      this.setState({
        shop: data,
        shopLoading: false
      });
      //console.log(data);
    });
  }

  _loadDeliveryInfos() {
    getDeliveryFromApiWithShopId(this.token, this.shopId).then(data => {
      this.setState({
        delivery: data,
        deliveryLoading: false
      });
    });
  }

  _loadShoppingBasket() {
    AsyncStorage.getItem(this.shoppingBasketCode).then(data => {
      if(data != null) {
        var shoppingBasket = JSON.parse(data);
        this.setState({shoppingBasket: shoppingBasket});
      }
    });
  }

  _setShopRating() {
    setShopRatingFromApiWithShopId(this.token, this.shopId, this.state.currentRating).then(data => {
      console.log(data);
      /*this.setState(prevState => ({
        shop: {
          ...prevState.shop,
          user_rating: this.state.currentRating
        }
      }))*/
      this._loadShop();
    });
  }

  async saveItem(item, selectedValue) {
    try {
      await AsyncStorage.setItem(item, selectedValue);
    } catch (error) {
      console.error('AsyncStorage error: ' + error.message);
    }
  }

  _displayProductDetailWithOptions = (product) => {
    this.props.navigation.navigate('ProductDetailWithOptions', {product: product, addToShoppingBasket: this._addToShoppingBasket});
  }

  _addToShoppingBasket = async (article) => {
     try {
       const index = this.state.shoppingBasket.articles.map(
         e => e.article_code + "-" + e.bar_code + "-" + e.name
       ).indexOf(article.article_code + "-" + article.bar_code + "-" + article.name);
       if(index === -1) {
         article.quantity = 1;
         await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
           articles: [...this.state.shoppingBasket.articles, ...[article]],
           articleCount: this.state.shoppingBasket.articleCount + 1,
           totalAmount: this.state.shoppingBasket.totalAmount + parseFloat(article.price_ttc.replace(/,/g,"."))
         }));
       }
       else {
         var articles = this.state.shoppingBasket.articles;
         articles[index].quantity++;
         await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
           articles: articles,
           articleCount: this.state.shoppingBasket.articleCount + 1,
           totalAmount: this.state.shoppingBasket.totalAmount + parseFloat(article.price_ttc.replace(/,/g,"."))
         }));
       }

       Toast.show({
           title: "Panier",
           timing: 2000,
           text: "L'article a bien été ajouté au panier",
           color: '#2ecc71',
           icon:(<Ionicons
               name="ios-checkmark"
               style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
             />)
       });
       Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);

     } catch (error) {
       Toast.show({
           title: "Panier",
           timing: 2000,
           text: "Un problème est survenu lors de l'ajout, veuillez réessayer ultérieurement.",
           color: '#FF453A',
           icon:(<Ionicons
               name="ios-close"
               style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
             />)
       });
       //console.error('AsyncStorage error: ' + error.message);
     }
     this._loadShoppingBasket();
   }

  _deliveryInfoState(state) {
    var yes = "Oui", no = "Non";
    return state ? yes : no;
  }

  _setRatingModalVisible = (visible) => {
    this.setState({ ratingModalVisible: visible });
  }

  _displayProductsDetails = (family) => {
    this.props.navigation.navigate('ProductsDetails', {
      token: this.token,
      family: family,
      shopId: this.shopId,
      addToShoppingBasket: this._addToShoppingBasket,
      onGoBack: () => this._loadShoppingBasket()
    });
  }

  _displayShoppingBasket = () => {
    this.props.navigation.navigate('ShoppingBasket', {
      token: this.token,
      shoppingBasket: this.state.shoppingBasket,
      clearShoppingBasket: this._clearShoppingBasket,
      refreshShoppingBasket: () => this._loadShoppingBasket(),
      shoppingBasketCode: this.shoppingBasketCode
    });
  }

  _displayLoading() {
    if(this.state.shopLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='#674171' />
        </View>
      );
    }
  }

  _clearShoppingBasket = async () => {
    try {
      await AsyncStorage.removeItem(this.shoppingBasketCode);
      this.setState({
        shoppingBasket: {
          articles: [],
          articleCount: 0,
          totalAmount: 0.00
        }
      });
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  _favored(state) {
    var iconName = "ios-heart"
    if(!state)
      iconName+="-empty"
    return (
      <Ionicons
        name={iconName}
        style={{color: '#F8CE3E', fontSize: 25, paddingTop: 5, paddingLeft: 1}}
      />
    );
  }

  _displayFamilies() {
    this.state.shop.families.map((family, index) => {
      console.log(family);
      return (
        <FamilyItem family={this.state.shop.families[0]} />
      );
    });
  }


  componentDidMount() {
    this._loadShoppingBasket();
    this._loadShop();
    this._loadDeliveryInfos();
  }

  render() {
    return (
      <Root>
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            {Platform.OS === 'ios' ? null :   // to solve zIndex problem after build on Android
            <TouchableOpacity
              style={{position: 'absolute', top: 2, left: 3}}
              onPress={() => this.props.navigation.goBack()}
            >
              <Ionicons
                  name="md-arrow-back"
                  style={{fontSize: 24, color: 'white', paddingTop: 38, paddingLeft: 15}}
                />
            </TouchableOpacity>}
            <Text  style={styles.topTitle}>
                Détails du commerce
            </Text>
            <TouchableOpacity
              style={{position: 'absolute', bottom: 13, right: 7}}
              onPress={() => this._displayShoppingBasket()}
            >
              <FontAwesome
                name="shopping-basket"
                style={{fontSize: 23, paddingRight: 3, color: 'white'}}
              />
              <View style={styles.articleCount}>
                <Text style={styles.textArticleCount}>{this.state.shoppingBasket.articleCount}</Text>
              </View>
            </TouchableOpacity>
        </View>
        {this.state.shopLoading ?
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='white' />
        </View> :
        <View style={styles.mainContainer}>
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.ratingModalVisible}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <Text style={styles.ratingModalTitle}>Noter le commerce</Text>
                <AirbnbRating
                  count={5}
                  size={25}
                  reviews={["Terrible", "Décevant", "Bien", "Génial", "Incroyable"]}
                  reviewSize={18}
                  defaultRating={this.state.shop.user_rating}
                  isDisabled={false}
                  selectedColor={'#F8CE3E'}
                  reviewColor={'rgba(249, 207, 62, 0.85)'}
                  onFinishRating={(rating) => this.setState({currentRating: rating}) }
                />
                <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10}}>
                  <TouchableOpacity
                    style={{width: 100, height: 40, backgroundColor: '#674171', borderRadius: 30, justifyContent: 'center', alignItems: 'center'}}
                    onPress={() => {
                      this._setRatingModalVisible(!this.state.ratingModalVisible);
                    }}
                  >
                    <Text style={{color: 'white', fontSize: 16}}>{"Annuler"}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{width: 100, height: 40, backgroundColor: '#674171', borderRadius: 30, justifyContent: 'center', alignItems: 'center'}}
                    onPress={() => {
                      this._setRatingModalVisible(!this.state.ratingModalVisible);
                      this._setShopRating();
                    }}
                  >
                    <Text style={{color: 'white', fontSize: 16}}>{"Valider"}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          <ParallaxScrollView
            backgroundColor='#D9D8D9'
            contentBackgroundColor='#D9D8D9'
            parallaxHeaderHeight={200}
            backgroundScrollSpeed={7}
            outputScaleValue={6}
            renderBackground={this._renderNavBar}
            scrollIndicatorInsets={{ right: 1 }}
            contentContainerStyle={{paddingBottom: 100}}
          >
            <View style={{backgroundColor:'#D9D8D9'}}>
              <View style={styles.topContainer}>
              <View style={styles.usefulShopInfos}>
                <Text style={styles.shopName} numberOfLines={2}>{this.state.shop.name}</Text>
                <Text style={styles.shopDescription} numberOfLines={10}>{this.state.shop.description}</Text>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                  <TouchableOpacity
                    style={styles.ratingContainer}
                    activeOpacity={0.6}
                    onPress={() => {
                      this._setRatingModalVisible(!this.state.ratingModalVisible);
                    }}
                  >
                    <AirbnbRating
                      count={5}
                      size={18}
                      reviews={[]}
                      reviewSize={12}
                      defaultRating={this.state.shop.average_rating}
                      isDisabled={true}
                      selectedColor={'#F8CE3E'}
                      reviewColor={'rgba(249, 207, 62, 0.85)'}
                    />
                    <Text style={styles.ratingNumber}>{"~" + this.state.shop.average_rating.toFixed(1)}</Text>
                  </TouchableOpacity>
                  <View style={styles.shopDistanceContainer}>
                    <MaterialIcons
                      name="location-on"
                      style={{fontSize: 18, paddingRight: 3, color: '#F8CE3E'}}
                    />
                    <Text style={styles.shopDistance}>{(this.shopDistance != null && this.shopDistance != -1) ? this.shopDistance.toFixed(2) + " km" : " - "}</Text>
                  </View>
                </View>
                <View style={styles.addressesContainer}>
                  <Text style={styles.infoTitle}>{"Adresse : "} </Text>
                  <View style={styles.textAddressContainer}>
                    <Text numberOfLines={2} style={styles.addressPart1}>{this.state.shop.addresses.length > 0 ? (this.state.shop.addresses[0].street + ",") : "Non communiqué"}</Text>
                    <Text numberOfLines={1} style={styles.addressPart2}>{this.state.shop.addresses.length > 0 ? (this.state.shop.addresses[0].postal_code + ", " + this.state.shop.addresses[0].city) : ""}</Text>
                  </View>
                </View>
                <View style={styles.openingHoursContainer}>
                  <Text style={styles.infoTitle}>{"Horaires d'ouverture : "} </Text>
                  <Text style={styles.openingHoursValue} numberOfLines={2}>{this.state.shop.opening_hour + " - " + this.state.shop.closing_hour} </Text>
                </View>
                <View style={styles.specialOfferContainer}>
                  <Text style={styles.infoTitle}>{"Offre spéciale : "} </Text>
                  <View style={{flexDirection: 'row', marginLeft: 10, marginTop: 2}}>
                    <MaterialIcons
                      name="local-offer"
                      style={{fontSize: 18, paddingRight: 3, color: '#F8CE3E'}}
                    />
                    <Text style={styles.specialOfferValue} numberOfLines={4}>{this.state.shop.special_offer}</Text>
                  </View>
                </View>
                <View style={styles.actionItemsContainer}>
                  <TouchableOpacity
                    style={styles.btnActionContainer}
                    onPress={() => {
                      var phone = this.state.shop.phone_number;
                      if(phone != null)
                        Linking.openURL('tel:' + phone);
                    }}
                  >
                    <FontAwesome
                      name="phone"
                      style={{fontSize: 25, paddingTop: 2, color: '#F8CE3E'}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.btnActionContainer}
                    onPress={() => {
                      var addresses = this.state.shop.addresses;
                      //console.log(addresses)
                      if(addresses != null && addresses.length > 0) {
                        Linking.openURL('https://www.google.com/maps?q=@' + addresses[0].lat + ',' + addresses[0].lon);
                      }
                    }}
                  >
                    <MaterialIcons
                      name="location-on"
                      style={{fontSize: 25, paddingTop: 2, color: '#F8CE3E'}}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.btnActionContainer}
                    onPress={() => {
                      this.setFavoriteShop(this.state.shop);
                      this.setState(prevState => ({
                        shop: {
                            ...prevState.shop,
                            is_favored: !this.state.shop.is_favored
                        }
                      }));
                    }}
                  >
                    {this._favored(this.state.shop.is_favored)}
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.otherShopInfosContainer}>
                <Text style={styles.titleShopInformation}>{"Informations complémentaires :"}</Text>
                <Text style={styles.shopInformation}>{this.state.shop.information}</Text>
                {this.state.deliveryLoading ? null : <View style={styles.deliveryInfosContainer}>
                  <View style={styles.deliveryLeftContainer}>
                    <View style={styles.deliveryInfoContainer}>
                      <View style={styles.deliveryIconContainer}>
                        <MaterialCommunityIcons
                          name="cursor-default-click"
                          style={[{fontSize: 20, paddingTop: 2}, styles.deliveryIcon]}
                        />
                      </View>
                      <View style={styles.deliveryInfoTextContainer}>
                        <Text style={styles.deliveryInfoTitle}>{"CLICK & COLLECT"}</Text>
                        <Text style={styles.deliveryInfoValue}>{this._deliveryInfoState(this.state.delivery.click_collect)}</Text>
                      </View>
                    </View>
                    {this.state.delivery.house_delivery ?
                    <View style={styles.deliveryInfoContainer}>
                      <View style={styles.deliveryIconContainer}>
                        <Ionicons
                          name="ios-time"
                          style={[{fontSize: 21, paddingTop: 2}, styles.deliveryIcon]}
                        />
                      </View>
                      <View style={styles.deliveryInfoTextContainer}>
                        <Text style={styles.deliveryInfoTitle}>{"DÉLAI DE LIVRAISON"}</Text>
                        <Text style={styles.deliveryInfoValue}>{this.state.delivery.delivery_delay}</Text>
                      </View>
                    </View> : null}
                  </View>
                  <View style={styles.deliveryRightContainer}>
                    <View style={styles.deliveryInfoContainer}>
                      <View style={styles.deliveryIconContainer}>
                        <MaterialCommunityIcons
                          name="truck-delivery"
                          style={[{fontSize: 19, paddingTop: 2, paddingLeft: 1}, styles.deliveryIcon]}
                        />
                      </View>
                      <View style={styles.deliveryInfoTextContainer}>
                        <Text style={styles.deliveryInfoTitle}>{"LIVRAISON"}</Text>
                        <Text style={styles.deliveryInfoValue}>{this._deliveryInfoState(this.state.delivery.house_delivery)}</Text>
                      </View>
                    </View>
                    {this.state.delivery.house_delivery ?
                    <View style={styles.deliveryInfoContainer}>
                      <View style={styles.deliveryIconContainer}>
                        <FontAwesome5
                          name="money-bill-wave"
                          style={[{fontSize: 15, paddingTop: 2}, styles.deliveryIcon]}
                        />
                      </View>
                      <View style={styles.deliveryInfoTextContainer}>
                        <Text style={styles.deliveryInfoTitle}>{"PRIX DE LIVRAISON"}</Text>
                        <Text style={styles.deliveryInfoValue}>{this.state.delivery.delivery_price != null ? this.state.delivery.delivery_price.toFixed(2) + " €" : "0.00 €"}</Text>
                      </View>
                    </View> : null}
                  </View>
                </View>}
              </View>
              </View>
              <View style={styles.familiesContainer}>
                {this.state.shop.families.map((family, key) => {
                  return (
                    <FamilyItem key={key} familyKey={key} family={family} displayProductsDetails={this._displayProductsDetails} addToShoppingBasket={this._addToShoppingBasket} displayProductDetailWithOptions={this._displayProductDetailWithOptions} />
                  );
                })}
              </View>
            </View>
          </ParallaxScrollView>
        </View>}
      </ImageBackground>
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171',
    zIndex: 1
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  topContainer: {
    width: '100%',
    backgroundColor:'white',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    marginBottom: 10
  },
  usefulShopInfos: {
    width: '94%',
    alignSelf: 'center',
    backgroundColor: '#674171',
    borderRadius: 15,
    marginTop: -45,
    paddingVertical: 12,
    paddingHorizontal: 10,
    zIndex: 1
  },
  otherShopInfosContainer: {
    width: '100%',
    alignSelf: 'center',
    marginTop: 10,
    //backgroundColor: 'white',
    borderRadius: 15,
    paddingTop: 15,
    paddingHorizontal: 10
  },
  familiesContainer: {
    marginTop: 0,
    marginLeft: -12,
    marginRight: -12
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:20,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  activeTabTextStyle: {
    color:'white'
  },
  coverImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover'
  },
  shopName: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
    marginBottom: 3
  },
  shopDescription: {
    fontSize: 12,
    fontWeight: '500',
    color: 'rgba(255, 255, 255, 0.65)',
    marginTop: 1,
    marginBottom: 5
  },
  openingHoursContainer: {
    flexDirection: 'row',
    marginVertical: 5,
    marginTop: 2,
    flexWrap: 'wrap'
  },
  infoTitle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'rgba(255, 255, 255, 0.65)'
  },
  openingHoursValue: {
    fontSize: 14,
    fontWeight: '500',
    color: 'rgba(255, 255, 255, 0.85)',
    marginLeft: 5,
  },
  shopDistanceContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 28,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#F8CE3E',
    paddingLeft: 5,
    paddingRight: 8,
    marginTop: 8
  },
  shopDistance: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'rgba(255, 255, 255, 0.65)',
  },
  ratingContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    height: 20,
    alignSelf: 'center'
  },
  ratingNumber: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#F8CE3E',
    marginLeft: 3,
    alignSelf: 'flex-start'
  },
  specialOfferContainer: {

  },
  specialOfferValue: {
    fontSize: 12,
    fontWeight: '500',
    color: '#F8CE3E',
    marginLeft: 3
  },
  actionItemsContainer: {
    width: '80%',
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 16
  },
  btnActionContainer: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.25)',
    borderRadius: 30,
  },
  addressesContainer: {
    flexDirection: 'row',
    marginVertical: 3,
    marginTop: 2,
    flexWrap: 'wrap',
    paddingTop: 8
  },
  textAddressContainer: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    //backgroundColor: 'red'
    paddingLeft: 5,
    paddingRight: 3,
  },
  addressPart1: {
    fontWeight: '500',
    textAlign: 'left',
    //color: '#674171',
    //color: 'white',
    color: 'rgba(255, 255, 255, 0.85)',
    fontSize: 14,
    paddingVertical: 0
  },
  addressPart2: {
    textAlign: 'left',
    color: 'rgba(255, 255, 255, 0.8)',
    paddingVertical: 0,
    fontSize: 12,
  },
  titleShopInformation: {
    color: '#674171',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 10,
    paddingHorizontal: 5
  },
  shopInformation: {
    textAlign: 'justify',
    color: '#674171',
    fontFamily: 'SciFly',
    fontSize: 16,
    paddingHorizontal: 5
  },
  deliveryInfosContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 8
  },
  deliveryLeftContainer: {
    width: 172,
    maxWidth: '49%',
    marginRight: 2
  },
  deliveryRightContainer: {
    width: 172,
    maxWidth: '49%',
    marginLeft: 2
  },
  deliveryInfoContainer: {
    flexDirection: 'row',
    marginVertical: 5,
    borderRadius: 8,
    //borderTopRightRadius: 5,
    //borderBottomRightRadius: 5,
    //borderRightWidth: 1.5,
    //borderRightColor: '#674171',
    //backgroundColor: 'rgba(255, 255, 255, 0.2)'
    backgroundColor: 'rgba(103, 65, 113, 0.10)'
  },
  deliveryIconContainer: {
    width: 35,
    height: 35,
    borderRadius: 8,
    backgroundColor: '#674171',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5
  },
  deliveryInfoTextContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingVertical: 2
  },
  deliveryInfoTitle: {
    color: 'grey',
    fontSize: 12,
    fontWeight: 'bold'
  },
  deliveryInfoValue: {
    color: 'grey',
    fontWeight: '500',
    fontSize: 12,
  },
  deliveryIcon: {
    color: '#F8CE3E'
    //color: 'white'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'flex-start',
    marginTop: 0
    //backgroundColor: 'rgba(0, 0, 0, 0.7)'
  },
  modalView: {
    width: '70%',
    height: 200,
    alignSelf: 'center',
    justifyContent: 'space-between',
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 10,
    paddingHorizontal: 14,
    backgroundColor: 'white',
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  btnCornerModal: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: '#674171',
    borderTopLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  ratingModalTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    justifyContent: 'center',
    color: '#674171'
  },
  articleCount: {
    height: 20,
    borderRadius: 30,
    position: 'absolute',
    right: -4,
    bottom: 14,
    backgroundColor: '#FF3B30',
    justifyContent: 'center',
    alignItems: 'center',
    minWidth: 20
  },
  textArticleCount: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    paddingHorizontal: 5
  }
});

export default ShopDetails;
