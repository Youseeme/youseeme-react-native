import React from 'react';
import { Alert, StyleSheet, View, ImageBackground, TouchableOpacity,Button, Image, Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView, ActivityIndicator, Easing, Platform, KeyboardAvoidingView} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import {getUserProfileFromApiWithAccountToken, getUserBalanceFromApiWithAccountToken, getUserFromApiWithPhone, sendMoneyToPhoneNumberFromApiWithInfos, sendMoneyToBankAccountFromApiWithIbanId, getWalletInfoFromApiWithToken} from '../API/YSMApi';
import { LinearGradient } from 'expo-linear-gradient';
import * as Contacts from 'expo-contacts';
import Root from './RootPopup';
import Toast from './Toast';
import AnimatedLoader from "react-native-animated-loader";

const { width, height } = Dimensions.get('screen')

export default class Transfer extends React.Component{
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf')
    });
    this.token = this.props.navigation.getParam('token');
    this.profileData = this.props.navigation.getParam('profileData');
    this.animatedColorValue = new Animated.Value(0);
    this.phoneTransferAmount = "0.00";
    this.ibanTransferAmount = "0.00";
    this.receiverPhone = "";
    this.phoneCurrencyTypes = [{key: 'EUR', name: 'EUR'}, {key: 'BAR', name: 'BAR'}]
    this.ibanCurrencyTypes = [{key: 'EUR', name: 'EUR'}]

    //console.log(this.profileData);

    this.state = {
        isLoading: true,
        balance:'',
        transfert: '',
        phoneCurrencyType: 'EUR',
        ibanCurrencyType: 'EUR',
        hasContactPermission: false,
        contacts: [],
        selectedContact: null,
        receiverProfile: {},
        bankInfo: {},
        phoneErrorMessage: "",
        ibanErrorMessage: "",
        showTransferLoading: false
    };
  }

  _getPermissionAsync = async () => {
    const { status } = await Contacts.requestPermissionsAsync();
      this.setState({ hasContactPermission: status === 'granted' });
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.PhoneNumbers],
        });

        if (data.length > 0) {
          this.setState({
            contacts: data
          })
          const contact = data[0];
        }
      }
  }

  _loadProfile = () => {
    getUserBalanceFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        balance: data,
        phoneErrorMessage: "",
        ibanErrorMessage: "",
        isLoading: false
      });
    });
  }

  _getBankInfo() {
    getWalletInfoFromApiWithToken(this.token).then(data => {
      this.setState({
        bankInfo: data
      })
    });
  }

  _getUserWithPhone(phone) {
    getUserFromApiWithPhone(this.token, phone).then(data => {
      this.setState({
        receiverProfile: data
      })
    });
  }

  _sendMoneyToUser() {
    const regex = new RegExp('Payment completed', 'i');
    var amount = parseFloat(this.phoneTransferAmount.replace(/,/g,".")).toFixed(2);

    if(this.receiverPhone.length >= 10) {
      var receiver;
      if(Object.entries(this.state.receiverProfile).length !== 0)
        receiver = "le compte de " + this.state.receiverProfile.first_name + " " + this.state.receiverProfile.last_name;
      else
        receiver = "le numéro " +  this.receiverPhone;
      if(this.state.phoneCurrencyType === 'EUR') {
        if(amount > 0 && amount <= parseFloat(this.state.balance.balance)) {
          if(Object.entries(this.state.bankInfo).length === 0 || this.state.bankInfo.status !== "5") {
            Alert.alert(
              "KYC",
              "Tu dois d'abord réaliser ton KYC pour effectuer un transfert en euro.",
              [
                {
                  text: "Voir plus",
                  //onPress: () => Linking.openURL('https://www.youseeme.pro/kyc-comment-ca-marche/'),
                  onPress: () => this.props.navigation.navigate("Kyc")
                },
                {
                  text: "OK",
                }
              ],
              {
                cancelable: false
              }
            );
          }
          else {
            this._confirmationRequest("Paiement euro", "Êtes vous sur de vouloir effectuer ce transfert de " + amount + " € vers " + receiver + " ?", 'Paiement euro', 'Le transfert a bien été annulé.', () => {
              sendMoneyToPhoneNumberFromApiWithInfos(this.token, this.receiverPhone, amount, 'euro').then(data => {
                console.log(data);
                if(regex.test(data)) {
                  this._showToast('Paiement terminé', 'Le transfert en euro a bien été effectué.', true);
                  this.phoneTransferAmountEntry.clear();
                }
                else
                  this._showToast('Paiement annulé', 'Un problème est survenu lors du transfert, veuillez réessayer ultérieurement.', false);
                this._loadProfile();
              })
            });
          }
        }
        else {
          this.setState({
            phoneErrorMessage: "Le montant n'est pas valide.$Rappel : Le montant du transfert ne doit pas être supérieur à votre solde eWallet en euros."
          });
        }
      }
      else if(this.state.phoneCurrencyType === 'BAR') {
        if(amount > 0 && amount <= parseFloat(this.state.balance.points)) {
          this._confirmationRequest("Paiement bartcoin", "Êtes vous sur de vouloir effectuer ce transfert de " + amount + " bartcoin(s) vers " + receiver + " ?", 'Paiement bartcoin', 'Le transfert a bien été annulé.', () => {
            sendMoneyToPhoneNumberFromApiWithInfos(this.token, this.receiverPhone, amount, 'bartcoin').then(data => {
              console.log(data);
              if(regex.test(data)) {
                this._showToast('Paiement terminé', 'Le transfert en bartcoin a bien été effectué.', true);
                this.phoneTransferAmountEntry.clear();
              }
              else
                this._showToast('Paiement annulé', 'Un problème est survenu lors du transfert, veuillez réessayer ultérieurement.', false);
              this._loadProfile();
            })
          });
        }
        else {
          this.setState({
            phoneErrorMessage: "Le montant n'est pas valide.$Rappel : Le montant du transfert ne doit pas être supérieur à votre solde eWallet en bartcoins."
          });
        }
      }
      else {
        this.setState({
          phoneErrorMessage: "La devise n'est pas valide."
        });
      }
    }
    else {
      this.setState({
        phoneErrorMessage: "Le numéro de téléphone n'est pas valide."
      });
    }
  }

  _sendMoneyToBankAccount() {
    this.setState({showTransferLoading: true});
    const regex = new RegExp('Debit completed', 'i');
    var amount = parseFloat(this.ibanTransferAmount.replace(/,/g,".")).toFixed(2);
    if(amount > 0 && amount <= parseFloat(this.state.balance.balance)) {
      this._confirmationRequest("Paiement euro", "Êtes vous sur de vouloir effectuer ce virement de " + amount + " € vers votre compte bancaire ?", 'Paiement euro', 'Le virement a bien été annulé.', () => {
        sendMoneyToBankAccountFromApiWithIbanId(this.token, this.state.bankInfo.id, amount).then(data => {
          if(regex.test(data)) {
            this._showToast('Paiement terminé', 'Le transfert a bien été effectué.', true);
            this.ibanTransferAmountEntry.clear();
          }
          else
            this._showToast('Paiement annulé', 'Un problème est survenu lors du transfert, veuillez réessayer ultérieurement.', false);
          this._loadProfile();
        })
      });
    }
    else {
      this.setState({
        ibanErrorMessage: "Le montant n'est pas valide.$Rappel : Le montant du virement ne doit pas être supérieur à votre solde eWallet en euros."
      });
    }
  }

  _displayNoResultsComponent(message) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red'}}>
        <Text style={{color: '#674171'}}>{message}</Text>
      </View>
    );
  }

  _displayReceiverNumber() {
    //console.log(this.state.selectedContact)
    if(this.state.selectedContact != null && this.state.selectedContact.phoneNumbers != null) {
      if(this.state.selectedContact.phoneNumbers.length > 0) {
        var number = this.state.selectedContact.phoneNumbers[0].number.replace(/ /g, "");
        console.log(number);
        if(number.length >= 10 && this.receiverPhone != number) {
          this._updateReceiverNumber(number);
          return number;
        }
      }
    }
    return this.receiverPhone;
  }

  _confirmationRequest(title, message, titleCancel, messageCancel, action) {
    Alert.alert(
      title,
      message,
      [
        {
          text: "Non",
          onPress: () => {
            this._loadProfile();
            this._showToast(titleCancel, messageCancel, false);
          }
        },
        {
          text: "Oui",
          onPress: () => action()
        }
      ],
      {
        cancelable: false
      }
    );
    //return response;
  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
          title: title,
          text: message,
          color: '#2ecc71',
          icon:(<Ionicons
              name="ios-checkmark"
              style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
            />)
      });
    }
    else {
      Toast.show({
          title: title,
          text: message,
          color: '#FF453A',
          icon:(<Ionicons
              name="ios-close"
              style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
            />)
      });
    }
  }

  _updateReceiverNumber(phone) {
    this.receiverPhone = phone;
    if(this.receiverPhone.length >= 10) {
      this._getUserWithPhone(this.receiverPhone);
    }
  }

  _currentDate() {
    const today = new Date();
    let mm = today.getMinutes();
    let hh = today.getHours();
    let dd = today.getDate();
    let mmmm = today.getMonth()+1;
    const yyyy = today.getFullYear();

    if(mm<10)
    {
        mm=`0${mm}`;
    }

    if(hh<10)
    {
        hh=`0${hh}`;
    }

    if(dd<10)
    {
        dd=`0${dd}`;
    }

    if(mmmm<10)
    {
        mmmm=`0${mmmm}`;
    }

    return (dd + "/" + mmmm + '/' + yyyy + " à " + hh + ":" + mm);
  }

  // Lemonway status
  _displayWalletStatus(status) {
    var statusName, color;
    switch (status) {
      case "4":
        statusName = "En attente";
        color = "#FF9500";
        break;
      case "5":
        statusName = "Validé";
        color = "#34C759";
        break;
      case "8":
        statusName = "Désactivé";
        color = "#FF3B30";
        break;
      case "9":
        statusName = "Rejeté";
        color = "#FF3B30";
        break;
      default:
        statusName = " - ";
        color = "grey";
    }
    return (
      <Text style={[styles.walletStatusName, {color: color}]}>{statusName}</Text>
    );
  }

  _displayReceiverUser() {
    let status = this.state.receiverProfile.status != null ? this.state.receiverProfile.status : "";
    var username = "";

    var color1 = "transparent";
    var color2 = "transparent";
    var color3 = "transparent";
    var color4 = "transparent";
    var color5 = "transparent";
    var color6 = "transparent";

    switch (status) {
      case "Bronze":
        color1 = "rgba(149, 92, 37, 0.2)";
        color2 = "rgba(199, 124, 49, 0.2)";
        color3 = "rgba(222, 142, 62, 0.35)";
        color4 = "rgba(218, 153, 44, 0.5)";

        color5 = "rgba(149, 92, 37, 1)";
        color6 = "rgba(199, 124, 49, 1)";
        break;
    case "Silver":
      color1="rgba(145, 145, 145, 0.2)";
      color2 = "rgba(128, 119, 122, 0.2)";
      color3="rgba(189, 183, 185, 0.35)";
      color4 = "rgba(98, 99, 99, 0.3)";

      color5="rgba(145, 145, 145, 1)";
      color6 = "rgba(128, 119, 122, 1)";
      break;
    case "Gold":
      color1="rgba(179, 150, 0, 0.2)";
      color2 = "rgba(240, 191, 0, 0.2)";
      color3="rgba(255, 215, 0, 0.25)";
      color4 = "rgba(230, 193, 0, 0.1)";

      color5="rgba(242, 205, 7, 1)";
      color6 = "rgba(240, 191, 0, 1)";
      break;
    case "Platinum":
      color1="rgba(91, 85, 87, 0.2)";
      color2 = "rgba(128, 119, 122, 0.2)";
      color3 = "rgba(2, 168, 244, 0.35)";
      color4 = "rgba(0, 119, 150, 0.1)";

      color5="rgba(91, 85, 87, 1)";
      color6 = "rgba(128, 119, 122, 1)";
      break;
    default:
      color1="grey";
      color2 = "grey";
      color3 = "grey";
      color4 = "grey";

      color5="grey";
      color6 = "grey";
  }

    if(this.state.receiverProfile.first_name != null && this.state.receiverProfile.last_name != null) {
      username = this.state.receiverProfile.first_name + " " + this.state.receiverProfile.last_name;
      this.runAnimationColor();
    }
    else
      username = "Inconnu";

    const backgroundColorConfig = this.animatedColorValue.interpolate(
    {
      inputRange: [0, 0.2, 0.4, 0.7, 0.9, 1],
      outputRange: [color1, color2, color3, color4, color2, color1]
    });

    return (
      <View style={styles.receiverStatus}>
        <LinearGradient start={[0.8, 0.1]} end={[0.2, 0.9]} colors={[color5, color6]} style={{width: '100%', height: '100%', borderRadius: 10}}>
          <Animated.View style={[styles.receiverStatusTextContainer, {backgroundColor: backgroundColorConfig}]}>
            <View style={{width: '100%', height: '70%', justifyContent: 'center', alignItems: 'center'}}>
              {/*<Text style={styles.btnStatusText}>{status.toUpperCase()}</Text>*/}
              <Text style={styles.receiverName}>{username}</Text>
            </View>
          </Animated.View>
        </LinearGradient>
      </View>
    );
  }

    runAnimationColor() {
      Animated.loop(Animated.timing(this.animatedColorValue, {
          toValue: 1,
          duration: 6000,
          //useNativeDriver: true
      })).start();
    }

  componentDidMount() {
    this._loadProfile();
    this._getBankInfo();
  }

  render() {
    return (
      <Root>
        <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
          <View style={styles.topHeadband}>
              <Text style={styles.descriptiveText}>
                  {"Envoie de l’argent ou des Bartcoins à tes proches, c’est instantané et gratuit"}
              </Text>
          </View>

          <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={styles.container}
          >
            <ScrollView
              style={{
                  width: '100%',
                  //alignItems: 'center',
                  marginBottom: 0,
                  backgroundColor:'#D9D8D9',
                  flex:2,
                  opacity: 0.8,
                  borderTopLeftRadius:15,
                  borderTopRightRadius:15,
              }}
              contentContainerStyle={{
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingBottom: 50
              }}
            >
            {this.state.isLoading ?
              <View style={styles.loading_container}>
                <ActivityIndicator size='large' color='#674171' />
              </View>
              :
              <View style={{
                width: '100%',
                //height: height/1.5,
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: 30
              }}>
                <View style={styles.bodyContainer}>
                  {/*<Text>{"Compte :"}</Text>*/}
                  <View style={styles.balanceContainer}>
                    <View style={styles.balanceTextContainer}>
                      <Text style={styles.balanceTitle}>{"Mon solde : "}</Text>
                      <Text style={styles.balanceValue}>{this.state.balance.balance + "€"}</Text>
                    </View>
                    <View style={styles.balanceTextContainer}>
                      <Text style={styles.balanceTitle}>{"Bartcoins : "}</Text>
                      <Text style={styles.balanceValue}>{parseFloat(this.state.balance.points).toFixed(2)}</Text>
                    </View>
                    <Text style={styles.currentDate}>{"Dernière mise à jour le " + this._currentDate()}</Text>
                  </View>
                </View>
                <View style={styles.blockTitleContainer}>
                  <Text style={styles.blockTitle}>{"TRANSFERT"}</Text>
                </View>
                <View style={styles.phoneTransferContainer}>
                  <View style={styles.entryContainer}>
                    <Text style={styles.entryDescriptionText}>{"Montant à transférer :"}</Text>
                    <View style={styles.entryBackground}>
                      <TextInput
                          ref={ref => this.phoneTransferAmountEntry = ref}
                          defaultValue=""
                          autoCompleteType={'off'}
                          keyboardType={'decimal-pad'}
                          returnKeyType={'done'}
                          style={styles.textEntry}
                          maxLength={50}
                          placeholder="Exemple : 10"
                          fontSize={18}
                          textAlign='left'
                          placeholderTextColor={'rgb(99, 89, 107)'}
                          selectionColor={'#674171'}
                          onChangeText={(amount) => {
                            this.phoneTransferAmount = amount;

                          }}
                        />
                        <TouchableOpacity
                          style={styles.btnEntry}
                          activeOpacity={0.8}
                        >
                          <SectionedMultiSelect
                            items={this.phoneCurrencyTypes}
                            colors={{
                              primary:'#F8CE3E',
                              success:'#674171',
                              chipColor: 'white'
                            }}
                            styles={currencyTypeSelectStyle}
                            uniqueKey="key"
                            subKey="children"
                            selectText="Choisir..."
                            confirmText="Valider"
                            selectedText="Sélectionnés"
                            removeAllText="Tout supprimer"
                            searchPlaceholderText="Rechercher une devise"
                            showDropDowns={true}
                            showCancelButton={false}
                            showRemoveAll={false}
                            readOnlyHeadings={false}
                            modalWithSafeAreaView={true}
                            modalWithTouchable={true}
                            hideSearch={true}
                            single={true}
                            showChips={false}
                            onConfirm={() => {
                            }}
                            onToggleSelector={() => {

                            }}
                            selectToggleIconComponent={
                              <Ionicons
                                name="ios-arrow-down"
                                style={styles.pickerArrow}
                              />
                            }
                            noItemsComponent={this._displayNoResultsComponent("Désolé, aucune devise n'a été trouvé.")}
                            noResultsComponent={this._displayNoResultsComponent("Désolé, aucune devise n'a été trouvé.")}
                            onSelectedItemsChange={(selectedCurrency) => {
                              const index = this.phoneCurrencyTypes.map(e => e.key).indexOf(selectedCurrency[0]);
                              //console.log(selectedCurrency);
                              //console.log(index);
                              if(index !== -1) {
                                //console.log(this.state.contacts[index]);
                                this.setState({phoneCurrencyType: selectedCurrency[0]});
                              }
                            }}
                            selectedItems={[this.state.phoneCurrencyType]}
                          />

                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.entryContainer}>
                      <Text style={styles.entryDescriptionText}>{"Numéro du destinataire :"}</Text>
                      <View style={styles.entryBackground}>
                        <TextInput
                            ref={ref => this.receiverPhoneEntry = ref}
                            keyboardType={'phone-pad'}
                            returnKeyType={'done'}
                            autoCompleteType={'off'}
                            style={styles.textEntry}
                            maxLength={14}
                            placeholder="Exemple : 06xxxxxxxx"
                            fontSize={18}
                            textAlign='left'
                            placeholderTextColor={'rgb(99, 89, 107)'}
                            selectionColor={'#674171'}
                            defaultValue={this._displayReceiverNumber()}
                            onChangeText={(phone) => {
                              if(this.state.receiverProfile != null) {
                                this.setState({receiverProfile: {}});
                              }
                              if(this.state.selectedContact != null) {
                                this.setState({selectedContact: null}, () => {
                                  this._updateReceiverNumber(phone);
                                });
                              }
                              else
                                this._updateReceiverNumber(phone);

                            }}
                          />
                          <TouchableOpacity
                            style={styles.btnEntry}
                            activeOpacity={0.8}
                          >
                            <SectionedMultiSelect
                              items={this.state.contacts}
                              colors={{
                                primary:'#F8CE3E',
                                success:'#674171',
                                chipColor: 'white'
                              }}
                              styles={contactsListSelectStyle}
                              uniqueKey="id"
                              subKey="children"
                              selectText="Choisir..."
                              confirmText="Terminer"
                              selectedText="Sélectionnés"
                              removeAllText="Tout supprimer"
                              searchPlaceholderText="Rechercher un contact"
                              showDropDowns={true}
                              showCancelButton={false}
                              showRemoveAll={true}
                              readOnlyHeadings={false}
                              modalWithSafeAreaView={true}
                              modalWithTouchable={true}
                              single={true}
                              showChips={false}
                              onConfirm={() => {
                                console.log("@@@@@@&@@@@@@@@@@")
                                console.log(this.state.selectedContact)
                                console.log("@@@@@@&@@@@@@@@@@")
                              }}
                              onToggleSelector={() => {
                                if(!this.state.hasContactPermission)
                                  this._getPermissionAsync();
                              }}
                              selectToggleIconComponent={
                                <AntDesign
                                  name="contacts"
                                  style={{fontSize: 28, paddingRight: 18, paddingTop: 1, color: '#F8CE3E'}}
                                />
                              }
                              noItemsComponent={this._displayNoResultsComponent("Désolé, aucun contact n'a été trouvé.")}
                              noResultsComponent={this._displayNoResultsComponent("Désolé, aucun contact n'a été trouvé.")}
                              onSelectedItemsChange={(selectedContact) => {
                                const index = this.state.contacts.map(e => e.id).indexOf(selectedContact[0]);
                                if(index !== -1) {
                                  console.log("@@@@@@@@@@@");
                                  console.log(this.state.contacts[index]);
                                  this.setState({selectedContact: this.state.contacts[index]});
                                }

                              }}
                              //selectedItems={this.state.selectedContact}
                            />
                          </TouchableOpacity>
                        </View>
                        <View style={{width: '100%', height: 38}}>
                          {this.state.receiverProfile != null && this.receiverPhone.length >= 10 ?
                          <View style={styles.receiverInfos}>
                            <Feather
                              name="corner-down-right"
                              style={{fontSize: 28, paddingRight: 8, paddingTop: 1, paddingBottom: 5, color: '#F8CE3E'}}
                            />

                            {this._displayReceiverUser()}
                          </View> : null}
                        </View>
                      </View>
                      {this.state.phoneErrorMessage !== "" ? <View style={styles.errorContainer}>
                      <View style={styles.errorTopPart}>
                        <Ionicons
                          name="ios-warning"
                          style={styles.errorIcon}
                        />
                        <Text style={styles.mainTxtError}>{this.state.phoneErrorMessage.split("$")[0]}</Text>
                      </View>
                        <Text style={styles.subTxtError}>{this.state.phoneErrorMessage.split("$")[1]}</Text>
                      </View> : null}
                      <TouchableOpacity
                        style={[styles.btnSend, {marginTop: 15}]}
                        activeOpacity={0.6}
                        onPress={() => {
                          this._sendMoneyToUser();
                        }}
                      >
                        <Text style={styles.btnSendText}>{"Transférer"}</Text>
                      </TouchableOpacity>
                </View>
                <View style={styles.blockTitleContainer}>
                  <Text style={styles.blockTitle}>{"VIREMENT VERS MON COMPTE"}</Text>
                </View>
                <View style={styles.ibanTransferContainer}>
                {Object.entries(this.state.bankInfo).length === 0 ?
                  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.textKyc}>{"Vous devez d'abord réaliser votre KYC pour effectuer un virement vers votre compte bancaire."}</Text>
                    <TouchableOpacity
                      style={styles.btnKyc}
                      onPress={() => this.props.navigation.navigate("Kyc")}>
                      <Text style={styles.textBtnKyc}>{"Voir"}</Text>
                    </TouchableOpacity>
                  </View> :
                  <View style={{flex: 1}}>
                    <View style={styles.bankInfoContainer}>
                      <Text style={styles.entryDescriptionText}>{"Informations bancaires :"}</Text>

                      <View style={styles.bankInfoItem}>
                        <View style={styles.bankInfoItemIconContainer}>
                          <FontAwesome5
                            name="user-alt"
                            style={[{fontSize: 18, paddingTop: 2}, styles.bankInfoIcon]}
                          />
                        </View>
                        <View style={[styles.bankInfoItemTextContainer, {flex: 0.7}]}>
                          <Text style={styles.bankInfoItemTitle}>{"DÉTENTEUR"}</Text>
                          <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.bankInfoItemValue, {marginRight: 5}]}>{this.profileData.first_name}</Text>
                            <Text style={styles.bankInfoItemValue}>{this.profileData.last_name}</Text>
                          </View>
                        </View>
                        <View style={styles.walletStatusContainer}>
                          {this._displayWalletStatus(this.state.bankInfo.status)}
                        </View>
                      </View>

                      <View style={styles.bankInfoItem}>
                        <View style={styles.bankInfoItemIconContainer}>
                          <FontAwesome5
                            name="money-check"
                            style={[{fontSize: 15, paddingTop: 2}, styles.bankInfoIcon]}
                          />
                        </View>
                        <View style={styles.bankInfoItemTextContainer}>
                          <Text style={styles.bankInfoItemTitle}>{"IBAN"}</Text>
                          <Text style={styles.bankInfoItemValue}>{this.state.bankInfo.iban}</Text>
                        </View>
                      </View>

                      <View style={styles.bankInfoItem}>
                        <View style={styles.bankInfoItemIconContainer}>
                          <Ionicons
                            name="ios-paper"
                            style={[{fontSize: 21, paddingTop: 2}, styles.bankInfoIcon]}
                          />
                        </View>
                        <View style={styles.bankInfoItemTextContainer}>
                          <Text style={styles.bankInfoItemTitle}>{"BIC"}</Text>
                          <Text style={styles.bankInfoItemValue}>{this.state.bankInfo.bic}</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.entryContainer}>
                      <Text style={styles.entryDescriptionText}>{"Montant du virement :"}</Text>
                      <View style={styles.entryBackground}>
                        <TextInput
                          ref={ref => this.ibanTransferAmountEntry = ref}
                          autoCompleteType={'off'}
                          keyboardType={'decimal-pad'}
                          returnKeyType={'done'}
                          style={styles.textEntry}
                          maxLength={50}
                          placeholder="Exemple : 10"
                          fontSize={18}
                          textAlign='left'
                          placeholderTextColor={'rgb(99, 89, 107)'}
                          selectionColor={'#674171'}
                          onChangeText={(amount) => {
                            this.ibanTransferAmount = amount;
                          }}
                        />
                        <TouchableOpacity
                          style={styles.btnEntry}
                          activeOpacity={0.8}
                        >
                          <SectionedMultiSelect
                            items={this.ibanCurrencyTypes}
                            colors={{
                              primary:'#F8CE3E',
                              success:'#674171',
                              chipColor: 'white'
                            }}
                            styles={currencyTypeSelectStyle}
                            uniqueKey="key"
                            subKey="children"
                            selectText="Choisir..."
                            confirmText="Valider"
                            selectedText="Sélectionnés"
                            removeAllText="Tout supprimer"
                            searchPlaceholderText="Rechercher une devise"
                            showDropDowns={true}
                            showCancelButton={false}
                            showRemoveAll={false}
                            readOnlyHeadings={false}
                            modalWithSafeAreaView={true}
                            modalWithTouchable={true}
                            hideSearch={true}
                            single={true}
                            showChips={false}
                            onConfirm={() => {
                            }}
                            onToggleSelector={() => {

                            }}
                            selectToggleIconComponent={
                              <Ionicons
                                name="ios-arrow-down"
                                style={styles.pickerArrow}
                              />
                            }
                            noItemsComponent={this._displayNoResultsComponent("Désolé, aucune devise n'a été trouvé.")}
                            noResultsComponent={this._displayNoResultsComponent("Désolé, aucune devise n'a été trouvé.")}
                            onSelectedItemsChange={(selectedCurrency) => {
                              const index = this.ibanCurrencyTypes.map(e => e.key).indexOf(selectedCurrency[0]);
                              if(index !== -1) {
                                this.setState({ibanCurrencyType: selectedCurrency[0]});
                              }
                            }}
                            selectedItems={[this.state.ibanCurrencyType]}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    {this.state.ibanErrorMessage !== "" ? <View style={styles.errorContainer}>
                    <View style={styles.errorTopPart}>
                      <Ionicons
                        name="ios-warning"
                        style={styles.errorIcon}
                      />
                      <Text style={styles.mainTxtError}>{this.state.ibanErrorMessage.split("$")[0]}</Text>
                    </View>
                      <Text style={styles.subTxtError}>{this.state.ibanErrorMessage.split("$")[1]}</Text>
                    </View> : null}
                    <TouchableOpacity
                      style={[styles.btnSend, {marginTop: 35}]}
                      activeOpacity={0.6}
                      onPress={() => {
                        this._sendMoneyToBankAccount();
                      }}
                    >
                      <Text style={styles.btnSendText}>{"Transférer"}</Text>
                    </TouchableOpacity>
                  </View>}
                </View>
              </View>}
              {/*<AnimatedLoader
                visible={true}
                source={require("../assets/loading.json")}
                animationStyle={styles.transferLoading}
                speed={1}
              />*/}
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Root>
    );
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50,
    position: 'absolute',
    top: 50
  },
  container: {
    marginTop: 170,
    height: '100%',
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyContainer: {
    width: '90%',
    height: 140,
    backgroundColor: 'rgba(103, 65, 113, 0.95)',
    borderRadius: 15,
    paddingVertical: 2,
    paddingHorizontal: 10,
    marginTop: 20,
    marginBottom: 30
  },
  balanceContainer: {
    flex: 1,
    paddingHorizontal: 5,
    justifyContent: 'space-around',
  },
  balanceTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  phoneTransferContainer: {
    width:'90%',
    justifyContent: 'space-between',
    backgroundColor:'white',
    borderRadius:13,
    paddingBottom: 18,
    marginBottom: '10%'
  },
  ibanTransferContainer: {
    width:'90%',
    backgroundColor:'white',
    borderRadius:13,
    paddingVertical: 18,
    //paddingHorizontal: 10,
  },
  bankInfoContainer: {
    width: '90%',
    alignSelf: 'center'
  },
  balanceTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: "white"
  },
  balanceValue: {
    fontSize: 32,
    fontWeight: 'bold',
    color: "#F8CE3E"
  },
  currentDate: {
    fontSize: 12,
    color: "white",
    textAlign: 'center'
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  loading_container: {
    height: height/1.3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  entryContainer: {
    width:'90%',
    alignSelf: 'center',
    //backgroundColor: 'red',
    marginTop: 25
  },
  entryBackground: {
    width:'100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor:'#FFFFFF',
    //backgroundColor: 'rgba(103, 65, 113, 0.8)',
    backgroundColor: '#D9D8D9',
    borderRadius: 8,
    height: 50,
    //borderWidth: 1,
    //borderColor: '#674171'
  },
  textEntry: {
    flex: 1,
    color:'black',
    //color: 'white',
    paddingLeft: 10,
  },
  entryDescriptionText: {
    color: '#674171',
    fontSize: 17,
    fontWeight: '600',
    marginBottom: 8
  },
  btnEntry: {
    width: 65,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: '#F8CE3E',
    backgroundColor: '#674171',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,

    marginRight: -2,
  },
  btnSend: {
    width: 140,
    height: 45,
    alignSelf: 'center',
    backgroundColor: '#674171',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSendText: {
    fontSize: 20,
    fontWeight: '400',
    color: 'white',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  pickerArrow: {
    color: '#F8CE3E',
    fontSize: 17,
    marginRight: 8,
    marginTop: 0
  },
  receiverInfos: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    paddingLeft: 10,
    paddingTop: 2
  },
  receiverName: {
    fontSize: 16,
    fontWeight: '500',
    color: 'white',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingVertical: 2,
    paddingHorizontal: 10
  },
  receiverStatus: {
    borderRadius: 10,
    //width: 100,
    height: 28,
    borderWidth: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -4
  },
  receiverStatusTextContainer: {
    //flex: 1,
    height: 28,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  textKyc: {
    fontSize: 17,
    fontWeight: '400',
    color: '#674171',
    fontFamily: "SciFly",
    textAlign: 'justify',
    paddingVertical: 2,
    paddingHorizontal: 15,
    marginBottom: 20
  },
  btnKyc: {
    width: 140,
    height: 45,
    alignSelf: 'center',
    backgroundColor: '#674171',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textBtnKyc: {
    fontSize: 20,
    fontWeight: '400',
    color: 'white',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  bankInfoItem: {
    flexDirection: 'row',
    marginVertical: 5,
    borderRadius: 8,
    borderRightColor: '#674171',
    backgroundColor: 'rgba(103, 65, 113, 0.10)'
  },
  bankInfoItemIconContainer: {
    width: 35,
    height: 35,
    borderRadius: 8,
    backgroundColor: '#674171',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5
  },
  bankInfoItemTextContainer: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingVertical: 2
  },
  bankInfoItemTitle: {
    color: 'grey',
    fontSize: 12,
    fontWeight: 'bold'
  },
  bankInfoItemValue: {
    color: 'grey',
    fontWeight: '500',
    fontSize: 12,
  },
  bankInfoIcon: {
    color: '#F8CE3E'
    //color: 'white'
  },
  walletStatusContainer: {
    flex: 0.3,
    alignSelf: 'center',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: 1,
    marginRight: 10
  },
  walletStatusName: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: "SciFly",
    alignSelf: 'flex-end',
    textAlign: 'right'
  },
  errorContainer: {
    flex: 1,
    //flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  errorTopPart: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  mainTxtError: {
    fontSize: 14,
    fontWeight: '500',
    color: '#FF453A',
    marginLeft: 10,
    textAlign: 'center'
  },
  subTxtError: {
    fontSize: 13,
    fontWeight: '500',
    color: '#FF453A',
    textAlign: 'center'
  },
  blockTitleContainer: {
    width: '86%',
    alignItems: 'flex-start',
    paddingBottom: 4
  },
  blockTitle: {
    color: 'rgba(121, 121, 124, 0.98)'
  },
  transferLoading: {
    width: 90,
    height: 90
  },
});

const currencyTypeSelectStyle = StyleSheet.create({
  container: {
    marginTop: Platform.OS === 'ios' ? 200 : 150,
    marginBottom: Platform.OS === 'ios' ? 400 : 300,
    borderRadius: 10
  },
  confirmText:{
    color: 'white'
  },
  selectToggle:{
    flex: 1,
    width: '100%',
    //alignSelf: 'center',
    //backgroundColor:'transparent',
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingTop: 15,
    paddingLeft: 11
  },
  selectToggleText: {
    width: '100%',
    //height: '100%',
    color: '#F8CE3E',
    fontSize: 16,
    fontWeight: 'bold',
    paddingRight: 4,
    marginLeft: -10,
    paddingRight: 1,
    textAlign: 'center',
    alignSelf: 'center'
  },
  selectedItemText: {
    color: '#674171',
  },
  itemText: {
    textAlign: 'center',
    fontSize: 18
  }
});

const contactsListSelectStyle = StyleSheet.create({
  container: {
    marginTop: 80,
    marginBottom: 80,
    borderRadius: 10
  },
  confirmText:{
    color: 'white'
  },
  selectToggle:{
    width:'100%',
    backgroundColor:'transparent',
    borderRadius: 8,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    paddingLeft: 10,
  },
  selectToggleText: {
    color: 'transparent',
    fontSize: 3
  },
  selectedItemText: {
    color: '#674171',
  }
});
