import React from 'react';
import {Alert, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import DeliveryAddressItem from './DeliveryAddressItem';
import * as Font from 'expo-font';
import {getUserAddressesFromApiWithToken, getShopAddressesFromApiWithId} from '../API/YSMApi';


class DeliveryAddress extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.deliveryWay = this.props.navigation.getParam('deliveryWay');
    this.shopId = this.props.navigation.getParam('shopId');
    this.refreshDeliveryAddress = this.props.navigation.getParam('refreshDeliveryAddress');

    this.state = {
      addresses: [],
      isLoading: false,
      selectedIndex: 0,
      deliveryAddress: this.props.navigation.getParam('deliveryAddress')
    };

    console.log(this.state.deliveryAddress);

  }

  _loadAddresses() {
    this.setState({isLoading: true});
    if(this.deliveryWay === "HOME") {
      getUserAddressesFromApiWithToken(this.token).then(data => {
        this.setState({
          addresses: data,
          isLoading: false
        });
      });
    }
    else {
      getShopAddressesFromApiWithId(this.token, this.shopId).then(data => {
        this.setState({
          addresses: data,
          isLoading: false
        });
      });
    }
  }

  _setSelectedAddress = (deliveryAddress, state) => {
    if(state) {
      this.refreshDeliveryAddress(deliveryAddress);
      this.props.navigation.goBack();
    }
    else {
      this.refreshDeliveryAddress(null);
    }
  }

  _displayAddButton() {
    if(this.deliveryWay === "HOME") {
      return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <TouchableOpacity
          style={styles.btnNewAddress}
          onPress= {() => this._navigateToNewAddressPage("Ajouter")}
        >
          <Text style={styles.textNewAddress}>{"Ajouter une adresses"}</Text>
        </TouchableOpacity>
      </View>
      );
    }
    else
      return null;
  }

  _displayTopTitle() {
    var title;
    switch (this.deliveryWay) {
      case "CLICK & COLLECT":
        title = "Adresse de récupération";
        break;
      case "HOME":
        title = "Votre adresse";
        break;
      default:
        title = "-";
    }
    return(
      <Text style={styles.topTitle}>
          {title}
      </Text>
    );
  }

  _navigateToNewAddressPage = (btnSubmitText, initialAddress) => {
    this.props.navigation.navigate('NewAddress', {token: this.token, btnSubmitText: btnSubmitText, address: initialAddress, onGoBack: () => this._loadAddresses()})
  }


  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
  }

  componentDidMount() {
    this._loadAddresses();
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
          {this._displayTopTitle()}
        </View>
        <View style={styles.mainContainer}>
        <FlatList
          style={styles.listContainer}
          contentContainerStyle={{paddingBottom: 100}}
          ListFooterComponent={this._displayAddButton()}
          data={this.state.addresses}
          extraData={this.state.addresses}
          keyExtractor={(item) => {
            if(this.deliveryWay === "HOME")
              return (item.id).toString();
            else
              return (++this.cptId).toString();
          }}
          renderItem={({item}) => {
            return (<DeliveryAddressItem address={item} type={this.deliveryWay} navigateToNewAddressPage={this._navigateToNewAddressPage} setSelectedAddress={this._setSelectedAddress} currentAddress={this.state.deliveryAddress}/>);
          }}
          onEndReachedThreshold={0.5}
          onRefresh={() => this._loadAddresses()}
          refreshing={this.state.isLoading}
          onEndReached={() => {
            //this._loadAddresses();
          }}
        />
        {/*this._displayLoading()*/}
      </View>
      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171'
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listContainer: {
    width: '98%',
    marginTop: 1,
    paddingTop: 15,
    paddingHorizontal: 10,
    marginHorizontal: 5,
    //backgroundColor: 'white',
    //borderTopLeftRadius: 10,
    //borderTopRightRadius: 10
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:22,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  btnNewAddress: {
    width: 200,
    backgroundColor: '#674171',
    borderRadius: 15,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30
  },
  textNewAddress: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '600'
  }
});

export default DeliveryAddress;
