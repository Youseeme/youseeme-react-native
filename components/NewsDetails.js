import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, ScrollView} from 'react-native';
import NewsItem from './NewsItem';
import * as Font from 'expo-font';
import {getNewsFromApiWithId} from '../API/YSMApi';
import Hyperlink from 'react-native-hyperlink';


class NewsDetails extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.newsId = this.props.navigation.getParam('newsId');

    this.currentPage = 0;
    this.tabValues = ["Tout", "Crédits", "Débits"];
    this.call1 = false;
    this.call2 = false;

    this.state = {
      news: null,
      isLoading: false,
      selectedIndex: 0,
    };

  }

  _loadNews() {
    this.setState({isLoading: true});
    getNewsFromApiWithId(this.newsId).then(data => {
      this.setState({
        news: data,
        isLoading: false
      })
    });
  }

  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='#674171' />
        </View>
      );
    }
  }


  componentDidMount() {
    this._loadNews();
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>

        </View>

          {this.state.news == null ? null :
            <ScrollView
              style={styles.mainContainer}
              contentContainerStyle={{paddingBottom: 100}}
              scrollIndicatorInsets={{ right: 1 }}
            >
            <View style={styles.newsImageContainer}>
              <Image
                style={styles.newsImage}
                source={{uri: this.state.news.image}}
              />
            </View>
            <View style={styles.newsBody}>
              <Text style={styles.newsTitle}>{this.state.news.title}</Text>
              <Hyperlink linkDefault={true} linkStyle={styles.linkStyle}>
                <Text style={styles.newsDescription}>{this.state.news.description}</Text>
              </Hyperlink>
            </View>
            {this._displayLoading()}
          </ScrollView>}

      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.08,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    //backgroundColor: 'rgba(239, 239, 244, 0.85)',
    backgroundColor: 'white',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  newsBody: {
    paddingVertical: 3,
    paddingHorizontal: 20
  },
  newsImageContainer: {
    marginTop: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  newsImage: {
    width: 200,
    height: 200,
    borderRadius: 20,
    resizeMode: 'contain',
  },
  newsTitle: {
    fontSize:25,
    fontWeight: 'bold',
    color: '#674171',
    marginTop:10,
    marginBottom:15,
    fontFamily: "SciFly",
    textAlign: 'center'
  },
  newsDescription: {
    fontSize:18,
    color: "grey",
    marginTop:10,
    fontFamily: "SciFly",
    textAlign: 'justify'
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  linkStyle: {
    color: '#2980b9',
    fontSize: 20
  }
});

export default NewsDetails;
