import React from 'react';
import { StyleSheet, View,ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView } from 'react-native';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

export default class RegisterPart2 extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            email: this.props.navigation.state.params.email,
            name: this.props.navigation.state.params.name,
            lastname:this.props.navigation.state.params.lastname,
            tel:this.props.navigation.state.params.tel,
            pincode: '',
            pass: '',
            passVerif: '',
            error: ''
        };

    }
    componentDidMount() {
        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });
    }

    Register() {
        let regnumber = /^(0|08|08[0-9]{1,9})$/;

        if(this.state.pass.length < 6 || this.state.pincode.length  < 4) {
            if (this.state.pincode >= 4 ) {
            }
            else {
                this.setState({error: "Le code pin n'est pas valide.$Rappel : 4 chiffres minimum"})
            }
            if (this.state.pass.length >= 6 ) {
            }
            else {
                this.setState({error: "Le mot de passe n'est pas valide.$Rappel : 6 caractères minimum"})

            }
            if (this.state.pass === this.state.passVerif ) {
            }
            else {
                this.setState({error: "Les mots de passes ne correspondent pas."})

            }
        }
        else {
            this.props.navigation.navigate('Four', {name: this.state.name, lastname: this.state.lastname,
                email: this.state.email,
                tel: this.state.tel, pass: this.state.pass, pincode:this.state.pincode, passVerif: this.state.passVerif})
        }
    }

    render() {
        return (
            <ImageBackground   source={require('../assets/fond.png')} style={{width: '100%', height: '100%'}}>
              <View style={styles.topHeadband}>
                <View style={{width: '100%', height: '55%'}}>
                  <Image
                      style={styles.logo}
                      source={require('../assets/logo.png')}
                  />
                </View>
                <Text style={styles.textTop}>{"Notre premier secret…"}</Text>
              </View>
              <ScrollView
                style={{flex: 0.9}}
                containerStyle={{paddingBottom: 10}}
              >
                <View style={[styles.container, { flex: 3.2}]}>
                  <View style={styles.entryContainer}>
                    <Text style={styles.entryDescriptionText}>{"Mot de passe :"}</Text>
                    <Input
                      style={styles.input}
                      placeholder="Mot de passe"
                      autoCompleteType={"password"}
                      password={true}
                      viewPass={true}
                      right
                      borderless
                      family="antdesign"
                      fontSize={18}
                      iconSize={25}
                      iconColor="rgba(0, 0, 0, 0.7)"
                      selectionColor={'#674171'}
                      returnKeyType='done'
                      onChangeText={(pass) => this.setState({pass})}
                      value={this.state.pass}
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      color={'black'}
                    />
                  </View>
                  <View style={styles.entryContainer}>
                    <Text style={styles.entryDescriptionText}>{"Confimer mot de passe :"}</Text>
                    <Input
                      style={styles.input}
                      placeholder="Mot de passe"
                      autoCompleteType={"password"}
                      password={true}
                      viewPass={true}
                      right
                      borderless
                      family="antdesign"
                      fontSize={18}
                      iconSize={25}
                      iconColor="rgba(0, 0, 0, 0.7)"
                      selectionColor={'#674171'}
                      returnKeyType='done'
                      onChangeText={(passVerif) => this.setState({passVerif})}
                      value={this.state.passVerif}
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      color={'black'}
                    />
                  </View>
                  <View style={styles.entryContainer}>
                    <Text style={styles.entryDescriptionText}>{"Code pin :"}</Text>
                    <Input
                      style={styles.input}
                      placeholder="Code pin à 4 chiffres"
                      right
                      password={true}
                      viewPass={true}
                      borderless
                      type='number-pad'
                      maxLength={4}
                      family="antdesign"
                      fontSize={18}
                      iconSize={25}
                      selectionColor={'#674171'}
                      selectionColor={'#674171'}
                      iconColor="rgba(0, 0, 0, 0.7)"
                      returnKeyType='done'
                      onChangeText={(pincode) => this.setState({pincode})}
                      value={this.state.pincode}
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      color={'black'}
                    />
                    <Text style={styles.txtCodePinInfo}>{"Ce code vous sera utile lorsque vous effectuerez vos achats."}</Text>
                  </View>
                  {this.state.error !== "" ? <View style={styles.errorContainer}>
                  <View style={styles.errorTopPart}>
                    <Ionicons
                      name="ios-warning"
                      style={styles.errorIcon}
                    />
                    <Text style={styles.mainTxtError}>{this.state.error.split("$")[0]}</Text>
                  </View>
                    <Text style={styles.subTxtError}>{this.state.error.split("$")[1]}</Text>
                  </View> : null}
                </View>
                <View style={{flex: 1}}>
                    <View>
                        <View
                          style={{
                              marginTop:2,
                              justifyContent: 'center',
                              alignItems: 'center',
                          }}
                        >
                          <TouchableOpacity
                              style={styles.btnNext}
                              onPress={()=>this.Register() }
                          >
                            <Text style={styles.txtBtnNext}>Suivant</Text>
                          </TouchableOpacity>
                      </View>
                  </View>
                </View>
              </ScrollView>
            </ImageBackground>
        );
    }
}




const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,
    marginTop: 55,
    marginBottom: 20
  },
  container: {
      height: '100%',
      flex: 3,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
  },
  input: {
      color:'black',
      backgroundColor:'rgba(255, 255, 255, 0.4)',
      borderRadius: 8,
      height: 50,
      marginTop: -8,
      width: '100%',
  },
  btnNext: {
    backgroundColor: '#F8CE3E',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 28,
    paddingVertical: 14,
    borderRadius: 30,
    marginTop: 25,
    marginBottom: 50
  },
  txtBtnNext: {
    color: '#674171',
    fontSize: 18,
    fontWeight: '700',
  },
  logo: {
      width: '100%',
      height: '100%',
      resizeMode: 'contain'
  },
  arrowleft: {
      marginTop: 22,
      marginLeft: 10,
  },
  entryContainer: {
    width:'80%',
    //backgroundColor: 'red',
    marginTop: 15
  },
  entryBackground: {
    width:'100%',
    backgroundColor:'rgba(255, 255, 255, 0.4)',
    borderRadius: 8,
    height: 50,
  },
  entryInput: {
    flex: 1,
    color:'black',
    paddingLeft: 10,
  },
  entryDescriptionText: {
    color: 'white',
    fontWeight: '600',
    marginBottom: 8
  },
  errorContainer: {
    width: '98%',
    //flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  errorTopPart: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  mainTxtError: {
    fontSize: 15,
    fontWeight: '500',
    color: '#FF453A',
    marginLeft: 10,
    textAlign: 'center'
  },
  subTxtError: {
    fontSize: 13,
    fontWeight: '500',
    color: '#FF453A',
    textAlign: 'center'
  },
  textTop: {
    color: '#F8CE3E',
    fontSize: 30,
    fontFamily: 'SciFly',
    marginTop: 10,
    marginBottom: 5
  },
  txtCodePinInfo: {
    fontSize: 12,
    fontWeight: '500',
    color: 'rgba(255, 255, 255, 0.65)',
    paddingHorizontal: 4
  }
});
