import React from 'react';
import { StyleSheet, View, ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView, KeyboardAvoidingView } from 'react-native';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import * as Font from "expo-font";
import { Ionicons } from '@expo/vector-icons';

export default class RegisterPart1 extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            name: '',
            lastname:'',
            tel:'',
            error: ''
        };

    }

    componentDidMount() {
        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });


    }
    Register() {

        let regtel = /^(0|08|08[0-9]{1,9})$/;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if(this.state.name.replace(/ /g,"").length < 1) {
          this.setState({error: "Le prénom n'est pas valide."});
        }
        else if(this.state.lastname.replace(/ /g,"").length < 1) {
          this.setState({error: "Le nom n'est pas valide."});
        }
        else if(this.state.lastname.replace(/ /g,"").toLocaleLowerCase() == this.state.name.replace(/ /g,"").toLocaleLowerCase()) {
          this.setState({error: "Le nom et le prénom ne doivent pas être identiques."});
        }
        else if(this.state.email.replace(/ /g,"").length < 1 || !this._validateEmail()) {
          this.setState({error: "L'adresse mail n'est pas valide."});
        }
        else if(this.state.tel.replace(/ /g,"").length < 10) {
          this.setState({error: "Le téléphone n'est pas valide."});
        }
        else {
            this.setState({error: ""});
            this.props.navigation.navigate('Tree', {
              name: this.state.name,
              lastname: this.state.lastname,
              email: this.state.email,
              tel: this.state.tel
            });
        }
    }

        _validateEmail() {
          var regex = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
          return regex.test(this.state.email);
        };


    render() {
        return (
            <ImageBackground source={require('../assets/fond.png')} style={{width: '100%', height: '100%'}}>
              <View style={styles.topHeadband}>
                <View style={{width: '100%', height: '55%'}}>
                  <Image
                      style={styles.logo}
                      source={require('../assets/logo.png')}
                  />
                </View>
                <Text style={styles.textTop}>{"Il y a un début à tout…"}</Text>
              </View>
              <KeyboardAvoidingView
                behavior={Platform.OS == "ios" ? "padding" : "height"}
                style={{flex: 1}}
              >
                <ScrollView
                  style={{flex: 0.9}}
                  containerStyle={{paddingBottom: 10}}
                >
                  <View style={[styles.container, { flex: 3.2}]}>
                    <View style={styles.entryContainer}>
                      <Text style={styles.entryDescriptionText}>{"Prénom :"}</Text>
                      <View style={styles.entryBackground}>
                        <TextInput
                          ref={ref => this.firstNameInput = ref}
                          autoCompleteType={'off'}
                          style={styles.entryInput}
                          maxLength={200}
                          placeholder="Votre prénom"
                          fontSize={18}
                          textAlign='left'
                          placeholderTextColor={'rgb(100, 95, 107)'}
                          selectionColor={'#674171'}
                          blurOnSubmit={false}
                          returnKeyType='next'
                          onChangeText={(name) => this.setState({name})}
                          onSubmitEditing={() => this.lastNameInput.focus()}
                          />
                        </View>
                      </View>
                      <View style={styles.entryContainer}>
                        <Text style={styles.entryDescriptionText}>{"Nom :"}</Text>
                        <View style={styles.entryBackground}>
                          <TextInput
                            ref={ref => this.lastNameInput = ref}
                            autoCompleteType={'off'}
                            style={styles.entryInput}
                            maxLength={200}
                            placeholder="Votre nom"
                            fontSize={18}
                            textAlign='left'
                            placeholderTextColor={'rgb(100, 95, 107)'}
                            selectionColor={'#674171'}
                            blurOnSubmit={false}
                            returnKeyType='next'
                            onChangeText={(lastname) => this.setState({lastname})}
                            onSubmitEditing={() => this.emailInput.focus()}
                            />
                          </View>
                        </View>
                        <View style={styles.entryContainer}>
                          <Text style={styles.entryDescriptionText}>{"Adresse mail :"}</Text>
                          <View style={styles.entryBackground}>
                            <TextInput
                              ref={ref => this.emailInput = ref}
                              autoCompleteType={'email'}
                              keyboardType={"email-address"}
                              style={styles.entryInput}
                              autoCapitalize={'none'}
                              maxLength={200}
                              placeholder="Votre email"
                              fontSize={18}
                              textAlign='left'
                              placeholderTextColor={'rgb(100, 95, 107)'}
                              selectionColor={'#674171'}
                              blurOnSubmit={false}
                              returnKeyType='next'
                              onChangeText={(email) => this.setState({email})}
                              onSubmitEditing={() => this.phoneInput.focus()}
                              />
                            </View>
                          </View>
                          <View style={styles.entryContainer}>
                            <Text style={styles.entryDescriptionText}>{"Téléphone :"}</Text>
                            <View style={styles.entryBackground}>
                              <TextInput
                                ref={ref => this.phoneInput = ref}
                                autoCompleteType={'tel'}
                                style={styles.entryInput}
                                keyboardType={'phone-pad'}
                                maxLength={10}
                                placeholder="Votre numéro"
                                fontSize={18}
                                textAlign='left'
                                placeholderTextColor={'rgb(100, 95, 107)'}
                                selectionColor={'#674171'}
                                returnKeyType='done'
                                onChangeText={(tel) => this.setState({tel})}
                                onSubmitEditing={() => this.phoneInput.focus()}
                              />
                            </View>
                          </View>
                      {this.state.error !== "" ?
                      <View style={styles.errorContainer}>
                        <Ionicons
                          name="ios-warning"
                          style={styles.errorIcon}
                        />
                        <Text style={styles.txtError}>{this.state.error}</Text>
                      </View> : null}
                    </View>
                    <View style={{flex: 2}}>
                      <View>
                        <View
                          style={{
                              marginTop:2,
                              justifyContent: 'center',
                              alignItems: 'center'
                          }}
                        >
                          <TouchableOpacity
                              style={styles.btnNext}
                              onPress={()=>this.Register()}
                          >
                            <Text style={styles.txtBtnNext}>Suivant</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </ScrollView>
              </KeyboardAvoidingView>
            </ImageBackground>
        );
    }
}




const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,
    marginTop: 55,
    marginBottom: 20
  },
  container: {
      height: '100%',
      flex: 3,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
  },
  input: {
      fontSize:   60,
      color:'black',
      backgroundColor:'#FFFFFF',
      borderRadius: 8,
      opacity: 0.4,
      height: 50,
      marginTop: -8,
      width:250,
  },
  btnNext: {
    backgroundColor: '#F8CE3E',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 28,
    paddingVertical: 14,
    borderRadius: 30,
    marginTop: 25,
    marginBottom: 50
  },
  txtBtnNext: {
    color: '#674171',
    fontSize: 18,
    fontWeight: '700',
  },
  logo: {
      width: '100%',
      height: '100%',
      resizeMode: 'contain'
  },
  arrowleft: {
      marginTop: 22,
      marginLeft: 10,
  },
  entryContainer: {
    width:'80%',
    //backgroundColor: 'red',
    marginTop: 25
  },
  entryBackground: {
    width:'100%',
    backgroundColor:'rgba(255, 255, 255, 0.4)',
    borderRadius: 8,
    height: 50,
  },
  entryInput: {
    flex: 1,
    color:'black',
    paddingLeft: 10,
  },
  entryDescriptionText: {
    color: 'white',
    fontWeight: '600',
    marginBottom: 8
  },
  errorContainer: {
    width: '80%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  txtError: {
    fontSize: 16,
    color: '#FF453A',
    marginLeft: 10
  },
  textTop: {
    color: '#F8CE3E',
    fontSize: 30,
    fontFamily: 'SciFly',
    marginTop: 10,
    marginBottom: 5
  }
});
