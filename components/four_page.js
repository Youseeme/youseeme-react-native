import React from 'react';
import { FlatList, ScrollView, StyleSheet, View,ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager } from 'react-native';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import * as Font from 'expo-font';
import {setAccountFromApiWithInformations} from '../API/YSMApi';
import { Ionicons } from '@expo/vector-icons';
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { NavigationActions, StackActions } from 'react-navigation';

export default class RegisterPart3 extends React.Component{
    constructor(props) {
        super(props);
        this.user = {
          firstName: this.props.navigation.state.params.name,
          lastName:this.props.navigation.state.params.lastname,
          email: this.props.navigation.state.params.email,
          phone:this.props.navigation.state.params.tel,
          pincode: this.props.navigation.state.params.pincode,
          password: this.props.navigation.state.params.pass,
          passwordVerification: this.props.navigation.state.params.passVerif,
          birthDate: new Date(),
          civility: 'CIVILITÉ*',
          sponsorCode: ""
        };

        this.birthDay = '';
        this.birthMonth = '';
        this.birthYear = '';

        this.state = {
          error: '',
          sponsorship: false,
          acceptConditions: false
        };

    }
    componentDidMount() {
        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });
    }


    register() {
      var birthDay = this.birthDay.replace(/ /g,"");
      var birthMonth = this.birthMonth.replace(/ /g,"");
      var birthYear = this.birthYear.replace(/ /g,"");
      this.user.birthDate =  birthYear + '-' + birthMonth + '-' + birthDay;

      console.log(this.user);

      if(birthDay.length !== 2 || parseInt(birthDay, 10) < 1 || parseInt(birthDay, 10) > 31) {
        this.setState({error: "Le jour d'anniversaire n'est pas valide."});
        return;
      }
      if(birthMonth.length !== 2 || parseInt(birthMonth, 10) < 1 || parseInt(birthMonth, 10) > 12) {
        this.setState({error: "Le mois d'anniversaire n'est pas valide."});
        return;
      }
      if(birthYear.length !== 4 || parseInt(birthYear, 10) < 1900 || parseInt(birthYear, 10) > (parseInt(new Date().getFullYear(), 10) - 5)) {
        this.setState({error: "L'année d'anniversaire n'est pas valide."});
        return;
      }
      if(!this.state.acceptConditions) {
        this.setState({error: "Vous n'avez pas accepté les CGU"});
        return;
      }

      setAccountFromApiWithInformations(this.user).then(data => {
        console.log("=====RETURN=====");
        console.log(data);
        if(data.error != null && data.error.message != null) {
          if(new RegExp('The email has already been taken', 'i').test(data.error.message))
            this.setState({error: "L'adresse email est déja utilisée"});
          else if(new RegExp('The mobile number has already been taken', 'i').test(data.error.message))
            this.setState({error: "Le numéro de téléphone est déja utilisé"});
          else
            this.setState({error: "Un problème est survenu lors de l'enregistrement, veuillez réessayer ultérieurement."});
        }
        else if(data.is_verified != null) {
          this.props.navigation.dispatch(StackActions.reset({
            index:0,
            actions:[
              NavigationActions.navigate({
                routeName:'KycRegistration',
                params: {
                  data: data
                }
              })
            ]
          }));
        }
      });
    }

    render() {
        //const { shift } = this.state;
        return (
          <ImageBackground   source={require('../assets/fond.png')} style={{width: '100%', height: '100%'
            }}>
            <View style={styles.topHeadband}>
              <View style={{width: '100%', height: '55%'}}>
                <Image
                    style={styles.logo}
                    source={require('../assets/logo.png')}
                />
              </View>
              <Text style={styles.textTop}>{"On y est presque !"}</Text>
            </View>
              <ScrollView style={styles.safeAccess}>
                <View style={styles.container}>
                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Date de naissance :"}</Text>
                    <View style={styles.birthDateContainer}>
                    <View style={styles.birthDateBackground}>
                        <TextInput
                            ref={ref => this.birthDayEntry = ref}
                            style={styles.birthDate}
                            autoFocus={true}
                            maxLength={2}
                            placeholder="JJ"
                            fontSize={18}
                            textAlign='center'
                            placeholderTextColor={'rgb(100, 95, 107)'}
                            selectionColor={'#674171'}
                            keyboardType={'numeric'}
                            returnKeyType='done'
                            onChangeText={(birthDay) => {
                              if(birthDay.length >= 2)
                                this.birthMonthEntry.focus();
                              this.birthDay = birthDay
                            }}
                        />
                        </View>
                        <View style={styles.birthDateBackground}>
                        <TextInput
                            ref={ref => this.birthMonthEntry = ref}
                            style={styles.birthDate}
                            maxLength={2}
                            placeholder="MM"
                            fontSize={18}
                            textAlign='center'
                            placeholderTextColor={'rgb(100, 95, 107)'}
                            selectionColor={'#674171'}
                            keyboardType={'numeric'}
                            returnKeyType='done'
                            onChangeText={(birthMonth) => {
                              if(birthMonth.length >= 2)
                                this.birthYearEntry.focus();
                              this.birthMonth = birthMonth
                            }}
                        />
                        </View>
                        <View style={styles.birthDateBackground}>
                        <TextInput
                            ref={ref => this.birthYearEntry = ref}
                            style={styles.birthDate}
                            maxLength={4}
                            placeholder="YYYY"
                            fontSize={18}
                            textAlign='center'
                            placeholderTextColor={'rgb(100, 95, 107)'}
                            selectionColor={'#674171'}
                            keyboardType={'numeric'}
                            returnKeyType='done'
                            onChangeText={(birthYear) => {
                              /*if(birthYear.length >= 4)
                                this.sponsorCodeEntry.focus();
                              else*/
                              this.birthYear = birthYear
                            }}
                        />
                        </View>
                        </View>
                        </View>
                        <View style={styles.cardCheckbox}>
                          <BouncyCheckbox
                            borderColor='transparent'
                            fillColor='#F8CE3E'
                            unfillColor='rgba(255, 255, 255, 0.4)'
                            iconColor={this.state.sponsorship ? 'white' : 'transparent'}
                            checkboxSize={25}
                            textStyle={{
                              fontSize: 14,
                              fontWeight: '500',
                              color: 'white',
                              marginLeft: -6
                            }}
                            disableTextDecoration={true}
                            borderRadius={6}
                            text="Possédez vous un code de parrainage ?"
                            onPress={() => this.setState({sponsorship: !this.state.sponsorship})}
                          />
                        </View>
                        {this.state.sponsorship ?
                        <View style={styles.entryContainer}>
                          <Text style={styles.entryDescriptionText}>{"Code de parrainage :"}</Text>
                          <View style={styles.sponsorCodeBackground}>
                            <TextInput
                                ref={ref => this.sponsorCodeEntry = ref}
                                style={styles.sponsorCode}
                                placeholder="Code Parrain"
                                returnKeyType='done'
                                fontSize={18}
                                placeholderTextColor={'rgb(100, 95, 107)'}
                                selectionColor={'#674171'}
                                onChangeText={(sponsorCode) => {
                                  this.user.sponsorCode = sponsorCode
                                }}
                            />
                          </View>
                        </View> : null}
                        <View style={[styles.cardCheckbox, {flexDirection: 'row'}]}>
                          <BouncyCheckbox
                            borderColor='transparent'
                            fillColor='#F8CE3E'
                            unfillColor='rgba(255, 255, 255, 0.4)'
                            iconColor={this.state.acceptConditions ? 'white' : 'transparent'}
                            checkboxSize={25}
                            textStyle={{
                              fontSize: 14,
                              fontWeight: '500',
                              color: 'white',
                              marginLeft: -6
                            }}
                            disableTextDecoration={true}
                            borderRadius={6}
                            text="J'ai lu et j'accepte les CGU"
                            onPress={() => this.setState({acceptConditions: !this.state.acceptConditions})}
                          />
                          <TouchableOpacity
                            style={styles.btnReadConditions}
                            onPress={() => this.props.navigation.navigate('Browser', {url: "https://www.youseeme.pro/cgu/", name: "CGU & RGPD"})}
                          >
                            <Text style={styles.textBtnReadConditions}>{"Lire"}</Text>
                          </TouchableOpacity>
                        </View>
                        {this.state.error !== "" ? <View style={styles.errorContainer}>
                        <View style={styles.errorTopPart}>
                          <Ionicons
                            name="ios-warning"
                            style={styles.errorIcon}
                          />
                          <Text style={styles.mainTxtError}>{this.state.error.split("$")[0]}</Text>
                        </View>
                          <Text style={styles.subTxtError}>{this.state.error.split("$")[1]}</Text>
                        </View> : null}
                </View>
                <View style={{flex: 1}}>
                    <View >
                        <View
                          style={{
                              marginTop:2,
                              justifyContent: 'center',
                              alignItems: 'center',
                          }}
                        >
                          <TouchableOpacity
                              style={styles.btnSend}
                              onPress={()=>this.register() }
                          >
                            <Text style={styles.txtBtnSend}>{"S'inscrire"}</Text>
                          </TouchableOpacity>
                      </View>
                  </View>
                </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}




const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,
    marginTop: 55,
    marginBottom: 20
  },
    container: {
        flex: 1,
        marginTop: 40,
        marginBottom: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    entryContainer: {
      width:'80%',
      marginTop: 5
    },
    birthDateContainer: {
      width: '100%',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 20

    },
    entryDescriptionText: {
      color: 'white',
      fontWeight: '600',
      marginBottom: 8
    },
    sponsorCode: {
      flex: 1,
      width: '100%',
      color:'black'
    },
    textStyleforget: {
      color: "#F8CD3E",
    },
    btnSend: {
      backgroundColor: '#F8CE3E',
      alignItems: 'center',
      justifyContent: 'center',
      paddingHorizontal: 28,
      paddingVertical: 14,
      borderRadius: 30,
      marginTop: 25,
      marginBottom: 50
    },
    txtBtnSend: {
      color: '#674171',
      fontSize: 18,
      fontWeight: '700',
    },
    logo: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    textStyle: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 7,
        paddingBottom: 7,
        fontFamily: "SciFly"
    },

    arrowleft: {
        marginTop: 22,
        marginLeft: 10,
    },
    sponsorCodeBackground: {
      width: '100%',
      height: 50,
      color:'black',
      backgroundColor: 'rgba(255, 255, 255, 0.4)',
      borderRadius: 8,
      //justifyContent: 'center',
      //alignItems: 'flex-start',
      paddingLeft: 10,
      paddingRight: 5
    },
    birthDateBackground: {
      width:80,
      backgroundColor: 'rgba(255, 255, 255, 0.4)',
      borderRadius: 8,
      height: 50,
    },
    birthDate: {
      flex: 1,
      color: 'black',
    },
    safeAccess: {
      flex: 0.9,
    },
    textTop: {
      color: '#F8CE3E',
      fontSize: 30,
      fontFamily: 'SciFly',
      marginTop: 10,
      marginBottom: 5
    },
    errorContainer: {
      width: '98%',
      //flexDirection: 'row',
      marginTop: 15,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: 20
    },
    errorTopPart: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    errorIcon: {
      fontSize: 24,
      color: '#FF453A',
    },
    mainTxtError: {
      fontSize: 15,
      fontWeight: '500',
      color: '#FF453A',
      marginLeft: 10,
      textAlign: 'center'
    },
    subTxtError: {
      fontSize: 13,
      fontWeight: '500',
      color: '#FF453A',
      textAlign: 'center'
    },
    textTop: {
      color: '#F8CE3E',
      fontSize: 30,
      fontFamily: 'SciFly',
      marginTop: 10
    },
    cardCheckbox: {
      width: '83%',
      marginTop: 5,
      marginLeft: 0
    },
    btnReadConditions: {
      height: 20,
      justifyContent: 'center',
      alignItems: 'center',
      alignSelf: 'center',
      backgroundColor: '#F8CE3E',
      borderRadius: 15,
      paddingHorizontal: 10
    },
    textBtnReadConditions: {
      color: 'white',
      fontWeight: 'bold',
      fontSize: 11,
      textAlign: 'center'
    }
});
