import React from 'react';
import {
  AsyncStorage,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

import AppIntro from './appIntro/AppIntro';
import Swiper from 'react-native-swiper';
import {getLoginFromApiWithAccountCredentials} from '../API/YSMApi';
import { NavigationActions, StackActions } from 'react-navigation';

export default class Intro extends React.Component {
  constructor(props) {
      super(props);

      this.state = {
          fontLoaded: false,
      };
  }

  _login(email, password) {
    getLoginFromApiWithAccountCredentials(email, password).then((data) => {
      if(data.is_banned == null) {
        this.props.navigation.dispatch(StackActions.reset({
          index:0,
          actions:[
            NavigationActions.navigate({
              routeName:'Dashboard',
              params: {
                data: data
              }
            })
          ]
        }));
      }
    });
  }

  async componentDidMount() {
    AsyncStorage.getItem('credentials').then((data) => {
      if(data != null) {
        const credentials = JSON.parse(data);
        this._login(credentials.email, credentials.password);
      }
    });
    await Expo.Font.loadAsync({
      'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.setState({ fontLoaded: true });
  }

  render() {
    if(this.state.fontLoaded)
    return (
      <AppIntro
        skipBtnLabel={"Passer"}
        doneBtnLabel={"Finir"}
        onSkipBtnClick={() => this.props.navigation.navigate('First')}
        onDoneBtnClick={() => this.props.navigation.navigate('First')}
      >
        <View style={[styles.slide,{ backgroundColor: '#674171' }]}>
          <View style={styles.topTextContainer} level={-18}><Text style={[styles.text1, {color: '#F8CE3E'}]}>{"Trouve tes commerçants favoris"}</Text></View>
          <View style={{width: '100%', height: '60%'}}>
            <View style={{position: 'absolute', bottom: -90, right: -60, width: '140%', height: '140%'}} level={10}><Image style={styles.image} source={require('../assets/intro1Shop.png')} /></View>
            <View style={{position: 'absolute', bottom: -75, left: -100, width: '145%', height: '145%'}} level={16}><Image style={styles.image} source={require('../assets/intro1Bag.png')} /></View>
            <View style={{position: 'absolute', bottom: -64, right: 3, width: '66%', height: '120%'}} level={25}><Image style={[styles.image]} source={require('../assets/intro1Splash.png')} /></View>
            <View style={{position: 'absolute', bottom: -57, right: -50, width: '125%', height: '125%'}} level={25}><Image style={styles.image} source={require('../assets/intro1Phone.png')} /></View>
            <View style={{position: 'absolute', bottom: -25, right: -5, width: '125%', height: '125%'}} level={8}><Image style={styles.image} source={require('../assets/intro1Board.png')} /></View>
            <View style={{position: 'absolute', bottom: -25, right: -5, width: '125%', height: '125%'}} level={-4}><Image style={styles.image} source={require('../assets/intro1Percentage.png')} /></View>
          </View>
          <View style={styles.botomTextContainer} level={-18}><Text style={[styles.text1, {color: '#F8CE3E'}]}>{"Achète et paie en un clic"}</Text></View>
        </View>
        <View style={[styles.slide, { backgroundColor: '#ec4673' }]}>
          <View style={styles.topTextContainer} level={-4}><Text style={[styles.text1, {color: '#F8CE3E'}]}>{"Récupère tes commandes facilement"}</Text></View>
          <View style={{width: '100%', height: '60%'}}>
            <View style={{position: 'absolute', bottom: -28, right: -8, width: '100.7%', height: '120%'}} level={20}><Image style={[styles.image, {resizeMode: 'cover'}]} source={require('../assets/intro2Map.png')} /></View>
            <View style={{position: 'absolute', bottom: -28, left: 5.5, width: '100.7%', height: '119.9%'}} level={20}><Image style={[styles.image, {resizeMode: 'cover'}]} source={require('../assets/intro2Girl.png')} /></View>
            <View style={{position: 'absolute', bottom: 20, right: -9, width: '100.7%', height: '101.5%'}} level={-15}><Image style={[styles.image, {resizeMode: 'cover'}]} source={require('../assets/intro2DeliveryMan.png')} /></View>
          </View>
          <View style={styles.botomTextContainer} level={14}><Text style={[styles.text1, {color: '#F8CE3E'}]}>{"en boutique ou à domicile"}</Text></View>
        </View>
        <View style={[styles.slide,{ backgroundColor: '#F8CE3E' }]}>
          <View style={styles.topTextContainer} level={8}><Text style={[styles.text1, {color: '#674171'}]}>{"Envoie rapidement de l’argent ou des Bartcoins"}</Text></View>
          <View style={{width: '100%', height: '60%'}}>
            <View style={{position: 'absolute', top: '-1%', right: 51, width: '65%', height: '65%'}} level={-3}><Image style={styles.image} source={require('../assets/intro3fast.png')} /></View>
            <View style={{position: 'absolute', bottom: -25, right: -42, width: '115%', height: '115%'}} level={-20}><Image style={styles.image} source={require('../assets/intro3Clouds.png')} /></View>
            <View style={{position: 'absolute', bottom: -45, right: -30, width: '120%', height: '120%'}} level={25}><Image style={styles.image} source={require('../assets/intro3Friends.png')} /></View>
          </View>
          <View style={styles.botomTextContainer} level={-8}><Text style={[styles.text1, {color: '#674171'}]}>{"d’un simple clic et gratuitement"}</Text></View>
        </View>
        <View style={[styles.slide,{ backgroundColor: '#40BCF8' }]}>
          <View style={styles.topTextContainer} level={8}><Text style={[styles.text1, {color: 'white'}]}>{"Gagne un max de Bartcoins"}</Text></View>
          <View style={{width: '100%', height: '60%'}}>
            <View style={{position: 'absolute', bottom: -45, left: -15, width: '120%', height: '120%'}} level={25}><Image style={styles.image} source={require('../assets/intro4Background.png')} /></View>
            <View style={{position: 'absolute', bottom: '-17%', left: -60, width: '140%', height: '140%'}} level={-20}><Image style={styles.image} source={require('../assets/intro4Shop.png')} /></View>
            <View style={{position: 'absolute', top: 25, right: 45, width: '60%', height: '60%'}} level={-5}><Image style={styles.image} source={require('../assets/intro4Coins.png')} /></View>
            <View style={{position: 'absolute', bottom: '-22.5%', left: -100, width: '155%', height: '155%'}} level={20}><Image style={styles.image} source={require('../assets/intro4Gift.png')} /></View>
          </View>
          <View style={styles.botomTextContainer} level={-8}><Text style={[styles.text1, {color: 'white'}]}>{"Utilise-les chez tous nos commerçants"}</Text></View>
        </View>
      </AppIntro>
    );
    else
      return null;
  }
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  topTextContainer: {
    position: 'absolute',
    top: '8%',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  botomTextContainer: {
    position: 'absolute',
    bottom: '11%',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  },
  text1: {
    fontSize: 34,
    fontWeight: '900',
    textAlign: 'center',
    fontFamily: 'SciFly'
  },
  text2: {

  }
});

AppRegistry.registerComponent('Intro', () => Intro);
