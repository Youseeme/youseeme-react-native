import React from 'react';
import {Alert, ImageBackground, StyleSheet, Text, TextInput, View, Button, TouchableOpacity, ScrollView, KeyboardAvoidingView} from 'react-native';
import {getUserProfileFromApiWithAccountToken, contactYouseemeFromApiWithContent} from '../API/YSMApi';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons } from '@expo/vector-icons';
import Root from './RootPopup';
import Toast from './Toast';

export default class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.token = this.props.navigation.getParam('token');
    this.contactMessage = {
      email: "",
      subject: "",
      body: ""
    };

    this.state = {
      errorMessage: "",
    }
  }

  _loadProfile() {
    getUserProfileFromApiWithAccountToken(this.token).then(data => {

    });
  }

  _sendMessage() {
    if(this.contactMessage.subject.replace(/ /g,"").length > 0) {
      if(this.contactMessage.body.replace(/ /g,"").length > 0) {
        getUserProfileFromApiWithAccountToken(this.token).then(data1 => {
          const regex = new RegExp('Contact email sent', 'i');
          if(data1 != null && data1.email != null) {
            this.contactMessage.email = data1.email;
            contactYouseemeFromApiWithContent(this.token, this.contactMessage).then(data2 => {
              console.log(data2);
              if(data2 != null && regex.test(data2.feedback))
                this._showToast('E-mail de contact', 'Le message a bien été envoyé.', true);
              else
                this._showToast('E-mail de contact', 'Un problème est survenu lors de l\'envoi, veuillez réessayer ultérieurement.', false);
            })
          }
          else
            this._showToast('E-mail de contact', 'Un problème est survenu lors de l\'envoi, veuillez réessayer ultérieurement.', false);
        });
      }
      else {
        this.setState({
          errorMessage: "Le contenu du message n'est pas valide.$Rappel : Le message ne doit pas être vide."
        });
      }
    }
    else {
      this.setState({
        errorMessage: "Le sujet n'est pas valide.$Rappel : Le message ne doit pas être vide."
      });
    }
  }

  _confirmationRequest(title, message, titleCancel, messageCancel, action) {
    Alert.alert(
      title,
      message,
      [
        {
          text: "Non",
          onPress: () => {
            this._loadProfile();
            this._showToast(titleCancel, messageCancel, false);
          }
        },
        {
          text: "Oui",
          onPress: () => action()
        }
      ],
      {
        cancelable: false
      }
    );
  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
          title: title,
          text: message,
          color: '#2ecc71',
          icon:(<Ionicons
              name="ios-checkmark"
              style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
            />)
      });
    }
    else {
      Toast.show({
          title: title,
          text: message,
          color: '#FF453A',
          icon:(<Ionicons
              name="ios-close"
              style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
            />)
      });
    }
  }


  render() {
    return (
      <Root>
        <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
          <View style={styles.topHeadband}>
            <Text style={styles.headerTitleStyle}></Text>
          </View>
          <KeyboardAvoidingView>
          <ScrollView
            contentContainerStyle={{paddingTop: 1, paddingBottom: 30}}
          >
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start', marginTop: 1}}>
              <View style={styles.entryContainer}>
                <Text style={styles.entryDescriptionText}>{"Sujet  :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.subjectEntry = ref}
                      autoCompleteType={'off'}
                      style={styles.textEntry}
                      maxLength={200}
                      placeholder="Le sujet du message"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      blurOnSubmit={false}
                      onSubmitEditing={() => this.bodyEntry.focus()}
                      onChangeText={(subject) => {
                        this.contactMessage.subject = subject;
                      }}
                  />
                </View>
              </View>
              <View style={styles.entryContainer}>
                <Text style={styles.entryDescriptionText}>{"Contenu du message  :"}</Text>
                <View style={[styles.entryBackground, {height: 300, justifyContent: 'flex-start', alignItems: 'flex-start'}]}>
                  <TextInput
                      ref={ref => this.bodyEntry = ref}
                      multiline={true}
                      autoCompleteType={'off'}
                      style={[styles.textEntry, {paddingTop: 10, paddingRight: 10, width: '100%', alignSelf: 'flex-start', textAlignVertical: 'top'}]}
                      maxLength={5000}
                      placeholder="Votre message"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      onChangeText={(message) => {
                        this.contactMessage.body = message;
                      }}
                  />
                </View>
              </View>
              {this.state.errorMessage !== "" ? <View style={styles.errorContainer}>
              <View style={styles.errorTopPart}>
                <Ionicons
                  name="ios-warning"
                  style={styles.errorIcon}
                />
                <Text style={styles.mainTxtError}>{this.state.errorMessage.split("$")[0]}</Text>
              </View>
                <Text style={styles.subTxtError}>{this.state.errorMessage.split("$")[1]}</Text>
              </View> : null}
              <TouchableOpacity
                style={styles.btnSend}
                onPress={() => this._sendMessage()}
              >
                <Text style={styles.txtBtnSend}>{"Envoyer"}</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </Root>
    )
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginTop: 30
  },
  webPage: {
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  },
  entryContainer: {
    width:'90%',
    //backgroundColor: 'red',
    marginTop: 25
  },
  entryBackground: {
    width:'100%',
    backgroundColor:'rgba(255, 255, 255, 0.4)',
    borderRadius: 8,
    height: 50,
  },
  entryDescriptionText: {
    color: 'white',
    fontWeight: '600',
    marginBottom: 8
  },
  textEntry: {
    flex: 1,
    color:'black',
    paddingLeft: 10,
  },
  btnSend: {
    backgroundColor: '#F8CE3E',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 12,
    borderRadius: 30,
    marginTop: 25,
    marginBottom: 50
  },
  txtBtnSend: {
    color: '#674171',
    fontSize: 18,
    fontWeight: '700'
  },
  errorContainer: {
    flex: 1,
    //flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20
  },
  errorTopPart: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 24,
    color: '#FF453A',
  },
  mainTxtError: {
    fontSize: 14,
    fontWeight: '500',
    color: '#FF453A',
    marginLeft: 10,
    textAlign: 'center'
  },
  subTxtError: {
    fontSize: 13,
    fontWeight: '500',
    color: '#FF453A',
    textAlign: 'center'
  }
});
