import React from 'react';
import {Alert, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View, Platform} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import ArticleItem from './ArticleItem';
import * as Font from 'expo-font';
import {getOrderDetailsFromApiWithId, askRefundFromApiWithOrderId} from '../API/YSMApi';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, Entypo } from '@expo/vector-icons';
import { BlurView } from 'expo-blur';
import Moment from 'react-moment';
import 'moment-timezone';
import Root from './RootPopup';
import Toast from './Toast';
import * as FileSystem from 'expo-file-system';

class OrderDetails extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });

    this.token = this.props.navigation.getParam('token');
    this.orderId = this.props.navigation.getParam('orderId');

    this.state = {
      order: null,
      isLoading: false,
      selectedIndex: 0,
    };

  }

  _loadOrder() {
    this.setState({isLoading: true});
    getOrderDetailsFromApiWithId(this.token, this.orderId).then(data => {
      console.log("@@@@@@");
      console.log(data);
      console.log("@@@@@@");
      this.setState({
        order: data,
        isLoading: false
      });
    });
  }

  _loadBill() {
    this.props.navigation.navigate("PDFViewer", {token: this.token, orderId: this.orderId, name: "Ma facture"});
  }

  _askRefund() {
    this._confirmationRequest("Remboursement", "Êtes vous sur de vouloir effectuer cette demande de remboursement auprès de '" + this.state.order.shop.name + "' ?\nAttention cette demande doit être justifiée.", 'Remboursement', 'La demande de remboursement a bien été annulée.', () => {
      askRefundFromApiWithOrderId(this.token, this.orderId).then(data => {
        const regex = new RegExp('success', 'i');
        console.log(data);
        if(data != null && regex.test(data.message))
          this._showToast('Remboursement', 'La demande de remboursement a bien été envoyée.', true);
        else
          this._showToast('Remboursement', 'Un problème est survenu lors de l\'envoi, veuillez réessayer ultérieurement.', false);
      });
    });
  }

  _confirmationRequest(title, message, cancelTitle, cancelMessage, action) {
    Alert.alert(
      title,
      message,
      [
        {
          text: "Non",
          onPress: () => {
            this._showToast(cancelTitle, cancelMessage, false);
          }
        },
        {
          text: "Oui",
          onPress: () => action()
        }
      ],
      {
        cancelable: false
      }
    );
  }

  _showToast(title, message, success) {
    if(success) {
      Toast.show({
          title: title,
          text: message,
          color: '#2ecc71',
          icon:(<Ionicons
              name="ios-checkmark"
              style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
            />)
      });
    }
    else {
      Toast.show({
          title: title,
          text: message,
          color: '#FF453A',
          icon:(<Ionicons
              name="ios-close"
              style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
            />)
      });
    }
  }


  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='#674171' />
        </View>
      );
    }
  }

  _displayDeliveryInfos() {
    if(this.state.order.delivery_address != "") {
      var title, value;
      if(this.state.order.delivery_type == "click_collect") {
        title = "CLICK & COLLECT : ";
        value = this.state.order.delivery_address;
      }
      else {
        title = "ADRESSE DE LIVRAISON : ";
        value = this.state.order.delivery_address;
      }
        return (
          <View style={styles.orderInfoContainer}>
            <View style={styles.orderInfoIconContainer}>
              <MaterialCommunityIcons
                name={"truck-delivery"}
                style={[styles.deliveryIcon, {fontSize: 28, paddingTop: 4}]}
              />
            </View>
            <View style={styles.orderInfoTextContainer}>
              <Text style={styles.titleOrderInfo}>{title}</Text>
              <Text style={styles.orderInfo} numberOfLines={2}>{value}</Text>
            </View>
          </View>
        );
    }
    else
      return null;
  }

  _displayPaymentMethod() {
    var paymentMethod;
    switch (this.state.order.payment_method) {
      case "wallet":
        paymentMethod = "Porte-monnaie";
        break;
      case "bartcoin":
        paymentMethod = "Bartcoin";
        break;
      case "preset_card":
        paymentMethod = "Carte bancaire enregistrée";
        break;
      case "new_card":
        paymentMethod = "Nouvelle carte bancaire";
        break;
      default:
        paymentMethod = "Porte-monnaie";
    }

    return paymentMethod;
  }

  _displayStatus() {
    var color = "#674171", code = this.state.order.status != null ? this.state.order.status.code : "";
    switch (code) {
      case "NEW":
        color = "#007AFF";
        break;
      case "PENDING":
        color = "#FF9500";
        break;
      case "DELIVERING":
        color = "#34C759";
        break;
      case "FINISHED":
        color = "#34C759";
        break;
      case "CANCELED":
        color = "#FF3B30";
        break;
      default:
        return (
          <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: "#34C759", borderRadius: 20, paddingVertical: 4, paddingHorizontal: 15, alignSelf: 'center'}}>
            <Text style={[styles.textDeliveryState]}>{"TERMINÉE"}</Text>
          </View>
        );
    }
    return (
      <View style={{justifyContent: 'center', alignItems: 'center', backgroundColor: color, borderRadius: 20, paddingVertical: 4, paddingHorizontal: 15, alignSelf: 'center'}}>
        <Text style={[styles.textDeliveryState]}>{this.state.order.status.name.toUpperCase()}</Text>
      </View>
    );
  }

  _displayStatus2() {
    var color = "#674171", code = this.state.order.status != null ? this.state.order.status.code : "";
    switch (code) {
      case "NEW":
        color = "#007AFF";
        break;
      case "PENDING":
        color = "#007AFF";
        break;
      case "DELIVERING":
        color = "#34C759";
        break;
      case "FINISHED":
        color = "#34C759";
        break;
      case "CANCELED":
        color = "#FF3B30";
        break;
      default:
        return <Text style={[styles.orderInfo, {color: color, fontSize: 18}]}>{"INCONNU"}</Text>;
    }
    return (
      <Text style={[styles.orderInfo, {color: color, fontSize: 18}]}>{this.state.order.status.name.toUpperCase()}</Text>
    );
  }

  _displayOrderInfos() {
    return (
      <View style={{flex: 0.5, marginRight: 0}}>
      <View style={styles.orderInfosContainer}>
          <View style={{justifyContent: 'space-between', alignItems: 'flex-start', marginBottom: 8, paddingHorizontal: 10, backgroundColor: 'rgba(103, 65, 113, 0.96)', borderRadius: 15, paddingTop: 12, paddingBottom: 5}}>
          <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <Text style={[styles.titleOrderInfo, {fontSize: 18, color: 'rgba(255, 255, 255, 0.8)'}]}>{"TOTAL : "}</Text>
                <Text style={[styles.orderInfo, {fontSize: 20, color: '#F8CE3E'}]}>{this.state.order.amount.toFixed(2) + " €"}</Text>
              </View>
              <Text style={{color: 'rgba(255, 255, 255, 0.65)', fontSize: 12, fontWeight: '500', marginTop: -3}}>{"- " + this.state.order.discount.toFixed(2) + " €"}</Text>
            </View>
            <View style={{flexDirection: 'row', alignSelf: 'center'}}>
              {/*<Text style={[styles.titleOrderInfo, {fontSize: 18, color: 'rgba(255, 255, 255, 0.8)'}]}>{"STATUT : "}</Text>*/}
              {this._displayStatus()}
            </View>
          </View>
          <View style={{flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'flex-end', marginTop: 14}}>
            <Text style={styles.updateDate}>{"Dernière mise à jour le "}</Text>
            <Moment style={styles.updateDate} element={Text} interval={0} tz="Europe/Paris" format="DD/MM/YYYY à HH:mm:ss">
              {this.state.order.updated_at.substring(0, 10) + " " + this.state.order.updated_at.substring(11, 16) + ":" + this.state.order.updated_at.substring(17, 19)}
            </Moment>
          </View>
        </View>

        <View style={styles.orderInfoContainer}>
          <View style={styles.orderInfoIconContainer}>
            <FontAwesome5
              name={"money-bill"}
              style={[styles.deliveryIcon, {fontSize: 22, paddingTop: 2}]}
            />
          </View>
          <View style={styles.orderInfoTextContainer}>
            <Text style={styles.titleOrderInfo}>{"MOYEN DE PAIEMENT : "}</Text>
            <Text style={styles.orderInfo}>{this._displayPaymentMethod()}</Text>
          </View>
        </View>

        <View style={styles.orderInfoContainer}>
          <View style={styles.orderInfoIconContainer}>
            <Entypo
              name={"shop"}
              style={[styles.deliveryIcon, {fontSize: 27, paddingTop: 2}]}
            />
          </View>
          <View style={styles.orderInfoTextContainer}>
            <Text style={styles.titleOrderInfo}>{"COMMERCE : "}</Text>
            <Text style={styles.orderInfo} numberOfLines={2}>{this.state.order.shop.name}</Text>
          </View>
        </View>

        <View style={styles.orderInfoContainer}>
          <View style={styles.orderInfoIconContainer}>
            <Ionicons
              name={"ios-time"}
              style={[styles.deliveryIcon, {fontSize: 28, paddingTop: 2}]}
            />
          </View>
          <View style={styles.orderInfoTextContainer}>
            <Text style={styles.titleOrderInfo}>{"DATE DE L'ACHAT : "}</Text>
            <Moment style={styles.orderInfo} element={Text} interval={0} tz="Europe/Paris" format="DD/MM/YYYY à HH:mm:ss">
              {this.state.order.created_at.substring(0, 10) + " " + this.state.order.created_at.substring(11, 16) + ":" + this.state.order.created_at.substring(17, 19)}
            </Moment>
          </View>
        </View>
        {this._displayDeliveryInfos()}
        {/*<View style={styles.orderInfoContainer}>
          <View style={styles.orderInfoIconContainer}>
            <FontAwesome
              name={"shopping-basket"}
              style={{fontSize: 23, color: 'white', paddingTop: 2}}
            />
          </View>
          <View style={styles.orderInfoTextContainer}>
            <Text style={styles.titleOrderInfo}>{"PANIER : "}</Text>
          </View>
        </View>*/}
        </View>
      </View>
    );
  }


  componentDidMount() {
    this._loadOrder();
  }

  render() {
    return (
      <Root>
        <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
          <View style={styles.topHeadband}>
              <Text style={styles.descriptiveText}>
                  {this.state.order != null ? "n°" + this.state.order.id : " - "}
              </Text>
          </View>

          <View style={styles.mainContainer}>
          {this.state.order == null ?
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' color='#674171' />
            </View>
          :
            <View style={{flex: 1, paddingBottom: 10}}>


              <FlatList
                style={styles.listContainer}
                contentContainerStyle={{paddingTop: 5, paddingBottom: 100}}
                scrollIndicatorInsets={{ right: 1 }}
                ListHeaderComponent={this._displayOrderInfos()}
                data={this.state.order.items}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({item}) => <ArticleItem  article={item} />}
                refreshing={this.state.isLoading}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                  //this._loadTransactions();
                }}
              />
              <BlurView intensity={Platform.OS === 'ios' ? 96 : 220} style={styles.btnBottomContainer}>
                <TouchableOpacity
                  style={styles.btnRefund}
                  onPress={() => this._askRefund()}
                >

                  <Text style={styles.textRefund}>{"Remboursement"}</Text>
                  <MaterialCommunityIcons
                    name={"cash-refund"}
                    style={{fontSize: 25, color: 'white'}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.btnBill}
                  //onPress={() => {this.props.navigation.navigate("PDFViewer", {token: this.token})}}
                  onPress={() => {this._loadBill()}}
                >
                  <Text style={styles.textBill}>{"Facture"}</Text>
                  <FontAwesome5
                    name={"file-invoice"}
                    style={{fontSize: 21, color: 'white'}}
                  />
                </TouchableOpacity>
              </BlurView>
            </View>
          }
          {/*this._displayLoading()*/}
          </View>
        </ImageBackground>
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.1,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    //backgroundColor: 'white',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  listContainer: {
    marginTop: 1,
    paddingTop: 15,
    //marginLeft: 2,
    //marginRight: 2,
    paddingLeft: 10,
    paddingRight: 10,
    height: 330,
    flex: 1,
    //flex: 0.42
    //backgroundColor: 'white',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    //borderBottomWidth: 1,
    //borderBottomColor: 'grey'
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  orderInfosContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    marginTop: 1,
    marginBottom: 20
  },
  orderInfoContainer: {
    height: 55,
    width: '95%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginVertical: 10,

    //paddingLeft: 8,
    //borderRadius: 5,
    //borderRightWidth: 2,
    //borderRightColor: '#674171',

    /*width: '100%',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#674171'*/

    marginHorizontal: 8,
    backgroundColor: 'rgba(239, 239, 244, 0.95)',
    borderRadius: 10,

  },
  orderInfoIconContainer: {
    backgroundColor: '#674171',//'#AEAEB2',
    borderRadius: 10,
    marginRight: 8,
    width: 55,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center'
  },
  orderInfoTextContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
    //backgroundColor: 'green'
  },
  descriptiveText: {
    color: 'white',
    alignSelf: 'center',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20,
    paddingTop: 18,
    fontSize: 20
  },
  titleOrderInfo: {
    color: '#8E8E93',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'left',
    fontFamily: "SciFly"
  },
  orderInfo: {
    color: '#674171',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'left',
    fontFamily: "SciFly"
  },
  btnBottomContainer: {
    width: '100%',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: Platform.OS === 'ios' ? 20 : 2,
    zIndex: 1
  },
  btnRefund: {
    flexDirection: 'row',
    backgroundColor: '#FF453A',
    width: 165,
    height: 45,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 5,
    paddingHorizontal: 8
  },
  btnBill: {
    flexDirection: 'row',
    backgroundColor: 'rgba(103, 65, 113, 0.8)',
    width: 165,
    height: 45,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: 5,
    paddingHorizontal: 8
  },
  textRefund: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '600',
    marginRight: 8
  },
  textBill: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '600',
    //marginRight: 8
  },
  deliveryIcon: {
    color: '#F8CE3E'
  },
  textDeliveryState: {
    fontSize: 16,
    color: 'white',
    fontFamily: "SciFly",
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  updateDate: {
    fontSize: 12,
    color: "white",
    textAlign: 'center'
  }
});

export default OrderDetails;
