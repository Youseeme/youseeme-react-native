import React from 'react';
import { AsyncStorage, ActivityIndicator, Animated, StyleSheet, View,ImageBackground, TouchableOpacity, TouchableWithoutFeedback, Button, Image, ScrollView, Linking, Alert, Easing, Dimensions } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import BottomDrawer from 'rn-bottom-drawer';
import MapView from "react-native-maps";
import * as Font from 'expo-font';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { FloatingAction } from "react-native-floating-action";
import { getUserProfileFromApiWithAccountToken, getShopListFromApiWithToken, getShopFromApiWithId, getCategoriesFromApi, getSubCategoriesFromApiWithCategoryId, getWalletInfoFromApiWithToken } from '../API/YSMApi';
import { Ionicons, SimpleLineIcons } from '@expo/vector-icons';
import ConfettiCannon from 'react-native-confetti-cannon';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import NetInfo from '@react-native-community/netinfo';
import Home from './Map';
import CategoriesShop from './CategoriesShop';
import FavoriteShops from './FavoriteShops';


const Tab = createMaterialTopTabNavigator();
const { width, height } = Dimensions.get('screen');

const TAB_BAR_HEIGHT = 0;
const HEADER_HEIGHT = 0;

export default class Dashboard extends React.Component {

  constructor(props) {
    super(props);
    this.spinValue = new Animated.Value(1);
    this.btnQrHeightValue = new Animated.Value(60);
    this.btnQrWidthValue = new Animated.Value(60);
    this.btnQRBottomValue = new Animated.Value(Platform.OS === 'ios' ? 110: 140);
    this.blockDrawerWidthValue = new Animated.Value(55);
    this.imageDrawerWidthValue = new Animated.Value(48);
    this.blockDrawerTopValue = new Animated.Value(0);
    this.blockDrawerBottomValue = new Animated.Value(0);
    this.firstConnectionTopValue = new Animated.Value(100);
    this.textDrawerValue = new Animated.Value(0);

    this.token = this.props.navigation.state.params.data.token,

    this.state = {
      mapRegion: {
        latitude: 48.8534, //Paris
        longitude: 2.3488, //Paris
        latitudeDelta: 0.04864195044303443,
        longitudeDelta: 0.040142817690068,
      },
      hasLocationPermissions: false,
      locationResult: null,
      profileData: this.props.navigation.state.params.data,
      floatingActionPress: false,
      search: '',
      shops: [],
      pickerItems: [],
      largeMap: false,
      firstConnection: this.props.navigation.state.params.firstConnection == null ? false :  this.props.navigation.state.params.firstConnection
    };

    this.actions = [
      {
        text: "Payer",
        icon: require("../assets/pay.png"),
        name: "btn_pay",
        color: "#7b5086",
        size: 35,
        position: 2
      },
      {
        text: "Envoyer la facture",
        icon: require("../assets/bill.png"),
        name: "btn_send_bill",
        color: "#7b5086",
        size: 35,
        position: 1
      }
    ];
  }


  _updateSearch = search => {
    this.setState({ search: search });
  };

  _selector(name) {
    if(name === "btn_pay")
      this.props.navigation.navigate('Scan', {token: this.token});
    else
      this.props.navigation.navigate('TakePicture', {token: this.token});
  }

  _drawerAnim() {
    Animated.parallel (
      [
        Animated.timing(
          this.spinValue,
          {
            toValue: 1,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.btnQRBottomValue,
          {
            toValue: Platform.OS === 'ios' ? 110: 140,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.blockDrawerWidthValue,
          {
            toValue: 55,
            duration: 300,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.blockDrawerTopValue,
          {
            toValue: -5,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.blockDrawerBottomValue,
          {
            toValue: 10,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.imageDrawerWidthValue,
          {
            toValue: 48,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.textDrawerValue,
          {
            toValue: 0,
            duration: 300,
            easing: Easing.linear
          }
        )
      ],
      {
        useNativeDriver: true
      }
    ).start ();
  }

  _drawerAnimBack() {
    Animated.parallel (
      [
        Animated.timing(
          this.spinValue,
          {
            toValue: 0,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.btnQRBottomValue,
          {
            toValue: Platform.OS === 'ios' ? 250: 280,
            duration: 170,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.blockDrawerWidthValue,
          {
            toValue: 70,
            duration: 300,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.blockDrawerTopValue,
          {
            toValue: 0,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.blockDrawerBottomValue,
          {
            toValue: 0,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.imageDrawerWidthValue,
          {
            toValue: 60,
            duration: 400,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.textDrawerValue,
          {
            toValue: 1,
            duration: 300,
            easing: Easing.linear
          }
        )
      ],
      {
        useNativeDriver: true
      }
    ).start ();
  }



  _extend() {
    Animated.parallel (
      [
        Animated.timing(
          this.btnQrWidthValue,
          {
            toValue: 130,
            duration: 20,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.btnQrHeightValue,
          {
            toValue: 190,
            duration: 20,
            easing: Easing.linear
          }
        )
      ],
      {
        useNativeDriver: true
      }
    ).start ();
  }

  _reduce() {
    Animated.parallel (
      [
        Animated.timing(
          this.btnQrWidthValue,
          {
            toValue: 60,
            duration: 20,
            easing: Easing.linear
          }
        ),
        Animated.timing(
          this.btnQrHeightValue,
          {
            toValue: 60,
            duration: 20,
            easing: Easing.linear
          }
        )
      ],
      {
        useNativeDriver: true
      }
    ).start ();
  }

  _skipTutorial() {
    Animated.parallel (
      [
        Animated.timing(
          this.firstConnectionTopValue,
          {
            toValue: -500,
            duration: 500,
            easing: Easing.out(Easing.exp)
          }
        ),
      ],
      {
        useNativeDriver: true
      }
    ).start(() => {
      this.setState({firstConnection: false});
    });
  }

  _showTutorial() {
    Animated.parallel (
      [
        Animated.timing(
          this.firstConnectionTopValue,
          {
            toValue: -500,
            duration: 400,
            easing: Easing.out(Easing.exp)
          }
        ),
      ],
      {
        useNativeDriver: true
      }
    ).start(() => {
      this.setState({firstConnection: false}, () => {
        this.props.navigation.navigate('Browser', {url: "https://www.youseeme.pro/tuto/", name: "Tutorial"});
      });
    });
  }

  _loadProfile() {
    getUserProfileFromApiWithAccountToken(this.token).then(data => {
      this.setState({profileData: data});
    });
  }

  _loadShops() {
    this.setState({isLoading: true});
    getShopListFromApiWithToken(this.token).then(data => {
      this.setState({
        shops: data,
        isLoading: false
      });
    });
  }

  /*_loadPickerItems2() {
      getCategoriesFromApi().then(data1 => {
        var pickerItems = [];
        data1.map((category, index) => {
          getSubCategoriesFromApiWithCategoryId(category.id).then(data2 => {
            data2.map((subCategory, index) => {
              pickerItems.push({label: subCategory.name, value: subCategory.id});
            });
          });
        });
        pickerItems.push({label: "Tout", value: -1});
        return pickerItems;

      }).then((pickerItems) => {
        this.setState({pickerItems: pickerItems});
      });
  }*/

  _loadPickerItems() {
      getCategoriesFromApi().then(data1 => {
        var pickerItems = [];
        data1.map((category, index) => {
          getSubCategoriesFromApiWithCategoryId(category.id).then(data2 => {
            //data2.map((subCategory, index) => {

            pickerItems.push({name: category.name, id: category.id, children: data2});
            //});
          });
        });
        pickerItems.push({name: "Tout", id: -1, children: []});
        return pickerItems;

      }).then((pickerItems) => {
        this.setState({pickerItems: pickerItems});
      });
  }

  _setLargeMap = () => {
    this.setState({
      largeMap: !this.state.largeMap
    });
  }

  _refreshShop = (shop) => {
    //var index = this.state.shops.indexOf(shop);
    var index = this.state.shops.map(function(element) { return element.uid; }).indexOf(shop.uid);
    //console.log("#######try########");
    //console.log(index);

    var newShops = this.state.shops;
    if(index !== -1) {
      /*getShopFromApiWithId(this.token, shop.uid).then(data => {
        console.log(newShops[index]);
        console.log("*****************************");
        newShops[index] = data;
        console.log(data);
        //this.state.shops[index].is_favored = !this.state.shops[index].is_favored;


      });*/

      this.state.shops[index].is_favored = !this.state.shops[index].is_favored;
      this.setState({shops: newShops});
    }

    /*this.setState(prevState => ({
      shops: prevState.shops.map(element => {
        if(element.uid === shop.uid)
          console.log(element.is_favored)
        return element.uid === shop.uid ? { ...element, is_favored: !element.is_favored }: element
      }
      )
    }));*/
  }

  _displayShops = (category) => {
    this.props.navigation.navigate("ShopList", {category: category, token: this.token, shops: this.state.shops, refreshShop: this._refreshShop, userLocation: this.state.mapRegion, displayShopDetails: this._displayShopDetails});
  }

  _displayShopDetails = (id, distance, setFavoriteShop) => {
    this.props.navigation.navigate('ShopDetails', {token: this.token, shopId: id, shopDistance: distance, setFavoriteShop: setFavoriteShop});
  }

  async getLocationAsync() {
   let { status } = await Permissions.askAsync(Permissions.LOCATION);
   if (status !== 'granted') {
     this.setState({
       locationResult: 'Permission to access location was denied',
     });
   } else {
     this.setState({ hasLocationPermissions: true });
   }

   let location = await Location.getCurrentPositionAsync({});
   this.setState({ locationResult: JSON.stringify(location) });
   // Center the map on the location we just fetched.
   this.setState({mapRegion: {latitude: location.coords.latitude, longitude: location.coords.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421}});
  }

  /*_checkProfileStatus() {
    getWalletInfoFromApiWithToken(this.token).then(data => {
      //console.log(data);
      if(data == null || data.status !== "5") {
        Alert.alert(
          "KYC",
          "Afin de finaliser votre inscription, n'oubliez pas de compléter le formulaire d'identification KYC." +
          "Sans ces documents, les recharges de portemonnaie ou les paiements ne seront pas possibles.",
          //"Pour plus d'info, cliquez sur ce lien sinon cliquez sur le bouton.",
          [
            {
              text: "Voir plus",
              onPress: () => Linking.openURL('https://www.youseeme.pro/kyc-comment-ca-marche/'),
            },
            {
              text: "OK",
              //onPress: () => console.log("OK Pressed")
            }
          ],
          {
            cancelable: false
          }
        );
      }
    });
  }*/

  componentDidMount() {
    this.getLocationAsync();
    this._loadPickerItems();
    this._loadProfile();
    this._loadShops();

    console.log("-------");
    console.log(this.state.profileData);
  }


  renderContent = () => {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '180deg']
    })
      return (

          <Block style={{flex:1,flexDirection: 'column', alignItems: 'center'}}>
              <View style={{justifyContent: 'center',flex:0, alignItems: 'center', marginTop: -25, marginBottom: 0, backgroundColor: '#674171', width: 70, height: 50, borderRadius: 30}}>
                {<Animated.View style={{transform: [{rotate: spin}]}}>
                  <Icon style={styles.chevron}
                        name="chevrons-down"
                        family="Feather"
                        color="#F8CE3E"
                        size={28}
                  />
                </Animated.View>}
              </View >

              <Block style={{flex:0, flexDirection: 'row'}}>
                  <TouchableOpacity
                    style={{
                      flex:0.33,
                      justifyContent: 'center',
                      alignItems: 'center'
                    }}
                    onPress={() => this.props.navigation.navigate('Scan', {token: this.token, profileData: this.state.profileData})}
                  >
                    <Animated.View
                      style={[
                        styles.circle,{
                        width:this.blockDrawerWidthValue,
                        height:this.blockDrawerWidthValue,
                        marginBottom: this.blockDrawerBottomValue,
                        marginTop: this.blockDrawerTopValue
                      }]}
                    >
                      <Animated.Image
                        style={{
                          width:this.imageDrawerWidthValue,
                          height:this.imageDrawerWidthValue
                        }}
                        source={require('../assets/QRCode.png')}
                      />
                    </Animated.View>
                    <Animated.Text style={[styles.drawerBlockText, {opacity: this.textDrawerValue}]}>
                      Payer
                    </Animated.Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{flex:0.35 , justifyContent: 'center', alignItems: 'center',}}
                    onPress={() => this.props.navigation.navigate('Reload', {
                      data: this.state.profileData,
                      token: this.token
                    })}
                  >
                    <Animated.View
                      style={[
                        styles.circle,{
                        width:this.blockDrawerWidthValue,
                        height:this.blockDrawerWidthValue,
                        marginBottom: this.blockDrawerBottomValue,
                        marginTop: this.blockDrawerTopValue
                      }]}
                    >
                      <Animated.Image
                        style={{
                          width:this.imageDrawerWidthValue,
                          height:this.imageDrawerWidthValue
                        }}
                        source={require('../assets/reload.png')}
                      />
                    </Animated.View>
                    <Animated.Text style={[styles.drawerBlockText, {opacity: this.textDrawerValue}]}>
                      Recharger
                    </Animated.Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{flex:0.34, justifyContent: 'center',
                      alignItems: 'center',}}
                    onPress={() => this.props.navigation.navigate('Transfer', {
                      profileData: this.state.profileData,
                      token: this.token
                    })}
                  >
                    <Animated.View
                      style={[
                        styles.circle,{
                        width:this.blockDrawerWidthValue,
                        height:this.blockDrawerWidthValue,
                        marginBottom: this.blockDrawerBottomValue,
                        marginTop: this.blockDrawerTopValue
                      }]}
                    >
                      <Animated.Image
                        style={{
                          width:this.imageDrawerWidthValue,
                          height:this.imageDrawerWidthValue
                        }}
                        source={require('../assets/transfer.png')}
                      />
                    </Animated.View>
                    <Animated.Text style={[styles.drawerBlockText, {opacity: this.textDrawerValue}]}>
                      Transférer
                    </Animated.Text>
                  </TouchableOpacity>
              </Block>

              <Block style={{flex:0, flexDirection: 'row',top:15}}>
                  <TouchableOpacity style={{flex:0.33 ,justifyContent: 'center', alignItems: 'center',}}
                                    onPress={() => this.props.navigation.navigate('Wallet', {token: this.token, profileData: this.state.profileData})}>
                      <Block style={styles.circle}>
                          {/*<Icon
                              name="Safety"
                              family="AntDesign"
                              color="white"
                              size={30}
                          />*/}
                          <Image style={styles.image} source={require('../assets/wallet.png')} />
                      </Block>
                      <Text style={styles.drawerBlockText}>
                        Porte-monnaie
                      </Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{flex:0.35 , justifyContent: 'center', alignItems: 'center'}}
                                    onPress={() => this.props.navigation.navigate('Profile', {
                                      profileData: this.state.profileData,
                                      token: this.token
                                    })}>
                      <Block style={styles.circle}>
                          {/*<Icon
                              name="credit-card"
                              family="Fontisto"
                              color="white"
                              size={30}
                          />*/}
                          <Image style={styles.image} source={require('../assets/profile.png')} />
                      </Block>
                      <Text style={styles.drawerBlockText}>
                        Profil
                      </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{flex:0.34 ,  justifyContent: 'center', alignItems: 'center',}}
                    onPress={() => this.props.navigation.navigate('Settings',{token: this.token})}
                  >
                      <Block style={styles.circle}>
                          {/*<Icon
                              name="clipboard-notes"
                              family="Foundation"
                              color="white"
                              size={30}
                          />*/}
                          <Image style={styles.image} source={require('../assets/settings.png')} />
                      </Block>
                      <Text style={styles.drawerBlockText}>
                        Paramètres
                      </Text>
                  </TouchableOpacity>
              </Block>
          </Block>
      )
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1}}>
        <View style={{flex: 1, marginTop: 30}}>
          {/*<SearchBar
            placeholder="Rechercher..."
            containerStyle={{
              backgroundColor: 'transparent',
              borderBottomColor: 'transparent',
              borderTopColor: 'transparent',
              height: 48,
              justifyContent: 'center'
            }}
            inputContainerStyle={{
              backgroundColor: 'rgba(67, 39, 74, 0.8)',
              height: 35
            }}
            inputStyle={{
              color: 'white'
            }}
            onChangeText={this._updateSearch}
            value={this.state.search}
            round={true}
            showCancel={true}
          />*/}
          {<View style={{flex: 0.05, justifyContent: 'center', alignItems: 'center'}}>
            <Image
              style={styles.logo}
              source={require('../assets/logo.png')}
            />
          </View>}
          <NavigationContainer style={{flex: 0.95}}>
            <Tab.Navigator
            swipeEnabled={false}
            tabBarOptions={{
              activeTintColor: '#F8CE3E',
              inactiveTintColor: 'rgb(190, 190, 190)',
              style: {
                backgroundColor: '#674171',
                height: this.state.largeMap ? 0 : 43,
              },
              indicatorStyle: {
                backgroundColor: '#F8CE3E',
                borderRadius: 20
              },
              showLabel: false,
              showIcon: true
            }}
          >
            <Tab.Screen
              name="Map"
              profileData={this.state.profileData}
              options={{
                tabBarIcon: ({ focused, tintColor }) => {
                  var color = ""
                  if(focused)
                    color = "#F8CE3E";
                  else
                    color = "white";
                  return (
                    <SimpleLineIcons
                      name="location-pin"
                      style={{ color: color, fontSize: 21}}
                    />
                  );
                }
              }}
            >
              {props => {
                if(this.state.shops.length > 0 && this.state.locationResult != null && this.state.pickerItems.length > 0)
                  return(<Home {...props} token={this.token} shops={this.state.shops} mapRegion={this.state.mapRegion} pickerItems={this.state.pickerItems} setLargeMap={this._setLargeMap} refreshShop={this._refreshShop} displayShopDetails={this._displayShopDetails} />);
                else
                  return(
                      <View style={{ flex: 1 }}>
                        <View style={{ flex: 1 }}>

                            <MapView
                                //ref={map => this.map = map}
                                //mapPadding={{bottom: 150}}
                                maxZoomLevel={10}
                                loadingEnabled
                                showsUserLocation={true}
                                showsMyLocationButton={true}
                                //followsUserLocation={true}
                                showsCompass={true}
                                //provider={"google"}
                                style={{ flex: 1, borderRadius: 15}}
                            />
                        </View>
                        <View style={styles.loading_container}>
                           <ActivityIndicator
                             size='large'
                             color='#674171'
                           />
                         </View>
                      </View>
                    );

              }}
              </Tab.Screen>
              <Tab.Screen
                name="Commerces"
                options={{
                  tabBarIcon: ({ focused,tintColor }) => {
                    var color = ""
                    if(focused)
                      color = "#F8CE3E";
                    else
                      color = "white";
                    return (
                      <Ionicons
                        name="ios-list"
                        style={{ color: color, fontSize: 23}}
                      />
                    );
                  }
                }}
              >
              {props => {
                if(this.state.locationResult != null)
                  return(<CategoriesShop {...props} token={this.token} displayShops={this._displayShops} shops={this.state.shops} userLocation={this.state.locationResult} />);
                else
                  return(
                    <View style={styles.loading_container}>
                       <ActivityIndicator
                         size='large'
                         color='#674171'
                       />
                     </View>
                  );
              }}
              </Tab.Screen>
              <Tab.Screen
                name="Favoris"
                token={this.token}
                options={{
                  tabBarIcon: ({ focused,tintColor }) => {
                    var color = ""
                    if(focused)
                      color = "#F8CE3E";
                    else
                      color = "white";
                    return (
                      <Ionicons
                        name="ios-heart-empty"
                        style={{ color: color, fontSize: 23}}
                      />
                    );
                  }
                }}
              >
              {props => {
                if(this.state.locationResult != null)
                  return(<FavoriteShops {...props} token={this.token} profileData={this.state.profileData} refreshShop={this._refreshShop} userLocation={this.state.mapRegion} displayShopDetails={this._displayShopDetails} />);
                else
                  return(
                    <View style={styles.loading_container}>
                       <ActivityIndicator
                         size='large'
                         color='#674171'
                       />
                    </View>
                  );
              }}
              </Tab.Screen>
            </Tab.Navigator>
            {/*this.state.largeMap ? null : <Animated.View style={{position: 'absolute', right: 1, bottom: this.btnQRBottomValue, width: this.btnQrWidthValue, height: this.btnQrHeightValue, borderRadius: 20}}>
              <FloatingAction
                floatingIcon={require('../assets/qr-code.png')}
                showBackground={false}
                color={'#674171'}
                iconWidth={20}
                iconHeight={20}
                buttonSize={50}
                actions={this.actions}
                position={'right'}
                distanceToEdge= {10}
                onPressItem={name => {
                  this._selector(name);
                }}
                onPressMain={
                  (state) => (state ? this._extend() : this._reduce())
                }
              ></FloatingAction>
            </Animated.View>*/}
            {this.state.largeMap ? null : <BottomDrawer
              startUp={false}
              roundedEdges={true}
              containerHeight={Platform.OS === 'ios' ? 270: 260}
              downDisplay={Platform.OS === 'ios' ? 145: 146}
              shadow={true}
              offset={TAB_BAR_HEIGHT + HEADER_HEIGHT}
              backgroundColor={'#674171'}
              style={{borderRadius: 20}}
              onCollapsed={() => {
                  this._drawerAnim();
              }}
              onExpanded={() => {
                  this._drawerAnimBack();
              }}
              onCompletedToggle={() => {

              }}
            >

                {this.renderContent()}
            </BottomDrawer>}
          </NavigationContainer>
          {this.state.firstConnection ?
            <Animated.View elevation={5} style={[styles.firstConnectionContainer, {top: this.firstConnectionTopValue}]}>
              <TouchableOpacity
                style={styles.firstConnectionClose}
                activeOpacity={0.9}
                onPress={() => this._skipTutorial()}
              >
                <Ionicons
                  name={"ios-close"}
                  style={{color: '#F8CE3E', fontSize: 35}}
                />
              </TouchableOpacity>
              <Text style={styles.firstConnectionTitle}>{"Bienvenue !"}</Text>
              <Text style={styles.firstConnectionUserName}>{this.state.profileData.first_name}</Text>
              <View>
                <Text style={styles.firstConnectionTopText}>
                {`Félicitation ! Tu viens de rejoindre la Youseeme Family !
Nous venons de t’envoyer un mail, valide vite ton inscription pour bénéficier de tes premiers Bartcoins !`}
                </Text>
                <Text style={styles.firstConnectionBottomText}>
                  {"Veux-tu que l’on te montre comment utiliser l’application ?"}
                </Text>
              </View>
              <View style={styles.actionsContainer}>
                {/*<TouchableOpacity
                  style={[styles.btn, {backgroundColor: '#40BCF8'}]}
                  onPress={() => this._skipTutorial()}
                >
                  <Text style={styles.btnText}>{"Passer"}</Text>
                </TouchableOpacity>*/}
                <TouchableOpacity
                  style={[styles.btn, {backgroundColor: '#674171'}]}
                  onPress={() => this._showTutorial()}
                >
                  <Text style={styles.btnText}>{"C'est parti !"}</Text>
                </TouchableOpacity>
              </View>
            </Animated.View>
          : null}
          {this.state.firstConnection ?
            <ConfettiCannon
              count={200}
              origin={{x: width/2, y: height/4}}
              fadeOut={true}
              autoStart={true}
              fallSpeed={6000}
              explosionSpeed={300}
              autoStartDelay={600}
              ref={ref => (this.explosion = ref)}
              //onAnimationEnd={() => this.run()}
            /> : null}
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        left: 0,
        top: 0,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    logo: {
        width: '100%',
        height: '90%',
        resizeMode: 'contain',
    },
    circle: {
        width:70,
        height:70,
        borderRadius: 70/2,
        backgroundColor:"rgba(255, 255, 255, 0.5)",
        justifyContent: 'center',
        alignItems: 'center',

    },
    chevon: {
        flexDirection: 'column',
        justifyContent: 'center',
    },
    scrollContainer: {
        height:100,
        flex:3},
    drawerBlockText: {
      marginTop: 3,
      color: '#F8CE3E'
      //fontFamily: "SciFly"
    },
    image: {
      width: 60,
      height: 60,
    },
    smallCircle: {
      marginBottom: 10,
      marginTop: -5
    },
    loading_container: {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 100,
      bottom: 0,
      justifyContent: 'center',
      alignItems: 'center'
    },
    firstConnectionContainer: {
      position: 'absolute',
      width: '85%',
      //height: '65%',
      alignSelf: 'center',
      backgroundColor: 'white',
      borderRadius: 20,
      paddingVertical: 20,
      paddingHorizontal: 15,
      shadowColor: 'grey',
      shadowOffset: {
        width: 0,
        height: 3
      },
      shadowRadius: 5,
      shadowOpacity: 0.7
    },
    firstConnectionTitle: {
      textAlign: 'center',
      fontSize: 35,
      fontWeight: 'bold',
      fontFamily: "SciFly",
      color: '#674171',
      marginBottom: 8
    },
    firstConnectionTopText: {
      textAlign: 'center',
      fontSize: 20,
      fontWeight: '500',
      fontFamily: "SciFly",
      color: '#674171',
    },
    firstConnectionBottomText: {
      textAlign: 'center',
      fontSize: 20,
      fontWeight: '500',
      fontFamily: "SciFly",
      color: '#674171',
      marginTop: 60,
    },
    firstConnectionUserName: {
      textAlign: 'center',
      fontSize: 32,
      fontWeight: '500',
      fontFamily: "SciFly",
      color: '#F8CE3E',
      marginBottom: 15
    },
    actionsContainer: {
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'space-around',
      alignItems: 'center',
      marginTop: 20
    },
    btn: {
      width: 125,
      height: 48,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 30,
    },
    btnText: {
      color: 'white',
      fontSize: 19,
      fontFamily: 'SciFly',
      textAlign: 'center',
      paddingTop: Platform.OS === 'ios' ? 4 : 0
    },
    firstConnectionClose: {
      position: 'absolute',
      top: 3,
      right: 3,
      borderTopLeftRadius: 0,
      borderTopRightRadius: 15,
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 0,
      width: 35,
      height: 35,
      justifyContent: 'center',
      alignItems: 'center',
      zIndex: 1,
    }
});
