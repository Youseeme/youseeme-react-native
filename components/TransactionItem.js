import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Moment from 'react-moment';
import 'moment-timezone';

class TransactionItem extends React.Component {
  _displayAmount(amount, transactionType, currencyType) {
    if(transactionType === "debit") {
      return (
        <Text style={styles.credit}> + {amount} {currencyType ? "€" : "Bc"}</Text>
      );
    }
    else {
      return (
        <Text style={styles.debit}> - {amount} {currencyType ? "€" : "Bc"}</Text>
      );
    }
  }

  _displayTransactionType(type) {

  }


  _displayTransaction() {

  }

  render() {
    const transaction = this.props.transaction;
    const date = new Date(transaction.created_at);

    return (
      <View style={styles.main_container}>
        <View style={styles.transactionInfos}>
          {/*<Text style={styles.transactionName}>{transaction.type === "debit" ? "Rechargement " : "Virement vers "} {transaction.shop}</Text>*/}
          <Text style={styles.transactionName}>{transaction.shop}</Text>
          {<Moment style={styles.date} element={Text} interval={0} tz="Europe/Paris" format="\L\e DD/MM/YYYY à HH:mm">
            {date.toString()}
          </Moment>}
        </View>
        <View style={styles.transactionAmount}>
          {this._displayAmount(transaction.amount, transaction.type, transaction.is_euro)}
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'row',
    height: 55,
    marginTop: 5,
    paddingTop: 3,
    paddingBottom: 5,
    marginHorizontal: 10,
    paddingHorizontal: 2,
    borderBottomColor: 'rgba(161, 161, 161, 0.8)',
    borderBottomWidth: 1,
  },
  transactionInfos: {
    flex: 2.5,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  transactionAmount: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  transactionName: {
    fontWeight: 'bold',

  },
  date: {
    color:'#674171'
  },
  debit: {
    color:'#FF3B30',
    fontSize: 15,
    fontWeight: 'bold'
  },
  credit: {
    color:'#34C759',
    fontSize: 15,
    fontWeight: 'bold'
  }
});

export default TransactionItem;
