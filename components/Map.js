import React from 'react';
import { Animated, StyleSheet, Dimensions, View, ImageBackground, TouchableOpacity, TouchableWithoutFeedback, TouchableHighlight, Button, Image, ScrollView, Linking, Alert, Easing, Platform } from 'react-native';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import BottomDrawer from 'rn-bottom-drawer';
import * as Font from 'expo-font';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { FloatingAction } from "react-native-floating-action";
import Ee from './Ee';
import MapView from "react-native-map-clustering";
import { Marker, Callout, PROVIDER_GOOGLE} from 'react-native-maps'

import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';

import {createAppContainer} from 'react-navigation';
import CategoriesShop from "./CategoriesShop";

import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign } from '@expo/vector-icons';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { getDistance, getPreciseDistance } from 'geolib';
import RNPickerSelect from 'react-native-picker-select';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

import CustomMarker from './CustomMarker';
import { getCategoriesFromApi, getShopListFromApiWithToken, setFavoriteFromApiWithShopId, getSubCategoriesFromApiWithCategoryId } from '../API/YSMApi';
import Root from './RootPopup';
import Toast from './Toast';

const { width, height } = Dimensions.get('screen')


const CARD_HEIGHT = height / 6;
const CARD_WIDTH = CARD_HEIGHT;


const mapStyle = [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#263c3f"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6b9a76"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#38414e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#212a37"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9ca5b3"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#1f2835"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#f3d19c"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2f3948"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#515c6d"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  }
]

  class Map extends React.Component {
    constructor(props) {
        super(props);
        this.data = this.props.data;
        this.token = this.props.token;
        this.setLargeMap = this.props.setLargeMap;

        this.index = 0;
        this.animation = new Animated.Value(0);
        this.animatedValue = new Animated.Value(0);
        this.shopInfosBottomValue = new Animated.Value(-300);

        this.markers = this.props.shops;
        this.refreshShop = this.props.refreshShop;
        this._displayShopDetails = this.props.displayShopDetails;

        this.showEe = false;

        var temp = {
          latitude: 48.8262535,
          latitudeDelta: 0.0922,
          longitude: 2.3634459,
          longitudeDelta: 0.0421,
        }
        console.log(this.props.mapRegion);

        this.state = {
            categoryItems: this.props.pickerItems,
            mapRegion: this.props.mapRegion,
            hasLocationPermissions: false,
            locationResult: null,
            currentCategory: -1,
            selectedCategoryItems: [-1],
            currentMarkers: this.markers,
            firstLoad: true,
            selectedMarker: this.props.shops[0]
        };

        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });
      }

        /*_getShoplist() {
          getShopListLatitudeAndLongitude(this.state.token).then(response => {
              console.log("=====RETURN=====");
              this.setState({ locations: response })
              // console.log(this.state.shoplist)
             // const { location } = this.state.locations;
              console.log(this.state.locations);

          });
        }*/

        handleMapRegionChange(mapRegion){
          //console.log(mapRegion);
          this.setState({mapRegion: mapRegion});
        }

        /*_loadPickerItem() {
            getCategoriesFromApi().then(data1 => {
              var pickerItems = [];
              data1.map((category, index) => {
                getSubCategoriesFromApiWithCategoryId(category.id).then(data2 => {
                  data2.map((subCategory, index) => {
                    pickerItems.push({label: subCategory.name, value: subCategory.id});
                  });
                });
              });
              pickerItems.push({label: "Tout", value: -1});
              return pickerItems;

            }).then((pickerItems) => {
              this.setState({pickerItems: pickerItems});
            });
        }*/

        _getPreciseDistance(shopAddresses) {
          var pdis;
          var pdisMin = -1;

          if(shopAddresses != null && this.state.mapRegion != null) {
            shopAddresses.forEach((address) => {
              if(address.lat != null && address.lon != null) {
                pdis = getPreciseDistance(
                  { latitude: this.state.mapRegion.latitude, longitude: this.state.mapRegion.longitude },
                  { latitude: address.lat, longitude: address.lon }
                );
              }

              if(pdis < pdisMin || pdisMin === -1)
                pdisMin = pdis;
            });
          }

          //console.log((pdis/1000) + " km")
          if(pdisMin === -1)
            return -1;
          else
            return (pdisMin/1000);
        }


        _filterItems = (searchTerm, items, props) => {
          const {
            subKey, uniqueKey, displayKey
          } = props;

          let filteredItems = [];
          let newFilteredItems = [];

          let getProp = (object, key) => object && object[key];

          let rejectProp = (items, fn) => items.filter(fn);

          if(searchTerm === 'EasterEgg!') {
            this.showEe = true;
          }

          items &&
            items.forEach((item) => {
              const parts = searchTerm
                .replace(/[\^$\\.*+?()[\]{}|]/g, '\\$&')
                .trim()
                .split(' ')
              const regex = new RegExp(`(${parts.join('|')})`, 'i');

              if (regex.test(getProp(item, displayKey))) {
                filteredItems.push(item);
              }
              if (item[subKey]) {
                const newItem = Object.assign({}, item)
                newItem[subKey] = [];
                item[subKey].forEach((sub) => {
                  if (regex.test(getProp(sub, displayKey))) {
                    newItem[subKey] = [...newItem[subKey], sub]
                    newFilteredItems = rejectProp(
                      filteredItems,
                      singleItem => item[uniqueKey] !== singleItem[uniqueKey],
                    );
                    newFilteredItems.push(newItem);
                    filteredItems = newFilteredItems;
                  }
                })
              }
            })

          return filteredItems;
        }

        _displayNoResultsComponent(message) {
          return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red'}}>
              <Text style={{color: '#674171'}}>{message}</Text>
            </View>
          );
        }

        _favored(state) {
          var iconName = "ios-heart"
          if(!state)
            iconName+="-empty"
          return (
            <Ionicons
              name={iconName}
              style={{color: '#F8CE3E', fontSize: 25, width: 21, height: 23, marginLeft: 11, zIndex: 1}}
            />
          );
        }

        _displayShopInfos(type) {
          var value = 0;
          if(type === "up")
            value = 130;
          else
            value = -300;
          Animated.parallel (
            [
              Animated.timing(
                this.shopInfosBottomValue,
                {
                  toValue: value,
                  duration: 900,
                  easing: Easing.elastic(3)
                }
              )
            ],
            {
              useNativeDriver: true
            }
          ).start(() => {
            if(type === "down")
              this.setState({selectedMarker: null});
          });

          return true;
        }

        componentDidUpdate(prevProps, prevState) {
          if (prevState.selectedCategoryItems !== this.state.selectedCategoryItems && this.state.selectedCategoryItems.indexOf(-1) !== -1) {
            this.map.animateCamera({heading: 20, altitude: 3000, zoom: 12}, 2);
          }

          if(this.showEe) {
            this.multiSelectRef._closeSelector();
            this.explosion.run();
            this.showEe = false;
          }
        }

        componentDidMount() {

        }

      _refreshMarkers() {
        if(this.state.selectedCategoryItems.length == 0)
          this.setState({currentMarkers: []});
        else if(this.state.selectedCategoryItems.indexOf(-1) != -1)
          this.setState({currentMarkers: this.markers});
        else {
          var currentMarkers = [];
          var promises = this.markers.map((marker) => {
            this.state.selectedCategoryItems.forEach(category => {
                if(category == marker.category.id) {
                  currentMarkers.push(marker);
                }
            });
          });

          Promise.all(promises).then(() => {
            this.setState({currentMarkers: currentMarkers});
          });
        }
      }

      _zoomClamping() {
        const index = this.state.selectedCategoryItems.indexOf(-1);
        if(this.state.firstLoad)
          return 1;
        else if(index !== -1) {
          return 12;
        }
        else
          return 0;
      }

      _setFavoriteShop = (shop) => {
        setFavoriteFromApiWithShopId(this.token, shop.uid).then(data => {
          console.log(data);
      });

        this.refreshShop(shop);

        //this._loadShops();

        /*var index = this.state.shops.indexOf(shop);
        if(index !== -1)
          this.state.shops[index].is_favored = !this.state.shops[index].is_favored;

        this.setState()*/

        //this.reloadShops();
        //this._loadShops();
      }

      render() {

        return (
            <View style={{flex: 1, backgroundColor:'#674171'}}>
            <View style={{postion:'absolute', top: 10, marginLeft: 100, marginRight: 100, justifyContent: 'center', alignItems: 'center', zIndex: 1, borderRadius: 10, flexDirection: 'row'}}>
              <TouchableOpacity
                style={{width: 30, height: 30, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', zIndex: 1, marginRight: 5, borderWidth: 1, borderColor: 'rgb(172, 172, 172)'}}
                onPress={() => {
                  this.map.animateCamera({center: this.state.mapRegion, heading: 20, altitude: 2000, zoom: 16}, 2);
                }}
              >
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                  <MaterialIcons
                    name="my-location"
                    style={{color: '#674171', fontSize: 20}}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{width: '100%', height: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.9)', borderRadius: 10, borderWidth: 1, borderColor: 'rgb(172, 172, 172)'}}>
                  <SectionedMultiSelect
                    ref={ref => {this.multiSelectRef = ref}}
                    items={this.state.categoryItems}
                    colors={{
                      primary:'#F8CE3E',
                      success:'#F8CE3E',
                      chipColor: 'white',
                      itemBackground: '#674171',
                      subItemBackground: '#674171',
                      text: 'white',
                      subText: '#bec2c2',
                      searchPlaceholderTextColor: 'rgba(255, 255, 255, 0.5)'
                    }}
                    styles={multiSelectStyles}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Choisir..."
                    confirmText="Valider"
                    selectedText="Sélectionnés"
                    removeAllText="Tout supprimer"
                    searchPlaceholderText="Rechercher un loisir"
                    filterItems={(searchText, items, props) => this._filterItems(searchText, items, props)}
                    showDropDowns={true}
                    selectChildren={true}
                    showChips={false}
                    hideConfirm={true}
                    showCancelButton={false}
                    readOnlyHeadings={false}
                    modalWithSafeAreaView={true}
                    modalWithTouchable={true}
                    modalAnimationType="slide"
                    searchIconComponent={
                      <Ionicons
                        name="md-search"
                        style={{fontSize: 19, paddingLeft: 13, paddingRight: 10, paddingTop: 1, color: 'rgba(255, 255, 255, 0.7)'}}
                      />
                    }
                    selectToggleIconComponent={
                      <Ionicons
                        name="ios-arrow-down"
                        style={styles.pickerArrow}
                      />
                    }
                    noResultsComponent={this._displayNoResultsComponent("Désolé, aucune catégorie n'a été trouvé.")}
                    onSelectedItemsChange={(selectedItems) => {
                      const index1 = selectedItems.indexOf(-1);
                      const index2 = this.state.selectedCategoryItems.indexOf(-1);

                      if(index1 > -1 && selectedItems.length > 1) {
                        if(index2 > -1)
                          selectedItems.splice(index1, 1);
                        else
                          selectedItems = [-1]
                      }
                      this.setState({selectedCategoryItems: selectedItems}, () => {
                        this._refreshMarkers();
                      });
                    }}
                    selectedItems={this.state.selectedCategoryItems}
                  />
                </TouchableOpacity>

              </View>
              <View style={{flex: 1, marginTop: -30}}>
                <MapView
                  //ref={component => this._map = component}
                  mapRef={map => this.map = map}

                  onDoublePress={() => this.setLargeMap()}
                  //mapPadding={{bottom: 150}}
                  loadingEnabled
                  radius={80}
                  showsUserLocation={true}
                  showsMyLocationButton={false}
                  //followsUserLocation={true}
                  showsCompass={true}
                  //provider={PROVIDER_GOOGLE}
                  style={{ flex: 1 }}
                  clusterColor={'#674171'}
                  initialRegion={this.state.mapRegion}
                  zoomTapEnabled={false}
                  minZoomLevel={this._zoomClamping()}    // curb

                  onMapReady={() => {this.state.firstLoad ? this.setState({firstLoad: false}) : null}}

                  //customMapStyle={mapStyle}
                  //onLayout={Platform.OS === 'ios' ? this.mapReadyResolve: null}

                  //onRegionChangeComplete((e) => ....your_function(e))
                  //region={this.state.mapRegion}
                  //onRegionChange={(region) => this.setState({mapRegion: region})}
                  //onRegionChangeComplete={(region) => this.setState({mapRegion: region})}
                >
                  {/*this.renderMarkers()*/}
                  {/*<MapView.Polyline
                      strokeWidth={2}
                      strokeColor="purple"
                      coordinates={coords}
                  />*/}
                  {/*<Image
                      source={{ uri: destination && destination.image_url }}
                      style={{
                          flex: 1,
                          width: width * 0.95,
                          alignSelf: 'center',
                          height: height * 0.15,
                          position: 'absolute',
                          bottom: height * 0.05
                      }}
                  />*/}

                  {this.state.currentMarkers.map((marker, index) => {
                    //console.log("@@@@@@-------------@@@@@@");
                    //marker.addresses.forEach(address => {

                    //console.log(address);

                    /*const scaleStyle = {
                      transform: [
                        {
                          scale: interpolations[index].scale,
                        },
                      ],
                    };
                    const opacityStyle = {
                      opacity: interpolations[index].opacity,
                    };*/

                      //if(marker.category.id === this.state.currentCategory || this.state.currentCategory === -1) {
                    return (
                      //<MapView.Marker key={marker.uid} coordinate={{latitude: parseFloat(address.lat), longitude: parseFloat(address.lon)}}>
                      <Marker
                        ref={markerRef => this.markerRef = markerRef}
                        style={{width: 50, height: 50}}
                        key={marker.uid}
                        title={marker.name}
                        //tracksViewChanges={this.state.firstLoad}

                        tracksViewChanges={false}
                        coordinate={{latitude: marker.addresses.length != 0 ? parseFloat(marker.addresses[0].lat) : 0, longitude: marker.addresses.length != 0 ? parseFloat(marker.addresses[0].lon) : 0}}
                        onCalloutPress={() => {
                          //console.log(this._getPreciseDistance(marker.addresses != null ? marker.addresses : null));
                          //this._getPreciseDistance(marker.addresses != null ? marker.addresses : null).then((distance) => console.log(distance));
                          const promise = new Promise((resolve, reject) => {
                            marker.distance = this._getPreciseDistance(marker.addresses != null ? marker.addresses : null);
                            resolve();
                          })

                          promise.then(() => this.setState({selectedMarker: marker}));
                          this._displayShopInfos("up");

                        }}

                        image={Platform.OS == "ios" ? require('../assets/locationV2.png') : require('../assets/location.png')}
                      >
                        {/*<Animated.View style={[styles.markerWrap, opacityStyle]}>
                          <Animated.View style={[styles.ring, scaleStyle]} />
                          <View style={styles.marker} />
                        </Animated.View>*/}
                        {/*<MaterialIcons
                          name="location-on"
                          style={{color: '#674171', fontSize: 25}}
                        />*/}
                        {/*<Callout tooltip style={styles.customView}>
                              {<View style={styles.calloutText}>
                              </View>}
                        </Callout>*/}
                        {Platform.OS == "ios" ? <Callout>
                          <View style={{justifyContent: 'center', alignItems: 'center', height: 130, width: 90, marginLeft: -12, marginRight: -12, marginTop: -10, marginBottom: -5}}>
                            <View style={{width: '100%', height: '70%', borderRadius: 15, marginBottom: 5}}>
                              <Image
                                style={{width: '100%', height: '100%', resizeMode: 'cover'}}
                                source={{uri: marker.profile_image}}
                              />
                            </View>
                            <View style={{width: '100%', height: '30%', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 8}}>
                              <Text style={{textAlign: 'center', fontWeight: '500', color: '#674171'}} numberOfLines={2}>{marker.name}</Text>
                            </View>
                          </View>
                        </Callout> : null
                        /*<Callout
                          ref={calloutRef => this.calloutRef = calloutRef}
                          tooltip
                        >
                          <View style={{width: 90, marginTop: 0, marginBottom: 10, borderRadius: 10, shadowOffset:{width: 0,  height: 3}, shadowColor: 'black', shadowOpacity: 1.0, shadowRadius: 5, backgroundColor: 'white'}}>
                            <View style={{flex: 1, overflow: 'hidden', borderRadius: 10, borderWidth: 0.5, borderRadius: 10, borderColor: 'rgb(179, 179, 179)'}}>
                              <Text style={{width: '100%', height: 140, textAlign: 'center', textAlignVertical: 'top', marginTop: -50, borderRadius: 10}}>
                                <Image
                                  //style={{flex: 0.7, width: 90, height: 100, resizeMode: 'cover', position: 'absolute', top: 1}}
                                  style={{width: 100, height: 100, resizeMode: 'cover', borderRadius: 10}}
                                  source={{uri: marker.profile_image}}
                                />
                              </Text>
                            {<View style={{width: '100%', height: '30%', justifyContent: 'flex-start', alignItems: 'center', paddingHorizontal: 8, paddingBottom: 5}}>
                              <Text style={{textAlign: 'center', fontWeight: '500', color: '#674171'}} numberOfLines={2}>{marker.name}</Text>
                            </View>}
                            </View>
                            <AntDesign
                              name="caretdown"
                              style={{ color: 'white', fontSize: 18, alignSelf: 'center', position: 'absolute', bottom: -14, paddingBottom: 5, zIndex: 2}}
                            />
                          </View>
                        </Callout>*/}




                        {/*<Image
                          style={{width: 28, height: 28}}
                          source={require('../assets/location.png')}
                        />*/}

                        {/*<MapView.Callout tooltip style={styles.customView}>
                          <TouchableHighlight onPress= {()=>this.markerClick()} underlayColor='#dddddd'>
                            <View style={styles.calloutText}>
                              <Text>{marker.name}{"\n"}{marker.description}</Text>
                            </View>
                          </TouchableHighlight>
                        </MapView.Callout>*/}

                      </Marker>
                    );
                  })}
                </MapView>

                {this.state.selectedMarker == null ? null : <Animated.View
                  style={{backgroundColor: 'white', width: '95%', height: 230, borderRadius: 15, alignSelf: 'center', bottom: this.shopInfosBottomValue, position: 'absolute'}}
                  >
                  <View style={{flex: 1}}>
                  <TouchableOpacity
                    style={styles.btnCornerShop}
                    activeOpacity={0.99}
                    onPress={() => {
                      this._displayShopInfos("down");
                    }}
                    >
                      {/*this._favored(this.state.favored)*/}
                      <Ionicons
                        name={"ios-close"}
                        style={{/*color: '#674171'*/color: '#F8CE3E', fontSize: 35}}
                      />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.main_container}
                    onPress={() => this._displayShopDetails(this.state.selectedMarker.uid, this.state.selectedMarker.distance, this._setFavoriteShop)}
                  >
                    <View style={styles.coverContainer}>
                      <Image
                        style={styles.coverImage}
                        source={{uri: this.state.selectedMarker.cover_image}}
                      />

                    </View>
                    <View style={styles.infosContainer}>
                      <View style={styles.profileContainer}>
                        <View style={styles.imageContainer}>
                          <Image
                            style={styles.profileImage}
                            source={{uri: this.state.selectedMarker.profile_image}}
                          />
                        </View>
                        <View style={styles.textContainer}>
                          <Text style={styles.title}>{this.state.selectedMarker.name}</Text>
                          <Text style={styles.distance}>{(this.state.selectedMarker.distance != null && this.state.selectedMarker.distance != -1) ? this.state.selectedMarker.distance.toFixed(2) + " km" : "Aucun établissemeent"}</Text>
                        </View>
                      </View>
                      <View style={styles.ratingContainer}>
                        <AirbnbRating
                          count={5}
                          size={15}
                          reviews={["Terrible", "Décevant", "Bien", "Génial", "Incroyable"]}
                          reviewSize={12}
                          defaultRating={this.state.selectedMarker.average_rating}
                          isDisabled={true}
                          selectedColor={'#F8CE3E'}
                          reviewColor={'rgba(249, 207, 62, 0.85)'}
                        />
                      </View>
                    </View>
                    {/*<View style={styles.descriptionContainer}>
                      <Text style={styles.description} numberOfLines={2}>{this.state.selectedMarker.description}</Text>
                    </View>*/}
                    <View style={styles.descriptionContainer}>
                      <View style={styles.textAddressContainer}>
                        <Text numberOfLines={2} style={styles.addressPart1}>{this.state.selectedMarker.addresses.length > 0 ? (this.state.selectedMarker.addresses[0].street + ",") : "Non communiqué"}</Text>
                        <Text numberOfLines={2} style={styles.addressPart2}>{this.state.selectedMarker.addresses.length > 0 ? (this.state.selectedMarker.addresses[0].postal_code + ", " + this.state.selectedMarker.addresses[0].city) : ""}</Text>
                      </View>
                      <View style={styles.categoryContainer}>
                        <Text numberOfLines={3} style={styles.textCategory}>{this.state.selectedMarker.category.name}</Text>
                      </View>
                      <View style={styles.favorite}>
                      <TouchableOpacity
                        style={{flex: 1, width: '90%', height: '90%', justifyContent: 'center', alignItems: 'center'}}
                        activeOpacity={0.39}
                        onPress={() => {
                          this._setFavoriteShop(this.state.selectedMarker);
                          //console.log("ok");
                          //this.setState({favored: !this.state.favored});

                        }}
                      >
                        {this._favored(this.state.selectedMarker.is_favored)}
                      </TouchableOpacity>
                      </View>
                    </View>
                  </TouchableOpacity>
                  </View>
                </Animated.View>}
              </View>
            <Ee ref={ref => (this.explosion = ref)} />
          </View>
        );
    }
}




const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    logo: {
        marginTop: 13,
        marginBottom:20,
        width: 200,
        height: 60,
        resizeMode: 'contain'
    },

  scrollView: {
    position: "absolute",
    bottom: 100,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 2,
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
    borderRadius: 15
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
    borderRadius: 15
  },
  textContent: {
    flex: 1,
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold",
  },
  cardDescription: {
    fontSize: 12,
    color: "#444",
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: "rgba(130,4,150, 0.3)",
    position: "absolute",
    borderWidth: 1,
    borderColor: "rgba(130,4,150, 0.5)",
  },
  pickerArrow: {
    color: '#674171',
    fontSize: 20,
    marginRight: 10,
    marginLeft: 5,
    marginTop: 8
  },










  main_container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    //height: '100%',
    //width: '100%'
    //marginTop: 5,
    paddingTop: 3,
    //paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 15,
    backgroundColor: '#674171',
    borderColor: '#674171',
    borderWidth: 2
  },
  coverContainer: {
    flex: 0.45,
    alignItems: 'center',
    justifyContent: 'center'
  },
  infosContainer: {
    flex: 0.28,
    paddingTop: 3,
    paddingBottom: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderBottomWidth: 1,
    borderBottomColor: '#C7C7CC',
  },
  descriptionContainer: {
    flex: 0.27,
    flexDirection: 'row',
    justifyContent: 'center',
    //backgroundColor: '#674171',
    borderRadius: 10,
    marginTop: 5,
    marginRight: -5,
    marginLeft: -5,
    paddingLeft: 8,
    paddingRight: 8
  },
  profileContainer: {
    flex: 0.7,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ratingContainer: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  imageContainer: {
    flex: 0.22,
    justifyContent: 'center',
  },
  textContainer: {
    flex: 0.78,
    paddingTop: 2,
    paddingLeft: 2,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  coverImage: {
    marginTop: 3,
    width: '100%',
    height: '100%',
    borderRadius: 10
  },
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 10,
    //borderWidth: 1,
    //borderColor: '#674171',
    resizeMode: 'stretch'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14,
    //color: '#674171',
    color: 'white',
    flexWrap: 'wrap'
  },
  distance: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'rgba(255, 255, 255, 0.65)',
    opacity: 1
  },
  textAddressContainer: {
    flex: 0.6,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    //backgroundColor: 'red'
    borderRightWidth: 1,
    borderRightColor: '#C7C7CC',
    marginTop: 5,
    marginBottom: 3,
    paddingRight: 3
  },
  categoryContainer: {
    flex: 0.25,
    alignItems: 'center',
    justifyContent: 'center',
    borderRightWidth: 1,
    borderRightColor: '#C7C7CC',
    marginTop: 5,
    marginBottom: 3,
    paddingHorizontal: 6
  },
  favorite: {
    flex: 0.15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    marginBottom: 3,
    //backgroundColor: 'red'
  },
  addressPart1: {
    fontWeight: '400',
    textAlign: 'left',
    //color: '#674171',
    //color: 'white',
    color: '#F8CE3E',
    fontSize: 13,
    paddingVertical: 0
  },
  addressPart2: {
    textAlign: 'left',
    //color: 'grey',
    color: 'rgba(249, 207, 62, 0.8)',
    paddingVertical: 0,
    fontSize: 11,
  },
  textCategory: {
    //color: 'grey',
    color: '#F8CE3E',
    fontSize: 13,
    textAlign: 'center',
  },
  btnCornerShop: {
    position: 'absolute',
    top: 0,
    right: 0,
    backgroundColor: '#674171',
    /*backgroundColor: 'white',*/
    borderTopLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  }
});

const multiSelectStyles = StyleSheet.create({
  modalWrapper: {
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    //backgroundColor: 'red',
    //height: 10,
    //height: '10%',
    minHeight: 30
  },
  container: {
    //marginTop: Platform.OS === 'ios' ? 390 : 350,
    marginBottom: Platform.OS === 'ios' ? -35 : 0,
    borderRadius: 10,
    //alignSelf:'flex-end',
    flex: Platform.OS === 'ios' ? 0.51 : 0.4,
    //borderWidth: 1,
    //borderColor: '#674171',
    backgroundColor: '#674171',
    width: '100%',
    marginLeft: 0,
    marginRight: 0
  },
  scrollView: {
    paddingBottom: 25,
  },
  searchBar: {
    backgroundColor: 'rgba(76, 45, 84, 0.5)',
  },
  searchTextInput: {
    color: 'white'
  },
  separator: {
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
  subSeparator: {
    backgroundColor: 'rgba(205, 205, 205, 0.1)',
  },
  confirmText:{
    color: 'white'
  },
  selectToggle:{
    width:'100%',
    backgroundColor:'#FFFFFF',
    borderRadius: 8,
    //opacity: 0.4,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
  },
  selectToggleText: {
    color: '#674171',
    fontSize: 14,
  },
  chipsWrapper: {
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectedItemText: {
    color:'#F8CE3E'
  },
  selectedSubItemText: {
    color:'#F8CE3E'
  },
  button: {
    backgroundColor: '#674171'
  },

});

export default Map;
