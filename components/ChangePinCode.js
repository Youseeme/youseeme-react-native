import React from 'react';
import {Alert, ImageBackground, StyleSheet, Text, TextInput, View, Button, TouchableOpacity} from 'react-native';
import {setPinCodeFromApiWithCode} from '../API/YSMApi';

export default class ChangePinCode extends React.Component {
  constructor(props) {
    super(props);
    this.token = this.props.navigation.getParam('token');
    this.pinCode = "";
    this.pinCodeVerif = "";

    this.state = {
      error: "",
    }
  }

  _setPinCode() {
    console.log(this.pinCode + " === " + this.pinCodeVerif)
    if(this.pinCode === this.pinCodeVerif && this.pinCode.length === 4) {
      setPinCodeFromApiWithCode(this.token, this.pinCode).then(data => {
        Alert.alert(
          "Votre code pin a bien été modifié"
        );
        this.props.navigation.goBack();
      });
    }
    else {
      var errorMessage = ""
      if(this.pinCode.length < 4 || this.pinCodeVerif.length < 4)
        errorMessage = "Les pins code n'ont pas la bonne taille";
      else
        errorMessage = "Les pins code ne correspondent pas";
      this.setState({error: errorMessage});
    }
  }

  _verification() {
    if(this.pinCode.substring(0, this.pinCodeVerif.length) !== this.pinCodeVerif)
      this.setState({error: "Les pins code ne correspondent pas"});
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
          <Text style={styles.headerTitleStyle}>{this.props.navigation.getParam('name')}</Text>
        </View>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
          <View style={styles.codePinInputBackground}>
            <TextInput
                ref={ref => this.codePinInput = ref}
                style={styles.codePinInput}
                autoFocus={true}
                secureTextEntry={true}
                maxLength={4}
                placeholder="Code pin"
                fontSize={18}
                textAlign='left'
                placeholderTextColor={'rgb(100, 95, 107)'}
                selectionColor={'#674171'}
                keyboardType={'numeric'}
                onChangeText={(pinCode) => {
                  this.pinCode = pinCode;
                  if(pinCode.length >= 4)
                    this.codePinVerifInput.focus();
                }}
            />
          </View>
          <View style={styles.codePinInputBackground}>
            <TextInput
                ref={ref => this.codePinVerifInput = ref}
                style={styles.codePinInput}
                secureTextEntry={true}
                maxLength={4}
                placeholder="Confirmer code pin"
                fontSize={18}
                textAlign='left'
                placeholderTextColor={'rgb(100, 95, 107)'}
                selectionColor={'#674171'}
                keyboardType={'numeric'}
                onChangeText={(pinCode) => {
                  this.pinCodeVerif = pinCode;
                  //this._verification();
                }}
            />
          </View>
          <Text style={styles.txtError}>{this.state.error}</Text>
          <TouchableOpacity
            style={styles.btnChange}
            onPress={() => this._setPinCode()}
          >
            <Text style={styles.txtBtnChange}>Changer</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.15,
    marginTop: 30
  },
  webPage: {
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  },
  codePinInputBackground: {
    width:'70%',
    backgroundColor:'#FFFFFF',
    borderRadius: 8,
    opacity: 0.4,
    height: 50,
    marginTop: 30
  },
  codePinInput: {
    flex: 1,
    color:'black',
    paddingLeft: 10,
  },
  btnChange: {
    backgroundColor: '#F8CE3E',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 12,
    borderRadius: 30,
    marginTop: 25,
    marginBottom: 50
  },
  txtBtnChange: {
    color: '#674171',
    fontSize: 18,
    fontWeight: '700'
  },
  txtError: {
    color: '#FF453A',
    marginTop: 20
  }
});
