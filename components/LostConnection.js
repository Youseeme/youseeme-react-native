import React from 'react';
import {ImageBackground, StyleSheet, Text, View, TouchableOpacity, ActivityIndicator} from 'react-native';
import LottieView from "lottie-react-native";
import * as Font from 'expo-font';
import { NavigationActions, StackActions } from 'react-navigation';
import NetInfo from '@react-native-community/netinfo';

export default class LostConnection extends React.Component {
  constructor(props) {
    super(props);

    this._refresh = this.props.navigation.getParam('refresh');

    this.state = {
      isLoading: false
    }

    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
  }

  _currentDate() {
    const today = new Date();
    let ss = today.getSeconds();
    let mm = today.getMinutes();
    let hh = today.getHours();
    let dd = today.getDate();
    let mmmm = today.getMonth()+1;
    const yyyy = today.getFullYear();

    if(ss<10)
        ss=`0${ss}`;
    if(mm<10)
        mm=`0${mm}`;
    if(hh<10)
        hh=`0${hh}`;
    if(dd<10)
        dd=`0${dd}`;
    if(mmmm<10)
        mmmm=`0${mmmm}`;

    return (dd + "/" + mmmm + '/' + yyyy + " - " + hh + ":" + mm + ":" + ss);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.currentDateContainer}>
          <Text style={styles.currentDateText}>{this._currentDate()}</Text>
        </View>
        {Platform.OS === 'ios' ?
        <LottieView
          style={styles.successAnimation}
          source={require('../assets/error-state-dog.json')}
          autoPlay
        /> :
        <LottieView
          style={styles.successAnimation}
          source={require('../assets/error-state-dog.json')}
          progress={1}
        />}
        <Text style={styles.successText}>
        {
          `Oups, aucune connexion internet`
        }
        </Text>
        {<TouchableOpacity
          style={styles.btnFinish}
          onPress={() => {
            NetInfo.fetch().then(state => {
              if(state.isConnected) {
                this.props.navigation.popToTop();
                //this.props.navigation.goBack();
                //this._refresh();
              }
              else {
                this.setState({isLoading: true}, () => {
                  setTimeout(() => this.setState({isLoading: false}), 2000);
                })
              }
            });
          }}
        >
          <Text style={styles.textBtnFinish}>{"Réessayer"}</Text>
        </TouchableOpacity>}
        {this.state.isLoading ?
        <View style={styles.loadingContainer}>
          <ActivityIndicator size='large' color='#674171' />
        </View> : null}
      </View>
    )
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  currentDateContainer: {
    height: 32,
    paddingHorizontal: 12,
    position: 'absolute',
    top: 90,
    backgroundColor: '#674171',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  currentDateText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  successAnimation: {
    width: 260,
    height: 260,
    marginTop: -40
  },
  successText: {
    marginTop: -50,
    color: '#674171',
    fontWeight: '500',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'SciFly'
  },
  btnFinish: {
    width: 250,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#674171',
    position: 'absolute',
    bottom: 80,
  },
  textBtnFinish: {
    color: 'white',
    fontSize: 19,
    fontFamily: 'SciFly',
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  loadingContainer: {
    flex: 1,
    //backgroundColor: "rgba(0, 0, 0, 0.2)",
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
