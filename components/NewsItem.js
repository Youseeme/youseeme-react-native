import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import Moment from 'react-moment';
import 'moment-timezone';

class NewsItem extends React.Component {

  render() {
    const {news, showNews} = this.props;

    return (
      <TouchableOpacity
        style={styles.main_container}
        onPress={() => showNews(news.id)}
      >
        <Image
          style={styles.image}
          source={{uri: news.image}}
        />
        <View style={styles.newsInfos}>
          <Text style={styles.title} numberOfLines={2}>{news.title}</Text>
          <Text style={styles.description} numberOfLines={4}>{news.description}</Text>
        </View>
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'row',
    height: 150,
    marginTop: 5,
    paddingVertical: 10,
    paddingLeft: 10,
    paddingRight: 20,
    backgroundColor: 'white',
    borderRadius: 15
  },
  newsInfos: {
    flex: 0.78,
    justifyContent: 'space-around'
  },
  image: {
    flex: 0.22,
    borderRadius: 15,
    marginRight: 15
  },
  title: {
    fontWeight: 'bold',
    fontSize: 21,
    textAlign: 'left',
    color: '#674171',
    fontFamily: "SciFly"
  },
  description: {
    fontSize: 13,
    textAlign: 'justify',
    color: 'grey',
    fontFamily: "SciFly"
  }
});

export default NewsItem;
