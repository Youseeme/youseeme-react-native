import React from 'react';
import {Alert, ImageBackground, StyleSheet, Text, View, ScrollView, TouchableOpacity} from 'react-native';
import * as Font from "expo-font";
import {WebView} from 'react-native-webview';
import { NavigationActions, StackActions } from 'react-navigation';

export default class KycRegistrationAccepted extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.props.navigation.getParam("data");
  }

  _confirmationRequest(title, message, action) {
    Alert.alert(
      title,
      message,
      [
        {
          text: "Non",
          onPress: () => {
          }
        },
        {
          text: "Oui",
          onPress: () => action()
        }
      ],
      {
        cancelable: false
      }
    );
    //return response;
  }

  _finish() {
    this._confirmationRequest("KYC", "Êtes vous sur d'avoir tout rempli et appuyé sur le bouton 'Envoyer' ?", () => {
      this.props.navigation.dispatch(StackActions.reset({
        index:0,
        actions:[
          NavigationActions.navigate({
            routeName:'Dashboard',
            params: {
              data: this.data,
              firstConnection: true
            }
          })
        ]
      }));
    });
  }

  componentDidMount() {
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
          <Text style={styles.headerTitleStyle}>{"Je fais mon KYC"}</Text>
        </View>
        <ScrollView style={{flex: 0.9}}>
          <View style={styles.mainContainer}>
            <WebView
              style={styles.webPage}
              originWhitelist={['*']}
              source={{uri: "https://www.youseeme.pro/kyc-2/"}}
            />
          </View>
          <TouchableOpacity
            style={styles.btnSend}
            onPress={() => this._finish()}
          >
            <Text style={styles.btnSendText}>{"Finir"}</Text>
          </TouchableOpacity>
        </ScrollView>
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.10,
    marginTop: 30
  },
  mainContainer: {
    width: '100%',
    height: Platform.OS === 'ios' ? 600 : 1000,
    overflow: 'hidden',
    borderRadius: 10,
    marginBottom: 20
  },
  webPage: {
    flex: 1,
    borderRadius: 10,
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  },
  btnSend: {
    width: 115,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    alignSelf: 'center',
    backgroundColor: '#F8CE3E',
    marginBottom: 30
  },
  btnSendText: {
    color: 'black',
    fontSize: 19,
    fontFamily: 'SciFly',
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  }
});
