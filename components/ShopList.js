import React from 'react';
import {Animated, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, TouchableOpacity, ScrollView, Platform} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import { SearchBar } from 'react-native-elements';
import ShopItem from './ShopItem';
import * as Font from 'expo-font';
import * as Haptics from 'expo-haptics';
import { getShopListFromApiWithToken, setFavoriteFromApiWithShopId, getSubCategoriesFromApiWithCategoryId, getCategoriesFromApi } from '../API/YSMApi';
import { getDistance, getPreciseDistance } from 'geolib';
import LottieView from "lottie-react-native";

class ShopList extends React.Component {
  constructor(props) {
    super(props);
    this.token = this.props.navigation.getParam('token');
    this.profileData = this.props.profileData;
    this.tempShops = this.props.navigation.getParam('shops');
    this.refreshShop = this.props.navigation.getParam('refreshShop');
    this.userLocation = this.props.navigation.getParam('userLocation');
    this.category = this.props.navigation.getParam('category');
    this._displayShopDetails = this.props.navigation.getParam('displayShopDetails');
    this.btnScrollTopVisible = false;
    this.btnScrollTopBottomValue = new Animated.Value(-55);
    this.state = {
      shops: [],
      isLoading: false,
      search: ''
    };
  }

  _updateSearch(search) {
    this.setState({
      search: search,
      categories: []
    }, () => this._searchShop());
  };

  _searchShop() {
    this.setState({isLoading: true});
    var shops = [], regex = new RegExp(this.state.search, 'i');

    this.tempShops.forEach((shop) => {
      if(regex.test(shop.name) || regex.test(shop.category.name))
        shops.push(shop);
    });


    this.setState({
      shops: shops.sort((a, b) => ((a.distance > b.distance) && (b.distance != -1)) ? 1 : -1),
      isLoading: false
    });
  }

  _displayScollTopButton(state) {
    if(state) {
      Animated.parallel (
        [
          Animated.timing(this.btnScrollTopBottomValue, {
              toValue: 30,
              duration: 1300,
            }
          )
        ],
        {
          useNativeDriver: false
        }
      ).start();
    }
    else {
      Animated.parallel (
        [
          Animated.timing(this.btnScrollTopBottomValue, {
            toValue: -55,
            duration: 500,
            }
          )
        ],
        {
          useNativeDriver: false
        }
      ).start(() => {this.btnScrollTopVisible = false});
    }
  }


  _loadShops(fresh) {
    this.setState({isLoading: true});

    var filterShopsByCategory = (tempShops) => {
      if(this.category.id === -1) {
        var promises = tempShops.map(shop => {
          shop.distance = this._getPreciseDistance(shop.addresses != null ? shop.addresses : null);
        });

        Promise.all(promises).then(() => {
          this.setState({
            shops: tempShops.sort((a, b) => ((a.distance > b.distance) && (b.distance != -1)) ? 1 : -1),
            isLoading: false
          });
        });
      }
      else {
        var shops = [];
        getSubCategoriesFromApiWithCategoryId(this.category.id).then(data => {
          var promises = tempShops.map(shop => {
            data.forEach(category => {
              if((shop.category.id === category.id)){
                shop.distance = this._getPreciseDistance(shop.addresses != null ? shop.addresses : null);
                shops.push(shop);
              }
            });
            if((shop.category.id === this.category.id)){
              shop.distance = this._getPreciseDistance(shop.addresses != null ? shop.addresses : null);
              shops.push(shop);
            }
          });

          Promise.all(promises).then(() => {
            this.setState({
              shops: shops.sort((a, b) => ((a.distance > b.distance) && (b.distance != -1)) ? 1 : -1),
              isLoading: false
            }, () => this.tempShops = shops);
          });
        });
      }
    };

    if(fresh) {
      getShopListFromApiWithToken(this.token).then(data => {
        this.tempShops = data;
        filterShopsByCategory(data);
      });
    }
    else
      filterShopsByCategory(this.tempShops);
  }

  _getPreciseDistance = (shopAddresses) => {
    var pdis;
    var pdisMin = -1;

    if(shopAddresses != null && this.userLocation != null) {
      shopAddresses.forEach((address) => {
        if(address.lat != null && address.lon != null) {
          pdis = getPreciseDistance(
            { latitude: this.userLocation.latitude, longitude: this.userLocation.longitude },
            { latitude: address.lat, longitude: address.lon }
          );
        }

        if(pdis < pdisMin || pdisMin === -1)
          pdisMin = pdis;
      });
    }

    //console.log((pdis/1000) + " km")
    if(pdisMin === -1)
      return -1;
    else
      return (pdisMin/1000);
  }



  _setFavoriteShop = (shop) => {
    Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
    setFavoriteFromApiWithShopId(this.token, shop.uid);

    this.refreshShop(shop);

    //this._loadShops();

    /*var index = this.state.shops.indexOf(shop);
    if(index !== -1)
      this.state.shops[index].is_favored = !this.state.shops[index].is_favored;

    this.setState()*/

    //this.reloadShops();
    //this._loadShops(true);

    /*if(true) {
      console.log("-----------------");
      console.log(shop);
      console.log("-----------------");
      console.log("@@@@@@@####### try in shop list ########@@@@@@@");

      this.setState(prevState => ({
        shops: prevState.shops.map(
          (element,index) => element.uid === shop.uid? { ...element, is_favored: !this.state.shops[index].is_favored }: element
        )
      }));
    }*/
  }

  _scrollTop() {
    this.shopsList.scrollToOffset({ animated: true, offset: 0 });
    this._displayScollTopButton(false);
  }

  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='#674171' />
        </View>
      );
    }
  }

  componentDidMount() {
    this._loadShops();
  }

  render() {
    return(
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
      <View style={styles.mainContainer}>
        <View style={styles.topHeadband}>
          <Text style={styles.headerTitleStyle}>{this.category.name}</Text>
        </View>
        <SearchBar
          placeholder="Rechercher..."
          containerStyle={{
            backgroundColor: 'transparent',
            borderBottomColor: 'transparent',
            borderTopColor: 'transparent',
            height: 48,
            justifyContent: 'center',
            marginRight: 9
          }}
          inputContainerStyle={{
            backgroundColor: 'rgba(67, 39, 74, 0.8)',
            height: 35
          }}
          inputStyle={{
            color: 'white'
          }}
          onChangeText={(search) => this._updateSearch(search)}
          value={this.state.search}
          round={true}
          showCancel={true}
        />
        {this.state.shops.length > 0 ?
        <FlatList
          ref={(ref) => {this.shopsList = ref}}
          style={styles.listContainer}
          data={this.state.shops}
          initialNumToRender={30}
          keyExtractor={(item) => item.uid.toString()}
          renderItem={({item}) => <ShopItem shop={item} setFavoriteShop={this._setFavoriteShop} getPreciseDistance={this._getPreciseDistance} displayShopDetails={this._displayShopDetails} />}
          onEndReachedThreshold={0.5}
          contentContainerStyle={{paddingBottom:110}}
          onScroll={e => {
            const offsetY = e.nativeEvent.contentOffset.y;
            if(offsetY > 1100 && !this.btnScrollTopVisible) {
              this.btnScrollTopVisible = true;
              this._displayScollTopButton(true);
            }
            else if(offsetY <= 1100 && this.btnScrollTopVisible) {
              this.btnScrollTopVisible = false;
              this._displayScollTopButton(false);
            }
          }}
          onRefresh={() => this._loadShops(true)}
          //extraData={this.state}
          refreshing={this.state.isLoading}
          onEndReached={() => {
            //this._loadShops();
          }}
        /> :
        <ScrollView
          style={{flex: 1}}
          contentContainerStyle={{justifyContent: 'center', alignItems: 'center', paddingTop: 60, paddingBottom: 90}}
        >
          {Platform.OS === 'ios' ?
          <LottieView
            style={styles.noResultsAnimation}
            source={require('../assets/error-state-dog.json')}
            autoPlay
            loop
          /> :
          <LottieView
            style={styles.noResultsAnimation}
            source={require('../assets/error-state-dog.json')}
          />}
          <Text style={styles.noResultsText}>{"Désolé, aucun résultat n'a été trouvé."}</Text>
        </ScrollView>}
        <Animated.View style={{bottom: this.btnScrollTopBottomValue, position: 'absolute', right: 15, borderRadius: 30}}>
          <TouchableOpacity
            style={styles.btnScrollTop}
            onPress={() => this._scrollTop()}
            activeOpacity={0.8}
          >
            <Ionicons
                name="ios-arrow-round-up"
                style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 0}}
              />
          </TouchableOpacity>
        </Animated.View>
        {/*this._displayLoading()*/}
      </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.05,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(103, 65, 113, 0.6)',
    paddingLeft: 10,
    paddingRight: 0
  },
  listContainer: {
    flex: 1,
    paddingRight: 10,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    marginTop: 5
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  },
  btnScrollTop: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#674171',
    borderRadius: 30
    //backgroundColor: '#F8CE3E',
  },
  noResultsAnimation: {
    width: 300,
    height: 300
  },
  noResultsText: {
    marginTop: 8,
    color: 'white',
    fontWeight: '500',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'SciFly'
  }
});

export default ShopList;
