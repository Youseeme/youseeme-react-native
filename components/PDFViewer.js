import * as React from 'react';
import { View, ImageBackground, Text, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import PDFReader from 'rn-pdf-reader-js';
import * as Sharing from 'expo-sharing';
import * as FileSystem from 'expo-file-system';
import { Feather } from '@expo/vector-icons';

export default class PDFViewer extends React.Component {

  _share(orderId, token) {
    FileSystem.downloadAsync(
      'https://web.youseeme.pro/api/v1/mcommerce/bill/' + orderId,
      FileSystem.documentDirectory + 'bill-' + orderId + '.pdf',
      {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token
        }
      }
    )
    .then(({ uri, status }) => {
      console.log('Finished downloading to ', uri);

      Sharing.shareAsync(uri);
    })
  }

  render() {
    const {token, orderId, name} = this.props.navigation.state.params;
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
          <Text style={styles.headerTitleStyle}>{name}</Text>
          <TouchableOpacity
            style={styles.btnShare}
            onPress={() => this._share(orderId, token)}
          >
            <Feather
              name="share"
              style={{fontSize: 27, paddingRight: 3, color: 'white'}}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.mainContainer}>
          <PDFReader
            source={{
              uri: 'https://web.youseeme.pro/api/v1/mcommerce/bill/' + orderId,
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
              }
            }}
          />
          </View>
        </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.10,
    marginTop: 30
  },
  mainContainer: {
    flex: 1,
    overflow: 'hidden',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  },
  btnShare: {
    position: 'absolute',
    top: Platform.OS === 'ios' ? 21 : 6,
    right: 7
  }
});
