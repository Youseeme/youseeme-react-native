import React from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import Moment from 'react-moment';
import 'moment-timezone';
import FamilyProductItem from './FamilyProductItem';

class FamilyItem extends React.Component {
  render() {
    const {familyKey, family, displayProductsDetails, addToShoppingBasket, displayProductDetailWithOptions} = this.props;
    var cptId = 0;

    return (
      <View key={familyKey} style={styles.main_container}>
        <View style={styles.topContainer}>
          <View style={styles.familyTitleContainer}>
            <Text style={styles.familyName}>{family.name}</Text>
            <Text style={styles.familyNbItems}>{"(" + family.nbItems + ")"}</Text>
          </View>
          <TouchableOpacity
            style={styles.showAllContainer}
            onPress={() => displayProductsDetails(family)}
          >
            <Text style={styles.showAllText}>{"Tout voir"}</Text>
            <Ionicons
              name={"ios-arrow-forward"}
              style={styles.arrowForward}
            />
          </TouchableOpacity>
        </View>
        {family.nbItems != null && family.nbItems > 0 ?
        <FlatList
          horizontal
          style={styles.listContainer}
          data={family.items}
          keyExtractor={(item) => (++cptId).toString()}
          renderItem={({item}) => <FamilyProductItem item={item} addToShoppingBasket={addToShoppingBasket} displayProductDetailWithOptions={displayProductDetailWithOptions} />}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            //
          }}
        /> : null}
      </View>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    marginTop: 10,
    paddingTop: 3,
    marginHorizontal: 10,
    paddingHorizontal: 2,
    borderRadius: 10,
    //backgroundColor: 'red'
  },
  listContainer: {
    paddingBottom: 15
  },
  topContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 10
  },
  familyTitleContainer: {
    flex: 0.67,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  familyName: {
    color: 'grey',
    fontSize: 18,
    fontWeight: 'bold',
    marginRight: 5,
    textAlign: 'left'
  },
  familyNbItems: {
    color: 'grey',
    fontSize: 16,
    fontWeight: 'bold'
  },
  showAllContainer: {
    flex: 0.33,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  showAllText:{
    color: '#674171',
    fontWeight: '600',
    fontSize: 14
  },
  arrowForward: {
    color: '#674171',
    fontSize: 19,
    paddingTop: 3,
    paddingLeft: 5
  }
});

export default FamilyItem;
