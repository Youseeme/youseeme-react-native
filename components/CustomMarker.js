import React from 'react';
import {StyleSheet, Image} from 'react-native';
import {Marker} from 'react-native-maps';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons } from '@expo/vector-icons';

export default class CustomMarker extends React.Component {
    constructor(props)
    {
        super(props);
        this.state = {
            tracksViewChanges: true,
        }
    }

    stopRendering = () =>
    {
        this.setState({ tracksViewChanges: false });
    }

    render()
    {
        //var marker = this.props.marker;
        const {key2, coordinate, title, image, onCalloutPress} = this.props;
        //console.log(this.props.marker);
        return (
            <Marker
                key={key2}
                coordinate={coordinate}
                title={title}
                tracksViewChanges={false}
                onCalloutPress={() => onCalloutPress()}
            >
                {/*<Image
                    source={require('../assets/location.png')}
                    style={styles.mapMarker}
                    onLoad={this.stopRendering}
                />*/}
                {/*<MaterialIcons
                  name="location-on"
                  style={{color: '#674171', fontSize: 25}}
                />*/}
            </Marker>
        )
    }

}


const styles = StyleSheet.create({
    mapMarker:
    {
        width: 25,
        height: 25,
    },
});
