import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign } from '@expo/vector-icons';
import Moment from 'react-moment';
import 'moment-timezone';
import Lightbox from 'react-native-lightbox';
import * as Haptics from 'expo-haptics';

class ShoppingBasketItem extends React.Component {
  constructor(props) {
    super(props);

    this.product = this.props.product;
    this._refreshQuantity = this.props.refreshQuantity;
    this._removeProduct = this.props.removeProduct;

    this.state = {
      quantity: this.props.product.quantity
    }
  }

  _renderImage = (image) => {
    return (
      <View style={{flex: 1, borderRadius: 5}}>
        <Image
          style={{flex: 1, resizeMode: 'contain', borderRadius: 0}}
          source={{uri: image}}
        />
      </View>
    )
  }

  _changeQuantity(sign) {
    if(sign === "+") {
      this.setState({quantity: this.state.quantity + 1}, () => {
        this._refreshQuantity(this.product, sign);
      });
      Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
    }
    else {
      if(this.state.quantity > 1) {
        this.setState({quantity: this.state.quantity - 1}, () => {
          this._refreshQuantity(this.product, sign);
        });
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
      }
      else {
        this._removeProduct(this.product);
      }
    }
  }

  _displayStockState(state) {
    var message = "", iconName = "", color = "grey";
    if(state) {
      iconName = "ios-checkmark"
      color = "rgba(52, 199, 89, 0.9)";
      message = "En stock";
    }
    else {
      iconName = "ios-close"
      color = "rgba(255, 59, 48, 0.9)";
      message = "Indisponible";
    }

    return (
      <View style={styles.bottomInfosContainer}>
        <View style={[styles.stockStateContainer, {borderColor: 'transparent', backgroundColor: color}]}>
          <Text style={[styles.stockState, {color: 'white'}]}>{message}</Text>
          {/*<Ionicons
            name={iconName}
            style={{fontSize: 25, paddingLeft: 5, paddingTop: 0, color: color}}
          />*/}
        <View style={{width: 43, height: 43, position: 'absolute', right: -1, backgroundColor: '#674171', borderRadius: 30}}>
        </View>
        <TouchableOpacity
          style={styles.btnAddToCart}
          onPress={() => {}}
        >
          <FontAwesome
            name={"shopping-basket"}
            style={{fontSize: 19, paddingRight: 1, paddingTop: 3, color: "white"}}
          />
          <Ionicons
            name={"ios-add-circle"}
            style={{fontSize: 16, paddingLeft: 0, paddingTop: 0, color: "white", position: 'absolute', top: 6, right: 6}}
          />
        </TouchableOpacity>
        </View>
      </View>
    );
  }

  _displayQuantitySelector() {
    return (
      <View style={styles.bottomInfosContainer}>
        <View style={styles.quantitySelector}>
          <TouchableOpacity
            style={styles.spinButton}
            onPress={() => this._changeQuantity("-")}
          >
            <Ionicons
              name={"ios-remove"}
              style={{fontSize: 25, paddingLeft: 1, paddingTop: 2, color: "white"}}
            />
          </TouchableOpacity>
          <Text style={styles.quantitySelectorValue}>{this.state.quantity}</Text>
          <TouchableOpacity
            style={styles.spinButton}
            onPress={() => this._changeQuantity("+")}
          >
            <Ionicons
              name={"ios-add"}
              style={{fontSize: 25, paddingLeft: 1, paddingTop: 2, color: "white"}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <TouchableOpacity
          style={styles.btnCorner}
          activeOpacity={0.9}
          onPress={() => this._removeProduct(this.product)}
        >
          <Ionicons
            name={"ios-close"}
            style={{color: '#F8CE3E', fontSize: 35}}
          />
        </TouchableOpacity>
        <View style={styles.topContainer}>
        <Lightbox style={styles.imageContainer} renderContent={() => this._renderImage(this.product.image)}>
        <View style={{flex: 1, width: 182, height: 182, backgroundColor: '#674171'}}>
            <Image
              style={styles.image}
              source={{uri: this.product.image}}
            />
          </View>
        </Lightbox>
          <View style={styles.infosContainer}>
            <View style={styles.topInfosContainer}>
              <Text numberOfLines={3} style={styles.productName}>{this.product.name}</Text>
              <View style={styles.productPricesContainer}>
                <View style={styles.productPriceContainer}>
                  <Text numberOfLines={1} style={styles.productPriceUnitTitle}>{"UNITÉ : "}</Text>
                  <Text numberOfLines={1} style={styles.productPriceUnitValue}>{this.product.price_ttc != null ? parseFloat(this.product.price_ttc.replace(/,/g,".")).toFixed(2) + " €" : "0.00 €"}</Text>
                </View>
                {/*<Text numberOfLines={1} style={styles.productPrice}>{this.product.price != null && this.product.price != this.product.price_ttc ? "HT : " + this.product.price + " €" : "0.00 €"}</Text>*/}
                <View style={styles.productPriceContainer}>
                  <Text numberOfLines={1} style={styles.productPriceTotalTitle}>{"TOTAL : "}</Text>
                  <Text numberOfLines={1} style={styles.productPriceTotalValue}>{(parseFloat(this.product.price_ttc.replace(/,/g,".")) * this.product.quantity).toFixed(2) + " €"}</Text>
                </View>
              </View>
            </View>
            {this._displayQuantitySelector()}
          </View>
        </View>
        <View style={styles.descriptionContainer}>
          <Text numberOfLines={2} style={styles.productDescription}>{this.product.description}</Text>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  mainContainer: {
    //height: 235,
    flex: 1,
    width: '98%',
    alignSelf: 'center',
    marginTop: 5,
    paddingVertical: 8,
    borderRadius: 15,
    backgroundColor: '#674171',
    paddingHorizontal: 10
  },
  topContainer: {
    flex: 0.75,
    flexDirection: 'row',

  },
  imageContainer: {
    flex: 0.45,
    //width: '50%',
    height: 140,
    //alignSelf: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: 'rgba(255, 255, 255, 0.65)'
  },
  topInfosContainer: {
    flex: 0.6,
    justifyContent: 'space-between'
  },
  bottomInfosContainer: {
    flex: 0.4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  infosContainer: {
    flex: 0.55,
    paddingLeft: 10,
    paddingRight: 2
  },
  descriptionContainer: {
    flex: 0.25,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginTop: 12,
    paddingHorizontal: 3,
  },
  productName: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 15,
    fontWeight: 'bold',
    marginRight: 20,
  },
  productDescription: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 14,
    fontWeight: '500',
    textAlign: 'justify',
    fontFamily: "SciFly"
  },
  productPriceContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  productPriceUnitTitle: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 3,
    fontFamily: "SciFly",
    paddingTop: 5
  },
  productPriceUnitValue: {
    color: '#F8CE3E',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 3,
    fontFamily: "SciFly",
    paddingTop: 5
  },
  productPriceTotalTitle: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 0,
    fontFamily: "SciFly"
  },
  productPriceTotalValue: {
    color: 'rgba(249, 207, 62, 0.85)',
    fontSize: 18,
    fontWeight: '500',
    marginTop: 0,
    fontFamily: "SciFly"
  },
  productPricesContainer: {

  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 10
  },
  stockStateContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 1,
    paddingLeft: 10,
    marginTop: 8,
    //position: 'absolute',
    //top: -10,
    //right: -5
  },
  stockState: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'rgba(255, 255, 255, 0.65)',
  },
  btnAddToCart: {
    width: 43,
    height: 43,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    //backgroundColor: '#674171',
    marginRight: -1,
    //borderWidth: 1,
    //borderColor: 'white'
    backgroundColor: 'rgba(255, 255, 255, 0.45)'
  },
  quantitySelector: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    //height: 50,
    //width: '100%'
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    borderRadius: 30,
    paddingHorizontal: 0,
    paddingVertical: 0
  },
  spinButton: {
    width: 30,
    height: 30,
    backgroundColor: 'rgba(255, 255, 255, 0.65)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30
  },
  quantitySelectorValue: {
    color: '#F8CE3E',
    fontSize: 22,
    fontWeight: '500',
    fontFamily: "SciFly",
    textAlign: 'center',
    marginHorizontal: 28,
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  btnCorner: {
    position: 'absolute',
    top: 0,
    right: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 0,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  }
});

export default ShoppingBasketItem;
