import React from 'react';
import {ImageBackground, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import LottieView from "lottie-react-native";
import * as Font from 'expo-font';
import { NavigationActions, StackActions } from 'react-navigation';

export default class SuccessPurchase extends React.Component {
  constructor(props) {
    super(props);

    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
  }

  _currentDate() {
    const today = new Date();
    let ss = today.getSeconds();
    let mm = today.getMinutes();
    let hh = today.getHours();
    let dd = today.getDate();
    let mmmm = today.getMonth()+1;
    const yyyy = today.getFullYear();

    if(ss<10)
        ss=`0${ss}`;
    if(mm<10)
        mm=`0${mm}`;
    if(hh<10)
        hh=`0${hh}`;
    if(dd<10)
        dd=`0${dd}`;
    if(mmmm<10)
        mmmm=`0${mmmm}`;

    return (dd + "/" + mmmm + '/' + yyyy + " à " + hh + ":" + mm + ":" + ss);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.currentDateContainer}>
          <Text style={styles.currentDateText}>{this._currentDate()}</Text>
        </View>
        {Platform.OS === 'ios' ?
        <LottieView
          style={styles.successAnimation}
          source={require('../assets/success.json')}
          autoPlay
          loop={false}
        /> :
        <LottieView
          style={styles.successAnimation}
          source={require('../assets/success.json')}
          autoPlay
          loop={false}
          //progress={1}
        />}
        <Text style={styles.successText}>
        {
          `Merci pour votre commande vous serez notifié dès qu'elle sera prête !`
        }
        </Text>
        <TouchableOpacity
          style={styles.btnFinish}
          onPress={() => {
            this.props.navigation.popToTop();
          }}
        >
          <Text style={styles.textBtnFinish}>{"Continuer mes achats"}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  currentDateContainer: {
    height: 32,
    paddingHorizontal: 12,
    position: 'absolute',
    top: 90,
    backgroundColor: '#674171',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center'
  },
  currentDateText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  successAnimation: {
    width: 260,
    height: 260,
    marginTop: -40
  },
  successText: {
    marginTop: -50,
    color: '#674171',
    fontWeight: '500',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'SciFly'
  },
  btnFinish: {
    width: 250,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: '#674171',
    position: 'absolute',
    bottom: 80,
  },
  textBtnFinish: {
    color: 'white',
    fontSize: 19,
    fontFamily: 'SciFly',
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
});
