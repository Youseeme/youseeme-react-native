import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, ScrollView, Platform} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import TransactionItem from './TransactionItem';
import * as Font from 'expo-font';
import {getTransactionsFromApiWithAccountToken} from '../API/YSMApi';
import LottieView from "lottie-react-native";

class AccountStatement extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.currentPage = 0;
    this.tabValues = ["Tout", "Crédits", "Débits"];
    this.call1 = false;
    this.call2 = false;

    this.state = {
      transactions: [],
      isLoading: true,
      selectedIndex: 0,
    };

  }

  _loadTransactions() {
    this.setState({
      isLoading: true,
      transactions: []
    });
    if(this.currentPage === 0 || this.currentPage === 1) {
      getTransactionsFromApiWithAccountToken(this.token, 'debit').then(data => {
        this.setState({
          transactions: [...this.state.transactions, ...data]
        }, () => {
          this.call1 = true;
          this._refresh();
        });
      });
    }
    if(this.currentPage === 0 || this.currentPage === 2) {
      getTransactionsFromApiWithAccountToken(this.token, 'credit').then(data => {
        this.setState({
          transactions: [...this.state.transactions, ...data],
        }, () => {
          this.call2 = true;
          this._refresh();
        });
      });
    }
  }

  _refresh() {
    if((this.currentPage === 0 && this.call1 && this.call2) || (this.currentPage === 1 && this.call1) || (this.currentPage === 2 && this.call2)) {
      this.setState({
        isLoading: false,
        transactions: this._sortByDate(this.state.transactions)
      }, () => {
        this._sortByDate(this.state.transactions);

        this.call1 = false;
        this.call2 = false;
      });
    }
  }

  _sortByDate(transactions) {
    const sortByMapped = (map,compareFn) => (a,b) => compareFn(map(a.created_at),map(b.created_at));
    const toDate = e => new Date(e).getTime();
    const byValue = (a,b) => b - a;
    const byDate = sortByMapped(toDate,byValue);

    return [...transactions].sort(byDate);
  }

  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' color='#674171' />
        </View>
      );
    }
  }

  _handleIndexChange = index => {
    if(this.tabValues[index] === "Tout")
      this.currentPage = 0;
    else if(this.tabValues[index] === "Crédits")
      this.currentPage = 1;
    else
      this.currentPage = 2;

    this.setState({
      selectedIndex: this.currentPage,
      transactions: []
    }, () => this._loadTransactions());

  };

  componentDidMount() {
    this._loadTransactions();
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            <Text style={styles.descriptiveText}>
                {"Retrouve l’historique de tes opérations"}
            </Text>
        </View>
        <View style={styles.mainContainer}>
        <View style={styles.tabView}>
          <SegmentedControlTab
            borderRadius = {15}
            tabsContainerStyle={styles.tabsContainerStyle}
            tabStyle={styles.tabStyle}
            firstTabStyle={styles.firstTabStyle}
            lastTabStyle={styles.lastTabStyle}
            tabTextStyle={styles.tabTextStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTabTextStyle={styles.activeTabTextStyle}
            allowFontScaling={false}
            values={this.tabValues}
            selectedIndex={this.state.selectedIndex}
            onTabPress={this._handleIndexChange}
          />
        </View>
        {this.state.transactions.length > 0 || this.state.isLoading ?
        <FlatList
          style={styles.listContainer}
          data={this.state.transactions}
          scrollIndicatorInsets={{ right: 1 }}
          keyExtractor={(item) => (++this.cptId).toString()}
          renderItem={({item}) => <TransactionItem transaction={item} />}
          refreshing={this.state.isLoading}
          onEndReachedThreshold={0.5}
          onRefresh={() => this._loadTransactions()}
          onEndReached={() => {
            //this._loadTransactions();
          }}
        /> :
        <ScrollView
          style={styles.listContainer}
          contentContainerStyle={{justifyContent: 'center', alignItems: 'center', paddingTop: 60, paddingBottom: 90}}
        >
          {Platform.OS === 'ios' ?
          <LottieView
            style={styles.noResultsAnimation}
            source={require('../assets/error-state-dog.json')}
            autoPlay
            loop
          /> :
          <LottieView
            style={styles.noResultsAnimation}
            source={require('../assets/error-state-dog.json')}
          />}
          <Text style={styles.noResultsText}>{"Votre historique de transactions est vide."}</Text>
        </ScrollView>}
        {/*this._displayLoading()*/}
      </View>
      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  listContainer: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:35,
    color: "#f8cd3e",
    marginTop:10,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
  },
  tabView: {
    marginTop: 15,
    marginRight: 40,
    marginLeft: 40
  },
  tabStyle: {
    borderColor: '#6D4877',
    height: 35,
    backgroundColor: 'transparent'
  },
  tabTextStyle: {
    color:'#6D4877',
    fontFamily: "SciFly"
  },
  activeTabStyle: {
    backgroundColor: '#6D4877',
  },
  activeTabTextStyle: {
    color:'white'
  },
  noResultsAnimation: {
    width: 300,
    height: 300
  },
  noResultsText: {
    marginTop: 8,
    color: '#6D4877',
    fontWeight: '500',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'SciFly',
    paddingHorizontal: 30
  }
});

export default AccountStatement;
