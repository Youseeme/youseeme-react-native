import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image, Platform} from 'react-native';
import Moment from 'react-moment';
import 'moment-timezone';

class ArticleItem extends React.Component {

  render() {
    const {article} = this.props;

    return (
      <View
        style={styles.main_container}
      >
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{uri: article.image}}
          />
        </View>
        <View style={styles.articleInfos}>
          <Text style={styles.name} numberOfLines={2}>{article.name}</Text>
          <Text style={styles.price}>{article.price_ttc.toFixed(2) + " €"}</Text>
          <View style={styles.descriptionContainer}>
            <Text style={styles.description} numberOfLines={4}>{article.description}</Text>
          </View>
          <View style={styles.quantityContainer}>
            <Text style={styles.titleQuantity}>{"Quantité : "}</Text>
            <Text style={styles.quantity} numberOfLines={4}>{article.pivot.quantity}</Text>
          </View>
        </View>
        {/*<View style={styles.articleInfosRight}>
          <View style={styles.quantityContainer}>
            <Text style={styles.titleQuantity}>{"Quantité : "}</Text>
            <Text style={styles.quantity} numberOfLines={4}>{article.pivot.quantity}</Text>
          </View>
        </View>*/}
      </View>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'row',
    height: 130,
    marginTop: 5,
    paddingVertical: 10,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: 'white',
    borderRadius: 15
  },
  quantityContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  articleInfos: {
    flex: 0.71,
    justifyContent: 'space-around',
    marginTop: 3
  },
  articleInfosRight: {
    flex: 0.3,
    justifyContent: 'space-around',
    alignItems: 'flex-end'
  },
  imageContainer: {
    flex: 0.39,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 15,

    borderWidth: 1,
    borderColor: '#674171',
    marginRight: 15,
    overflow: 'hidden'
  },
  image: {
    //width: 107,
    //height: 107,
    width: '100%',
    height: '100%',
    //borderRadius: 15,
    //marginRight: 15,
    //borderWidth: 1,
    //borderColor: Platform.OS == "ios" ? '#674171' : 'transparent',
    resizeMode: 'cover'
  },
  name: {
    fontWeight: 'bold',
    fontSize: 19,
    textAlign: 'left',
    color: 'black',
    fontFamily: "SciFly"
  },
  price: {
    fontWeight: '400',
    fontSize: 18,
    textAlign: 'left',
    color: '#674171',
    fontFamily: "SciFly",
    marginTop: 1,
    marginBottom: 3
  },
  titleQuantity: {
    fontWeight: '400',
    fontSize: 18,
    textAlign: 'right',
    color: 'black',
    fontFamily: "SciFly",
    marginRight: 8
  },
  quantity: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'right',
    color: '#674171',
    fontFamily: "SciFly"
  },
  description: {
    fontSize: 13,
    textAlign: 'justify',
    color: 'grey',
    fontFamily: "SciFly",
  }
});

export default ArticleItem;
