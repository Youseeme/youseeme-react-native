import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import NewsItem from './NewsItem';
import * as Font from 'expo-font';
import {getNewsFromApi, getNewsFromApiWithId} from '../API/YSMApi';


class News extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.currentPage = 0;
    this.tabValues = ["Tout", "Crédits", "Débits"];
    this.call1 = false;
    this.call2 = false;

    this.state = {
      news: [],
      isLoading: false,
      selectedIndex: 0,
    };

  }

  _loadNews() {
    this.setState({isLoading: true});
    getNewsFromApi().then(data => {
      this.setState({
        news: data,
        isLoading: false
      })
    });
  }

  _showNews = (id) => {
    this.props.navigation.navigate('NewsDetails', {token: this.token, newsId: id});
  }

  componentDidMount() {
    this._loadNews();
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            <Text style={styles.descriptiveText}>
                {"\nInfos croustillantes, offres inratables, nouveautés sur l’application...\nC\’est ici que ça se passe."}
            </Text>
        </View>
        <View style={styles.mainContainer}>
        <FlatList
          style={styles.listContainer}
          contentContainerStyle={{paddingTop: 10, paddingBottom: 100}}
          data={this.state.news}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => <NewsItem news={item} showNews={this._showNews}/>}
          refreshing={this.state.isLoading}
          onEndReachedThreshold={0.5}
          onRefresh={() => this._loadNews()}
          onEndReached={() => {
            //this._loadTransactions();
          }}
        />
      </View>
      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  listContainer: {
    marginTop: 1,
    marginLeft: 10,
    paddingRight: 10,
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:35,
    color: "#f8cd3e",
    marginTop:10,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
  }
});

export default News;
