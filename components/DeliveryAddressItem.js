import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import * as Font from 'expo-font';
import Moment from 'react-moment';
import 'moment-timezone';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';

class DeliveryAddressItem extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });

    this._setSelectedAddress = this.props.setSelectedAddress;
    this.currentAddress = this.props.currentAddress;
    this.address = this.props.address;
    this.type = this.props.type;

    this.state = {
      selected:  (JSON.stringify(this.currentAddress) === JSON.stringify(this.address))
    }
  }

  _select(state) {
    this.setState({
      selected: !state
    }, () => {
      this._setSelectedAddress(this.address, !state);
    });
  }

  componentDidMount() {
    /*if(this.currentAddress == this.address) {
      this.setState({
        selected: !state
      });
    }*/
  }

  render() {
    return (
      <TouchableOpacity
        style={[styles.mainContainer, {borderColor: this.state.selected ? '#34C759' : 'transparent'}]}
        onPress={() => this._select(this.state.selected)}
      >
        <View style={styles.addressNameContainer}>
          <Text numberOfLines={1} style={styles.addressName}>{this.address.name}</Text>
        </View>
        <View style={styles.addressContainer}>
          <View style={styles.addressInfos}>
            <View style={styles.textAddressContainer}>
              <Text numberOfLines={2} style={styles.addressPart1}>{this.address.address + ","}</Text>
              <Text numberOfLines={2} style={styles.addressPart2}>{this.address.zipcode + ", " + this.address.city}</Text>
            </View>
            {this.type === "HOME" ?
            <Moment style={styles.date} element={Text} interval={0} tz="Europe/Paris" format="\Mo\difié \l\e DD/MM/YYYY à HH:mm:ss">
              {this.address.updated_at.substring(0, 10) + " " + this.address.updated_at.substring(11, 16) + ":" + this.address.updated_at.substring(17, 19)}
            </Moment> : null}
          </View>

          <View style={styles.chooseIconContainer}>
            <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.selected ? '#34C759' : '#D9D8D9'}]}>
              <Ionicons
                name="ios-checkmark"
                style={[styles.chooseIcon, {color: this.state.selected ? 'white' : '#D9D8D9'}]}
              />
            </View>
          </View>

        </View>
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    height: 150,
    marginTop: 15,
    paddingTop: 3,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 15,
    backgroundColor: 'white',
    borderWidth: 1.25,
    borderColor: 'transparent',

    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.5
  },
  addressNameContainer: {
    flex: 0.2,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'grey'
  },
  addressContainer: {
    flex: 0.8,
    paddingTop: 3,
    flexDirection: 'row',
  },
  addressInfos: {
    flex: 0.9,
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  addressName: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center'
  },
  textAddressContainer: {
    flex: 0.9,
    alignItems: 'flex-start',
    justifyContent: 'center',
    //backgroundColor: 'red'
  },
  addressPart1: {
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#674171',
    fontSize: 16,
    paddingVertical: 3
  },
  addressPart2: {
    textAlign: 'left',
    color: 'grey',
    paddingVertical: 3
  },
  date: {
    color:'#674171',
    fontSize: 12,
  },
  chooseIconContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  chooseIconSubContainer: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    backgroundColor: '#D9D8D9',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30
  },
  chooseIcon: {
    color: 'white',
    fontSize: 30
  }
});

export default DeliveryAddressItem;
