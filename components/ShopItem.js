import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TouchableHighlight} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Rating, AirbnbRating } from 'react-native-ratings';

class ShopItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      favored: this.props.shop.is_favored
    };

  }

  componentDidMount(prevProps) {

  }

  componentDidUpdate(prevProps, prevState) {
    /*if (prevProps.shop.is_favored !== this.state.favored) {
      console.log("\n\n\n\n")
      console.log("prevProps.shop.is_favored : " + prevProps.shop.is_favored)
      console.log("------------------------")
      console.log("prevState.favored : " + prevState.favored)
      console.log("------------------------")
      console.log("this.state.favored : " + this.state.favored)
      //this.setState({favored: prevProps.shop.is_favored});

    }*/
  }

  _favored(state) {
    var iconName = "ios-heart"
    if(!state)
      iconName+="-empty"
    return (
      <Ionicons
        name={iconName}
        style={{/*color: '#F8CE3E'*/color: '#674171', fontSize: 25, width: 21, height: 23}}
      />
    );
  }

  render() {
    const {shop, setFavoriteShop, getPreciseDistance, displayShopDetails} = this.props;
    const WATER_IMAGE = require('../assets/wallet.png')
    return (
      <View styles={{flex: 1}}>
        <TouchableOpacity
          style={{ position: 'absolute', top: 5, right: 0, /*backgroundColor: '#674171',*/backgroundColor: '#F8CE3E', borderTopLeftRadius: 0, borderTopRightRadius: 15, borderBottomLeftRadius: 20, borderBottomRightRadius: 0, width: 35, height: 35, justifyContent: 'center', alignItems: 'center', zIndex: 1}}
          activeOpacity={0.99}
          onPress={() => {
            setFavoriteShop(shop);
            console.log(shop.is_favored);
            this.setState({favored: !this.state.favored});
          }}
          >
            {this._favored(this.state.favored)}
            {/*this._favored(shop.is_favored)*/}
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.main_container}
          onPress={() => displayShopDetails(shop.uid, shop.distance, setFavoriteShop)}
        >
          <View style={styles.coverContainer}>
            <Image
              style={styles.coverImage}
              source={{uri: shop.cover_image}}
            />

          </View>
          <View style={styles.infosContainer}>
            <View style={styles.profileContainer}>
              <View style={styles.imageContainer}>
                <Image
                  style={styles.profileImage}
                  source={{uri: shop.profile_image}}
                />
              </View>
              <View style={styles.textContainer}>
                <Text style={styles.title}>{shop.name}</Text>
                <Text style={styles.distance}>{(shop.distance != null && shop.distance != -1) ? shop.distance.toFixed(2) + " km" : "Aucun établissemeent"}</Text>
                {/*<Rating
                  type='custom
                  ratingImage={WATER_IMAGE}
                  ratingColor='#3498db'
                  ratingBackgroundColor='rgba(255, 255, 255, 0)'
                  ratingCount={5}
                  imageSize={30}
                  onFinishRating={this.ratingCompleted}
                  style={{ paddingVertical: 10 }}
                />*/}
              </View>
            </View>
            <View style={styles.ratingContainer}>
              <AirbnbRating
                count={5}
                size={15}
                reviews={["Terrible", "Décevant", "Bien", "Génial", "Incroyable"]}
                reviewSize={12}
                defaultRating={shop.average_rating}
                isDisabled={true}
                selectedColor={'#674171'}
                reviewColor={'rgba(103, 65, 113, 0.9)'}
              />
            </View>
          </View>
          <View style={styles.descriptionContainer}>
            <Text style={styles.description} numberOfLines={2}>{shop.description}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    height: 230,
    marginTop: 5,
    paddingTop: 3,
    paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 15,
    backgroundColor: 'white'
  },
  coverContainer: {
    flex: 0.55,
    alignItems: 'center',
    justifyContent: 'center'
  },
  infosContainer: {
    flex: 0.25,
    paddingTop: 2,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  descriptionContainer: {
    flex: 0.2,
    justifyContent: 'center'
  },
  profileContainer: {
    flex: 0.7,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  ratingContainer: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  imageContainer: {
    flex: 0.22,
    justifyContent: 'center',
  },
  textContainer: {
    flex: 0.78,
    paddingTop: 2,
    paddingLeft: 2,
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  coverImage: {
    marginTop: 3,
    width: '100%',
    height: '100%',
    borderRadius: 10
  },
  profileImage: {
    width: 50,
    height: 50,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#674171'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#674171',
    flexWrap: 'wrap'
  },
  distance: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'grey',
    opacity: 1
  },
  description: {
    fontSize: 13,
    color: 'grey',
    opacity: 1,
    textAlign: 'justify'
  }

});

export default ShopItem;
