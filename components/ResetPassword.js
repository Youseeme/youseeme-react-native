import React from 'react';
import {  StyleSheet, View,ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager } from 'react-native';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import {setPasswordFromApiWithEmail} from '../API/YSMApi';

export default class ResetPassword extends React.Component{
    constructor(props) {
        super(props);
        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });

        this.state = {
            email: '',

        };
    }

    componentDidMount() {

    }

    ResetPassword() {
        setPasswordFromApiWithEmail(this.state.email).then(data => {

            console.log("=====RETURN=====");
            console.log(data);
            if(data === '{"feedback":"Reset password email sent."}') {
                this.props.navigation.navigate('First');
            }
            else {
                alert("Cette email n'existe pas");

            }

        });
    }
    render() {

        return (

            <ImageBackground  source={require('../assets/fond.png')} style={{width: '100%', height: '100%'
            }}>
                <View style={styles.container}>



                    <View
                        style={{
                            width:350,
                            alignItems: 'center',
                            marginBottom: 15,
                            flex:0.4,
                            marginTop:27,
                            justifyContent: 'center',

                        }}>
                        <Image
                            style={styles.logo}
                            source={require('../assets/logo.png')}/>
                        <Text  style={{
                            fontSize:35, marginTop: 30, color: "#f8cd3e", fontFamily: "SciFly",


                        }}>
                            Mot de passe oublié
                        </Text>

                    </View>
                    <View
                        style={{
                            width: '100%',
                            alignItems: 'center',
                            justifyContent:'center',
                            marginBottom: 0,
                            backgroundColor:'#D9D8D9',
                            flex:2,
                            opacity: 0.8,
                            borderRadius:13,


                        }}>



                        <View style={{
                            width:350,
                            alignItems: 'center',
                            justifyContent:'center',
                            backgroundColor:'white',
                            flex:0.6,
                            borderRadius:13,
                            marginTop:25,
                            marginBottom:70
                        }}>
                            <View style={{
                                width:300,
                                alignItems: 'center',
                                justifyContent:'center',
                                borderRadius:13,
                                marginTop:0,
                            }}>
                                <Text style={{
                                    fontSize:20, marginBottom: 30, color: "black", fontFamily: "SciFly",

                                }}>Votre email
                                </Text>
                                <Input
                                    style={styles.input}
                                    placeholder="Email@youseeme.pro"
                                    fontSize={18}
                                    family="antdesign"
                                    onChangeText={(email) => this.setState({email})}
                                    value={this.state.email}
                                    placeholderTextColor={'black'}
                                    color={'black'}
                                />
                                <TouchableOpacity
                                    onPress={()=>this.ResetPassword()}
                                    style={styles.buttonStyle1}>
                                    <View style={styles.textStyle}>
                                        <Text style={styles.textStyle}>Envoyer</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View
                                style={{
                                    marginTop:12,
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    marginLeft:120,

                                }}>

                            </View>
                        </View>



                    </View>

                </View>
            </ImageBackground>

        );

    }



}




const styles = StyleSheet.create({

    container: {
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    input: {
        fontSize:   60,
        width: 300,
        color:'black',
        backgroundColor:'#D9D8D9',
        borderRadius: 8,
        opacity: 0.6,
        height: 50,
        marginTop: 0,

    },

    buttonStyle: {
        backgroundColor: "#40BCF8",
        marginRight: 50,
        borderRadius: 50,
        width: 110,
        borderWidth: 0.1,

    },
    buttonStyle1: {
        backgroundColor: "#f8cd3e",
        borderRadius: 50,
        width: 110,
        borderWidth: 0.1,
        marginTop:30

    },
    buttonStyle2: {
        backgroundColor: "#C0C0C0",
        borderRadius: 10,
        width: 260,
        borderWidth: 0.1,
        marginTop:20,
        opacity: 0.7,


    },
    logo: {
        width: 200,
        height: 60,
        resizeMode: 'contain'
    },
    profil_photo: {
        width: 130,
        height: 130,
        resizeMode: 'contain',
        marginTop:15,
    },
    textStyleforget: {
        fontSize: 20,
        color: "#f8cd3e",
        top:10,
        fontFamily: "SciFly",

    },

    textStyle: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 5,
        paddingBottom: 5,
        fontFamily: "SciFly",

    },
    textStyleButton: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 10,
        paddingBottom: 10,
        fontFamily: "SciFly",

    },
    backblue: {
        backgroundColor: "#FB1352",
        height:80,
        alignSelf : 'stretch',
        textAlign: 'center',
        //marginTop: 5,
        color: 'white',
        fontSize:25,
        paddingTop: 25,
    },
});
