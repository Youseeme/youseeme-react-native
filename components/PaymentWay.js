import React from 'react';
import {AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Platform, TouchableOpacity, ScrollView} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign, Feather } from '@expo/vector-icons';
import SegmentedControlTab from "react-native-segmented-control-tab";
import * as Font from 'expo-font';
import { BlurView } from 'expo-blur';
import {getUserBalanceFromApiWithAccountToken, getCardsFromApiWithAccountToken, getWalletInfoFromApiWithToken} from '../API/YSMApi';

class PaymentWay extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.token = this.props.navigation.getParam('token');
    this.deliveryWay = this.props.navigation.getParam('deliveryWay');
    this.totalAmount = this.props.navigation.getParam('totalAmount');
    this._refreshPaymentWay = this.props.navigation.getParam('refreshPaymentWay');

    this.state = {
      paymentWay: this.props.navigation.getParam('paymentWay'),
      balance: {},
      savedCards: [],
      bankInfo: {},
      isLoading: true,
      selectedSavedCard: this.props.navigation.getParam('selectedSavedCard'),
    };
  }



  _loadProfile = () => {
    getUserBalanceFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        balance: data,
        isLoading: false
      });
    });
  }

  _refreshSelectedSavedCard = (card) => {
    this.setState({
      paymentWay: "SAVED-CARD",
      selectedSavedCard: card
    }, () => {
      this._refreshPaymentWay("SAVED-CARD", this.state.selectedSavedCard);
      this.props.navigation.goBack();
    });
  }

  _loadCards() {
    getCardsFromApiWithAccountToken(this.token).then(data => {
      console.log(data);
      this.setState({
        savedCards: data,
        isLoading: false
      });
    });
  }

  _getBankInfo() {
    getWalletInfoFromApiWithToken(this.token).then(data => {
      console.log(data)
      this.setState({
        bankInfo: data
      });
    });
  }

  _displayEWalletOption() {
    const disabledColor = '#D9D8D9';
    var valid = (parseFloat(this.state.balance.balance) >= this.totalAmount);
      if(Object.entries(this.state.balance).length !== 0) {
        return (
          <View style={[styles.chooseContainer, {marginTop: 5}]}>
            {/*<Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>*/}
            <TouchableOpacity
              style={
                [styles.btnChoose,
                {borderColor: this.state.paymentWay === "E-WALLET" ? '#34C759' : 'transparent'}]
              }
              disabled={!valid}
              onPress={() => {
                if(this.state.paymentWay === "E-WALLET") {
                  this.setState({
                    paymentWay: null,
                  });
                  this._refreshPaymentWay(null, null);
                }
                else {
                  this.setState({
                    paymentWay: "E-WALLET",
                  }, () => {
                    this._refreshPaymentWay("E-WALLET", parseFloat(this.state.balance.balance));
                    this.props.navigation.goBack();
                  });
                }
              }}
            >
              {/*<View style={styles.deliveryFeeContainer}>
                <Text style={styles.deliveryFee}>{"GRATUIT"}</Text>
              </View>*/}
              <View style={styles.rightContainer}>
                <View style={styles.infoChooseContainer}>
                  <Text style={[styles.btnChooseTitle, {color: valid ? '#674171' : disabledColor}]}>{"Porte-monnaie en euros"}</Text>
                  <Text style={[styles.btnChooseDescription, {color: valid ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{"Votre solde est de : " + parseFloat(this.state.balance.balance).toFixed(2) + " €"}</Text>
                </View>
                <View style={styles.chooseImageContainer}>
                  <Image
                    style={[styles.chooseImage, {width: '75%', height: '75%'}]}
                    source={valid ? require('../assets/eWallet.png') : require('../assets/eWalletDisabled.png')}
                  />
                </View>
              </View>
              <View style={styles.chooseIconContainer}>
                <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.paymentWay === "E-WALLET" ? '#34C759' : '#D9D8D9'}]}>
                  <Ionicons
                    name="ios-checkmark"
                    style={[styles.chooseIcon, {color: this.state.paymentWay === "E-WALLET" ? 'white' : '#D9D8D9'}]}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
      );
    }
    else
      return null;
  }

  _displayBWalletOption() {
    const disabledColor = '#D9D8D9';
    var valid = (parseFloat(this.state.balance.points) >= this.totalAmount);
      if(Object.entries(this.state.balance).length !== 0) {
        return (
          <View style={styles.chooseContainer}>
            {/*<Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>*/}
            <TouchableOpacity
              style={
                [styles.btnChoose,
                {borderColor: this.state.paymentWay === "B-WALLET" ? '#34C759' : 'transparent'}]
              }
              disabled={!valid}
              onPress={() => {
                if(this.state.paymentWay === "B-WALLET") {
                  this.setState({
                    paymentWay: null,
                  });
                  this._refreshPaymentWay(null, null);
                }
                else {
                  this.setState({
                    paymentWay: "B-WALLET",
                  }, () => {
                    this._refreshPaymentWay("B-WALLET", parseFloat(this.state.balance.points));
                    this.props.navigation.goBack();
                  });
                }
              }}
            >
              <View style={styles.rightContainer}>
                <View style={styles.infoChooseContainer}>
                  <Text style={[styles.btnChooseTitle, {color: valid ? '#674171' : disabledColor}]}>{"Porte-monnaie en bartcoins"}</Text>
                  <Text style={[styles.btnChooseDescription, {color: valid ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{"Votre solde est de : " + parseFloat(this.state.balance.points).toFixed(2) + " bartcoin(s)"}</Text>
                </View>
                <View style={styles.chooseImageContainer}>
                  <Image
                    style={[styles.chooseImage, {width: '85%', height: '85%'}]}
                    source={valid ? require('../assets/bWallet.png') : require('../assets/bWalletDisabled.png')}
                  />
                </View>
              </View>
              <View style={styles.chooseIconContainer}>
                <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.paymentWay === "B-WALLET" ? '#34C759' : '#D9D8D9'}]}>
                  <Ionicons
                    name="ios-checkmark"
                    style={[styles.chooseIcon, {color: this.state.paymentWay === "B-WALLET" ? 'white' : '#D9D8D9'}]}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
      );
    }
    else
      return null;
  }

  _displaySavedCardOption() {
    const disabledColor = '#D9D8D9';
    var valid = (this.state.savedCards.length > 0);
        return (
        <View style={styles.chooseContainer}>
          {/*<Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>*/}
          <TouchableOpacity
            style={
              [styles.btnChoose,
              {borderColor: this.state.paymentWay === "SAVED-CARD" ? '#34C759' : 'transparent'}]
            }
            disabled={!valid}
            onPress={() => {
              if(this.state.paymentWay === "SAVED-CARD") {
                this.setState({
                  paymentWay: null,
                });
                this._refreshPaymentWay(null, null);
              }
              else {
                /*this.setState({
                  paymentWay: "SAVED-CARD",
                }, () => {
                  this._refreshPaymentWay("SAVED-CARD", null);
                  this.props.navigation.goBack();
                });*/
                this.props.navigation.navigate('BankCardsChoice', {
                  cards: this.state.savedCards,
                  refreshSelectedSavedCard: this._refreshSelectedSavedCard
                });
              }
            }}
          >
            <View style={styles.rightContainer}>
            {this.state.selectedSavedCard != null ?
              <View style={styles.infoChooseContainer}>
                <Text style={[styles.btnChooseTitle, {color: valid ? '#674171' : disabledColor}]}>{"Carte bancaire enregistrée"}</Text>
                <Text style={[styles.btnChooseDescription, {color: valid ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{this.state.selectedSavedCard.code.replace(/X/g, "•") + "\n" + this.state.selectedSavedCard.expiration}</Text>
              </View> :
              <View style={styles.infoChooseContainer}>
                <Text style={[styles.btnChooseTitle, {color: valid ? '#674171' : disabledColor}]}>{"Carte bancaire enregistrée"}</Text>
                <Text style={[styles.btnChooseDescription, {color: valid ? 'rgba(103, 65, 113, 0.7)' : disabledColor}]}>{"Choisissez parmi vos cartes enregistrées"}</Text>
              </View>}
              <View style={styles.chooseImageContainer}>
                <Image
                  style={styles.chooseImage}
                  source={valid ? require('../assets/savedCard.png') : require('../assets/savedCardDisabled.png')}
                />
              </View>
            </View>
            <View style={styles.chooseIconContainer}>
              <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.paymentWay === "SAVED-CARD" ? '#34C759' : '#D9D8D9'}]}>
                <Ionicons
                  name="ios-checkmark"
                  style={[styles.chooseIcon, {color: this.state.paymentWay === "SAVED-CARD" ? 'white' : '#D9D8D9'}]}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
  }


  componentDidMount() {
    this._loadProfile();
    this._getBankInfo();
    this._loadCards();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#674171'}}>
        <View style={styles.topHeadband}>
            <Text  style={styles.topTitle}>
                {"Moyen de paiement"}
            </Text>
        </View>
        <View style={styles.mainContainer}>
          {this.state.isLoading ?
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' color='#674171' />
            </View> :
            <ScrollView
              style={styles.listContainer}
              scrollIndicatorInsets={{ right: 1 }}
              contentContainerStyle={{ paddingBottom: 90}}
            >
              {this._displayEWalletOption()}
              {this._displayBWalletOption()}

                <View style={styles.chooseContainer}>
                  {/*<Text style={styles.chooseDescriptionText}>{"MODE DE LIVRAISON"}</Text>*/}
                  <TouchableOpacity
                    style={
                      [styles.btnChoose,
                      {borderColor: this.state.paymentWay === "NEW-CARD" ? '#34C759' : 'transparent'}]
                    }
                    onPress={() => {
                      if(this.state.paymentWay === "NEW-CARD") {
                        this.setState({
                          paymentWay: null,
                        });
                        this._refreshPaymentWay(null, null);
                      }
                      else {
                        this.setState({
                          paymentWay: "NEW-CARD",
                        }, () => {
                          this._refreshPaymentWay("NEW-CARD", null);
                          this.props.navigation.goBack();
                        });
                      }
                    }}
                  >
                    <View style={styles.rightContainer}>
                      <View style={styles.infoChooseContainer}>
                        <Text style={styles.btnChooseTitle}>{"Nouvelle carte bancaire"}</Text>
                        <Text style={styles.btnChooseDescription}>{"Utilisez une carte que vous n'avez pas enregistrée pour cet achat"}</Text>
                      </View>
                      <View style={styles.chooseImageContainer}>
                        <Image
                          style={[styles.chooseImage, {width: '70%', height: '65%', marginTop: 25}]}
                          source={require('../assets/newCard.png')}
                        />
                      </View>
                    </View>
                    <View style={styles.chooseIconContainer}>
                      <View style={[styles.chooseIconSubContainer, {backgroundColor: this.state.paymentWay === "NEW-CARD" ? '#34C759' : '#D9D8D9'}]}>
                        <Ionicons
                          name="ios-checkmark"
                          style={[styles.chooseIcon, {color: this.state.paymentWay === "NEW-CARD" ? 'white' : '#D9D8D9'}]}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>

                {this._displaySavedCardOption()}

            </ScrollView>}
          </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171'
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    overflow: 'hidden'
  },
  listContainer: {
    width: '100%',
    //flex: 1,
    //paddingTop: 30,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    paddingHorizontal: 10,
    paddingTop: 15,
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:22,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  chooseContainer: {
    width: '99%',
    alignSelf: 'center',
    marginTop: 15
  },
  chooseDescriptionText: {
    color: '#674171',
    fontSize: 15,
    fontWeight: '600',
    marginBottom: 8,
    paddingLeft: 20
  },
  btnChoose: {
    width: '100%',
    height: 200,
    flexDirection: 'row',
    alignSelf: 'center',
    borderWidth: 1.25,
    borderColor: 'transparent',

    //backgroundColor: '#674171',
    backgroundColor: 'white',

    borderRadius: 15,
    paddingTop: 15,
    paddingBottom: 5,
    paddingLeft: 3,
    paddingRight: 12,

    shadowColor: 'grey',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 0.5
  },
  chooseIconContainer: {
    flex: 0.1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  chooseIconSubContainer: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    backgroundColor: '#D9D8D9',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30
  },
  chooseIcon: {
    color: 'white',
    fontSize: 30
  },
  rightContainer: {
    flex: 0.9,
    //alignItems: 'flex-start',
    justifyContent: 'space-around',
    marginLeft: 10,
    marginRight: 15
  },
  infoChooseContainer: {
    flex: 0.25,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  chooseImageContainer: {
    flex: 0.75,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    paddingRight: 20
  },
  chooseImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    marginTop: 10,
    //backgroundColor: 'red'
  },
  btnChooseTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    //color: 'white',
    color: '#674171',
    marginBottom: 5
  },
  btnChooseDescription: {
    fontSize: 15,
    fontWeight: '400',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.7)',
    marginBottom: 8
  },
  btnChooseNoSelect: {
    fontSize: 15,
    fontWeight: '400',
    //color: 'rgba(255, 255, 255, 0.65)',
    color: 'rgba(103, 65, 113, 0.7)',
  }
});

export default PaymentWay;
