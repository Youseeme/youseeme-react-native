import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign } from '@expo/vector-icons';
import Moment from 'react-moment';
import 'moment-timezone';
import Lightbox from 'react-native-lightbox';

class ProductItem extends React.Component {
  constructor(props) {
    super(props);

    this.product = this.props.product;
    this._addToShoppingBasket = this.props.addToShoppingBasket;
    this._displayProductDetailWithOptions = this.props.displayProductDetailWithOptions;

    this.descriptionStyle = this.props.descriptionStyle;
  }

  _renderImage = (image) => {
    return (
      <View style={{flex: 1, borderRadius: 5}}>
        <Image
          style={{flex: 1, resizeMode: 'contain', borderRadius: 0}}
          source={{uri: image}}
        />
      </View>
    )
  }

  _displayStockState(state) {
    var message = "", iconName = "", color = "grey";
    if(state) {
      iconName = "ios-checkmark"
      color = "rgba(52, 199, 89, 0.9)";
      message = "En stock";
    }
    else {
      iconName = "ios-close"
      color = "rgba(255, 59, 48, 0.9)";
      message = "Indisponible";
    }

    return (
      <View style={styles.bottomInfosContainer}>
        <View style={[styles.stockStateContainer, {borderColor: 'transparent', backgroundColor: color}]}>
          <Text style={[styles.stockState, {color: 'white'}]}>{message}</Text>
          {/*<Ionicons
            name={iconName}
            style={{fontSize: 25, paddingLeft: 5, paddingTop: 0, color: color}}
          />*/}
        <View style={{width: 43, height: 43, position: 'absolute', right: -1, backgroundColor: '#674171', borderRadius: 30}}>
        </View>
        <TouchableOpacity
          style={styles.btnAddToCart}
          onPress={() => this._addToShoppingBasket(this.product)}
        >
          <FontAwesome
            name={"shopping-basket"}
            style={{fontSize: 19, paddingRight: 1, paddingTop: 3, color: "white"}}
          />
          <Ionicons
            name={"ios-add-circle"}
            style={{fontSize: 16, paddingLeft: 0, paddingTop: 0, color: "white", position: 'absolute', top: 6, right: 6}}
          />
        </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.mainContainer}
        onPress={() => this._displayProductDetailWithOptions(this.product)}
      >
        <View style={styles.topContainer}>
        <Lightbox style={styles.imageContainer} renderContent={() => this._renderImage(this.product.image)}>
        <View style={{flex: 1, width: 182, height: 182, backgroundColor: '#674171'}}>
            <Image
              style={styles.image}
              source={{uri: this.product.image}}
            />
          </View>
        </Lightbox>
          <View style={styles.infosContainer}>
            <View style={styles.topInfosContainer}>
              <Text numberOfLines={3} style={styles.productName}>{this.product.name}</Text>
              <View style={styles.productPricesContainer}>
                <Text numberOfLines={1} style={styles.productTotalPrice}>{this.product.price_ttc != null ? parseFloat(this.product.price_ttc.replace(/,/g,".")).toFixed(2) + " €" : "0.00 €"}</Text>
                {<Text numberOfLines={1} style={styles.productPrice}>{this.product.price != null && this.product.price != this.product.price_ttc ? "HT : " + this.product.price + " €" : "0.00 €"}</Text>}
              </View>
            </View>
            {this._displayStockState(this.product.in_stock)}
          </View>
        </View>
        <View style={this.descriptionStyle != null ? this.descriptionStyle : styles.descriptionContainer}>
          <Text numberOfLines={6} style={styles.productDescription}>{this.product.description}</Text>
        </View>
      </TouchableOpacity>
    );
  };
}

const styles = StyleSheet.create({
  mainContainer: {
    //height: 235,
    flex: 1,
    width: '98%',
    alignSelf: 'center',
    marginTop: 5,
    paddingVertical: 10,
    borderRadius: 15,
    backgroundColor: '#674171',
    paddingHorizontal: 10
  },
  topContainer: {
    flex: 0.75,
    flexDirection: 'row',

  },
  imageContainer: {
    flex: 0.55,
    //width: '50%',
    height: 170,
    //alignSelf: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    backgroundColor: 'rgba(255, 255, 255, 0.65)'
  },
  topInfosContainer: {
    flex: 0.6,
    justifyContent: 'space-between'
  },
  bottomInfosContainer: {
    flex: 0.4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  infosContainer: {
    flex: 0.45,
    paddingLeft: 10,
    paddingRight: 2
  },
  descriptionContainer: {
    flex: 0.27,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    marginTop: 12,
    paddingHorizontal: 3,
  },
  productName: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 15,
    fontWeight: 'bold',
    marginRight: 5,
  },
  productDescription: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 14,
    fontWeight: '500',
    textAlign: 'justify',
    fontFamily: "SciFly",
    paddingTop: 2
  },
  productTotalPrice: {
    color: '#F8CE3E',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 3,
    fontFamily: "SciFly"
  },
  productPrice: {
    color: 'rgba(255, 255, 255, 0.65)',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 0,
    fontFamily: "SciFly"
  },
  productPricesContainer: {

  },
  image: {
    width: '100%',
    height: '100%'
  },
  stockStateContainer: {
    flexDirection: 'row',
    width: '100%',
    height: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 1,
    paddingLeft: 10,
    marginTop: 8,
    //position: 'absolute',
    //top: -10,
    //right: -5
  },
  stockState: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'rgba(255, 255, 255, 0.65)',
  },
  btnAddToCart: {
    width: 43,
    height: 43,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    //backgroundColor: '#674171',
    marginRight: -1,
    //borderWidth: 1,
    //borderColor: 'white'
    backgroundColor: 'rgba(255, 255, 255, 0.45)'
  }
});

export default ProductItem;
