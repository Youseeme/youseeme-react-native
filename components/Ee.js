import React from 'react';
import { Animated, View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import ConfettiCannon from 'react-native-confetti-cannon';
import * as Haptics from 'expo-haptics';
//import CryptoJS from "react-native-crypto-js";

const { width, height } = Dimensions.get('screen');
var CryptoJS = require('crypto-js');

export default class Ee extends React.Component {
  explosion;
  constructor(props) {
    super(props);

    this.state = {
      show: false
    }
  }

  run = () => {
    if(!this.state.show) {
      this.setState({
        show: true
      }, () => {
        this.explosion && this.explosion.start();
      })
    }
    else
      this.explosion && this.explosion.start();
    Haptics.notificationAsync();
  };

  stop = () => {
    this.setState({
      show: false
    }, () => {
      this.explosion && this.explosion.stop();
    })
  }


/********************************************************************************
 * An Easter Egg ? Why not ?
 * If you are a new developer, add your own things... and keep the Easter Egg going. :)
 * Have fun !
 ********************************************************************************/

  render() {
    if(this.state.show) {
      return (
        <View style={{width: width, height: height, zIndex: 3, position: 'absolute'}}>
          <View style={{
            width: '90%',
            height: 220,
            backgroundColor: '#674171',
            alignSelf: 'center',
            justifyContent: 'space-between',
            alignItems: 'center',
            borderRadius: 20,
            marginTop: 50,
            shadowColor: 'grey',
            shadowOffset: {
              width: 0,
              height: 3
            },
            shadowRadius: 5,
            shadowOpacity: 0.7
          }}
          >
            <TouchableOpacity
              style={{
                position: 'absolute',
                top: 2,
                right: 2,
                borderTopLeftRadius: 0,
                borderTopRightRadius: 15,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 0,
                width: 35,
                height: 35,
                justifyContent: 'center',
                alignItems: 'center',
                zIndex: 1,
              }}
              activeOpacity={0.9}
              onPress={() => this.stop()}
            >
              <Ionicons
                name={"ios-close"}
                style={{color: '#F8CE3E', fontSize: 35}}
              />
            </TouchableOpacity>
            <View style= {{
              width: '90%',
              height: 100,
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center'
            }}>
              <Animated.Image
                style={{
                  width: '80%',
                  height: 60,
                  resizeMode: 'contain'
                }}
                source={require('../assets/logo.png')}
              />
              <Text style={{
                fontWeight: 'bold',
                fontSize: 12,
                color: 'rgba(255, 255, 255, 0.85)'
              }}>
                {"Congratulations, you found the app's easter egg."}
              </Text>
            </View>
            <View style={{
              marginBottom: 20
            }}>
              <Text style={{
                fontWeight: 'bold',
                fontSize: 15,
                color: 'rgba(255, 255, 255, 0.85)',
                textAlign: 'center'
              }}>
                {CryptoJS.AES.decrypt("U2FsdGVkX18h7EYyHjjuK5CJP1nMGTQ5XtNwWWSTd5dKOvfBLRs6cYvDEmEqqFvPY474qJHtV5THDE12xWhMrQ==", 'key').toString(CryptoJS.enc.Utf8)}
              </Text>
              <Text style={{
                fontWeight: 'bold',
                fontSize: 25,
                color: '#F8CE3E',
                textAlign: 'center',
                paddingVertical: 10
              }}>
                {CryptoJS.AES.decrypt("U2FsdGVkX18irc5f+QQAS4gxH2QxrSG0HmpicLVwhD4=", 'key').toString(CryptoJS.enc.Utf8)}
              </Text>
              <Text style={{
                fontWeight: 'bold',
                fontSize: 15,
                color: 'rgba(255, 255, 255, 0.85)',
                textAlign: 'center'
              }}>
                {CryptoJS.AES.decrypt("U2FsdGVkX1/FWYo2I9B9jLIuYNzM0HRGtBn69AD0MuVZjApui/6tmlflTfND1o/R", 'key').toString(CryptoJS.enc.Utf8)}
              </Text>
            </View>
          </View>
          <ConfettiCannon
            count={200}
            origin={{x: width/2, y: height/2}}
            fadeOut={true}
            autoStart={false}
            fallSpeed={4000}
            ref={ref => (this.explosion = ref)}
            onAnimationEnd={() => this.run()}
          />
        </View>
      );
    }
    else
      return null;
  }
}
