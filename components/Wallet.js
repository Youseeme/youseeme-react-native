import React from 'react';
import {  StyleSheet, View, ImageBackground, TouchableOpacity,Button, Image, Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView, ActivityIndicator, Easing } from 'react-native';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import {getUserProfileFromApiWithAccountToken, getUserBalanceFromApiWithAccountToken} from '../API/YSMApi';
import { LinearGradient } from 'expo-linear-gradient';
import NetInfo from '@react-native-community/netinfo';

const { width, height } = Dimensions.get('screen')

export default class Wallet extends React.Component{
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf')
    });
    this.token = this.props.navigation.getParam('token');
    this.profileData = this.props.navigation.getParam('profileData');

    this.state = {
        isLoading: true,
        user: {},
        balance:'',
        transfert: '',
        num:'',
    };
  }

  loadProfile = () => {
    getUserBalanceFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        balance: data,
        isLoading: false
      });
    });
  }

  _currentDate() {
    const today = new Date();
    let mm = today.getMinutes();
    let hh = today.getHours();
    let dd = today.getDate();
    let mmmm = today.getMonth()+1;
    const yyyy = today.getFullYear();

    if(mm<10)
    {
        mm=`0${mm}`;
    }

    if(hh<10)
    {
        hh=`0${hh}`;
    }

    if(dd<10)
    {
        dd=`0${dd}`;
    }

    if(mmmm<10)
    {
        mmmm=`0${mmmm}`;
    }

    return (dd + "/" + mmmm + '/' + yyyy + " à " + hh + ":" + mm);
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      if(!state.isConnected)
        this.props.navigation.navigate('LostConnection', {refresh: this._loadProfile});
      else {
        this.loadProfile();
      }
    });
  }

  render() {

    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            <Text style={styles.descriptiveText}>
              {"Gère tes opérations et consulte tes commandes"}
            </Text>
        </View>
        <View style={styles.container}>
          <ScrollView
            style={{
                width: '100%',
                //alignItems: 'center',
                marginBottom: 0,
                backgroundColor:'#D9D8D9',
                flex:2,
                opacity: 0.8,
                borderTopLeftRadius:15,
                borderTopRightRadius:15,
            }}
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingBottom: 50
            }}
          >
          {this.state.isLoading ?
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' color='#674171' />
            </View>
            :
            <View style={{
              width: '100%',
              //height: height/1.5,
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 30
            }}>
              <View style={styles.bodyContainer}>
                {/*<Text>{"Compte :"}</Text>*/}
                <View style={styles.balanceContainer}>
                  <View style={styles.balanceTextContainer}>
                    <Text style={styles.balanceTitle}>{"Mon solde : "}</Text>
                    <Text style={styles.balanceValue}>{this.state.balance.balance + "€"}</Text>
                  </View>
                  <View style={styles.balanceTextContainer}>
                    <Text style={styles.balanceTitle}>{"Bartcoins : "}</Text>
                    <Text style={styles.balanceValue}>{parseFloat(this.state.balance.points).toFixed(2)}</Text>
                  </View>
                  <Text style={styles.currentDate}>{"Dernière mise à jour le " + this._currentDate()}</Text>
                </View>
              </View>
              <View style={styles.buttonsContainer}>
                <View style={{
                    flex: 1,
                    borderRadius:13,
                    alignItems: 'center',
                    paddingTop: 10,
                }}>
                  <TouchableOpacity
                      style={styles.buttonStyle2}
                      onPress={() => this.props.navigation.navigate('Reload', {user: this.state.user, token: this.token, refreshProfile: this.loadProfile, balance: this.state.balance})}
                  >
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Recharger</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      style={styles.buttonStyle2}
                      onPress={() => this.props.navigation.navigate('Transfer', {token: this.token, profileData: this.profileData, balance: this.state.balance})}
                  >
                      <View
                        style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Transférer</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('BankCards', {token: this.token, balance: this.state.balance})}
                      style={styles.buttonStyle2}>
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Cartes bancaires</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      onPress={()=>this.props.navigation.navigate('AccountStatement', {token: this.token})}
                      style={styles.buttonStyle2}>
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Relevé de compte</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      onPress={()=>this.props.navigation.navigate('Orders', {token: this.token})}
                      style={styles.buttonStyle2}>
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Commandes</Text>
                      </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>}
          </ScrollView>
        </View>
      </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  container: {
    height: '100%',
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bodyContainer: {
    width: '90%',
    height: '25%',
    backgroundColor: 'rgba(103, 65, 113, 0.95)',
    borderRadius: 15,
    paddingVertical: 2,
    paddingHorizontal: 10,
    marginTop: 20
  },
  balanceContainer: {
    flex: 1,
    paddingHorizontal: 5,
    justifyContent: 'space-around'
  },
  balanceTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  buttonsContainer: {
    width:'90%',
    height: 380,
    justifyContent: 'center',
    backgroundColor:'white',
    borderRadius:13,
    paddingBottom: 0,
    marginTop: '10%'
  },
  balanceTitle: {
    fontSize: 20,
    fontWeight: '500',
    color: "white"
  },
  balanceValue: {
    fontSize: 32,
    fontWeight: 'bold',
    color: "#F8CE3E"
  },
  currentDate: {
    fontSize: 12,
    color: "white",
    textAlign: 'center'
  },
  logo: {
    width: 200,
    height: 60,
    resizeMode: 'contain'
  },
  profil_photo: {
    width: 110,
    height: 90,
    resizeMode: 'contain',
    marginTop:15,
  },
  statusImage: {
    width: 110,
    height: 110,
    resizeMode: 'contain',
    marginTop:15,
  },
  name: {
    fontSize:30,
    color: '#674171',
    fontFamily: "SciFly",
    paddingVertical: 5,
  },
  btnStatus: {
    borderRadius: 10,
    width: 100,
    height: 30,
    borderWidth: 0.1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnStatusTextContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  btnStatusText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 17,
    fontFamily: "SciFly",
  },
  textStyleButton: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 20,
    paddingVertical: 9,
    fontFamily: "SciFly",
  },
  buttonStyle2: {
    backgroundColor: "#674171",
    borderRadius: 10,
    width: '85%',
    borderWidth: 0.1,
    marginTop:12,
    opacity: 0.95,
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  loading_container: {
    height: height/1.3,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
