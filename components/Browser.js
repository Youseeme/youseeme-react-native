import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import {WebView} from 'react-native-webview';

export default class Browser extends React.Component {

  _onNavigationStateChange(webViewState) {
    var splittedUrl = webViewState.url.split(/&|=|\\|\?/);
    var status = null;

    for(var i = 0; i < splittedUrl.length; i++) {
      if (splittedUrl[i] === "status" && splittedUrl.length > i + 1)
        status = splittedUrl[i + 1];
    };

    if(status != null) {
      this.props.navigation.state.params.feedback(status);
      this.props.navigation.goBack();
    }
  }


  render() {
    const {url, name, type} = this.props.navigation.state.params;
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
          <Text style={styles.headerTitleStyle}>{name}</Text>
        </View>
        <View style={styles.mainContainer}>
          <WebView
            style={styles.webPage}
            onNavigationStateChange={type != null && type === "payment-webkit" ? this._onNavigationStateChange.bind(this) : null}
            onError={() => console.log("error")}
            originWhitelist={['*']}
            source={{uri: url}}
          />
        </View>
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.10,
    marginTop: 30
  },
  mainContainer: {
    flex: 1,
    overflow: 'hidden',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  webPage: {
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  }
});
