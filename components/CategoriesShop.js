import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import { SearchBar } from 'react-native-elements';
import CategorieItem from './CategorieItem';
import * as Font from 'expo-font';
import { getCategoriesFromApi, getShopListFromApiWithToken, getSubCategoriesFromApiWithCategoryId } from '../API/YSMApi';


class CategoriesShop extends React.Component {
  constructor(props) {
    super(props);

    this.token = this.props.token;
    this.displayShops = this.props.displayShops;
    this.shops = this.props.shops;

    this.state = {
      categories: [],
      isloading: false,
      search: '',
    };
  }

  _checkCategory(id1, id2) {
    console.log(id1 + " == " + id2);
    return id1 == id2;
  }

  _updateSearch(search) {
    this.setState({
      search: search,
      categories: []
    }, () => this._loadCategories());
  };

  _loadCategories() {
    this.setState({isLoading: true});
    var regex = new RegExp(this.state.search, 'i');

    //console.log("ENTER");
    getCategoriesFromApi().then(data1 => {
      //console.log("ENTER1");
      var advancedCategories = [{id: -1, name: "Tout"}];

      data1.forEach((cat) => {
        if(regex.test(cat.name))
          advancedCategories.push(cat);
      });



      advancedCategories.forEach(cat => {
        //if(cat.name.match(regex) != null || (this.state.search.replace(/ /g,"").length < 1)) {
          getSubCategoriesFromApiWithCategoryId(cat).then(data2 => {
            //console.log("ENTER2");
            this.shops.forEach(shop => {
              //console.log("ENTER3");
              data2.forEach(category => {
                if(shop.category.id == category.id){
                  //console.log(shop.category.id + " == " + category.id);
                  if(cat.nbShop)
                    cat.nbShop += 1;
                  else
                    cat.nbShop = 1;
                }
              });
            });
          });
    });

    this.setState({
      categories: advancedCategories,
      isLoading: false
    });

    });
  }

  /*_displayShops = (idCategorie) => {
    this.displayShops();

    this.props.navigation.navigate("ShopList", {idCategorie: idCategorie});
  }*/

  componentDidMount() {
    this._loadCategories();
  }

  render() {
    return(
      <View style={styles.mainContainer}>
        <SearchBar
          placeholder="Rechercher..."
          containerStyle={{
            backgroundColor: 'transparent',
            borderBottomColor: 'transparent',
            borderTopColor: 'transparent',
            height: 48,
            justifyContent: 'center',
            marginRight: 9
          }}
          inputContainerStyle={{
            backgroundColor: 'rgba(67, 39, 74, 0.8)',
            height: 35
          }}
          inputStyle={{
            color: 'white'
          }}
          onChangeText={(search) => this._updateSearch(search)}
          value={this.state.search}
          round={true}
          showCancel={true}
        />
        <FlatList
          style={styles.listContainer}
          data={this.state.categories}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => <CategorieItem category={item} displayShops={this.displayShops} />}
          contentContainerStyle={{paddingBottom:110}}
          onEndReachedThreshold={0.5}
          onEndReached={() => {
            //this._loadCategories()
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(103, 65, 113, 0.6)',
    paddingLeft: 10,
    paddingRight: 0
  },
  listContainer: {
    flex: 1,
    paddingRight: 10
  }

});

export default CategoriesShop;
