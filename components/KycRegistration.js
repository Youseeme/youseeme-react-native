import React from 'react';
import { StyleSheet, View,ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView } from 'react-native';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import * as Font from "expo-font";
import { Ionicons } from '@expo/vector-icons';
import { NavigationActions, StackActions } from 'react-navigation';

export default class KycRegistration extends React.Component {
    constructor(props) {
      super(props);
      this.data = this.props.navigation.getParam("data");
    }

    componentDidMount() {
      Font.loadAsync({
          'SciFly': require('../assets/fonts/SciFly.ttf'),
      });
    }

    _skip() {
      //this.props.navigation.navigate('Dashboard', {data: this.data, firstConnection: true});
      this.props.navigation.dispatch(StackActions.reset({
        index:0,
        actions:[
          NavigationActions.navigate({
            routeName:'Dashboard',
            params: {
              data: this.data,
              firstConnection: true
            }
          })
        ]
      }));
    }

    _registerKyc() {
      this.props.navigation.navigate('KycRegistrationAccepted', {data: this.data});
    }

    _moreInfo() {
      this.props.navigation.navigate('Browser', {url: "https://www.youseeme.pro/kyc-comment-ca-marche/", name: "Aide KYC"});
    }

    render() {
        return (
            <ImageBackground source={require('../assets/fond.png')} style={{width: '100%', height: '100%'}}>
                <View style={styles.topHeadband}>
                  <View style={{width: '100%', height: '55%'}}>
                    <Image
                        style={styles.logo}
                        source={require('../assets/logo.png')}
                    />
                  </View>
                  <Text style={styles.textTop}>{"Une dernière chose !"}</Text>
                </View>
                <ScrollView
                  style={{flex: 0.9}}
                  containerStyle={{paddingBottom: 10}}
                >
                  <View style={[styles.container]}>
                    <View style={styles.infoKycContainer}>
                      <View style={styles.infoKycTitleContainer}>
                        <Text style={styles.infoKycTitle}>KYC</Text>
                        <Text style={styles.infoKycSubTitle}>(Know Your Customer)</Text>
                      </View>
                      <Text style={styles.infoKycBody}>
                      {`  Conformément à la réglementation européenne en vigueur concernant l’attribution d’un portefeuille éléctronique et pour profiter pleinement pleinement de toutes les fonctionnalités de Youseeme, nous devons valider vos informations personnels.
Pour cela vous devez nous transmettre les justificatifs qui vous permettront de beneficier d’un compte en ligne gratuit et sécurisé.
Sans cette validation, vous pourrez uniquement commander et payer en ligne avec votre carte bancaire.\n
  Gagnez un max de Bartcoins rapidement grâce à vote portefeuille sécurisé youseeme !`}
                      </Text>
                      <TouchableOpacity
                        style={[styles.btn, {backgroundColor: '#674171', height: 45, width: 120}]}
                        onPress={() => this._moreInfo()}
                      >
                        <Text style={[styles.btnText, {color: 'white', fontSize: 17}]}>{"Plus d’infos"}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.actionsContainer}>
                    <TouchableOpacity
                      style={[styles.btn, {backgroundColor: '#40BCF8'}]}
                      onPress={() => this._skip()}
                    >
                      <Text style={styles.btnText}>{"Passer"}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[styles.btn, {backgroundColor: '#F8CE3E'}]}
                      onPress={() => this._registerKyc()}
                    >
                      <Text style={styles.btnText}>{"OK !"}</Text>
                    </TouchableOpacity>
                  </View>
              </ScrollView>
            </ImageBackground>
        );
    }
}



const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 90,
    marginTop: 55,
    marginBottom: 20
  },
  container: {
      height: '100%',
      flex: 3,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
  },
  logo: {
      width: '100%',
      height: '100%',
      resizeMode: 'contain'
  },
  textTop: {
    color: '#F8CE3E',
    fontSize: 30,
    fontFamily: 'SciFly',
    marginTop: 10,
    marginBottom: 5
  },
  infoKycContainer: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    borderRadius: 15,
    marginHorizontal: 15,
    paddingHorizontal: 20,
    paddingTop: 18,
    paddingBottom: 15,
    alignItems: 'center',
    marginTop: 10
  },
  infoKycTitleContainer: {
    width:'95%',
    paddingBottom: 5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: 'grey',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    paddingHorizontal: 50
  },
  infoKycTitle: {
    fontSize: 23,
    fontWeight: '400',
    color:'#674171',
    textAlign: 'center',
    fontFamily: "SciFly"
  },
  infoKycSubTitle: {
    fontSize: 19,
    fontWeight: '400',
    color:'#674171',
    textAlign: 'center',
    fontFamily: "SciFly",
    paddingBottom: 5,
  },
  infoKycBody: {
    fontSize: 18,
    color:'#674171',
    textAlign: 'justify',
    fontFamily: "SciFly",
    marginTop: 10
  },
  actionsContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 30
  },
  btn: {
    width: 115,
    height: 48,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },
  btnText: {
    color: 'black',
    fontSize: 19,
    fontFamily: 'SciFly',
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  }
});
