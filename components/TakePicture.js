import React, { useState, useEffect, useRef } from 'react';
import { Animated, TouchableOpacity, Text, View, StyleSheet, Button, Easing } from 'react-native';
import { FontAwesome, Ionicons,MaterialCommunityIcons } from '@expo/vector-icons';

import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import { Camera } from 'expo-camera';


class TakePicture extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      topPosition: new Animated.Value(-270),
      hasPermission: null,
      type: Camera.Constants.Type.back,
    }
  }


  async componentDidMount() {
    this._getPermissionAsync()

    Animated.decay(
      this.state.topPosition,
      {
        velocity: 0.8,
        deceleration: 0.997,
      }
    ).start();
  }


  _getPermissionAsync = async () => {
    // Camera roll Permission
    if (Platform.OS === 'ios') {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
    // Camera Permission
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasPermission: status === 'granted' });
  }


  _handleCameraType= () =>{
    const { cameraType } = this.state

    this.setState({cameraType:
      cameraType === Camera.Constants.Type.back
      ? Camera.Constants.Type.front
      : Camera.Constants.Type.back
    })
  }

  _takePicture = async () => {
    if (this.camera) {
      let photo = await this.camera.takePictureAsync();
      console.log(photo);
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
    });
  }





  render(){
    const { hasPermission } = this.state
    if (hasPermission === null) {
      return <View />;
    } else if (hasPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
          <View style={{ flex: 1 }}>
            <Camera
            ref={ref => {
              this.camera = ref;
            }}
            style={{ flex: 1 }} type={this.state.cameraType}
            >
              <Animated.View style={{flex:1, flexDirection:"row",justifyContent:"space-between",margin:20, top: this.state.topPosition}}>
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                  }}
                  onPress={()=>this._pickImage()}
                >
                  <Ionicons
                      name="ios-photos"
                      style={{ color: "#fff", fontSize: 40}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                  }}
                  onPress={()=>this._takePicture()}
                >
                  <MaterialCommunityIcons
                      name="circle-outline"
                      style={{ color: "#fff", fontSize: 80}}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                  }}
                  onPress={()=>this._handleCameraType()}
                >
                  <Ionicons
                      name="ios-reverse-camera"
                      style={{ color: "#fff", fontSize: 50}}
                  />
                </TouchableOpacity>
              </Animated.View>
            </Camera>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  result: {
    color: 'white',
    fontSize: 20,
    marginTop: 20
  }
});

export default TakePicture;
