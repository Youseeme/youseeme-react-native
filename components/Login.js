import React from 'react';
import { AsyncStorage, StyleSheet, View,ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView } from 'react-native';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import {getLoginFromApiWithAccountCredentials} from '../API/YSMApi';
import Dashboard from '../components/Dashboard';
import { Ionicons } from '@expo/vector-icons';
import { NavigationActions, StackActions } from 'react-navigation';
import NetInfo from '@react-native-community/netinfo';

export default class Login extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            error: '',
            password: '',
            code : 449,
            fontLoaded: false,
        };
    }

    async saveItem(item, selectedValue) {
      try {
        await AsyncStorage.setItem(item, selectedValue);
      } catch (error) {
        console.error('AsyncStorage error: ' + error.message);
      }
    }

    _login(email, password, auto) {
      NetInfo.fetch().then(state => {
        if(!state.isConnected)
          this.props.navigation.navigate('LostConnection', {refresh: this._loadProfile});
        else {
          getLoginFromApiWithAccountCredentials(email, password).then((data) => {
            console.log(data)
              if(this.state.email == "" ) {
                  this.setState({error: "Un email est requis"})
              }
              else if (this.state.password == "" ) {
                  this.setState({error: "Un mot de passe est requis"})
              }
              else if (data == null || data.error != null) {
                  this.setState({error: "Mauvais mot de passe et/ou email"})
              }
              else if(data != null && data.is_banned == null) {
                if(!auto) {
                  this.saveItem('credentials', JSON.stringify({
                    email: this.state.email,
                    password: this.state.password
                  }));
                }
                this.props.navigation.dispatch(StackActions.reset({
                  index:0,
                  actions:[
                    NavigationActions.navigate({
                      routeName:'Dashboard',
                      params: {
                        data: data
                      }
                    })
                  ]
                }));
                //console.log(data);
              }
          })
        }
      });
    }

    async componentDidMount() {
      AsyncStorage.getItem('credentials').then((data) => {
        if(data != null) {
          const credentials = JSON.parse(data);
          this._login(credentials.email, credentials.password, true);
        }
      });
      await Expo.Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
      });
      this.setState({ fontLoaded: true });
    }

    render() {
        return (
            <ImageBackground  source={require('../assets/fond.png')} style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
                <ScrollView
                  style={styles.container}
                  contentContainerStyle={{
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',

                  }}
                >
                    <View
                        style={{
                            width:280,
                        }}>
                        <View
                            style={{
                                width:280,
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginBottom: 0,
                            }}>
                            <Image
                                style={styles.logo}
                                source={require('../assets/logo.png')}/>
                        </View>
                        {/*<Text  style={{
                            color:'red',
                            textAlign: 'center',
                        }}>{this.state.error}</Text>*/}
                        <Input
                            //onRef={ref => this.emailInput = ref}
                            style={styles.input}
                            placeholder="Email"
                            autoCompleteType={"email"}
                            keyboardType={"email-address"}
                            returnKeyType='done'
                            clearButtonMode={'while-editing'}
                            fontSize={18}
                            family="antdesign"
                            onChangeText={(email) => this.setState({email})}
                            value={this.state.email}
                            placeholderTextColor={'black'}
                            color={'black'}
                            autoCapitalize = 'none'
                            //nSubmitEditing={() => this.passwordInput.focus()}
                        />

                        <Input
                            //onRef={ref => this.passwordInput = ref}
                            style={styles.input}
                            placeholder="Mot de Passe"
                            autoCompleteType={"password"}
                            returnKeyType='done'
                            password={true}
                            viewPass={true}
                            right
                            family="antdesign"
                            fontSize={18}
                            iconSize={25}
                            iconColor="black"
                            onChangeText={(password) => this.setState({password})}
                            value={this.state.password}
                            placeholderTextColor={'black'}
                            color={'black'}
                            autoCapitalize = 'none'
                        />
                        {this.state.error !== "" ? <View style={styles.errorContainer}>
                          <Ionicons
                            name="ios-warning"
                            style={styles.errorIcon}
                          />
                          <Text style={styles.txtError}>{this.state.error}</Text>
                        </View> : null}
                        {this.state.fontLoaded ? (
                        <View
                            style={{
                                marginTop:40,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            <TouchableOpacity
                                style={styles.buttonStyle}
                                onPress={()=>this.props.navigation.navigate('Second')}>

                                <View style={styles.textStyle}>
                                    <Text style={styles.textStyle}>S'inscrire</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this._login(this.state.email, this.state.password, false) }
                                style={styles.buttonStyle1}>
                                <View style={styles.textStyle}>
                                    <Text style={styles.textStyle}>Connexion</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                        ) : null}
                        {this.state.fontLoaded ? (
                        <View  style={{
                            marginTop:10,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <TouchableOpacity
                                style={{paddingVertical: 10}}
                                onPress={()=>this.props.navigation.navigate('ResetPassword')}
                            >
                              <Text style={styles.textStyleforget}>Mot de passe oublié ?</Text>
                            </TouchableOpacity>
                        </View>
                        ) : null}
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
}




const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
    container: {
      width: '100%',
      paddingTop: 190
    },
    input: {
        fontSize:   60,
        color:'black',
        backgroundColor:'#FFFFFF',
        borderRadius: 8,
        opacity: 0.4,
        height: 50,
        marginTop: 0,

    },
    buttonStyle: {
        backgroundColor: "#40BCF8",
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 15,
        borderRadius: 50,
        width: 110,
        height: 48,
    },
    buttonStyle1: {
        backgroundColor: "#f8cd3e",
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 15,
        borderRadius: 50,
        width: 110,
        height: 48,
    },
    logo: {
        width: 200,
        height: 60,
        resizeMode: 'contain'
    },
    textStyleforget: {
        fontSize: 20,
        color: "#f8cd3e",
        top:10,
        fontFamily: "SciFly",
    },
    textStyle: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        fontFamily: "SciFly",
        paddingTop: Platform.OS === 'ios' ? 2 : 0
    },
    errorContainer: {
      height: 25,
      width: '100%',
      flexDirection: 'row',
      marginTop: 15,
      justifyContent: 'center',
      alignItems: 'center'
    },
    errorIcon: {
      fontSize: 25,
      color: '#FF453A',
    },
    txtError: {
      fontSize: 17,
      color: '#FF453A',
      marginLeft: 10
    }
});
