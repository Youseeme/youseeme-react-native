import React from 'react';
import { StyleSheet, View,ImageBackground, TouchableOpacity,Button, Image,Animated, Dimensions, Keyboard, TextInput, UIManager } from 'react-native';
import { Input, Block,Icon,Text, NavBar } from 'galio-framework';
import DatePicker from 'react-native-datepicker';
import * as Font from 'expo-font';

export default class RankPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            pass: '',
            date: new Date(),

        };

    }
    componentDidMount() {
            Font.loadAsync({
                'SciFly': require('../assets/fonts/SciFly.ttf'),
            });


    }
    render() {
        const { shift } = this.state;

        return (

            <ImageBackground   source={require('../assets/fond.png')} style={{width: '100%', height: '100%'
            }}>
                <View style={{flex: 0.5, flexDirection: 'row'}}>
                    <Icon style={styles.arrowleft}
                          name="arrowleft"
                          family="AntDesign"
                          color="white"
                          size={25}
                          onPress={()=>this.props.navigation.goBack()}
                    />
                </View>
                <View style={{flex: 1, flexDirection: 'row'}}>

                    <View style={styles.container} >
                        <Image
                            style={styles.logo}
                            source={require('../assets/logo.png')}/>
                        <Text     style={{fontFamily: "SciFly"}} color="#F8CD3E" h4>C'est finis !</Text>

                    </View>
                </View>

                <View style={[styles.container, { flex: 5}]}>
                    <View >

                        <TouchableOpacity
                            style={styles.buttonStyle1}
                            onPress={()=>this.props.navigation.navigate('Rank')}>

                            <View style={styles.textStyle1}>
                                <Text style={styles.textStyle1}>ARGENT</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonStyle2}
                            onPress={()=>this.props.navigation.navigate('Rank')}>

                            <View style={styles.textStyle2}>
                                <Text style={styles.textStyle2}>GOLD</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonStyle3}
                            onPress={()=>this.props.navigation.navigate('Rank')}>

                            <View style={styles.textStyle3}>
                                <Text style={styles.textStyle3}>PLATINIUM</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.buttonStyle}
                            onPress={()=>this.props.navigation.navigate('Home')}>

                            <View style={styles.textStyle}>
                                <Text style={styles.textStyle}>Pas interessé</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </ImageBackground>

        );

    }



}




const styles = StyleSheet.create({

    container: {
        height: '100%',
        flex: 3,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',

    },
    input: {
        fontSize:   60,
        color:'black',
        backgroundColor:'#FFFFFF',
        borderRadius: 8,
        opacity: 0.4,
        height: 50,
        marginTop: -8,
        width:250,


    },


    buttonStyle: {
        backgroundColor: "red",
        borderRadius: 50,
        width: 210,
        borderWidth: 0.1,
        marginBottom: 20

    },
    buttonStyle1: {
        backgroundColor: "#D3D3D3",
        borderRadius: 50,
        width: 210,
        borderWidth: 0.1,
        marginBottom: 20

    },
    buttonStyle2: {
        backgroundColor: "#FFD700",
        borderRadius: 50,
        width: 210,
        borderWidth: 0.1,
        marginBottom: 20

    },
    buttonStyle3: {
        backgroundColor: "#cccccc",
        borderRadius: 50,
        width: 210,
        borderWidth: 0.1,
        marginBottom: 20

    },
    logo: {

        width: 200,
        height: 60,
        resizeMode: 'contain'
    },

    textStyle: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 20,
        paddingTop: 7,
        paddingBottom: 7,
        fontFamily: "SciFly",


    },
    textStyle1: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 7,
        paddingBottom: 7,
        fontFamily: "SciFly",

    },
    textStyle2: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 7,
        paddingBottom: 7,
        fontFamily: "SciFly",

    },
    textStyle3: {
        alignSelf: 'center',
        color: 'black',
        fontSize: 20,
        paddingTop: 7,
        paddingBottom: 7,
        fontFamily: "SciFly",

    },
    backblue: {
        backgroundColor: "#FB1352",
        height:80,
        alignSelf : 'stretch',
        textAlign: 'center',
        //marginTop: 5,
        color: 'white',
        fontSize:25,
        paddingTop: 25,
    },
    arrowleft: {
        marginTop: 22,
        marginLeft: 10,
    }
});
