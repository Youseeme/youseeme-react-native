import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import Moment from 'react-moment';
import 'moment-timezone';
import { CreditCardInput } from "./creditCard";

class CreditCardItem extends React.Component {
  constructor(props) {
    super(props);

    this.card = this.props.card;
  }

  componentDidMount() {
    this.CCInput.setValues({number: this.card.code, expiry: this.card.expiration.substring(0, 2) + this.card.expiration.substring(5, 7)});
  }

  render() {
    const {news, showNews} = this.props;

    return (
      <View style={styles.mainContainer}>
        <CreditCardInput
          ref={ref => this.CCInput = ref}
          cardScale={1.16}
        />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  mainContainer: {
    /*flex: 1,
    flexDirection: 'row',
    height: 150,
    marginTop: 5,
    paddingVertical: 10,
    paddingLeft: 10,
    paddingRight: 20,
    backgroundColor: 'white',
    borderRadius: 15*/
  }
});

export default CreditCardItem;
