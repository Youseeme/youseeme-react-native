import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Button, TouchableOpacity, ScrollView, Platform} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import OrderItem from './OrderItem';
import * as Font from 'expo-font';
import {getOrdersFromApiWithAccountToken} from '../API/YSMApi';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Ionicons } from '@expo/vector-icons';
import Moment from 'react-moment';
import 'moment-timezone';
import LottieView from "lottie-react-native";

class Orders extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.month = 4;
    this.year = 2018
    this.currentPage = 0;
    this.tabValues = [];

    this.tabValues = ["Tout", "Année", "Mois"];


    this.state = {
      orders: [],
      isLoading: true,
      selectedIndex: 0,
      date: Date.now(),
      show: false
    };
  }



  _loadOrders() {
    this.setState({
      isLoading: true,
      orders: []
    });

    var fetchOrders = (month, year) => {
      return getOrdersFromApiWithAccountToken(this.token, month, year);
    };

    var date = new Date(Date.now());
    //console.log(date.getFullYear()-2)

    switch(this.state.selectedIndex) {
      case 0:
        var date = new Date(Date.now());
        var promises = [];

          var orders = [];
          for (var year = parseInt(date.getFullYear(), 10)-2; year <= parseInt(date.getFullYear(), 10); year++) {
            for (var month = 12; month > 0; month--) {
              getOrdersFromApiWithAccountToken(this.token, month, year).then(data => {
                this.setState({
                  orders: [...this.state.orders, ...data].sort((a, b) => (a.id < b.id) ? 1 : -1),
                  isLoading: false
                });
              });
            }
          }

          /*Promise.all(orders).then(() => {
            console.log(orders);
            console.log("@@@@@@@@@@@@@");
            this.setState({
              orders: orders,
              isLoading: false
            });
          })*/

        console.log("@@@@@@@@@@@@@");
        //console.log(fetch());

        //console.log(orders);
        break;
      case 1:
        var date = new Date(this.state.date);
        var orders = [];
        for (var month = 12; month > 0; month--) {
          getOrdersFromApiWithAccountToken(this.token, month, date.getFullYear()).then(data => {
            this.setState({
              orders: [...this.state.orders, ...data].sort((a, b) => (a.id < b.id) ? 1 : -1),
              isLoading: false
            });
          });
        }
        break;
      default:
        var date = new Date(this.state.date);
        getOrdersFromApiWithAccountToken(this.token, date.getMonth(), date.getFullYear()).then(data => {
          this.setState({
            orders: data.sort((a, b) => (a.id < b.id) ? 1 : -1),
            isLoading: false
          });
        });
    }
  }

  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
  }



  onChange = (event, selectedDate) => {
    const currentDate = selectedDate || this.state.date;
    this.setState({
      show: Platform.OS === 'ios',
      date: currentDate,
      orders: []
    }, () => this._loadOrders());
  };

  showDatepicker = () => {
    this.setState({
      show: !this.state.show
    });
  };

  _handleIndexChange = index => {
    if(this.tabValues[index] === "Tout")
      this.currentPage = 0;
    else if(this.tabValues[index] === "Année")
      this.currentPage = 1;
    else
      this.currentPage = 2;
    this.setState({
      selectedIndex: this.currentPage,
      orders: []
    }, () => this._loadOrders());

  };

  _getDateFormat() {
    switch(this.state.selectedIndex) {
      case 0:
        return "-";
      case 1:
        return "YYYY";
      default:
        return "MM/YYYY";
    }
  }

  _displayOrderDetails = (id) => {
    this.props.navigation.navigate('OrderDetails', {token: this.token, orderId: id});
  }

  componentDidMount() {
    this._loadOrders();
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            <Text style={styles.descriptiveText}>
                Retrouve l’historique et l’état de tes commandes.
            </Text>
        </View>
        <View style={styles.mainContainer}>
        <View style={styles.tabView}>
          {/*<View>
            <Button onPress={this.showDatepicker} title="Show date picker!" />
          </View>*/}
          <SegmentedControlTab
            borderRadius = {15}
            tabsContainerStyle={styles.tabsContainerStyle}
            tabStyle={styles.tabStyle}
            firstTabStyle={styles.firstTabStyle}
            lastTabStyle={styles.lastTabStyle}
            tabTextStyle={styles.tabTextStyle}
            activeTabStyle={styles.activeTabStyle}
            activeTabTextStyle={styles.activeTabTextStyle}
            allowFontScaling={false}
            values={this.tabValues}
            selectedIndex={this.state.selectedIndex}
            onTabPress={this._handleIndexChange}
          />
          {this.state.selectedIndex === 0 ? null : <TouchableOpacity
            style={styles.btnDatePickerContainer}
            onPress={this.showDatepicker}
          >
            <View style={styles.btnDatePicker}>
              <Moment style={styles.date} element={Text} interval={0} tz="Europe/Paris" format={this._getDateFormat()}>
                {this.state.date}
              </Moment>
              <Ionicons
                name={this.state.show ? "ios-arrow-up" : "ios-arrow-down"}
                style={styles.pickerArrow}
              />
            </View>
          </TouchableOpacity>}

          {this.state.show && this.state.selectedIndex !== 0 ?
            <DateTimePicker
              testID="dateTimePicker"
              timeZoneOffsetInMinutes={0}
              value={this.state.date}
              mode={'date'}
              //locale='fr'
              is24Hour={true}
              display="default"
              onChange={this.onChange}
            /> : null
          }
        </View>
        {this.state.orders.length > 0 || this.state.isLoading ?
        <FlatList
          style={styles.listContainer}
          contentContainerStyle={{
            paddingBottom: 60
          }}
          scrollIndicatorInsets={{ right: 1 }}
          data={this.state.orders}
          keyExtractor={(item) => (++this.cptId).toString()}
          renderItem={({item}) => <OrderItem order={item} displayOrderDetails={this._displayOrderDetails} />}
          refreshing={this.state.isLoading}
          onEndReachedThreshold={0.5}
          onRefresh={() => this._loadOrders()}
          onEndReached={() => {
            //this._loadTransactions();
          }}
        /> :
        <ScrollView
          style={styles.listContainer}
          contentContainerStyle={{justifyContent: 'center', alignItems: 'center', paddingTop: 60, paddingBottom: 90}}
        >
          {Platform.OS === 'ios' ?
          <LottieView
            style={styles.noResultsAnimation}
            source={require('../assets/error-state-dog.json')}
            autoPlay
            loop
          /> :
          <LottieView
            style={styles.noResultsAnimation}
            source={require('../assets/error-state-dog.json')}
          />}
          <Text style={styles.noResultsText}>{"Vous n'avez pas encore passé de commande."}</Text>
        </ScrollView>}
        {/*this._displayLoading()*/}
      </View>
      </ImageBackground>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  listContainer: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:35,
    color: "#f8cd3e",
    marginTop:10,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
  },
  btnDatePickerContainer: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  btnDatePicker: {
    width: 140,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#674171',
    borderRadius: 10
  },
  date: {
    color: '#674171',
    fontSize: 24,
    fontWeight: '500',
    textAlign: 'center',
    paddingHorizontal: 10
  },
  tabView: {
    marginTop: 15,
    marginRight: 40,
    marginLeft: 40
  },
  tabStyle: {
    borderColor: '#6D4877',
    height: 35,
    backgroundColor: 'transparent'
  },
  tabTextStyle: {
    color:'#6D4877',
    fontFamily: "SciFly"
  },
  activeTabStyle: {
    backgroundColor: '#6D4877',
  },
  activeTabTextStyle: {
    color:'white'
  },
  pickerArrow: {
    color: '#674171',
    fontSize: 23,
    marginRight: 12,
    marginTop: 2
  },
  noResultsAnimation: {
    width: 300,
    height: 300
  },
  noResultsText: {
    marginTop: 8,
    color: '#674171',
    fontWeight: '500',
    fontSize: 20,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'SciFly'
  }
});

export default Orders;
