import React from 'react';
import {Alert, ImageBackground, StyleSheet, Text, TextInput, View, Button, TouchableOpacity, ScrollView, KeyboardAvoidingView} from 'react-native';
import {addUserAddressFromApiWithInfos, removeUserAddressFromApiWithId} from '../API/YSMApi';

export default class NewAddress extends React.Component {
  constructor(props) {
    super(props);
    this.token = this.props.navigation.getParam('token');
    this.pinCode = "";
    this.pinCodeVerif = "";
    this.btnSubmitText = this.props.navigation.getParam('btnSubmitText');
    this.address = this.props.navigation.getParam('address') != null ?
    this.props.navigation.getParam('address') :
    {
      name: "",
      address: "",
      zipcode: "",
      city: ""
    };

    this.state = {
      error: "",
    }
  }

  _addAddress = () => {
    const promise = new Promise((resolve, reject) => {
      if(this.address.name.replace(/ /g,"").length < 1) {
        resolve("Le nom de l'adresse n'est pas valide.");
      }
      else if(this.address.address.replace(/ /g,"").length < 1) {
        resolve("L'adresse postale n'est pas valide.");
      }
      else if(this.address.zipcode.replace(/ /g,"").length < 1) {
        resolve("Le code postale n'est pas valide.");
      }
      else if(this.address.city.replace(/ /g,"").length < 1) {
        resolve("La ville n'est pas valide.");
      }
      else {
        addUserAddressFromApiWithInfos(this.token, this.address.name, this.address.address, this.address.zipcode, this.address.city).then(data1 => {
          var finish = () => {
            Alert.alert(
              "Votre adresse a bien été enregistrée"
            );
            this.props.navigation.state.params.onGoBack();
            this.props.navigation.goBack();
          }

          if(this.props.navigation.getParam('address') != null) {
            removeUserAddressFromApiWithId(this.token, this.address.id).then(data2 => {
              if(data2.message === "success")
                finish();
              else
                resolve("Une erreur s'est produite veuillez réessayer ultérieurement.");
            });
          }
          else {
            if(data1 != null && Object.entries(data1).length !== 0)
              finish();
            else
              resolve("Une erreur s'est produite veuillez réessayer ultérieurement.");
          }
        });
      }
    });

    promise.then((errorMessage) => {
      this.setState({error: errorMessage});
    });
  }

  render() {
    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>

        </View>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{flex: 1}}
        >
        <ScrollView style={{flex: 1}}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
        <View style={styles.entryContainer}>
          <Text style={styles.entryDescriptionText}>{"Nom de l'adresse :"}</Text>
          <View style={styles.entryBackground}>
            <TextInput
                ref={ref => this.addressNameInput = ref}
                style={styles.entryText}
                defaultValue={this.address != null ? this.address.name : null}
                autoFocus={this.address.id == null}
                maxLength={200}
                placeholder="Nom de l'adresse"
                fontSize={18}
                textAlign='left'
                placeholderTextColor={'rgb(100, 95, 107)'}
                selectionColor={'#674171'}
                blurOnSubmit={false}
                onSubmitEditing={() => this.addressInput.focus()}
                onChangeText={(name) => {
                  this.address.name = name;
                }}
            />
          </View>
          </View>
          <View style={styles.entryContainer}>
            <Text style={styles.entryDescriptionText}>{"Adresse postale :"}</Text>
            <View style={styles.entryBackground}>
              <TextInput
                  ref={ref => this.addressInput = ref}
                  style={styles.entryText}
                  defaultValue={this.address != null ? this.address.address : null}
                  maxLength={200}
                  placeholder="Adresse postale"
                  fontSize={18}
                  textAlign='left'
                  placeholderTextColor={'rgb(100, 95, 107)'}
                  selectionColor={'#674171'}
                  blurOnSubmit={false}
                  onSubmitEditing={() => this.zipCodeInput.focus()}
                  onChangeText={(address) => {
                    this.address.address = address;
                  }}
              />
            </View>
          </View>
          <View style={styles.entryContainer}>
            <Text style={styles.entryDescriptionText}>{"Code postale :"}</Text>
            <View style={styles.entryBackground}>
              <TextInput
                  ref={ref => this.zipCodeInput = ref}
                  style={styles.entryText}
                  defaultValue={this.address != null ? this.address.zipcode : null}
                  maxLength={50}
                  placeholder="Code postale"
                  fontSize={18}
                  textAlign='left'
                  placeholderTextColor={'rgb(100, 95, 107)'}
                  selectionColor={'#674171'}
                  keyboardType={'numeric'}
                  blurOnSubmit={false}
                  onSubmitEditing={() => this.cityInput.focus()}
                  onChangeText={(zipCode) => {
                    this.address.zipcode = zipCode;
                  }}
              />
            </View>
          </View>
          <View style={styles.entryContainer}>
            <Text style={styles.entryDescriptionText}>{"Ville :"}</Text>
            <View style={styles.entryBackground}>
              <TextInput
                  ref={ref => this.cityInput = ref}
                  style={styles.entryText}
                  defaultValue={this.address != null ? this.address.city : null}
                  maxLength={200}
                  placeholder="Ville"
                  fontSize={18}
                  textAlign='left'
                  placeholderTextColor={'rgb(100, 95, 107)'}
                  selectionColor={'#674171'}
                  onChangeText={(city) => {
                    this.address.city = city;
                  }}
              />
            </View>
          </View>
          <Text style={styles.txtError}>{this.state.error}</Text>
          <TouchableOpacity
            style={styles.btnSubmit}
            onPress={() => this._addAddress()}
          >
            <Text style={styles.txtBtnSubmit}>{this.btnSubmitText}</Text>
          </TouchableOpacity>
        </View>
        </ScrollView>
        </KeyboardAvoidingView>
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.15,
    marginTop: 30
  },
  webPage: {
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  headerTitleStyle: {
    fontSize:25,
    color: "#f8cd3e",
    fontFamily: "SciFly",
    alignSelf: 'center',
  },
  entryContainer: {
    width:'80%',
    //backgroundColor: 'red',
    marginTop: 20
  },
  entryBackground: {
    width:'100%',
    backgroundColor:'#FFFFFF',
    borderRadius: 8,
    opacity: 0.4,
    height: 50,
    marginTop: 8
  },
  entryText: {
    flex: 1,
    color:'black',
    paddingLeft: 10,
  },
  btnSubmit: {
    backgroundColor: '#F8CE3E',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingVertical: 12,
    borderRadius: 30,
    marginTop: 25,
    marginBottom: 50
  },
  entryDescriptionText: {
    color: 'white',
    fontWeight: '600'
  },
  txtBtnSubmit: {
    color: '#674171',
    fontSize: 22,
    fontWeight: '500',
    fontFamily: 'SciFly',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  txtError: {
    color: '#FF453A',
    marginTop: 20
  }
});
