import React from 'react';
import {FlatList, Image, ImageBackground, StyleSheet, Text, TouchableOpacity, View, Linking, ScrollView} from 'react-native';
import TransactionItem from './TransactionItem';
import * as Font from 'expo-font';

class Kyc extends React.Component {
    constructor(props) {
        super(props);
        Font.loadAsync({
            'SciFly': require('../assets/fonts/SciFly.ttf'),
        });
    }

    _loadWebPage(url, name) {
      this.props.navigation.navigate('Browser', {url: url, name: name});
    }

    componentDidMount() {

    }

    render() {
        return (
            <ImageBackground  source={require('../assets/fond.png')} style={{width: '100%', height: '100%'}}>
              <View style={styles.topHeadband}>

              </View>
              <View style={styles.mainContainer}>
                <ScrollView contentContainerStyle={{paddingBottom: 60}}>
                  <View style={styles.listContainer}>
                      <View style={{alignItems: 'center',
                          justifyContent:'center'}}>
                <Text style={styles.bodyText}>
                {`
  Conformément à la réglementation européenne en vigueur concernant l’attribution d’un portefeuille éléctronique et pour profiter pleinement pleinement de toutes les fonctionnalités de Youseeme, nous devons valider vos informations personnels.\n
  Pour cela vous devez nous transmettre les justificatifs qui vous permettront de beneficier d’un compte en ligne gratuit et sécurisé.\n
 Sans cette validation, vous pourrez uniquement commander et payer en ligne avec votre carte bancaire.\n
  Gagnez un max de Bartcoins rapidement grâce à vote portefeuille sécurisé youseeme !`}
                </Text>

                  </View>
                      <View style={{
                          flexDirection: 'row',
                          justifyContent:'space-around',
                          alignItems: 'center',
                          marginTop: 5,
                          marginBottom: 20
                        }}
                      >
                      <TouchableOpacity
                          style={styles.buttonStyle}
                          onPress={() => this._loadWebPage("https://www.youseeme.pro/kyc-comment-ca-marche/", "Aide KYC")}>

                          <View style={styles.textStyle}>
                              <Text style={styles.textStyle}>En savoir plus</Text>
                          </View>
                      </TouchableOpacity>
                      <TouchableOpacity
                          onPress={() => this._loadWebPage("https://www.youseeme.pro/kyc-2/", "Remplir son KYC")}
                          style={styles.buttonStyle1}>
                          <View style={styles.textStyle}>
                              <Text style={styles.textStyle}>Accéder</Text>
                          </View>
                      </TouchableOpacity>
                      </View>
                </View>
                </ScrollView>
              </View>
            </ImageBackground>
        );
    };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.08,
    marginTop: 50
  },
    mainContainer: {
      flex: 1,
      marginTop: 0,
      backgroundColor: 'rgba(239, 239, 244, 0.85)',
      borderTopLeftRadius: 15,
      borderTopRightRadius: 15
    },
    listContainer: {
      marginTop: 30,
      marginLeft: 15,
      marginRight: 15,
      backgroundColor: 'white',
      borderRadius: 15,
      //borderTopLeftRadius: 15,
      //borderTopRightRadius: 15,
      flex: 1
    },
    topTitle: {
      fontSize:35,
      color: "#f8cd3e",
      marginTop:10,
      fontFamily: "SciFly"
    },
    logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
    },
    buttonStyle: {
      backgroundColor: "#40BCF8",
      height: 48,
      borderRadius: 30,
      width: 135,
      borderWidth: 0.1,
      justifyContent: 'center',
      alignItems: 'center'
      //marginRight:50
    },
    buttonStyle1: {
      backgroundColor: "#f8cd3e",
      borderRadius: 50,
      height: 48,
      width: 135,
      borderWidth: 0.1,
      justifyContent: 'center',
      alignItems: 'center'
      //marginLeft:30
    },
    textStyle: {
      alignSelf: 'center',
      color: 'black',
      fontSize: 20,
      fontFamily: "SciFly",
      paddingTop: Platform.OS === 'ios' ? 2 : 0
    },
    descriptiveText: {
      color: 'white',
      textAlign: 'center',
      marginLeft: 20,
      marginRight: 20
    },
    bodyText: {
      fontSize:18,
      color: '#674171',
      fontFamily: "SciFly",
      marginBottom:30,
      marginTop:10,
      textAlign: 'justify',
      paddingHorizontal: 20
    }
});

export default Kyc;
