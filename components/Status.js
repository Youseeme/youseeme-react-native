import React from 'react';
import {ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Platform, KeyboardAvoidingView, Dimensions} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import * as Font from 'expo-font';
import {getCountriesFromApi, getInterestsFromApi, setBronzeProfileFromApiWithInfos, setSilverProfileFromApiWithInfos, setGoldProfileFromApiWithInfos, setPlatinumProfileFromApiWithInfos} from '../API/YSMApi';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';
import RNPickerSelect from 'react-native-picker-select';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons } from '@expo/vector-icons';
import Root from './RootPopup';
import Toast from './Toast';

const { width, height } = Dimensions.get('screen')

class Status extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });

    this.user = this.props.navigation.getParam('user');
    this.token = this.props.navigation.getParam('token');
    this.refreshProfile = this.props.navigation.getParam('refreshProfile');



    this.birthDay = this.user.birthday != null ? this.user.birthday.substring(4, 6) : "";
    this.birthMonth = this._loadBirthMonth(this.user.birthday != null ? this.user.birthday.substring(0, 3) : "");
    this.birthYear = this.user.birthday != null ? this.user.birthday.substring(8, 12) : "";
    //this.user.birthday = this.birthYear + "-" + this.birthMonth + "-" + this.birthDay;

    this.bronzeProfile = {
      firstName: this.user.first_name != null ? this.user.first_name : "",
      lastName: this.user.last_name != null ? this.user.last_name : "",
      civility: this.user.title != null ? this.user.title : "",
      birthDate: this.user.birthday != null ? this.user.birthday : "",
      mobileNumber: this.user.mobile_number != null ? this.user.mobile_number : ""
    };

    console.log("-----");
    console.log(this.user.birthday);
    console.log(this.bronzeProfile.birthDate);
    console.log("-----");

    this.silverProfile = {
      street: this.user.addresses.length > 0 ? this.user.addresses[0].street : "",
      zipCode: this.user.addresses.length > 0 ? this.user.addresses[0].postal_code : "",
      city: this.user.addresses.length > 0 ? this.user.addresses[0].city : "",
      countryId: this.user.addresses.length > 0 ? this.user.addresses[0].country_id : ""
    };

    this.goldProfile = {
      maritalStatus: this.user.marital_status != null ? this.user.marital_status : "",
      profession: this.user.profession != null ? this.user.profession : "",
      interests: [],
    };

    this.platinumProfile = {
      revenue: this.user.revenue != null ? this.user.revenue : ""
    };

    this.state = {
      countries: [],
      interests: [],
      isLoading: false,
      isValid: false,
      errors: false,
      errorMessage: "",
      civilityPickerItems: [{label: "Mrs", value: 0}, {label: "Mr", value: 1}, {label: "Autre", value: 2}],
      currentCivility: -1,
      countryPickerItems: [],
      currentCountry: -1,
      interestItems: [],
      selectedInterestItems: []
    };
  }

  _setBronzeProfile() {
    return setBronzeProfileFromApiWithInfos(this.token, this.bronzeProfile).then(data => {
      var message = (data == undefined) ?  "error" : data;
      return (message.match(/Profile is updated/).length > 0);
    })
  }

  _setSilverProfile() {
    return setSilverProfileFromApiWithInfos(this.token, this.silverProfile).then(data => {
      console.log(data);
      var message = (data == undefined) ?  "error" : data;
      return (message.match(/Profile is updated/).length > 0);
    })
  }

  _setGoldProfile() {
    var stringifyInterests = "";
    var goldProfile = {};
    Object.assign(goldProfile, this.goldProfile);

    goldProfile.interests.forEach((interest) => {
      stringifyInterests += interest.id + ", ";
    });

    goldProfile.interests = stringifyInterests.substring(0, stringifyInterests.length-2);

    return setGoldProfileFromApiWithInfos(this.token, goldProfile).then(data => {
      var message = (data == undefined) ?  "error" : data;
      return (message.match(/Profile is updated/).length > 0);
    })
  }

  _setPlatinumProfile() {
    return setPlatinumProfileFromApiWithInfos(this.token, this.platinumProfile).then(data => {
      var message = (data == undefined) ?  "error" : data;
      return (message.match(/Profile is updated/).length > 0);
    })
  }

  _displayLoading() {
    if(this.state.isLoading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
  }

  _loadBirthMonth(month) {
    switch (month) {
      case "Jan":
        return "01";
      case "Feb":
        return "02";
      case "Mar":
        return "03";
      case "Apr":
        return "04";
      case "May":
        return "05";
      case "Jun":
        return "06";
      case "Jul":
        return "07";
      case "Aug":
        return "08";
      case "Sep":
        return "09";
      case "Oct":
        return "10";
      case "Nov":
        return "11";
      case "Dec":
        return "12";
      default:
        return "";
    }
  }

  _reverseBirthMonth(month) {
    switch (month) {
      case "01":
        return "Jan";
      case "02":
        return "Feb";
      case "03":
        return "Mar";
      case "04":
        return "Apr";
      case "05":
        return "May";
      case "06":
        return "Jun";
      case "07":
        return "Jul";
      case "08":
        return "Aug";
      case "09":
        return "Sep";
      case "10":
        return "Oct";
      case "11":
        return "Nov";
      case "12":
        return "Dec";
      default:
        return "";
    }
  }

  _loadCivility() {
    console.log(this.user.title)
    switch (this.user.title) {
      case "Mrs":
        this.setState({currentCivility: 0});
        break;
      case "Mr":
        this.setState({currentCivility: 1});
        break;
      default:
        this.setState({currentCivility: 2});
    }
  }

  _loadCountries() {
    getCountriesFromApi().then(data => {
      var countries = [];
      var promises = data.map((country) => {
        countries.push({label: country.name, value: country.id})
      });

      Promise.all(promises).then(() => {
        //console.log(this.user.addresses[0].country_id)
        this.setState({
          countryPickerItems: countries,
          currentCountry: this.user.addresses.length > 0 ? this.user.addresses[0].country_id : 250
        });
      });
    });
  }

  _loadInterests() {
    getInterestsFromApi().then(data => {
      var selectedInterests = [];
      var promises = this.user.interests.map((selectedInterest) => {
        selectedInterests.push(selectedInterest.id)
      });

      Promise.all(promises).then(() => {
        this.setState({
          interestItems: data,
          selectedInterestItems: selectedInterests
        });
      });
    });
  }

  _sameInterests(array1, array2) {
    console.log("array1---------");
    console.log(array1);
    console.log("array2---------");
    console.log(array2);
    console.log("End---------");

    var cpt = 0;
    if(array1.length !== array2.length)
      return false;

    array1.forEach((item1) => {
      array2.forEach((item2) => {
        if(item1.id === item2.id)
          cpt++;
      });
    });

    if(cpt === array1.length)
      return true;
    return false;
  }

  _onPreviousStep() {
    this.setState({
      errorMessage: ""
    });
  }

  _onNextStep = (status) => {
    var errorMessage = "";
    var birthDay = this.birthDay.replace(/ /g,"");
    var birthMonth = this.birthMonth.replace(/ /g,"");
    var birthYear = this.birthYear.replace(/ /g,"");
    switch (status) {
      case "bronze":
        if(this.bronzeProfile.civility.replace(/ /g,"").length < 1)
          errorMessage = "La civilité n'est pas valide.";
        else if(this.bronzeProfile.firstName.replace(/ /g,"").length < 1)
          errorMessage = "Le prénom n'est pas valide.";
        else if(this.bronzeProfile.lastName.replace(/ /g,"").length < 1)
          errorMessage = "Le nom n'est pas valide.";
        else if(this.bronzeProfile.birthDate.replace(/ /g,"").length < 1 ||
        birthDay.length !== 2 || parseInt(birthDay, 10) < 1 || parseInt(birthDay, 10) > 31 ||
        birthMonth.length !== 2 || parseInt(birthMonth, 10) < 1 || parseInt(birthMonth, 10) > 12 ||
        birthYear.length !== 4 || parseInt(birthYear, 10) < 1900 || parseInt(birthYear, 10) > (parseInt(new Date().getFullYear(), 10) - 5))
          errorMessage = "La date d'anniversaire n'est pas valide.";
        else if(this.bronzeProfile.mobileNumber.replace(/ /g,"").length !== 10)
          errorMessage = "Le téléphone n'est pas valide.";
        else {
          this.bronzeProfile.birthDate = this._reverseBirthMonth(this.birthMonth) + " " + this.birthDay + ", " + this.birthYear;
          if(!(this.user.first_name === this.bronzeProfile.firstName &&
            this.user.last_name === this.bronzeProfile.lastName &&
            this.user.title === this.bronzeProfile.civility &&
            this.user.birthday === this.bronzeProfile.birthDate &&
            this.user.mobile_number  === this.bronzeProfile.mobileNumber)) {
            this.bronzeProfile.birthDate = this.birthYear + "-" + this.birthMonth + "-" + this.birthDay;
            if(!this._setBronzeProfile())
              errorMessage = "Une erreur s'est produite veuillez réessayer ultérieurement.";
            else {
              //this.refreshProfile();
              Toast.show({
                  title: 'Profil bronze',
                  text: 'Votre profil a bien été mis à jour.',
                  color: '#2ecc71',
                  icon:(<Ionicons
                      name="ios-checkmark"
                      style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
                    />)
              });

              this.user.first_name = this.bronzeProfile.firstName;
              this.user.last_name = this.bronzeProfile.lastName;
              this.user.title = this.bronzeProfile.civility;
              this.user.birthday = this._reverseBirthMonth(this.birthMonth) + " " + this.birthDay + ", " + this.birthYear;
              this.bronzeProfile.birthDate = this.user.birthday;
            }
          }
        }
        break;
      case "silver":
        this.silverProfile.countryId = this.state.currentCountry;

        console.log(this.silverProfile.countryId);
        console.log(this.silverProfile.street);
        console.log(this.silverProfile.zipCode);
        console.log(this.silverProfile.city);

        if(this.silverProfile.street.replace(/ /g,"").length < 1)
          errorMessage = "L'adresse postale n'est pas valide.";
        else if(this.silverProfile.zipCode.replace(/ /g,"").length < 1)
          errorMessage = "Le code postal n'est pas valide.";
        else if(this.silverProfile.city.replace(/ /g,"").length < 1)
          errorMessage = "La ville n'est pas valide.";
        else if(this.silverProfile.countryId < 0)
          errorMessage = "Le pays n'est pas valide.";
        else {
          if(!(this.user.addresses.length > 0) || !(this.user.addresses[0].street === this.silverProfile.street &&
            this.user.addresses[0].postal_code === this.silverProfile.zipCode &&
            this.user.addresses[0].city === this.silverProfile.city &&
            this.user.addresses[0].country_id === this.silverProfile.countryId)) {
            if(!this._setSilverProfile())
              errorMessage = "Une erreur s'est produite veuillez réessayer ultérieurement.";
            else {
              //this.refreshProfile();
              Toast.show({
                  title: 'Profil argent',
                  text: 'Votre profil a bien été mis à jour.',
                  color: '#2ecc71',
                  icon:(<Ionicons
                      name="ios-checkmark"
                      style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
                    />)
              });

              if(!(this.user.addresses.length > 0))
                this.user.addresses.push({
                  street: this.silverProfile.street,
                  postal_code: this.silverProfile.zipCode,
                  city: this.silverProfile.city,
                  country_id: this.silverProfile.countryId
                });
              else {
                this.user.addresses[0].street = this.silverProfile.street;
                this.user.addresses[0].postal_code = this.silverProfile.zipCode;
                this.user.addresses[0].city = this.silverProfile.city;
                this.user.addresses[0].country_id = this.silverProfile.countryId;
              }
            }
          }
        }
        break;
      case "gold":
        this.goldProfile.interests = [];
        this.state.interestItems.map((interestItem) => {
          this.state.selectedInterestItems.forEach((selectedInterest) => {
            //console.log(interestItem.id + " == " + selectedInterest)
            if(interestItem.id == selectedInterest)
              this.goldProfile.interests.push(interestItem);
          });
        });

        if(this.goldProfile.maritalStatus.replace(/ /g,"").length < 1)
          errorMessage = "Le statut social n'est pas valide.";
        else if(this.goldProfile.profession.replace(/ /g,"").length < 1)
          errorMessage = "La profession n'est pas valide.";
        else if(this.goldProfile.interests.length < 1)
          errorMessage = "Aucun loisir séléctionné.";
        else {
          console.log("-------Result : ")
          console.log(!(this.user.marital_status === this.goldProfile.maritalStatus &&
            this.user.profession === this.goldProfile.profession &&
            this._sameInterests(this.user.interests, this.goldProfile.interests)));

          if(!(this.user.marital_status === this.goldProfile.maritalStatus &&
            this.user.profession === this.goldProfile.profession &&
            this._sameInterests(this.user.interests, this.goldProfile.interests))) {
            if(!this._setGoldProfile())
              errorMessage = "Une erreur s'est produite veuillez réessayer ultérieurement.";
            else {
              //this.refreshProfile();
              Toast.show({
                  title: 'Profil or',
                  text: 'Votre profil a bien été mis à jour.',
                  color: '#2ecc71',
                  icon:(<Ionicons
                      name="ios-checkmark"
                      style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
                    />)
              });

              this.user.marital_status = this.goldProfile.maritalStatus;
              this.user.profession = this.goldProfile.profession;
              this.user.interests = this.goldProfile.interests;
              //this.goldProfile.interests = [];
            }
          }
        }
        break;
      case "platinum":
        if(this.platinumProfile.revenue.replace(/ /g,"").length < 1)
          errorMessage = "Le revenu annuel n'est pas valide.";
        else {
          if(!(this.user.revenue === this.platinumProfile.revenue)) {
            if(!this._setPlatinumProfile())
              errorMessage = "Une erreur s'est produite veuillez réessayer ultérieurement.";
            else {
              //this.refreshProfile();
              Toast.show({
                  title: 'Profil platine',
                  text: 'Votre profil a bien été mis à jour.',
                  color: '#2ecc71',
                  icon:(<Ionicons
                      name="ios-checkmark"
                      style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
                    />)
              });

              this.user.revenue = this.platinumProfile.revenue;
            }
          }
          else {
            this.props.navigation.goBack();
          }
        }
        break;
      default:
    }

    this.setState({
      errors: (errorMessage !== ""),
      errorMessage: errorMessage
    });

    /*if (!this.state.isValid) {
      this.setState({
        errors: true,
        errorMessage: errorMessage
      });
    } else {
      this.setState({
        errors: false,
        errorMessage: errorMessage
      });
    }*/
  };

  _activeStep() {
    switch (this.user.status) {
      case "Bronze":
        return 1;
      case "Silver":
        return 2;
      case "Gold":
        return 3;
      case "Platinum":
        return 4;
      default:
        return 0;
    }
  }

  _displayNoResultsComponent(message) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'red'}}>
        <Text style={{color: '#674171'}}>{message}</Text>
      </View>
    );
  }

  componentDidMount() {
    this._loadCivility();
    this._loadCountries();
    this._loadInterests();
  }

  render() {
    return (
      <Root>
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            {/*<Image
                style={styles.logo}
                source={require('../assets/logo.png')}
              />

            <Text  style={styles.topTitle}>
                Mon relevé de compte
            </Text>*/}
            {/*<Text style={styles.descriptiveText}>
                Voici le résumé de l'ensemble des opérations ayant eu lieu sur votre compte.
            </Text>*/}
        </View>
        <KeyboardAvoidingView
          behavior={Platform.OS == "ios" ? "padding" : "height"}
          style={{flex: 1}}
        >
        {/*<ScrollView
          contentContainerStyle={{height: height/1.15}}
        >*/}
          <ProgressSteps
            borderWidth={3}
            activeStep={this._activeStep()}
            activeStepIconColor={'transparent'}
            activeLabelColor={'#F8CE3E'}
            activeStepNumColor={'white'}
            disabledStepNumColor={'#674171'}

            //disabledStepNumColor={'#F8CE3E'}
            progressBarColor={'#c3bfc4'}
            disabledStepIconColor={'#c3bfc4'}


            activeStepIconBorderColor={'#F8CE3E'}
            completedProgressBarColor={'#F8CE3E'}
            completedStepIconColor={'#F8CE3E'}
          >
            <ProgressStep
              label="Initial"
              nextBtnText="Suivant"
              nextBtnStyle={styles.nextBtn}
              nextBtnTextStyle={styles.nextBtnText}
            >
              <View style={styles.statutesPresentationContainer}>
                <View style={styles.statutesPresentationTitleContainer}>
                  <Text style={styles.statutesPresentationTitle}>Statut et Avantages</Text>
                </View>
                <Text style={styles.statutesPresentationBody}>
                {`
C’est assez simple, il y a 4 statuts possibles.
Plus tu complètes ton profil, plus tu y gagnes.
On te crédite des Bartcoins automatiquement à chaque achat et tu peux les dépenser tout de suite chez tous les commerçants de la plateforme.
  •	Bronze, tu récupères 1% de tes dépenses
  •	Argent, tu récupères 2% de tes dépenses
  •	Or, tu récupères 3% de tes dépenses
  •	Platine, jackpot, c’est 4% de tes dépenses.

Les données fournies nous servent uniquement à te proposer des offres personnalisées en fonction de tes habitudes de vie.`}
                </Text>
              </View>
            </ProgressStep>
            <ProgressStep
              label="Bronze"
              onNext={() => this._onNextStep("bronze")}
              onPrevious={() => this._onPreviousStep()}
              errors={this.state.errors}
              nextBtnText="Suivant"
              previousBtnText="C’est quoi ?"
              nextBtnStyle={styles.nextBtn}
              nextBtnTextStyle={styles.nextBtnText}
              previousBtnStyle={styles.previousBtn}
              previousBtnTextStyle={styles.previousBtnText}
            >
              <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
              <View style={[styles.entryContainer, {marginTop: 4}]}>
                <Text style={styles.entryDescriptionText}>{"Civilité :"}</Text>
                <View style={styles.pickerSelectBackground}>
                  <RNPickerSelect
                      style={pickerSelectStyles}
                      Icon={() => {
                        return (
                          <Ionicons
                            name="ios-arrow-down"
                            style={styles.pickerArrow}
                          />
                        );
                      }}
                      placeholder={{
                        label: 'Choisir...',
                        value: null,
                        color: '#9EA0A4',
                      }}
                      onValueChange={(value) => {
                        this.setState({currentCivility: value});
                        this.bronzeProfile.civility = this.state.civilityPickerItems[value].label;
                      }}
                      items={this.state.civilityPickerItems}
                      value={this.state.currentCivility}
                  />
                </View>
              </View>
              <View style={styles.entryContainer}>
                <Text style={styles.entryDescriptionText}>{"Prénom :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.firstNameInput = ref}
                      autoCompleteType={'off'}
                      style={styles.textEntry}
                      defaultValue={this.bronzeProfile.firstName}
                      maxLength={200}
                      placeholder="Votre prénom"
                      fontSize={18}
                      textAlign='left'
                      returnKeyType={'next'}
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      onSubmitEditing={() => this.lastNameInput.focus()}
                      onChangeText={(firstName) => {
                        this.bronzeProfile.firstName = firstName;
                      }}
                    />
                  </View>
                </View>
                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Nom  :"}</Text>
                  <View style={styles.entryBackground}>
                    <TextInput
                        ref={ref => this.lastNameInput = ref}
                        autoCompleteType={'off'}
                        style={styles.textEntry}
                        defaultValue={this.bronzeProfile.lastName}
                        maxLength={200}
                        placeholder="Votre nom"
                        fontSize={18}
                        textAlign='left'
                        returnKeyType={'next'}
                        placeholderTextColor={'rgb(100, 95, 107)'}
                        selectionColor={'#674171'}
                        onSubmitEditing={() => this.birthDayEntry.focus()}
                        onChangeText={(lastName) => {
                          this.bronzeProfile.lastName = lastName;
                        }}
                    />
                  </View>
                </View>

                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Date d'anniversaire :"}</Text>
                  <View style={styles.birthDateContainer}>
                  <View style={styles.birthDateBackground}>
                      <TextInput
                          ref={ref => this.birthDayEntry = ref}
                          autoCompleteType={'off'}
                          style={styles.birthDate}
                          defaultValue={this.birthDay != null ? this.birthDay : null}
                          maxLength={2}
                          placeholder="JJ"
                          fontSize={18}
                          textAlign='center'
                          placeholderTextColor={'rgb(100, 95, 107)'}
                          selectionColor={'#674171'}
                          keyboardType={'numeric'}
                          returnKeyType={'done'}
                          onChangeText={(birthDay) => {
                            if(birthDay.length >= 2)
                              this.birthMonthEntry.focus();
                            this.birthDay = birthDay
                          }}
                      />
                      </View>
                      <View style={styles.birthDateBackground}>
                      <TextInput
                          ref={ref => this.birthMonthEntry = ref}
                          autoCompleteType={'off'}
                          style={styles.birthDate}
                          defaultValue={this.birthMonth != null ? this.birthMonth : null}
                          maxLength={2}
                          placeholder="MM"
                          fontSize={18}
                          textAlign='center'
                          placeholderTextColor={'rgb(100, 95, 107)'}
                          selectionColor={'#674171'}
                          keyboardType={'numeric'}
                          returnKeyType={'done'}
                          onChangeText={(birthMonth) => {
                            if(birthMonth.length >= 2)
                              this.birthYearEntry.focus();
                            this.birthMonth = birthMonth
                          }}
                      />
                      </View>
                      <View style={styles.birthDateBackground}>
                        <TextInput
                            ref={ref => this.birthYearEntry = ref}
                            autoCompleteType={'off'}
                            style={styles.birthDate}
                            defaultValue={this.birthYear != null ? this.birthYear : null}
                            maxLength={4}
                            placeholder="YYYY"
                            fontSize={18}
                            textAlign='center'
                            placeholderTextColor={'rgb(100, 95, 107)'}
                            selectionColor={'#674171'}
                            keyboardType={'numeric'}
                            returnKeyType={'done'}
                            onChangeText={(birthYear) => {
                              if(birthYear.length >= 4)
                                this.phoneInput.focus();
                              this.birthYear = birthYear
                            }}
                        />
                      </View>
                    </View>
                </View>
                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Téléphone  :"}</Text>
                  <View style={styles.entryBackground}>
                    <TextInput
                        ref={ref => this.phoneInput = ref}
                        autoCompleteType={'off'}
                        style={styles.textEntry}
                        defaultValue={this.bronzeProfile.mobileNumber}
                        maxLength={10}
                        placeholder="Votre téléphone"
                        fontSize={18}
                        textAlign='left'
                        placeholderTextColor={'rgb(100, 95, 107)'}
                        selectionColor={'#674171'}
                        keyboardType={'numeric'}
                        returnKeyType={'done'}
                        onChangeText={(number) => {
                          this.bronzeProfile.mobileNumber = number;
                        }}
                    />
                  </View>
                </View>
                <View style={styles.errorContainer}>
                  {this.state.errorMessage !== "" ? <Ionicons
                    name="ios-warning"
                    style={styles.errorIcon}
                  /> : null}
                  <Text style={styles.txtError}>{this.state.errorMessage}</Text>
                </View>
              </View>
            </ProgressStep>

              <ProgressStep
                label="Argent"
                onNext={() => this._onNextStep("silver")}
                onPrevious={() => this._onPreviousStep()}
                errors={this.state.errors}
                nextBtnText="Suivant"
                previousBtnText="Précédent"
                nextBtnStyle={styles.nextBtn}
                nextBtnTextStyle={styles.nextBtnText}
                previousBtnStyle={styles.previousBtn}
                previousBtnTextStyle={styles.previousBtnText}
              >
              <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
              <View style={[styles.entryContainer, {marginTop: 4}]}>
                <Text style={styles.entryDescriptionText}>{"Adresse postale :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.streetInput = ref}
                      autoCompleteType={'street-address'}
                      style={styles.textEntry}
                      defaultValue={this.silverProfile.street}
                      maxLength={50}
                      placeholder="Votre adresse postale"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      returnKeyType={'next'}
                      onSubmitEditing={() => this.zipCodeInput.focus()}
                      onChangeText={(street) => {
                        this.silverProfile.street = street;
                      }}
                  />
                </View>
              </View>
              <View style={styles.entryContainer}>
                <Text style={styles.entryDescriptionText}>{"Code postal :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.zipCodeInput = ref}
                      autoCompleteType={'postal-code'}
                      keyboardType={'numeric'}
                      style={styles.textEntry}
                      defaultValue={this.silverProfile.zipCode}
                      maxLength={200}
                      placeholder="Votre code postal"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      returnKeyType={'done'}
                      onSubmitEditing={() => this.cityInput.focus()}
                      onChangeText={(zipCode) => {
                        this.silverProfile.zipCode = zipCode;
                      }}
                    />
                  </View>
                </View>
                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Ville  :"}</Text>
                  <View style={styles.entryBackground}>
                    <TextInput
                        ref={ref => this.cityInput = ref}
                        autoCompleteType={'off'}
                        style={styles.textEntry}
                        defaultValue={this.silverProfile.city}
                        maxLength={200}
                        placeholder="Nom de la ville"
                        fontSize={18}
                        textAlign='left'
                        placeholderTextColor={'rgb(100, 95, 107)'}
                        selectionColor={'#674171'}
                        returnKeyType={'done'}
                        onSubmitEditing={() => {}}
                        onChangeText={(city) => {
                          this.silverProfile.city = city;
                        }}
                    />
                  </View>
                </View>

                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Pays :"}</Text>
                  <View style={styles.pickerSelectBackground}>
                    <RNPickerSelect
                        style={pickerSelectStyles}
                        Icon={() => {
                          return (
                            <Ionicons
                              name="ios-arrow-down"
                              style={styles.pickerArrow}
                            />
                          );
                        }}
                        placeholder={{
                          label: 'Choisir...',
                          value: -1,
                          color: '#9EA0A4',
                        }}
                        onValueChange={(value) => {
                          this.setState({currentCountry: value});
                        }}
                        items={this.state.countryPickerItems}
                        value={this.state.currentCountry}
                    />
                  </View>
                </View>
                <View style={styles.errorContainer}>
                  {this.state.errorMessage !== "" ? <Ionicons
                    name="ios-warning"
                    style={styles.errorIcon}
                  /> : null}
                  <Text style={styles.txtError}>{this.state.errorMessage}</Text>
                </View>
              </View>
            </ProgressStep>
            <ProgressStep
              label="Or"
              onNext={() => this._onNextStep("gold")}
              onPrevious={() => this._onPreviousStep()}
              errors={this.state.errors}
              nextBtnText="Suivant"
              previousBtnText="Précédent"
              nextBtnStyle={styles.nextBtn}
              nextBtnTextStyle={styles.nextBtnText}
              previousBtnStyle={styles.previousBtn}
              previousBtnTextStyle={styles.previousBtnText}
            >
              <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
              <View style={[styles.entryContainer, {marginTop: 4}]}>
                <Text style={styles.entryDescriptionText}>{"Vous êtes :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.maritalStatusInput = ref}
                      autoCompleteType={'off'}
                      style={styles.textEntry}
                      defaultValue={this.goldProfile.maritalStatus}
                      maxLength={50}
                      placeholder="Votre statut social"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      returnKeyType={'next'}
                      onSubmitEditing={() => this.professionInput.focus()}
                      onChangeText={(maritalStatus) => {
                        this.goldProfile.maritalStatus = maritalStatus;
                      }}
                  />
                </View>
              </View>
              <View style={styles.entryContainer}>
                <Text style={styles.entryDescriptionText}>{"Profession :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.professionInput = ref}
                      autoCompleteType={'off'}
                      style={styles.textEntry}
                      defaultValue={this.goldProfile.profession}
                      maxLength={200}
                      placeholder="Votre profession"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      returnKeyType={'done'}
                      onChangeText={(profession) => {
                        this.goldProfile.profession = profession;
                      }}
                    />
                  </View>
                </View>
                <View style={styles.entryContainer}>
                  <Text style={styles.entryDescriptionText}>{"Loisirs :"}</Text>
                    <SectionedMultiSelect
                      items={this.state.interestItems}
                      colors={{
                        primary:'#F8CE3E',
                        success:'#674171',
                        chipColor: 'white'
                      }}
                      styles={multiSelectStyles}
                      uniqueKey="id"
                      subKey="children"
                      selectText="Choisir..."
                      confirmText="Valider"
                      selectedText="Sélectionnés"
                      removeAllText="Tout supprimer"
                      searchPlaceholderText="Rechercher un loisir"
                      showDropDowns={true}
                      showCancelButton={false}
                      showRemoveAll={true}
                      readOnlyHeadings={false}
                      modalWithSafeAreaView={true}
                      modalWithTouchable={true}
                      chipRemoveIconComponent={
                        <Ionicons
                          name="ios-close"
                          style={{fontSize: 24, paddingLeft: 6, paddingRight: 10, paddingTop: 3, color: 'white'}}
                        />
                      }
                      selectToggleIconComponent={
                        <Ionicons
                          name="ios-arrow-down"
                          style={styles.pickerArrow}
                        />
                      }
                      noResultsComponent={this._displayNoResultsComponent("Désolé, aucun loisir n'a été trouvé.")}
                      onSelectedItemsChange={(selectedItems) => {
                        this.setState({selectedInterestItems: selectedItems})
                      }}
                      selectedItems={this.state.selectedInterestItems}
                    />
                </View>
                <View style={styles.errorContainer}>
                  {this.state.errorMessage !== "" ? <Ionicons
                    name="ios-warning"
                    style={styles.errorIcon}
                  /> : null}
                  <Text style={styles.txtError}>{this.state.errorMessage}</Text>
                </View>
              </View>
            </ProgressStep>
            <ProgressStep
              label="Platine"
              onSubmit={() => this._onNextStep("platinum")}
              onPrevious={() => this._onPreviousStep()}
              errors={this.state.errors}
              //nextBtnDisabled={this.user.status === "Platinum"}
              //finishBtnText={this.user.status === "Platinum" ? "" : "Finir"}
              finishBtnText="Finir"
              previousBtnText="Précédent"
              //nextBtnStyle={this.user.status === "Platinum" ? null : styles.nextBtn}
              nextBtnStyle={styles.nextBtn}
              nextBtnTextStyle={styles.nextBtnText}
              previousBtnStyle={styles.previousBtn}
              previousBtnTextStyle={styles.previousBtnText}
            >
              <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
              <View style={[styles.entryContainer, {marginTop: 4}]}>
                <Text style={styles.entryDescriptionText}>{"Revenu annuel :"}</Text>
                <View style={styles.entryBackground}>
                  <TextInput
                      ref={ref => this.revenueInput = ref}
                      autoCompleteType={'off'}
                      style={styles.textEntry}
                      defaultValue={this.platinumProfile.revenue}
                      maxLength={50}
                      placeholder="Votre revenu annuel"
                      fontSize={18}
                      textAlign='left'
                      placeholderTextColor={'rgb(100, 95, 107)'}
                      selectionColor={'#674171'}
                      keyboardType={'numeric'}
                      returnKeyType={'done'}
                      onChangeText={(revenue) => {
                        this.platinumProfile.revenue = revenue;
                      }}
                    />
                  </View>
                </View>
                <View style={styles.errorContainer}>
                  {this.state.errorMessage !== "" ? <Ionicons
                    name="ios-warning"
                    style={styles.errorIcon}
                  /> : null}
                  <Text style={styles.txtError}>{this.state.errorMessage}</Text>
                </View>
              </View>
            </ProgressStep>
          </ProgressSteps>
          {/*</ScrollView>*/}
        </KeyboardAvoidingView>
      </ImageBackground>
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.05,
    marginTop: 50
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  nextBtn: {
    backgroundColor: '#F8CE3E',
    width: 100,
    height: 40,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: -30,
    marginBottom: 2
  },
  nextBtnText: {
    color: 'black',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  previousBtn: {
    backgroundColor: '#F8CE3E',
    width: 100,
    height: 40,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: -30,
    marginBottom: 2
  },
  previousBtnText: {
    color: 'black',
    fontFamily: "SciFly",
    textAlign: 'center',
    paddingTop: Platform.OS === 'ios' ? 4 : 0
  },
  statutesPresentationContainer: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    borderRadius: 15,
    marginHorizontal: 15,
    padding: 20,
    alignItems: 'center',
    marginTop: 10
  },
  statutesPresentationTitleContainer: {
    flex: 0.2,
    width:'85%',
    paddingBottom: 5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10
  },
  statutesPresentationTitle: {
    fontSize: 22,
    fontWeight: '400',
    color:'#674171',
    textAlign: 'center',
    borderBottomColor: 'grey',
    fontFamily: "SciFly",
    paddingBottom: 5,
  },
  statutesPresentationBody: {
    flex: 0.8,
    fontSize: 17,
    color:'#674171',
    textAlign: 'justify',
    fontFamily: "SciFly"
  },
  entryContainer: {
    width:'80%',
    //backgroundColor: 'red',
    marginTop: 25
  },
  entryBackground: {
    width:'100%',
    backgroundColor:'#FFFFFF',
    borderRadius: 8,
    opacity: 0.4,
    height: 50,
  },
  pickerSelectBackground: {
    width:'100%',
    backgroundColor:'rgba(255, 255, 255, 0.4)',
    borderRadius: 8,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  pickerArrow: {
    color: '#674171',
    fontSize: 23,
    marginRight: 12,
    marginTop: 5
  },
  textEntry: {
    flex: 1,
    color:'black',
    paddingLeft: 10,
  },
  btnSubmit: {
    backgroundColor: '#F8CE3E',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
    paddingVertical: 15,
    borderRadius: 30,
    marginTop: 30
  },
  entryDescriptionText: {
    color: 'white',
    fontWeight: '600',
    marginBottom: 8
  },
  txtBtnSubmit: {
    color: '#674171',
    fontSize: 18,
    fontWeight: '600'
  },
  errorContainer: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  errorIcon: {
    fontSize: 25,
    color: '#FF453A',
  },
  txtError: {
    fontSize: 18,
    color: '#FF453A',
    marginLeft: 10
  },

  birthDateContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'

  },
  birthDateBackground: {
    width:80,
    backgroundColor:'#FFFFFF',
    borderRadius: 8,
    opacity: 0.4,
    height: 50,
  },
  birthDate: {
    flex: 1,
    color:'black',
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    width: '100%',
    height: 30,
    fontSize: 18,
    paddingVertical: 2,
    paddingHorizontal: 10,
    borderRadius: 10,
    color: 'rgba(0, 0, 0, 0.63)',
    paddingRight: 25,
    textAlign: 'left'
  },
  inputAndroid: {
    width: '100%',
    height: 30,
    fontSize: 18,
    paddingVertical: 2,
    paddingHorizontal: 10,
    borderRadius: 10,
    color: 'rgba(0, 0, 0, 0.63)',
    paddingRight: 25,
    textAlign: 'left'
  },
});

const multiSelectStyles = StyleSheet.create({
  container: {
    marginTop: 80,
    marginBottom: 80,
    borderRadius: 10
  },
  confirmText:{
    color: 'white'
  },
  selectToggle:{
    width:'100%',
    backgroundColor:'#FFFFFF',
    borderRadius: 8,
    opacity: 0.4,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 10,
    paddingLeft: 10,
  },
  selectToggleText: {
    color: 'black',
    fontSize: 18,
  },
  chipsWrapper: {
    marginTop: 5,
    justifyContent: 'center',
    alignItems: 'center'
  },
  selectedItemText: {
    color:'#674171',
  }
});

export default Status;
