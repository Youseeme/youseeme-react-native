import React from 'react';
import {AsyncStorage, ActivityIndicator, FlatList, Image, ImageBackground, StyleSheet, Text, View, Platform, TouchableOpacity} from 'react-native';
import { Ionicons, SimpleLineIcons, Fontisto, MaterialCommunityIcons, FontAwesome5, FontAwesome, MaterialIcons, AntDesign } from '@expo/vector-icons';
import SegmentedControlTab from "react-native-segmented-control-tab";
import ProductItem from './ProductItem';
import * as Font from 'expo-font';
import * as Haptics from 'expo-haptics';
import Root from './RootPopup';
import Toast from './Toast';


class ProductsDetails extends React.Component {
  constructor(props) {
    super(props);
    Font.loadAsync({
        'SciFly': require('../assets/fonts/SciFly.ttf'),
    });
    this.cptId = 0;
    this.token = this.props.navigation.getParam('token');
    this.shopId = this.props.navigation.getParam('shopId');
    this.shoppingBasketCode = 'shoppingBasket-' + this.shopId;
    //this._addToShoppingBasket = this.props.navigation.getParam('addToShoppingBasket');

    this.state = {
      family: this.props.navigation.getParam('family'),
      isLoading: false,
      shoppingBasket: {
        articles: [],
        articleCount: 0,
        totalAmount: 0.00
      }
    };
  }

  _loadShoppingBasket() {
    AsyncStorage.getItem('shoppingBasket-' + this.shopId).then(data => {
      if(data != null) {
        var shoppingBasket = JSON.parse(data);
        this.setState({shoppingBasket: shoppingBasket});
      }
    });
  }

  _clearShoppingBasket = async () => {
    try {
      await AsyncStorage.removeItem(this.shoppingBasketCode);
      this.setState({
        shoppingBasket: {
          articles: [],
          articleCount: 0,
          totalAmount: 0.00
        }
      });
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }

  _displayShoppingBasket = () => {
    this.props.navigation.navigate('ShoppingBasket', {
      token: this.token,
      shoppingBasket: this.state.shoppingBasket,
      clearShoppingBasket: this._clearShoppingBasket,
      refreshShoppingBasket: () => this._loadShoppingBasket(),
      shoppingBasketCode: this.shoppingBasketCode
    });
  }

  _displayProductDetailWithOptions = (product) => {
    this.props.navigation.navigate('ProductDetailWithOptions', {product: product, addToShoppingBasket: this._addToShoppingBasket});
  }

  _addToShoppingBasket = async (article) => {
     try {
       const index = this.state.shoppingBasket.articles.map(
         e => e.article_code + "-" + e.bar_code + "-" + e.name
       ).indexOf(article.article_code + "-" + article.bar_code + "-" + article.name);
       if(index === -1) {
         article.quantity = 1;
         await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
           articles: [...this.state.shoppingBasket.articles, ...[article]],
           articleCount: this.state.shoppingBasket.articleCount + 1,
           totalAmount: this.state.shoppingBasket.totalAmount + parseFloat(article.price_ttc.replace(/,/g,"."))
         }));
       }
       else {
         var articles = this.state.shoppingBasket.articles;
         articles[index].quantity++;
         await AsyncStorage.setItem(this.shoppingBasketCode, JSON.stringify({
           articles: articles,
           articleCount: this.state.shoppingBasket.articleCount + 1,
           totalAmount: this.state.shoppingBasket.totalAmount + parseFloat(article.price_ttc.replace(/,/g,"."))
         }));
       }

       Toast.show({
           title: "Panier",
           timing: 2000,
           text: "L'article a bien été ajouté au panier",
           color: '#2ecc71',
           icon:(<Ionicons
               name="ios-checkmark"
               style={{fontSize: 32, color: 'white', paddingTop: 2, paddingLeft: 2}}
             />)
       });
       Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);

     } catch (error) {
       Toast.show({
           title: "Panier",
           timing: 2000,
           text: "Un problème est survenu lors du transfert, veuillez réessayer ultérieurement.",
           color: '#FF453A',
           icon:(<Ionicons
               name="ios-close"
               style={{fontSize: 35, color: 'white', paddingTop: 2, paddingLeft: 1}}
             />)
       });
     }
    this._loadShoppingBasket();
    this.props.navigation.state.params.onGoBack();
  }

  componentDidMount() {
    this._loadShoppingBasket();
  }

  render() {
    return (
      <Root>
        <View style={{flex: 1, backgroundColor: '#674171'}}>
          <View style={styles.topHeadband}>
            <Text  style={styles.topTitle}>
                {this.state.family.name}
            </Text>
            <TouchableOpacity
              style={{position: 'absolute', bottom: 13, right: 7}}
              onPress={() => this._displayShoppingBasket()}
            >
              <FontAwesome
                name="shopping-basket"
                style={{fontSize: 23, paddingRight: 3, color: 'white'}}
              />
              <View style={styles.articleCount}>
                <Text style={styles.textArticleCount}>{this.state.shoppingBasket.articleCount}</Text>
              </View>
            </TouchableOpacity>
          </View>
            <View style={styles.mainContainer}>
              <FlatList
                style={styles.listContainer}
                contentContainerStyle={{paddingTop: 8, paddingBottom: 50}}
                data={this.state.family.items}
                keyExtractor={(item) => (++this.cptId).toString()}
                renderItem={({item}) => <ProductItem product={item} shopId={this.shopId} familyId={this.state.family.id} addToShoppingBasket={this._addToShoppingBasket} displayProductDetailWithOptions={this._displayProductDetailWithOptions}/>}
                refreshing={this.state.isLoading}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                  //this._loadTransactions();
                }}
              />
            {/*this._displayLoading()*/}
          </View>
        </View>
      </Root>
    );
  };
}

const styles = StyleSheet.create({
  topHeadband: {
    flex:0.1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 18 : 25,
    backgroundColor: '#674171',
    zIndex: 2
  },
  mainContainer: {
    flex: 1,
    backgroundColor: 'rgba(239, 239, 244, 0.85)',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    overflow: 'hidden'
  },
  listContainer: {
    width: '100%',
    paddingTop: 10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    //overflow: 'hidden',
    //alignSelf: 'center',
    //backgroundColor: 'red',
    paddingHorizontal: 5
  },
  loading_container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 100,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  topTitle: {
    fontSize:20,
    color: "#f8cd3e",
    marginTop: Platform.OS === 'ios' ? 25 : 1,
    fontFamily: "SciFly"
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  logo: {
      width: 200,
      height: 60,
      resizeMode: 'contain'
  },
  tabView: {
    marginTop: 15,
    marginRight: 40,
    marginLeft: 40
  },
  tabStyle: {
    borderColor: '#6D4877',
    height: 35,
    backgroundColor: 'transparent'
  },
  tabTextStyle: {
    color:'#6D4877',
    fontFamily: "SciFly"
  },
  activeTabStyle: {
    backgroundColor: '#6D4877',
  },
  activeTabTextStyle: {
    color:'white'
  },
  articleCount: {
    height: 20,
    borderRadius: 30,
    position: 'absolute',
    right: -4,
    bottom: 14,
    backgroundColor: '#FF3B30',
    justifyContent: 'center',
    alignItems: 'center',
    minWidth: 20
  },
  textArticleCount: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    paddingHorizontal: 5
  }
});

export default ProductsDetails;
