import React from 'react';
import {  StyleSheet, View, ImageBackground, TouchableOpacity,Button, Image, Animated, Dimensions, Keyboard, TextInput, UIManager, ScrollView, ActivityIndicator, Easing } from 'react-native';
import { Input, Block, Text } from 'galio-framework';
import * as Font from 'expo-font';
import {getUserProfileFromApiWithAccountToken, getUserBalanceFromApiWithAccountToken} from '../API/YSMApi';
import { LinearGradient } from 'expo-linear-gradient';
import NetInfo from '@react-native-community/netinfo';

const { width, height } = Dimensions.get('screen')

export default class Profil extends React.Component{
  constructor(props) {
      super(props);
      Font.loadAsync({
          'SciFly': require('../assets/fonts/SciFly.ttf'),
      });
      this.token = this.props.navigation.getParam('token');

      this.spinImageValue = new Animated.Value(0);
      this.animatedColorValue = new Animated.Value(0);

      this.state = {
          isLoading: true,
          user: {},
          balance:'',
          transfert: '',
          num:'',
        };
      }

  _loadStatusImage(spin) {
    var image;

    switch (this.state.user.status) {
      case "Bronze":
        image = require('../assets/bronze.png');
        break;
      case "Silver":
        image = require('../assets/silver.png');
        break;
      case "Gold":
        image = require('../assets/gold.png');
        break;
      case "Platinum":
        image = require('../assets/platinum.png');
        break;
      default:
        return null;
    }

    //path = '../assets/' + this.state.user.status.toLowerCase() + '.png';
    return (
      <Animated.Image
        style={[styles.statusImage, {transform: [{rotate: spin}]}]}
        source={image}
      />
    );
  }

  _loadStatusButton() {
    let status = this.state.user.status != null ? this.state.user.status : "";
    var color1 = "transparent";
    var color2 = "transparent";
    var color3 = "transparent";
    var color4 = "transparent";
    var color5 = "transparent";
    var color6 = "transparent";

    if(status === "Bronze") {
      color1 = "rgba(149, 92, 37, 0.2)";
      color2 = "rgba(199, 124, 49, 0.2)";
      color3 = "rgba(222, 142, 62, 0.35)";
      color4 = "rgba(218, 153, 44, 0.5)";

      color5 = "rgba(149, 92, 37, 1)";
      color6 = "rgba(199, 124, 49, 1)";
    }
    else if(status === "Silver") {
      color1="rgba(145, 145, 145, 0.2)";
      color2 = "rgba(128, 119, 122, 0.2)";
      color3="rgba(189, 183, 185, 0.35)";
      color4 = "rgba(98, 99, 99, 0.3)";

      color5="rgba(145, 145, 145, 1)";
      color6 = "rgba(128, 119, 122, 1)";
    }
    else if(status === "Gold") {
      color1="rgba(179, 150, 0, 0.2)";
      color2 = "rgba(240, 191, 0, 0.2)";
      color3="rgba(255, 215, 0, 0.25)";
      color4 = "rgba(230, 193, 0, 0.1)";

      color5="rgba(242, 205, 7, 1)";
      color6 = "rgba(240, 191, 0, 1)";
    }
    else if(status === "Platinum") {
      color1="rgba(91, 85, 87, 0.2)";
      color2 = "rgba(128, 119, 122, 0.2)";
      color3 = "rgba(2, 168, 244, 0.35)";
      color4 = "rgba(0, 119, 150, 0.1)";

      color5="rgba(91, 85, 87, 1)";
      color6 = "rgba(128, 119, 122, 1)";
    }

    const backgroundColorConfig = this.animatedColorValue.interpolate(
    {
      inputRange: [0, 0.2, 0.4, 0.7, 0.9, 1],
      outputRange: [color1, color2, color3, color4, color2, color1]
    });

    return (
      <TouchableOpacity style={styles.btnStatus}>
        <LinearGradient start={[0.8, 0.1]} end={[0.2, 0.9]} colors={[color5, color6]} style={{width: '100%', height: '100%', borderRadius: 10}}>
          <Animated.View style={[styles.btnStatusTextContainer, {backgroundColor: backgroundColorConfig}]}>
            <View style={{width: '100%', height: '50%', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.btnStatusText}>{status.toUpperCase()}</Text>
            </View>
          </Animated.View>
        </LinearGradient>
      </TouchableOpacity>
    );
  }

  runAnimationImage() {
    this.spinImageValue.setValue(0);
      Animated.timing(this.spinImageValue, {
        toValue: 1,
        duration: 2000,
        delay: 4000,
        //easing: Easing.linear,
        useNativeDriver: true
    }).start(() => this.runAnimationImage());
  }

  runAnimationColor() {
    Animated.loop(Animated.timing(this.animatedColorValue, {
        toValue: 1,
        duration: 6000,
        //useNativeDriver: true
    })).start();
  }

  loadProfile = () => {
    getUserProfileFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        user: data
      });
    });

    getUserBalanceFromApiWithAccountToken(this.token).then(data => {
      this.setState({
        balance: data,
        isLoading: false
      });
    });
  }

  componentDidMount() {
    NetInfo.fetch().then(state => {
      if(!state.isConnected)
        this.props.navigation.navigate('LostConnection', {refresh: this._loadProfile});
      else {
        this.loadProfile();
        this.runAnimationImage();
        this.runAnimationColor();
      }
    });
  }

  render() {
    const spin = this.spinImageValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    return (
      <ImageBackground  source={require('../assets/fond.png')} style={{flex: 1, position: 'absolute', width: '100%', height: '100%'}}>
        <View style={styles.topHeadband}>
            <Text style={styles.descriptiveText}>
                {"Gère ton statut, tes adresses de livraison et consulte tes informations personnelles"}
            </Text>
        </View>
        <View style={styles.container}>
          <ScrollView
            style={{
                width: '100%',
                //alignItems: 'center',
                marginBottom: 0,
                backgroundColor:'#D9D8D9',
                flex:2,
                opacity: 0.8,
                borderTopLeftRadius:15,
                borderTopRightRadius:15,
            }}
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'space-between'
            }}
          >
          {this.state.isLoading ?
            <View style={styles.loading_container}>
              <ActivityIndicator size='large' color='#674171' />
            </View>
            :
            <View style={{
              width: '100%',
              //height: height/1.5,
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 30
            }}>
              <View style={{
                flex: 1,
                alignItems: 'center'
              }}>
                {/*<Image
                    style={styles.profil_photo}
                    source={require('../assets/icone.png')}
                />*/}
                {this._loadStatusImage(spin)}
                <Text style={styles.name}>
                    {this.state.user.last_name + " " + this.state.user.first_name}
                </Text>
                {this._loadStatusButton()}
              </View>
              <View style={{
                width:'90%',
                height: 315,
                //alignItems: 'center',
                justifyContent: 'center',
                backgroundColor:'white',
                borderRadius:13,
                paddingTop: 0,
                paddingBottom: 0,
                marginTop: 40
              }}>
                <View style={{
                    flex: 1,
                    borderRadius:13,
                    //backgroundColor: 'red',
                    alignItems: 'center',
                    paddingTop: 10,
                }}>
                  <TouchableOpacity
                      style={styles.buttonStyle2}
                      //disabled={true}
                      onPress={() => this.props.navigation.navigate('Status', {user: this.state.user, token: this.token, refreshProfile: this.loadProfile})}
                  >
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Mon statut</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      style={styles.buttonStyle2}
                      onPress={() => this.props.navigation.navigate('Addresses', {token: this.token})}
                  >
                      <View
                        style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Mes adresses de livraison</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('Sponsorship', {sponsorshipCode: this.state.user.sponsorship_token})}
                      style={styles.buttonStyle2}>
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>Parrainage</Text>
                      </View>
                  </TouchableOpacity>
                  <TouchableOpacity
                      onPress={()=>this.props.navigation.navigate('Kyc')}
                      style={styles.buttonStyle2}>
                      <View style={styles.textStyleButton}>
                          <Text style={styles.textStyleButton}>KYC</Text>
                      </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>}
          </ScrollView>
        </View>
      </ImageBackground>
    );
  }
}


const styles = StyleSheet.create({
  topHeadband: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:0.18,
    marginTop: 50
  },
  container: {
    height: '100%',
    width: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 200,
    height: 60,
    resizeMode: 'contain'
  },
  profil_photo: {
    width: 110,
    height: 90,
    resizeMode: 'contain',
    marginTop:15,
  },
  statusImage: {
    width: 110,
    height: 110,
    resizeMode: 'contain',
    marginTop:15,
  },
  name: {
    fontSize:30,
    color: '#674171',
    fontFamily: "SciFly",
    paddingVertical: 5,
  },
  btnStatus: {
    borderRadius: 10,
    width: 100,
    height: 30,
    borderWidth: 0.1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnStatusTextContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10
  },
  btnStatusText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 17,
    fontFamily: "SciFly",
  },
  textStyleButton: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 20,
    paddingVertical: 9,
    fontFamily: "SciFly",
  },
  buttonStyle2: {
    backgroundColor: "#674171",
    borderRadius: 10,
    width: '85%',
    borderWidth: 0.1,
    marginTop:12,
    opacity: 0.95,
  },
  descriptiveText: {
    color: 'white',
    textAlign: 'center',
    marginLeft: 20,
    marginRight: 20
  },
  loading_container: {
    height: height/1.3,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
